# README: Cassandra is here, Cassandra is now #

(Per l'italiano, che sicuramente sarà scritto un filino meglio, vedi più in basso)
Cassandra is a cool system for objects catalogation. It's developed in java (and php) in the Eclipse development environment by the civil protection unity of Locarno Vallemaggia region ( www.pcilocarno.ch ) for the protection of cultural heritage objects. It works for Museums, Churches, Art galleries and in every system that's need catalogation but also a reference on how to rescue the pieces of art from specific locations. The system is splitted on to three different projects: the master, with the source of the local Cassandra application and manuals, the design, with all assets for splash screen, about screen and icons, and - finally - the php project, with the server-side part of Cassandra system, used for the navigation on the whole database and for printout (in pdf) the content of an architecture. The Cassandra application it works locally without network connection, on a local database. Then this database is synchronized with the central server in a second time.

### Cassandra is open source, download now! ###

* Cassandra [ch.pcilocarno.pbc.inventari] -> the main application for the object census on the field, also works without connection. You can import multiple pictures of the same object, you can specify various information like the size, weight, materials, official name and familiar name and so on. Of course you can add pictures for the whole architecture where the objects are stored, but also pictures of the specific part of that architecture
* This is the Version 2.2 (and something) in Eclipse 4.7 "Oxygen" development environment 
* Cassandra [ch.pcilocarno.pbc.inventari.design] -> assets for splash screen, about screen, icons, fonts and a part of manual (with the explanation: "how to update Cassandra")
* Cassandra [ch.pcilocarno.pbc.inventari.php] -> everything you need for the server centralized database of the all architectures. This part is also essential for the printed out pdfs, designed in svg and then filled with the information taked from the database.

### How do I get set up? ###

* Eclipse development environment in java
* Server with MySQL installed an configured
* Dependencies will be explained in the future
* Database configuration will be explained in the future
* Push F11 to start a Cassandra local application session
* Clic on to "Export an Eclipse product" on the Window "cassandra.product" on Eclipse

### Contribution guidelines ###

* Actually we have not contribution guidelines 

### Who do I talk to? ###

* You can write directly to the findet-out address of the author/owner of application PAY ATTENTION! No response guaranteed


-----------------------


# File Leggimi: Cassandra è qui, Cassandra è ora #

Cassandra è un sistema per la catalogazione di oggetti. È sviluppato in java (e php) nell'ambiente di sviluppo Eclipse dall'unità di protezione civile della regione di Locarno Vallemaggia ( www.pcilocarno.ch ) per la protezione di beni culturali. È ok per musei, chiese, gallerie d'arte e ovunque sia necessario non solo un sistema di catalogazione, ma anche un riferimento su come salvare le opere d'arte da posizioni specifiche all'interno di un'architettura. Il sistema è diviso in tre progetti diversi: il Master, con l'applicazione locale di Cassandra più i manuali, il Design, con tutti gli asset per produrre/modificare la schermata iniziale, l'about screen, le icone e - finalmente - il progetto php, con la parte lato server del sistema Cassandra, utilizzato per la navigazione sull'intero database e per la stampa (in pdf) del contenuto di un'architettura. L'applicazione di Cassandra funziona localmente senza connessione di rete, su un database locale. Quindi questo database è sincronizzato con il server centrale in un secondo tempo.

### Cassandra è open source, metti subito le mani sul codice! ###

* Cassandra [ch.pcilocarno.pbc.inventari]-> l'applicazione principale per il censimento di oggetto sul campo, funziona anche senza connessione. È possibile importare più immagini dello stesso oggetto, è possibile specificare diverse informazioni come la dimensione, peso, materiali, nome ufficiale e nome familiare e così via. Naturalmente è possibile aggiungere immagini per tutta l'architettura dove gli oggetti vengono memorizzati, ma anche immagini della parte specifica di quella architettura
* Questa è la versione 2.2 (qualcosa) nell'ambiente di sviluppo Eclipse 4.7 «Oxygen»
* Cassandra [ch.pcilocarno.pbc.inventari.design]-> asset per la schermata iniziale, l'about screen, le icone, le font e una parte del manuale ("come aggiornare Cassandra")
* Cassandra [ch.pcilocarno.pbc.inventari.php]-> tutto il che necessario per il server centralizzato del database di tutte le architetture. Questa parte è inoltre essenziale per la stampa dei PDF, progettati nel formato svg modificabile e poi riempiti con le informazioni prese dal database.

### Com'è fatto? ###

* Sviluppato in java e php su Eclipse 4.7 (Oxygen)
* Richiede un server con MySQL installato e configurato
* Le Dependencies verranno spiegate in futuro
* La Configurazione del database verrà spiegata in futuro
* Premere F11 per avviare una sessione di applicazione locale di Cassandra da Eclipse (beh ma se stai leggendo questo lo saprai già!)
* Clicca su per "Esportare un prodotto Eclipse" nella finestra "cassandra.product" su Eclipse (idem come sopra)

### Linee guida contributo ###

* In realtà non abbiamo linee guida per i contributi

### Con chi devo parlare? ###

* È possibile scrivere direttamente all'indirizzo dell'autore/proprietario dell'applicazione che trovi MA ATTENZIONE! Nessuna risposta garantita