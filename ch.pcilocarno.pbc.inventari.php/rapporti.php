<?php
	require 'config.php';
	$arch_id=$_GET['id'];
    $mysqli->set_charset("utf8");
	$result = $mysqli->query("SELECT A.NOPCI, A.DENOMINAZIONE,A.COMUNE from ARCHITETTURA A where ID = '$arch_id'");
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

	
	$arch_nopci= $row['NOPCI'];
	$arch_denom= $row['DENOMINAZIONE'];
	$arch_comun= $row['COMUNE'];

	$rapporti_res = $mysqli->query("SELECT R.ID, R.CREATED, R.NOME, R.CONTENUTO FROM RAPPORTO R WHERE R.ARCHITETTURA_ID = '$arch_id' AND R.STATUS='A' ORDER BY R.CREATED DESC");
	$num = $rapporti_res->num_rows;
	
	$mysqli->close();
?>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PCi PBC - Inventari</title>
		<!-- Bootstrap -->
	    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/main.css" rel="stylesheet">
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="bootstrap/js/bootstrap.min.js"></script>
	</head>

	<body>
	    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">Cassandra</a>
	        </div>
	        <div class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="index.php"><span class="glyphicon glyphicon-home"></span>&nbsp;Elenco architetture</a></li>
	            <li><a href="architettura.php?id=<?php echo $arch_id ?>"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;Architettura</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </div>

		<div class="page-header">
			<h2>
			<?php echo $arch_nopci ?> 
			<?php echo $arch_denom ?> 
			<?php
			 if($arch_comun <> '')
			 { ?>
			    <small><?php echo $arch_comun ?></small>
			 <?php } ?>
			</h2>
		</div>
		

	    <div class="container">
		<?php
				if($num < 1){
				 ?>
					<center>----- Nessun rapporto -----</center>
				<?php 
				 }else{
					while ($row = $rapporti_res->fetch_assoc()) {
			
						$cdat 	= $row["CREATED"];
						$nome 	= $row["NOME"];
						$cont 	= $row["CONTENUTO"];
						$id		= $row["ID"];
				?>
				<div class="panel panel-default">
					<div class="panel-heading">
					<b><?php echo $nome ?></b> in data <?php echo date("d.m.Y \a\l\l\\e H:i", strtotime($cdat)) ?> ha scritto:
					</div>

					<div class="panel-body">
					<?php echo nl2br($cont) ?>
					</div>
				</div>
				
				<br>
				<?php
						
					}
				} ?>
		</div>
	</body>
</html>
