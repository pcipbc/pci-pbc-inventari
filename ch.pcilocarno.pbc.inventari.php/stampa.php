<?php
	require 'config.php';
	$arch_id=$_GET['id'];
    $mysqli->set_charset("utf8");
	$result = $mysqli->query("SELECT A.NOPCI, A.DENOMINAZIONE,A.COMUNE from ARCHITETTURA A where ID = '$arch_id'");
	$row = mysqli_fetch_array($result, MYSQLI_ASSOC);

	$num = $result->num_rows;
	$arch_nopci= $row['NOPCI'];
	$arch_denom= $row['DENOMINAZIONE'];
	$arch_comun= $row['COMUNE'];
	
	$mysqli->close();
?>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PCi PBC - Inventari</title>
		<!-- Bootstrap -->
	    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/main.css" rel="stylesheet">
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="bootstrap/js/bootstrap.min.js"></script>
	</head>

	<body>

	    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">Cassandra</a>
	        </div>
	        <div class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="index.php"><span class="glyphicon glyphicon-home"></span>&nbsp;Elenco architetture</a></li>
	            <li><a href="architettura.php?id=<?php echo $arch_id ?>"><span class="glyphicon glyphicon-chevron-left"></span>&nbsp;Architettura</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </div>

		<div class="page-header">
			<h2>
			<?php echo $arch_nopci ?> 
			<?php echo $arch_denom ?> 
			<?php
			 if($arch_comun <> '')
			 { ?>
			    <small><?php echo $arch_comun ?></small>
			 <?php } ?>
			</h2>
		</div>
		

	    <div class="container">
			<div class="alert alert-warning" role="alert">
				<strong>Attenzione: </strong> attendere il completamento di una stampa (PDF) prima di richiedere la seguente. <strong>Pazienza!</strong>
			</div>
			<table class="table">
				<tbody>
					<tr><td><span class="label label-default">00</span>&nbsp;<a href='<?php echo "pdf.php?id=".$arch_id."&idstampa=00"?>'>Copertina</a></td></tr>
					<tr><td><span class="label label-default">01</span>&nbsp;<a href='<?php echo "pdf.php?id=".$arch_id."&idstampa=01"?>'>Foglio di condotta</a></td></tr>
					<tr><td><span class="label label-default">02</span>&nbsp;<a href='<?php echo "pdf.php?id=".$arch_id."&idstampa=02"?>'>Oggetti prioritari da evacuare</a></td></tr>
					<tr><td><span class="label label-default">03</span>&nbsp;<a href='<?php echo "pdf.php?id=".$arch_id."&idstampa=03"?>'>Oggetti prioritari da proteggere</a></td></tr>
					<tr><td><span class="label label-default">04</span>&nbsp;<a href='<?php echo "pdf.php?id=".$arch_id."&idstampa=04"?>'>Foglio di evacuazione</a></td></tr>
					<tr><td><span class="label label-default">05</span>&nbsp;<a href='<?php echo "pdf.php?id=".$arch_id."&idstampa=05"?>'>Lista di evacuazione</a></td></tr>
					<tr><td><span class="label label-default">06</span>&nbsp;<a href='<?php echo "pdf.php?id=".$arch_id."&idstampa=06"?>'>Foglio di dettaglio - Evacuazione</a></td></tr>
					<tr><td><span class="label label-default">07</span>&nbsp;<a href='<?php echo "pdf.php?id=".$arch_id."&idstampa=07"?>'>Lista delle misure di protezione</a></td></tr>
					<tr><td><span class="label label-default">08</span>&nbsp;<a href='<?php echo "pdf.php?id=".$arch_id."&idstampa=08"?>'>Foglio di dettaglio - Protezione</a></td></tr>
					<tr><td><span class="label label-default">09</span>&nbsp;<a href='<?php echo "pdf.php?id=".$arch_id."&idstampa=09"?>'>Panoramiche bene culturale</a> </td></tr>
				</tbody>
			</table>
		</div>
	</body>
</html>
