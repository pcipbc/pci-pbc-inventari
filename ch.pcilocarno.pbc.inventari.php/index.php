<?php
	include 'config.php';	
	

	$q_s = "";
	if(isset($_GET['mode'])) {
		$q_s = '(A.ID LIKE "%'.$_POST['campo_libero'].'%" OR A.NOPCI LIKE "%'.$_POST['campo_libero'].'%" OR A.NOUBC LIKE "%'.$_POST['campo_libero'].'%" OR A.DENOMINAZIONE LIKE "%'.$_POST['campo_libero'].'%" OR A.BIBLIOGRAFIA LIKE "%'.$_POST['campo_libero'].'%" OR A.INDIRIZZO LIKE "%'.$_POST['campo_libero'].'%" OR A.FONDORFD LIKE "%'.$_POST['campo_libero'].'%" OR A.NOTE LIKE "%'.$_POST['campo_libero'].'%" OR A.RESPONSABILEPCI LIKE "%'.$_POST['campo_libero'].'%" OR A.COORDINATE LIKE "%'.$_POST['campo_libero'].'%")';
	
		if($_POST['comune'] !== "") {
			$q_s .= ' AND (A.COMUNE LIKE "'.$_POST['comune'].'")';
		}
		$q_s .= " AND";
	}
	$mysqli->set_charset("utf8");
	$result = $mysqli->query("SELECT A.NOPCI, A.DENOMINAZIONE,A.COMUNE,A.ID, A.DEFINITIVO, A.NOUBC FROM ARCHITETTURA A  WHERE ".$q_s." A.STATUS='A' ORDER BY A.COMUNE,A.DENOMINAZIONE");

	$num = $result->num_rows;


	// COMUNI
	$r_drop1= $mysqli->query("SELECT COMUNE FROM ARCHITETTURA GROUP BY COMUNE ORDER BY COMUNE");


	$mysqli->close();
?>
<html>
	<head>
		<meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PCi PBC - Inventari</title>
		<!-- Bootstrap -->
	    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/main.css" rel="stylesheet">
	    <link rel="stylesheet" href="css/bootstrap-select.css">
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="js/bootstrap-select.js"></script>
	</head>

	<body>

	    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">Cassandra</a>
	        </div>
	        <div class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li class="active"><a href="#"><span class="glyphicon glyphicon-home"></span>&nbsp;Elenco architetture</a></li>
	            <li><a href="ricerca.php"><span class="glyphicon glyphicon-search"></span>&nbsp;Ricerca</a></li>
				<li><a href="https://s.geo.admin.ch/7cba04dd4c" target="_blank" rel="noopener noreferrer"><span class="glyphicon glyphicon-new-window"></span>&nbsp;Apri la mappa</a></li>
				<li><a href="resources/INVENTARIO_PBC_ A-B-C_20130517.xlsx"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;Scarica elenco</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </div>

		<div class="page-header">
			<h2>Architetture</h2>
		</div>

	    <div class="container">
	    	<form method="POST" action="?mode=search" class="form-inline">
					<div class="form-group" style="margin-right: 40px;">
						<input type="text" name="campo_libero" placeholder="Cerca testo" class="form-control">
					</div>
	    		<div class="form-group" style="margin-right: 40px;">
	    			<label style="margin-right: 10px;">Comune:</label><select name="comune" title="Seleziona il comune" class="selectpicker show-tick" data-live-search="true">
		    			
		    			<?php
						while ($row = $r_drop1->fetch_assoc()) {
							echo '<option value="'.$row['COMUNE'].'">'.$row['COMUNE'].'</option>';  
						}
		    			
						?>
		    		</select>
	    		</div>
	    		<div class="form-group">
		    		<input type="hidden" name="search" value="true">
		    		<input class="btn btn-default" type="submit" value="Cerca!">
	    		</div>
	    	</form>
	    	
	    	
	    	
	    	
	    	<table class="table table-striped table-hover">
	    		<thead>
	    		<tr>
	    			<th></th>
					<th>No PCi</th>
					<th>No UBC</th>
					<th>Denominazione</th>
					<th>Comune</th>
					<th>&nbsp;</th>
				</tr>
				</thead>
				<tbody>
				<?php
					
				
					if($num < 1){
				 ?>
					<tr><td colspan='5'><center>----- Nessuna architettura -----</center></td></tr>
				<?php 
				 }else{
					while ($row = $result->fetch_assoc()) {
			
						$nopci 				= $row["NOPCI"];
						$denominazione 		= $row["DENOMINAZIONE"];
						$comune 			= $row["COMUNE"];
						$id					= $row["ID"];
						$def 				= $row["DEFINITIVO"];
						$noubc				= $row["NOUBC"];
						if( $nopci == '' ) {
							$nopci="&nbsp;";
						}
						if( $noubc == '' ) {
							$noubc = "&nbsp;";
						}
				?>
						<tr >
							<td><?php echo $def?"<span class='glyphicon glyphicon-check' />":"&nbsp;"?></td>
							<td><?php echo $nopci ?></td>
							<td><?php echo $noubc ?></td>
							<td><a href="architettura.php?id=<?php echo $id?>"><?php echo $denominazione ?></a></td>
							<td><?php echo $comune ?></td>
							<td>
								<a href="rapporti.php?id=<?php echo $id?>"><span class="glyphicon glyphicon-file" style="margin-right: 20px;"></span></a>
								<a href="stampa.php?id=<?php echo $id?>"><span class="glyphicon glyphicon-print"></span></a>
							</td>
						</tr>
				<?php
						
					}
				} ?>
				</tbody>
	    	</table>
	    	<br/><br/><br/>
	    </div><!-- /.container -->
	    <script>
		    $(document).ready(function() {
		    	$('.selectpicker').selectpicker();
			});
	    </script>
	</body>
</html>
