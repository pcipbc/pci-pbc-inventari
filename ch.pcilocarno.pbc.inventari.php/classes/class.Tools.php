<?php

class Tools
{

	const EMDASH = '—';
	const EMDASH_SP = ' — ';
	const IMMAGINE_DFT = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAAeAAD/7gAOQWRvYmUAZMAAAAAB/9sAhAAQCwsLDAsQDAwQFw8NDxcbFBAQFBsfFxcXFxcfHhcaGhoaFx4eIyUnJSMeLy8zMy8vQEBAQEBAQEBAQEBAQEBAAREPDxETERUSEhUUERQRFBoUFhYUGiYaGhwaGiYwIx4eHh4jMCsuJycnLis1NTAwNTVAQD9AQEBAQEBAQEBAQED/wAARCAASABIDASIAAhEBAxEB/8QASwABAQAAAAAAAAAAAAAAAAAAAAcBAQAAAAAAAAAAAAAAAAAAAAAQAQAAAAAAAAAAAAAAAAAAAAARAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AKAAAAAAAAD/2Q==";

	// clean a String for SVG
	public static function cleanForSVG($string = "", $len = 0)
	{
		if( empty($string))
			return $string;

		$nustring = $string;
		if( $len > 0 )
			$nustring = self::cut($nustring, $len);

		// $nustring = str_replace("°", "", $string);
		$nustring = iconv("UTF-8","UTF-8//IGNORE",$nustring);
		$nustring = str_replace("'", "’", $nustring);
		$nustring = str_replace("\"", "”", $nustring);
		// $nustring = str_replace("&", "&amp;", $nustring);
		$nustring = str_replace("<", " < ", $nustring);
		$nustring = str_replace(">", " > ", $nustring);
		$nustring = preg_replace('/\s+/', ' ',$nustring);
		$nustring = trim(htmlspecialchars($nustring, ENT_COMPAT, 'UTF-8')); // ENT_XML1
		//$nustring = htmlentities($string,ENT_COMPAT, "UTF-8");

		return $nustring;
	}

	//
	public static function cut($string, $len, $suffix = '[...]')
	{
		if( strlen($string) <= $len ) return $string;
		return substr($string, 0, $len) . $suffix;
	}

	// create a random string
	public static function rndString($len)
	{
		return substr(str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"),0,$len);
	}

	// converti cm3 in m3
	public static function volCM3toM3($cm3, $precision = 2)
	{
		return round($cm3 * 0.0000010, $precision);
	}

	/*
	 *
	 *
	 */ 
	public static function toArray($str, $linemaxchars = 95, $lines = 9)
	{
		$result = array();
		$str = self::remNL($str, "\n");
		
		$exploded = explode("\n",$str);
		
		for( $i = 0; $i < count($exploded); $i++)
		{
			$nstr = wordwrap($exploded[$i], $linemaxchars, "\n", false);
			$exploded2 = explode("\n", $nstr);

			for( $j = 0; $j < count($exploded2); $j++)
			{
				$result[]=$exploded2[$j];
			}
		}

		$rl = count($result);
		if( $rl < $lines )
		{
			for( $i = $rl; $i < $lines; $i++)
			{
				$result[$i] = '';
			}
		}

		return $result;
	}

	public static function remNL($str, $wchar = " — ")
	{
		$str = str_replace("\r\n", "\n", $str);
		$str = str_replace("\r", "\n", $str);
		$str = str_replace("\n", $wchar, $str);

		return $str;
	}
}

?>