<?php
require_once('class.Stampa.php');
require_once('tcpdf_include.php');
require_once('class.Tools.php');

class ListaProtezione extends Stampa
{
	public function createPDF($archid)
	{
		set_time_limit(0);
		// file svg da elaborare e stampare
		$svgfilename = "svg/07_lista_protezione.svg";
		$svgfilename_tot = "svg/07_totale_protezione.svg";
		/// connessione db
		$dbhost = "localhost";
		$dbuser = "pbc";
		$dbpass = "manuela";
		$dbname = "pci_pbc_inventari";
		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		$mysqli->set_charset("utf8");
		$result = $mysqli->query("select a.INDICAZIONIRIFUGIO As aINDICAZIONIRIFUGIO, a.CATEGORIA AS aCATEGORIA, a.SCUDI As aSCUDI, a.FONDORFD AS aFONDORFD, a.COORDINATE AS aCOORDINATE, a.INDIRIZZO AS aINDIRIZZO, a.COMUNE AS aCOMUNE, a.CAP AS aCAP, a.NOPCI AS aNOPCI, a.DENOMINAZIONE AS aDENOMINAZIONE, oa.PCIPERSONE AS oaPCIPERSONE, oa.PCIORE AS oaPCIORE, oa.PCIMODELLOPROTEZIONE AS oaPCIMODELLOPROTEZIONE, oa.GENERE AS oaGENERE, oa.NOPREC AS oaNOPREC, oa.ID AS oaID, oa.NOPCI AS oaNOPCI, oa.DENOMINAZIONE AS oaDENOMINAZIONE from OPERAARTE oa inner join PARTEARCHITETTONICA pa on pa.ID = oa.PARTEARCHITETTONICA_ID inner join ARCHITETTURA a on a.ID = pa.ARCHITETTURA_ID WHERE oa.STATUS='A' and pa.STATUS='A' and oa.STATO='IMMOBILE' and a.ID='" . $archid . "' ORDER BY oa.NOPCI");
		
		$num = $result->num_rows;
		

		// 
		$rndName = Tools::rndString(4);
		$outHomePath = "/tmp/";
		$outFilePath = $outHomePath . "PBC_" . $rndName . "/";
		mkdir($outFilePath);

		// gen pdf
		$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false, true);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->SetMargins(0, 0, 0, true);
		$pdf->setPageOrientation('', false, 0);	
		$pdf->SetFont('lato', '', 12);

		// calc tot pages
		$oapp = 15;
		$totpages = floor($num/$oapp)+($num%$oapp == 0 ?0:1)+1;

		// totali
		$totore = 0;
		$totogg = 0;
		$maxper = 1;
		$oreuom = 0;
		
		// itera su OA
		$i=0;
		$page=1;
		$imgMaxSize=530;
		$lastpage=$page;
		$str=file_get_contents($svgfilename);
		$oapn=1;

		while ($row = $result->fetch_assoc())  // $num
		{
			set_time_limit(0);
			// carica dati da result set
			$oa_id = $row["oaID"];
			$a_denominazione = $row["aDENOMINAZIONE"];
			$a_nopci = $row["aNOPCI"];
			$a_cap = $row["aCAP"];
			$a_comune = $row["aCOMUNE"];
			$a_indirizzo = $row["aINDIRIZZO"];
			$a_coordinate = $row["aCOORDINATE"];
			$a_mappale = $row["aFONDORFD"];
			$a_scudi = $row["aSCUDI"];
			$a_cat = $row["aCATEGORIA"];
			$a_indrifugio = $row["aINDICAZIONIRIFUGIO"];
			$oa_denominazione = $row["oaDENOMINAZIONE"];
			$oa_nopci = $row["oaNOPCI"];
			$oa_noprec = $row["oaNOPREC"];
			$oa_genere = $row["oaGENERE"];
			$oa_modprot = $row["oaPCIMODELLOPROTEZIONE"];
			$oa_ore = $row["oaPCIORE"];
			$oa_pers = $row["oaPCIPERSONE"];

			if( $oa_genere == $oa_denominazione )
				$oa_denominazione = "";

			if( $oa_noprec !== '' )
				$oa_nopci = $oa_nopci . "  (" . $oa_noprec . ")";

			// dati OA elenco
			$oa_sub="_" . str_pad($oapn, 2, "0", STR_PAD_LEFT);
			$str=str_replace("?OA_NOPCI" . $oa_sub, Tools::cleanForSVG($oa_nopci), $str);
			$str=str_replace("?OA_GENERE" . $oa_sub, Tools::cleanForSVG($oa_genere), $str);
			$str=str_replace("?OA_DENOMINAZIONE" . $oa_sub, Tools::cleanForSVG($oa_denominazione, 55), $str);
			$str=str_replace("?OA_MISUREPROTEZIONE" . $oa_sub, Tools::cleanForSVG($oa_modprot), $str);
			$str=str_replace("?O" . str_pad($oapn, 2, "0", STR_PAD_LEFT), round($oa_ore, 1), $str);
			$str=str_replace("?P" . str_pad($oapn, 2, "0", STR_PAD_LEFT), $oa_pers, $str);

			// aggiorna totali
			$totogg = $totogg + 1;
			$totore = $totore + $oa_ore;
			$oreuom = $oreuom + ($oa_pers * $oa_ore);
			if( $oa_pers > $maxper)
				$maxper = $oa_pers;

			$page = floor($i/$oapp)+1;

			// new page?
			if( $page != $lastpage || $i == ($num-1))
			{
				// Ev. togli placeholder per posizioni non elaborate
				for( $j = $oapn; $j < ($oapp+1); $j++ )
				{
					$oa_sub="_" . str_pad($j, 2, "0", STR_PAD_LEFT);
					$str=str_replace("?OA_NOPCI" . $oa_sub, '', $str);
					$str=str_replace("?OA_GENERE" . $oa_sub, '', $str);
					$str=str_replace("?OA_DENOMINAZIONE" . $oa_sub, '', $str);
					$str=str_replace("?OA_MISUREPROTEZIONE" . $oa_sub, '', $str);
					$str=str_replace("?P" . str_pad($j, 2, "0", STR_PAD_LEFT), '', $str);
					$str=str_replace("?O" . str_pad($j, 2, "0", STR_PAD_LEFT), '', $str);
				}

				// dati testata
				$str=str_replace("?A_DENOMINAZIONE", Tools::cleanForSVG($a_denominazione), $str);
				$str=str_replace("?A_NOPCI", Tools::cleanForSVG($a_nopci), $str);
				$str=str_replace("?A_COMUNE", Tools::cleanForSVG($a_comune), $str);
				$str=str_replace("?A_CAT", Tools::cleanForSVG($a_cat), $str);
				$str=str_replace("?A_INDICAZIONIRIFUGIO", Tools::cleanForSVG($a_indrifugio), $str);
				// dati footer
				$str=str_replace("?DATA", date("d.m.Y"), $str);
				$str=str_replace("?PAGINA", $lastpage, $str);
				$str=str_replace("?TOTPAGINA", $totpages, $str);
				
				// crea file temp SVG
				$outFileName = "PBC_ListaEvacuazione_" . str_pad($page, 4, "0", STR_PAD_LEFT);
				file_put_contents($outFilePath . $outFileName . ".svg", $str, LOCK_EX);
				
				// add a page
				$pdf->AddPage();
				$pdf->SetPrintHeader(false);
				$pdf->SetPrintFooter(false);
				$pdf->setMargins(0, 0, 0, true);
				$pdf->setPageOrientation('P', false, 0);		
				$pdf->ImageSVG($file=$outFilePath . $outFileName . ".svg", $x=0, $y=0, $w=745, $h=1052, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
				
				// elimina SVG temp
				unlink($outFilePath . $outFileName . ".svg");

				// reload clean svg
				$str=file_get_contents($svgfilename);
				$lastpage=$page;
				$oapn=0;
			}



			$oapn++;
			$i++;
		}

		// aggiungi PDF totali
		$pdf->AddPage();
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->setMargins(0, 0, 0, true);
		$pdf->setPageOrientation('P', false, 0);
		$totstr = file_get_contents($svgfilename_tot);
		$totstr = str_replace("?A_DENOMINAZIONE", Tools::cleanForSVG($a_denominazione), $totstr);
		$totstr = str_replace("?A_NOPCI", Tools::cleanForSVG($a_nopci), $totstr);
		$totstr = str_replace("?A_CAT", Tools::cleanForSVG($a_cat), $totstr);
		$totstr = str_replace("?COMUNE", Tools::cleanForSVG($a_comune), $totstr);
		$totstr = str_replace("?CAP", $a_cap, $totstr);
		$totstr = str_replace("?INDIRIZZO", $a_indirizzo, $totstr);
		$totstr = str_replace("?COORDINATE", $a_coordinate, $totstr);
		$totstr = str_replace("?NUMEROMAPPALE", $a_mappale, $totstr);
		$totstr = str_replace("?SCUDI", $a_scudi, $totstr);
		$totstr = str_replace("?TOTNUMEROOGGETTI", $totogg, $totstr);
		$totstr = str_replace("?TOTORE", $totore, $totstr);
		$totstr = str_replace("?MAXPERSONE", $maxper, $totstr);
		$totstr = str_replace("?OREUOMO", $oreuom, $totstr);
		$totstr = str_replace("?DATA", date("d.m.Y"), $totstr);
		$totstr = str_replace("?PAGINA", $totpages, $totstr);
		$totstr = str_replace("?TOTPAGINA", $totpages, $totstr);
		file_put_contents($outFilePath . "PBC_ListaEvacuazione_TOT.svg", $totstr, LOCK_EX);
		$pdf->ImageSVG($file=$outFilePath . "PBC_ListaEvacuazione_TOT.svg", $x=0, $y=0, $w=745, $h=1052, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
		unlink($outFilePath . "PBC_ListaEvacuazione_TOT.svg");

		// chiudi PDF e scrivi
		$pdf->Output($outHomePath . "07_ListaProtezione" . "" . ".pdf", 'FD');
		// elimina cartella temp
		rmdir($outFilePath);
		// chiudi conn mysql
		$mysqli->close();
	}
	
}
