<?php
require_once('class.Stampa.php');
require_once('tcpdf_include.php');
require_once('class.Tools.php');

class OggettiPrioProteggere extends Stampa
{
	public function createPDF($archid)
	{
		set_time_limit(0);
		// file svg da elaborare e stampare
		$svgfilename = "svg/03_lista_protezione_prioritari.svg";

		// connessione db
		$dbhost = "localhost";
		$dbuser = "pbc";
		$dbpass = "manuela";
		$dbname = "pci_pbc_inventari";
		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		$mysqli->set_charset("utf8");
		$result = $mysqli->query("select *, oa.DENOMINAZIONE AS oaDENOMINAZIONE, oa.NOPCI AS oaNOPCI, oa.NOPREC AS oaNOPREC, oa.GENERE AS oaGENERE, oa.PCIMODELLOPROTEZIONE AS oaPCIMODELLOPROTEZIONE, oa.PCIORE AS oaPCIORE, oa.PCIPERSONE AS oaPCIPERSONE from OPERAARTE oa inner join PARTEARCHITETTONICA pa on pa.ID = oa.PARTEARCHITETTONICA_ID inner join ARCHITETTURA a on a.ID = pa.ARCHITETTURA_ID WHERE oa.STATUS='A' and oa.PRIORITARIO=1 and oa.STATO='IMMOBILE' and a.ID='" . $archid . "' ORDER BY oa.NOPCI");

		$num = $result->num_rows;
		

		// 
		$rndName = Tools::rndString(4);
		$outHomePath = "/tmp/";
		$outFilePath = $outHomePath . "PBC_" . $rndName . "/";
		mkdir($outFilePath);

		// gen pdf
		$pdf = new TCPDF('L', 'mm', 'A4', true, 'UTF-8', false, true);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->SetMargins(0, 0, 0, true);
		$pdf->setPageOrientation('', false, 0);	
		$pdf->SetFont('lato', '', 12);

		// calc tot pages
		$oapp = 10;
		$totpages = floor($num/$oapp)+($num%$oapp == 0 ?0:1);
		
		// itera su OA
		$i=0;
		$page=1;
		$imgMaxSize=530;
		$lastpage=$page;
		$str=file_get_contents($svgfilename);
		$oapn=1;
		
		if($num > 0) {
			while ($row = $result->fetch_assoc())  // $num
			{
				// carica dati da result set
				$oa_id = $row["oa.ID"];
				$a_denominazione = $row["a.DENOMINAZIONE"];
				$a_nopci = $row["a.NOPCI"];
				$a_comune = $row["a.COMUNE"];
				$a_cat = $row["a.CATEGORIA"];
				$a_indrifugio = $row["a.INDICAZIONIRIFUGIO"];
				$oa_denominazione = $row["oaDENOMINAZIONE"];
				$oa_nopci = $row["oaNOPCI"];
				$oa_noprec = $row["oaNOPREC"];
				$oa_genere = $row["oaGENERE"];
				$oa_modprot = $row["oaPCIMODELLOPROTEZIONE"];
				$oa_ore = $row["oaPCIORE"];
				$oa_pers = $row["oaPCIPERSONE"];
	
				if( $oa_genere == $oa_denominazione ) 
					$oa_denominazione = "";
				else 
					$oa_denominazione = " - ".$oa_denominazione;
	
				if( $oa_noprec !== '' )
					$oa_nopci = $oa_nopci . "  (" . $oa_noprec . ")";
	
				// dati OA elenco
				$oa_sub="_" . str_pad($oapn, 2, "0", STR_PAD_LEFT);
				$str=str_replace("?OA_NOPCI" . $oa_sub, Tools::cleanForSVG($oa_nopci), $str);
				$str=str_replace("?OA_ORE_" . str_pad($oapn, 2, "0", STR_PAD_LEFT), round($oa_ore,1), $str);
				$str=str_replace("?OA_PERS_" . str_pad($oapn, 2, "0", STR_PAD_LEFT), $oa_pers, $str);
				
				$denel = Tools::toArray(Tools::cleanForSVG($oa_genere.$oa_denominazione), 60, 2);
				for( $f = 0; $f < count($denel); $f++)
				{
					$str=str_replace("?OA_DENOMINAZIONE" . $oa_sub . "_" . ($f+1), $denel[$f], $str);	
				}
				
				$misel = Tools::toArray(Tools::cleanForSVG($oa_modprot), 60, 2);
				for( $f = 0; $f < count($misel); $f++)
				{
					$str=str_replace("?OA_MISUREPROTEZIONE" . $oa_sub . "_" . ($f+1), $misel[$f], $str);				
				}
	
				$page = floor($i/$oapp)+1;
	
				// new page?
				if( $page != $lastpage || $i == ($num-1))
				{
					// Ev. togli placeholder per posizioni non elaborate
					for( $j = $oapn; $j < ($oapp+1); $j++ )
					{
						$oa_sub="_" . str_pad($j, 2, "0", STR_PAD_LEFT);
						$str=str_replace("?OA_NOPCI" . $oa_sub, '', $str);
						$str=str_replace("?OA_GENERE" . $oa_sub, '', $str);
						$str=str_replace("?OA_DENOMINAZIONE" . $oa_sub . "_1", '', $str);
						$str=str_replace("?OA_DENOMINAZIONE" . $oa_sub . "_2", '', $str);
						$str=str_replace("?OA_MISUREPROTEZIONE" . $oa_sub . "_1", '', $str);
						$str=str_replace("?OA_MISUREPROTEZIONE" . $oa_sub . "_2", '', $str);
						$str=str_replace("?OA_PERS_" . str_pad($j, 2, "0", STR_PAD_LEFT), '', $str);
						$str=str_replace("?OA_ORE_" . str_pad($j, 2, "0", STR_PAD_LEFT), '', $str);
					}
	
					// dati testata
					$str=str_replace("?A_DENOMINAZIONE", Tools::cleanForSVG($a_denominazione), $str);
					$str=str_replace("?A_NOPCI", Tools::cleanForSVG($a_nopci), $str);
					$str=str_replace("?A_COMUNE", Tools::cleanForSVG($a_comune), $str);
					$str=str_replace("?A_CAT", Tools::cleanForSVG($a_cat), $str);
					$str=str_replace("?A_INDICAZIONIRIFUGIO", Tools::cleanForSVG($a_indrifugio), $str);
					// dati footer
					$str=str_replace("?DATA", date("d.m.Y"), $str);
					$str=str_replace("?PAGINA", $lastpage, $str);
					$str=str_replace("?TOTPAGINA", $totpages, $str);
					
					// crea file temp SVG
					$outFileName = "PBC_OggettiPrioProteggere_" . str_pad($page, 4, "0", STR_PAD_LEFT);
					file_put_contents($outFilePath . $outFileName . ".svg", $str, LOCK_EX);
					
					// add a page
					$pdf->AddPage();
					$pdf->SetPrintHeader(false);
					$pdf->SetPrintFooter(false);
					$pdf->setMargins(0, 0, 0, true);
					$pdf->setPageOrientation('L', false, 0);		
					$pdf->ImageSVG($file=$outFilePath . $outFileName . ".svg", $x=0, $y=0, $w=1052, $h=745, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
					
					// elimina SVG temp
					unlink($outFilePath . $outFileName . ".svg");
	
					// reload clean svg
					$str=file_get_contents($svgfilename);
					$lastpage=$page;
					$oapn=0;
				}
	
	
	
				$oapn++;
				$i++;
			} 
		} else {
			$pdf->AddPage();
			$pdf->setPageOrientation('L', false, 0);
			$pdf->ImageSVG($file="svg/03_lista_protezione_prioritari_nessuno.svg", $x=0, $y=0, $w=1052, $h=745, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
		}

		// chiudi PDF e scrivi
		$pdf->Output($outHomePath . "03_OggettiPrioProteggere" . "" . ".pdf", 'FD');
		// elimina cartella temp
		rmdir($outFilePath);
		// chiudi conn mysql
		$mysqli->close();
	}
	
}
