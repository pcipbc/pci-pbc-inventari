<?php
require_once('class.Stampa.php');
require_once('tcpdf_include.php');
require_once('class.Tools.php');

class DettaglioEvacuazione extends Stampa
{
	public function createPDF($archid)
	{
		set_time_limit(0);
		// file svg da elaborare e stampare
		$svgfilename = "svg/06_dettaglio_evacuazione.svg";

		// connessione db
		$dbhost = "localhost";
		$dbuser = "pbc";
		$dbpass = "manuela";
		$dbname = "pci_pbc_inventari";
		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		$mysqli->set_charset("utf8");
		$result = $mysqli->query("select 
					a.DENOMINAZIONE, a.COMUNE, a.NOPCI, a.CATEGORIA,
					pa.DENOMINAZIONE AS paDENOMINAZIONE,
					oa.DENOMINAZIONE AS oaDENOMINAZIONE, oa.NOPCI AS oaNOPCI, oa.GENERE, oa.CONSFORMALE, oa.CONSSTRUTTURALE, oa.COLLOCAZIONE, 
					oa.LIVELLO, oa.QUANTITA, oa.MISPESO,oa.MISALTEZZA,oa.MISLARGHEZZA,oa.MISPROFONDITA, oa.NOTE,
					oa.PCIOSSERVAZIONI, oa.AUTORE, oa.DATAZIONE, oa.NOUBC, oa.ED1TIPO, oa.ED1TECNICA, oa.ED1POSIZIONE,
					oa.ED1DESCRIZIONE,oa.PCIMATERIALE, oa.PCIATTREZZI, oa.PRIORITARIO, oa.QTAIMMAGINI,
					i.CONTENUTO, oa.MATERIATECNICA, oa.NOPREC
				from OPERAARTE oa 
					inner join PARTEARCHITETTONICA pa on pa.ID = oa.PARTEARCHITETTONICA_ID 
					inner join ARCHITETTURA a on a.ID = pa.ARCHITETTURA_ID
					left outer join IMMAGINE i on i.IDPROP = oa.ID and i.STATUS = 'A'
				WHERE oa.STATUS='A' and pa.STATUS='A' and oa.STATO='MOBILE' and a.ID='" . $archid . "' 
				ORDER BY oa.NOPCI, i.created ASC");
		 $num = $result->num_rows;
		
		// file temp
		 $rndName = Tools::rndString(4);
		 $outHomePath = "/tmp/";
		 $outFilePath = $outHomePath . "PBC_" . $rndName . "/";
		 mkdir($outFilePath);

		// gen pdf
		 $pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false, true);
		 $pdf->SetPrintHeader(false);
		 $pdf->SetPrintFooter(false);
		 $pdf->SetMargins(0, 0, 0, true);
		 $pdf->setPageOrientation('', false, 0);	
		 $pdf->SetFont('lato', '', 12);

		// itera su OA
		$i=0;
		$page=0;
		$imgMaxSize=530;
		$noimg = 1;
		$lastoa = '';

		while ($row = $result->fetch_assoc())  // $num
		{
			set_time_limit(0);
			$page++;
			
			//print_r($row);
			//echo array_values($row)[0];
			//die();

			// leggi campi
			$a_denominazione = Tools::cleanForSVG(array_values($row)[0]);
			$a_comune = Tools::cleanForSVG(array_values($row)[1]);
			$a_nopci = Tools::cleanForSVG(array_values($row)[2]);
			$a_cat = Tools::cleanForSVG(array_values($row)[3]);

			$pa_denominazione = Tools::cleanForSVG(array_values($row)[4]);

			$oa_denominazione = Tools::cleanForSVG(array_values($row)[5]); // clean sotto
			$oa_nopci = Tools::cleanForSVG(array_values($row)[6]);
			$oa_genere = Tools::cleanForSVG(array_values($row)[7]);
			$oa_consform = Tools::cleanForSVG(array_values($row)[8]);
			$oa_consstru = Tools::cleanForSVG(array_values($row)[9]);
			$oa_colloc = Tools::cleanForSVG(array_values($row)[10]);
			$oa_livello = Tools::cleanForSVG(array_values($row)[11]);
			$oa_qta = array_values($row)[12];
			$oa_peso = array_values($row)[13];
			$oa_alt = array_values($row)[14];
			$oa_larg = array_values($row)[15];
			$oa_prof = array_values($row)[16];
			$oa_note = Tools::cleanForSVG(array_values($row)[17]);
			$oa_pcioss = Tools::cleanForSVG(array_values($row)[18], 140);
			$oa_aut = Tools::cleanForSVG(array_values($row)[19], 35);
			$oa_dataz = Tools::cleanForSVG(array_values($row)[20], 35);
			$oa_noubc = Tools::cleanForSVG(array_values($row)[21], 35);
			$oa_ed1tipo = Tools::cleanForSVG(array_values($row)[22]);
			$oa_ed1tec = Tools::cleanForSVG(array_values($row)[23]);
			$oa_ed1pos = Tools::cleanForSVG(array_values($row)[24]);
			$oa_ed1desc = Tools::cleanForSVG(array_values($row)[25], 62);
			$oa_pcimat = Tools::cleanForSVG(array_values($row)[26], 140);
			$oa_pciatt = Tools::cleanForSVG(array_values($row)[27], 140);
			$oa_prio = Tools::cleanForSVG(array_values($row)[28]);
			$oa_qtaimg = array_values($row)[29];
			$image = array_values($row)[30]; // image content
			$oa_mattec = Tools::cleanForSVG(array_values($row)[31]);
			$oa_noprec = Tools::cleanForSVG(array_values($row)[32]);

			// separa e clean denominazione
			 if( $oa_genere == $oa_denominazione ) 
			 	$oa_denominazione = "";
			 else
				$oa_denominazione = Tools::EMDASH_SP . Tools::cleanForSVG($oa_denominazione, (70 - strlen($oa_genere)));

			if( $oa_noprec !== '' )
				$oa_nopci = $oa_nopci . "  (" . $oa_noprec . ")";

			// cambia colore flag prioritario
			 $prio = "#ffffff";
			 if( "1" == $oa_prio ) $prio = "#000000";

			// adatta nopci con immagine/immagini
			 if( $lastoa != $oa_nopci)
			 {
			 	$noimg = 1;
			 	$lastoa = $oa_nopci;
			 }
			 else
			 {
			 	$noimg++;
			 }	
			 
			 if( $oa_qtaimg > 1 )
				$oa_nopci = $oa_nopci . " (" . $noimg . " di " . $oa_qtaimg . ")";

			// adatta livello
			 if ("LIV0" == $oa_livello) $oa_livello = "(0 - 1.5 m)";
			 if ("LIV1" == $oa_livello) $oa_livello = "(1.5 - 3 m)";
			 if ("LIV2" == $oa_livello) $oa_livello = "(oltre 3 m)";
			
			
			// immagine
			$imgb64 = Tools::IMMAGINE_DFT;
			$imgNuH=$imgMaxSize;
			$imgNuW=$imgMaxSize;
			// get size
			 if( $image != null )
			 {
				$imgb64 = "data:image/jpeg;base64," . base64_encode($image);
				$srcimg = imagecreatefromstring($image);
				$width = imagesx($srcimg);
				$height = imagesy($srcimg);
				$aspect_ratio = $height/$width;
		
				if( $width <= $imgNuW && $height <= $imgNuH )
				{
					$imgNuH = $height;
					$imgNuW = $width;
				}
				else
				{
					if( $width > $height )
					{
						$imgNuW = $imgMaxSize;
						$imgNuH = ( $imgNuW * $height ) / $width;
					}
					else if( $height > $width)
					{
						$imgNuH = $imgMaxSize;
						$imgNuW = ($imgNuH * $width ) / $height;
					}
				}
			 }

			// sost campi
			$str=file_get_contents($svgfilename);
			$str=str_replace("?A_DENOMINAZIONE", $a_denominazione, $str);
			$str=str_replace("?A_NOPCI", $a_nopci, $str);
			$str=str_replace("?A_COMUNE", $a_comune, $str);
			$str=str_replace("?A_CAT", $a_cat, $str);

			$str=str_replace("?OA_NOPCI", $oa_nopci, $str);
			$str=str_replace("?OA_DENOMINAZIONE", $oa_denominazione, $str);
			$str=str_replace("?OA_GENERE", $oa_genere, $str);
			$str=str_replace("?OA_PRIORITARIO", $prio, $str);

			$str=str_replace("?OA_CONSFORMALE", $oa_consform, $str);
			$str=str_replace("?OA_CONSSTRUTTURALE", $oa_consstru, $str);
			$str=str_replace("?OA_QUANTITA", $oa_qta, $str);
			$str=str_replace("?OA_MISALTEZZA", $oa_alt, $str);
			$str=str_replace("?OA_MISLARGHEZZA", $oa_larg, $str);
			$str=str_replace("?OA_MISPROFONDITA", $oa_prof, $str);
			$str=str_replace("?OA_MISPESO", $oa_peso, $str);

			$str=str_replace("?OA_ED1TIPO", $oa_ed1tipo, $str);
			$str=str_replace("?OA_ED1POSIZIONE", (empty($oa_ed1pos)?"":Tools::EMDASH_SP) . $oa_ed1pos, $str);
			$str=str_replace("?OA_ED1TECNICA", (empty($oa_ed1tec)?"":Tools::EMDASH_SP) . $oa_ed1tec, $str);
			$str=str_replace("?OA_ED1DESCRIZIONE", $oa_ed1desc, $str);

			if( !empty($oa_colloc))
				$oa_colloc = Tools::EMDASH_SP . $oa_colloc;

			$colel = Tools::toArray($pa_denominazione . Tools::EMDASH_SP . $oa_livello . $oa_colloc, 60, 2);
			for( $f = 0; $f < count($colel); $f++)
			{
				$str=str_replace("?OA_COLLOCAZIONE_" . ($f+1), $colel[$f], $str);
			}
			


			$notel = Tools::toArray(Tools::cleanForSVG($oa_note), 85 , 2);
			for( $f = 0; $f < count($notel); $f++)
			{
				$str=str_replace("?OA_NOTE_" . ($f+1), $notel[$f], $str);
			}
			$matecl = Tools::toArray(Tools::cleanForSVG($oa_mattec), 45, 2);
			for( $f = 0; $f < count($matecl); $f++)
			{
				$str=str_replace("?OA_MATERIAETECNICA_" . ($f+1), $matecl[$f], $str);
			}

			$str=str_replace("?OA_PCIOSSERVAZIONI", $oa_pcioss, $str);
			$str=str_replace("?OA_AUTORE", $oa_aut, $str);
			$str=str_replace("?OA_DATAZIONE", $oa_dataz, $str);
			$str=str_replace("?OA_NOUBC", $oa_noubc, $str);
			$str=str_replace("?OA_PCIMATERIALE", $oa_pcimat, $str);
			$str=str_replace("?OA_PCIATTREZZI", $oa_pciatt, $str);
			$str=str_replace("?IMMAGINE_C", $imgb64, $str);
			$str=str_replace("?IMMAGINE_H", $imgNuH, $str);
			$str=str_replace("?IMMAGINE_W", $imgNuW, $str);
			$str=str_replace("?DATA", date("d.m.Y"), $str);
			$str=str_replace("?PAGINA", $page, $str);
			$str=str_replace("?TOTPAGINA", $num, $str);

			$outFileName = "PBC_DettaglioEvacuazione_" . str_pad($page, 4, "0", STR_PAD_LEFT) ;
			file_put_contents($outFilePath . $outFileName . ".svg", $str, LOCK_EX);

			$i++;

			// add a page
			 $pdf->AddPage();
			 $pdf->SetPrintHeader(false);
			 $pdf->SetPrintFooter(false);
			 $pdf->setMargins(0, 0, 0, true);
			 $pdf->setPageOrientation('', false, 0);		
			 $pdf->ImageSVG($file=$outFilePath . $outFileName . ".svg", $x=0, $y=0, $w=745, $h=1052, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);

			//Close and output PDF document
			unlink($outFilePath . $outFileName . ".svg");
			
		}
		$pdf->Output($outHomePath . "06_DettaglioEvacuazione" . "" . ".pdf", 'FD');
		rmdir($outFilePath);
		$mysqli->close();
		echo "Pagine: ",$page,"\n";
	}
	
}


?>
