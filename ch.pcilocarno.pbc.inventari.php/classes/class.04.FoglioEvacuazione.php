<?php
require_once('class.Stampa.php');
require_once('tcpdf_include.php');
require_once('class.Tools.php');

class FoglioEvacuazione extends Stampa
{
	public function createPDF($archid)
	{
		set_time_limit(0);
		// file svg da elaborare e stampare
		$svgfilename = "svg/04_foglio_evacuazione.svg";

		/// connessione db
		$dbhost = "localhost";
		$dbuser = "pbc";
		$dbpass = "manuela";
		$dbname = "pci_pbc_inventari";
		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		$mysqli->set_charset("utf8");
		$result = $mysqli->query("select 
						NOPCI, CATEGORIA, DENOMINAZIONE, CAP, COMUNE, INDIRIZZO, COORDINATE, FONDORFD, SCUDI, 
						MATERIALEIMBALLAGGIO, INDICAZIONIRIFUGIO, MEZZITRASPORTO, QTAMEZZI, QTAPERSONE, QTAORE, 
						COLLABTRASPORTO, OSSERVAZIONIEVAC,
						(select count(oa.ID) FROM OPERAARTE oa INNER JOIN PARTEARCHITETTONICA pa on pa.ID = oa.PARTEARCHITETTONICA_ID where oa.STATO='MOBILE' and oa.STATUS='A' and pa.ARCHITETTURA_ID=ARCHITETTURA.ID ) as 'TOTNUMEROOGGETTI',
						(select sum(oa.QUANTITA) FROM OPERAARTE oa INNER JOIN PARTEARCHITETTONICA pa on pa.ID = oa.PARTEARCHITETTONICA_ID where oa.STATO='MOBILE' and oa.STATUS='A' and pa.ARCHITETTURA_ID=ARCHITETTURA.ID ) as 'TOTPEZZI',
						(select sum(coalesce(nullif(oa.MISCCALTEZZA*oa.MISCCLARGHEZZA*oa.MISCCPROFONDITA*oa.QUANTITA,0),oa.MISALTEZZA*oa.MISLARGHEZZA*oa.MISPROFONDITA*oa.QUANTITA)) FROM OPERAARTE oa INNER JOIN PARTEARCHITETTONICA pa on pa.ID = oa.PARTEARCHITETTONICA_ID where oa.STATO='MOBILE' and oa.STATUS='A' and pa.ARCHITETTURA_ID=ARCHITETTURA.ID ) as 'TOTVOLUMECM',
						(select sum(coalesce(nullif(oa.MISCCPESO, 0),oa.MISPESO)*oa.QUANTITA) FROM OPERAARTE oa INNER JOIN PARTEARCHITETTONICA pa on pa.ID = oa.PARTEARCHITETTONICA_ID where oa.STATO='MOBILE' and oa.STATUS='A' and pa.ARCHITETTURA_ID=ARCHITETTURA.ID ) as 'TOTPESO'
					from
						ARCHITETTURA
					where ID = '" . $archid . "'");
		
		$num = $result->num_rows;
		

		// 
		$rndName = Tools::rndString(4);
		$outHomePath = "/tmp/";
		$outFilePath = $outHomePath . "PBC_" . $rndName . "/";
		mkdir($outFilePath);

		// gen pdf
		$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false, true);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->SetMargins(0, 0, 0, true);
		$pdf->setPageOrientation('', false, 0);	
		$pdf->SetFont('lato', '', 12);

		// itera su OA
		$i=0;
		$page=1;
		$imgMaxSize=530;
		$lastpage=$page;
		$str=file_get_contents($svgfilename);
		$totpages=1;
		while ($row = $result->fetch_assoc())  // $num
		{
			// carica dati da result set
			$a_nopci = Tools::cleanForSVG($row["NOPCI"]);
			$a_categoria = Tools::cleanForSVG($row["CATEGORIA"]);
			$a_denominazione= Tools::cleanForSVG($row["DENOMINAZIONE"]);
			$a_cap = Tools::cleanForSVG($row["CAP"]);
			$a_comune = Tools::cleanForSVG($row["COMUNE"]);
			$a_indirizzo = Tools::cleanForSVG($row["INDIRIZZO"]);
			$a_coordinate = Tools::cleanForSVG($row["COORDINATE"]);
			$a_numeromappale = Tools::cleanForSVG($row["FONDORFD"]);
			$a_scudi = Tools::cleanForSVG($row["SCUDI"]);
			$a_matiimbadispo = Tools::cleanForSVG($row["MATERIALEIMBALLAGGIO"]);
			$a_indicrifugio = Tools::cleanForSVG($row["INDICAZIONIRIFUGIO"]);
			$a_mezzitrasporto = Tools::cleanForSVG($row["MEZZITRASPORTO"]);
			$a_qtamezzi = Tools::cleanForSVG($row["QTAMEZZI"]);
			$a_qtapersone = Tools::cleanForSVG($row["QTAPERSONE"]);
			$a_qtaore = Tools::cleanForSVG($row["QTAORE"]);
			$a_collabtrasporto = Tools::cleanForSVG($row["COLLABTRASPORTO"]);
			$a_osservazioni = Tools::cleanForSVG($row["OSSERVAZIONIEVAC"]);
			$c_totogg = Tools::cleanForSVG($row["TOTNUMEROOGGETTI"]);
			$c_totpzi = Tools::cleanForSVG($row["TOTPEZZI"]);
			$c_totvol = Tools::cleanForSVG($row["TOTVOLUMECM"]);
			$c_totpes = Tools::cleanForSVG($row["TOTPESO"]);
			$c_totmezzi = $a_qtapersone . ' / ' . $a_qtaore;
			//$page = floor($i/$oapp)+1;
			if( $a_scudi == 'NONE') $a_scudi = 'Nessuno';


			// dati testata
			$str=str_replace("?A_NOPCI", $a_nopci, $str);
			$str=str_replace("?A_CAT", $a_categoria, $str);
			$str=str_replace("?A_DENOMINAZIONE", $a_denominazione, $str);
			$str=str_replace("?CAP", $a_cap, $str);
			$str=str_replace("?COMUNE", $a_comune, $str);
			$str=str_replace("?INDIRIZZO", $a_indirizzo, $str);
			$str=str_replace("?COORDINATE", $a_coordinate, $str);
			$str=str_replace("?NUMEROMAPPALE", $a_numeromappale, $str);
			$str=str_replace("?SCUDI", $a_scudi, $str);
			$str=str_replace("?TOTNUMEROOGGETTI", $c_totogg, $str);
			$str=str_replace("?TOTPEZZI", $c_totpzi, $str);
			$str=str_replace("?TOTVOLUME", Tools::volCM3toM3($c_totvol), $str);
			$str=str_replace("?TOTPESO", $c_totpes, $str);
			$str=str_replace("?MEZZIDITRASPORTO", $a_mezzitrasporto, $str);
			$str=str_replace("?MEZZINECESSARI", $c_totmezzi, $str);

			$matel=Tools::toArray($a_matiimbadispo, 140, 8);
			for( $f = 0; $f < count($matel);$f++)
			{
				$str=str_replace("?MATERIALEIMBALLAGGIO_" . ($f+1), $matel[$f], $str);
			}
			$rifel=Tools::toArray($a_indicrifugio, 70, 3);
			for( $f = 0; $f <count($rifel);$f++)
			{
				$str=str_replace("?A_INDICAZIONIRIFUGIO_" . ($f+1), $rifel[$f], $str);
			}

			$colel = Tools::toArray($a_collabtrasporto, 140, 4);
			for($f = 0; $f < count($colel); $f++)
			{
				$str=str_replace("?TRASPORTOINCOLLABORAZIONECON_" . ($f+1), $colel[$f], $str);
			}
			
			$ossel=Tools::toArray($a_osservazioni, 140, 8);
			for( $f = 0; $f < count($ossel); $f++)
			{
				$str=str_replace("?OSSERVAZIONI_" . ($f+1), $ossel[$f], $str);	
			}
			

			// new page?
			// dati footer
			$str=str_replace("?DATA", date("d.m.Y"), $str);
			$str=str_replace("?PAGINA", $lastpage, $str);
			$str=str_replace("?TOTPAGINA", $totpages, $str);
				

			// crea file temp SVG
			$outFileName = "PBC_FoglioEvacuazione_" . str_pad($page, 4, "0", STR_PAD_LEFT);
			file_put_contents($outFilePath . $outFileName . ".svg", $str, LOCK_EX);
			$i++;

			// add a page
			$pdf->AddPage();
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			$pdf->setMargins(0, 0, 0, true);
			$pdf->setPageOrientation('P', false, 0);		
			$pdf->ImageSVG($file=$outFilePath . $outFileName . ".svg", $x=0, $y=0, $w=745, $h=1052, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
			
			// elimina SVG temp
			unlink($outFilePath . $outFileName . ".svg");

			// reload clean svg
			$str=file_get_contents($svgfilename);
			$lastpage=$page;
			
		}

		// chiudi PDF e scrivi
		$pdf->Output($outHomePath . "04_FoglioEvacuazione" . "" . ".pdf", 'FD');
		// elimina cartella temp
		rmdir($outFilePath);
		// chiudi conn mysql
		$mysqli->close();
	}
	
}
