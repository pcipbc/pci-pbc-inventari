<?php
require_once('class.Stampa.php');
require_once('tcpdf_include.php');
require_once('class.Tools.php');

class Panoramiche extends Stampa
{
	public function createPDF($archid)
	{
		set_time_limit(0);
		// file svg da elaborare e stampare
		$svgfilename = "svg/09_panoramiche.svg";
		$outHomePath = "/tmp/";

		//// connessione db
		$dbhost = "localhost";
		$dbuser = "pbc";
		$dbpass = "manuela";
		$dbname = "pci_pbc_inventari";
		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		$mysqli->set_charset("utf8");
		$result = $mysqli->query("select 
				coalesce(a.DENOMINAZIONE, pa.DENOMINAZIONE) AS paDENOMINAZIONE, 
				coalesce(pa.PIANO, '') AS PIANO, 
				coalesce(pa.NOPCI, '') AS paNOPCI, 
				img.NOME AS imgNOME, 
				img.ESTENSIONE AS imgESTENSIONE, 
				img.CONTENUTO AS imgCONTENUTO,
				coalesce(a.NOPCI, paa.NOPCI) AS NOPCI, 
				coalesce(a.CATEGORIA,paa.CATEGORIA) AS CATEGORIA, 
				coalesce(a.DENOMINAZIONE, paa.DENOMINAZIONE) AS DENOMINAZIONE,
				coalesce(a.CAP, paa.CAP) AS CAP,
				coalesce(a.COMUNE, paa.COMUNE) AS COMUNE,
				coalesce(a.INDIRIZZO, paa.INDIRIZZO) AS INDIRIZZO,
				coalesce(a.COORDINATE, paa.COORDINATE) AS COORDINATE,
				coalesce(a.FONDORFD, paa.FONDORFD) AS FONDORFD,
				coalesce(pa.ALTRADENOMINAZIONE, '') AS ALTRADENOMINAZIONE,
				img.DIDASCALIA
			from IMMAGINE img
				left outer join ARCHITETTURA a on a.ID = img.IDPROP
				left outer join PARTEARCHITETTONICA pa on pa.ID = img.IDPROP and pa.STATUS = 'A'
				left outer join ARCHITETTURA paa on paa.ID = pa.ARCHITETTURA_ID
			where
				(a.ID = '" . $archid . "' or pa.ARCHITETTURA_ID='" . $archid . "') 
				and img.STATUS = 'A' and coalesce(pa.STATUS, 'A')='A'
			order by
				(case a.ID when null then 2 else 1 end) ASC,
				pa.DENOMINAZIONE, pa.PIANO, pa.NOPCI, img.created
			");
		$num = $result->num_rows;

		// base pdf 
		 $rndName = Tools::rndString(4);
		 $outFilePath = $outHomePath . "PBC_" . $rndName . "/";
		 mkdir($outFilePath);

		// gen pdf
		 $pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false, true);
		 $pdf->SetPrintHeader(false);
		 $pdf->SetPrintFooter(false);
		 $pdf->SetMargins(0, 0, 0, true);
		 $pdf->setPageOrientation('', false, 0);	
		 $pdf->SetFont('lato', '', 12);

		// itera su OA
		$i=0;
		$page=0;
		$imgMaxSize=550;
		while ($row = $result->fetch_assoc())  // $num
		{
			set_time_limit(0);
			$page++; // pagina corrente

			$denominazione = Tools::cleanForSVG(array_values($row)[0]);
			$piano = Tools::cleanForSVG(array_values($row)[1]);
			$nopci = Tools::cleanForSVG(array_values($row)[2]);
			$nome = Tools::cleanForSVG(array_values($row)[3]);
			$ext = Tools::cleanForSVG(array_values($row)[4]);
			$image = array_values($row)[5];
			$a_nopci = array_values($row)[6];
			$a_categ = array_values($row)[7];
			$a_denom = array_values($row)[8];
			$a_cap = array_values($row)[9];
			$a_comune = array_values($row)[10];
			$a_indirizzo = array_values($row)[11];
			$a_coord = array_values($row)[12];
			$a_mappal = Tools::cleanForSVG(array_values($row)[13]);
			$altradenom = array_values($row)[14];
			$didascalia = Tools::cleanForSVG(array_values($row)[15]);

			$nomeparte = $denominazione;
			//if( $piano is not null )
			//	$nomeparte = $nomeparte . " (Piano " . $piano . ")";
			if( !empty($altradenom))
				$nomeparte = $nomeparte . " — " . $altradenom;

			
			$imgb64 = "data:image/jpeg;base64," . base64_encode($image);

			$imgNuH=$imgMaxSize;
			$imgNuW=$imgMaxSize;

			// get size
			$srcimg = imagecreatefromstring($image);
			$width = imagesx($srcimg);
			$height = imagesy($srcimg);
			$aspect_ratio = $height/$width;

			if( $width <= $imgNuW && $height <= $imgNuH )
			{
				$imgNuH = $height;
				$imgNuW = $width;
			}
			else
			{
				if( $width > $height )
				{
					$imgNuW = $imgMaxSize;
					$imgNuH = ( $imgNuW * $height ) / $width;
				}
				else if( $height > $width)
				{
					$imgNuH = $imgMaxSize;
					$imgNuW = ($imgNuH * $width ) / $height;
				}
			}


			// carica contenuto template svg
			$str=file_get_contents($svgfilename);

			// sostituisci campi
			$str=str_replace("?A_NOPCI", $a_nopci, $str); 
			$str=str_replace("?A_CAT", $a_categ, $str); 
			$str=str_replace("?A_DENOMINAZIONE", $a_denom, $str); 
			$str=str_replace("?CAP", $a_cap, $str); 
			$str=str_replace("?COMUNE", $a_comune, $str); 
			$str=str_replace("?INDIRIZZO", $a_indirizzo, $str); 
			$str=str_replace("?COORDINATE", $a_coord, $str); 
			$str=str_replace("?NUMEROMAPPALE", $a_mappal, $str); 
			$str=str_replace("?IMMAGINE_C", $imgb64, $str);
			$str=str_replace("?IMMAGINE_H", $imgNuH, $str);
			$str=str_replace("?IMMAGINE_W", $imgNuW, $str);
			$str=str_replace("?NOMEPARTE", $nomeparte, $str);
			$str=str_replace("?DATA", date("d.m.Y"), $str);
			$str=str_replace("?PAGINA", $page, $str);
			$str=str_replace("?TOTPAGINA", $num, $str);

			$dida = Tools::toArray($didascalia, 90, 3);
			for( $f = 0; $f < count($dida); $f++)
			{
				$str=str_replace("?DIDASCALIA" . "_" . ($f+1), $dida[$f], $str);
			}

			// scrivi svg riempito
			$outFileName = "PBC_Panoramiche_" . str_pad($page, 4, "0", STR_PAD_LEFT) ;
			file_put_contents($outFilePath . $outFileName . ".svg", $str, LOCK_EX);

			// add a page
			$pdf->AddPage();
			$pdf->SetPrintHeader(false);
			$pdf->SetPrintFooter(false);
			$pdf->setMargins(0, 0, 0, true);
			$pdf->setPageOrientation('', false, 0);		
			// $pdf->Line(0, 0, 745, 1052, array('width' => 5,  'color' => array(255, 0, 0)) );

			$pdf->ImageSVG($file=$outFilePath . $outFileName . ".svg", $x=0, $y=0, $w=745, $h=1052, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
			//Close and output PDF document
			unlink($outFilePath . $outFileName . ".svg");

			$i++; // aumenta contatore record
		}
		// scrivi pdf completo e rimuovi directory temp
		$pdf->Output($outHomePath . "09_Panoramiche" . "" . ".pdf", 'FD');
		rmdir($outFilePath);
		
		$mysqli->close(); 
	}
}
?>
