<?php
require_once('class.Stampa.php');
require_once('tcpdf_include.php');
require_once('class.Tools.php');

class Copertina extends Stampa
{
	public function createPDF($archid)
	{
	
		// file svg da elaborare e stampare
		$svgfilename = "svg/00_copertina.svg";
		$outHomePath = "/tmp/";

		// base pdf 
		 $rndName = Tools::rndString(4);
		 $outFilePath = $outHomePath . "PBC_" . $rndName . "/";
		 mkdir($outFilePath);

		// carica A da DB
		$dbhost = "localhost";
		$dbuser = "pbc";
		$dbpass = "manuela";
		$dbname = "pci_pbc_inventari";
		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		$mysqli->set_charset("utf8");
		 $result = $mysqli->query("select DENOMINAZIONE from ARCHITETTURA where ID = '" . $archid . "'");
		 $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

		// leggi campi
		 $a_denom = Tools::cleanForSVG($row['DENOMINAZIONE']);

		// sost. testo in svg
		 $str=file_get_contents($svgfilename);
		 $str = str_replace("?A_DENOMINAZIONE", $a_denom, $str);

		// output SVG
		 $outFileName = "PBC_Copertina_" . str_pad("0", 4, "0", STR_PAD_LEFT) ;
		 file_put_contents($outFilePath . $outFileName . ".svg", $str, LOCK_EX);
		
		// gen pdf
		$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false, true);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->SetMargins(0, 0, 0, true);
		$pdf->setPageOrientation('', false, 0);	
		$pdf->SetFont('lato', '', 12);
		$pdf->AddPage();
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->setMargins(0, 0, 0, true);
		$pdf->setPageOrientation('', false, 0);		
		$pdf->ImageSVG($file=$outFilePath . $outFileName . ".svg", $x=0, $y=0, $w=745, $h=1052, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
		$pdf->Output($outHomePath . "00_Copertina" . "" . ".pdf", 'FD');

		rmdir($outFilePath);
		
		$mysqli->close(); 
	}
	
}


?>
