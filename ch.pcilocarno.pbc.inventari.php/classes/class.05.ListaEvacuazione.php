<?php

require_once('class.Stampa.php');
require_once('tcpdf_include.php');
require_once('class.Tools.php');

class ListaEvacuazione extends Stampa
{
	public function createPDF($archid)
	{
		set_time_limit(0);
		// file svg da elaborare e stampare
		$svgfilename = "svg/05_lista_evacuazione.svg";
		$svgfilename_tot = "svg/05_totale_evacuazione.svg";
		// connessione db
		// connessione db
		$dbhost = "localhost";
		$dbuser = "pbc";
		$dbpass = "manuela";
		$dbname = "pci_pbc_inventari";
		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		$mysqli->set_charset("utf8");
		
		
		$result = $mysqli->query("select *, oa.ID AS oaID, oa.DENOMINAZIONE AS oaDENOMINAZIONE, oa.NOPCI AS oaNOPCI, oa.NOPREC AS oaNOPREC, oa.GENERE AS oaGENERE, oa.QUANTITA AS oaQUANTITA, oa.MISPESO AS oaMISPESO, oa.MISALTEZZA AS oaMISALTEZZA, oa.MISLARGHEZZA AS oaMISLARGHEZZA, oa.MISPROFONDITA AS oaMISPROFONDITA, oa.MISCCPESO AS oaMISCCPESO, oa.MISCCALTEZZA AS oaMISCCALTEZZA, oa.MISCCLARGHEZZA AS oaMISCCLARGHEZZA, oa.MISCCPROFONDITA AS oaMISCCPROFONDITA
				  from OPERAARTE oa 
				  inner join PARTEARCHITETTONICA pa on pa.ID = oa.PARTEARCHITETTONICA_ID 
				  inner join ARCHITETTURA a on a.ID = pa.ARCHITETTURA_ID 
				  where oa.STATUS='A' and pa.STATUS='A' and oa.STATO='MOBILE' and a.ID='" . $archid . "' 
				  order by oa.NOPCI");

		$num = $result->num_rows;
		

		// 
		$rndName = Tools::rndString(4);
		$outHomePath = "/tmp/";
		$outFilePath = $outHomePath . "PBC_" . $rndName . "/";
		mkdir($outFilePath);

		// gen pdf
		$pdf = new TCPDF('L', 'mm', 'A4', true, 'UTF-8', false, true);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->SetMargins(0, 0, 0, true);
		$pdf->setPageOrientation('', false, 0);	
		$pdf->SetFont('lato', '', 12);

		// calc tot pages
		$oapp = 15;
		$totpages = floor($num/$oapp)+1+($num%$oapp == 0 ?0:1);
		
		// var totali
		$tot_pezzi = 0;
		$tot_peso = 0;
		$tot_vol_cm = 0;

		// itera su OA
		$i=0;
		$page=1;
		$imgMaxSize=530;
		$lastpage=$page;
		$str=file_get_contents($svgfilename);
		$oapn=1;

		while ($row = $result->fetch_assoc())  // $num
		{
			// carica dati da result set
			$oa_id = $row["oaID"];
			$a_denominazione = $row["DENOMINAZIONE"];
			$a_nopci = $row["NOPCI"];
			$a_comune = $row["COMUNE"];
			$a_cat = $row["CATEGORIA"];
			$a_indrifugio = $row["INDICAZIONIRIFUGIO"];
			$oa_denominazione = $row["oaDENOMINAZIONE"];
			$oa_nopci = $row["oaNOPCI"];
			$oa_noprec = $row["oaNOPREC"];
			$oa_genere = $row["oaGENERE"];
			$oa_qta = $row["oaQUANTITA"];
			$oa_peso = $row["oaMISPESO"];
			
			$oa_alt = $row["oaMISALTEZZA"];
			$oa_larg = $row["oaMISLARGHEZZA"];
			$oa_prof = $row["oaMISPROFONDITA"];
			$oa_vol_cm = $oa_alt * $oa_larg * $oa_prof;
			
			$oa_peso_cc = $row["oaMISCCPESO"];
			$oa_alt_cc = $row["oaMISCCALTEZZA"];
			$oa_larg_cc = $row["oaMISCCLARGHEZZA"];
			$oa_prof_cc = $row["oaMISCCPROFONDITA"];
			$oa_vol_cm_cc = $oa_alt_cc * $oa_larg_cc * $oa_prof_cc;

			if( $oa_genere == $oa_denominazione ) 
				$oa_denominazione = "";
			else
				$oa_denominazione = " - " . $oa_denominazione;

			if( $oa_noprec !== '' )
				$oa_nopci = $oa_nopci . "  (" . $oa_noprec . ")";

			// prendi misure con cornice o senza?
			 if( !empty($oa_vol_cm_cc))
			 {
			 	$oa_peso = $oa_peso_cc;
			 	$oa_alt = $oa_alt_cc;
			 	$oa_larg = $oa_larg_cc;
			 	$oa_prof = $oa_prof_cc;
			 	$oa_vol_cm = $oa_vol_cm_cc;
			 }

			// aggiorna totali
			$tot_pezzi += $oa_qta;
			$tot_peso += ($oa_peso * $oa_qta);
			$tot_vol_cm += ($oa_vol_cm * $oa_qta);

			// dati OA elenco
			$oa_sub="_" . str_pad($oapn, 2, "0", STR_PAD_LEFT);
			$str=str_replace("?OA_NOPCI" . $oa_sub, Tools::cleanForSVG($oa_nopci), $str);
			$str=str_replace("?OA_DENOMINAZIONE" . $oa_sub, Tools::cleanForSVG($oa_genere . $oa_denominazione, 66), $str);

			$str=str_replace("?OA_QUANTITA" . $oa_sub, Tools::cleanForSVG($oa_qta), $str);
			$str=str_replace("?OA_MISALTEZZA" . $oa_sub, Tools::cleanForSVG($oa_alt), $str);
			$str=str_replace("?OA_MISLARGHEZZA" . $oa_sub, Tools::cleanForSVG($oa_larg), $str);
			$str=str_replace("?OA_MISPROFONDITA" . $oa_sub, Tools::cleanForSVG($oa_prof), $str);
			$str=str_replace("?OA_MISPESO" . $oa_sub, Tools::cleanForSVG($oa_peso), $str);
			$str=str_replace("?VOL" . $oa_sub, Tools::cleanForSVG(Tools::volCM3toM3($oa_vol_cm, 3)), $str);

			$page = floor(($i+1)/$oapp)+1;
			// new page?
			if( $page != $lastpage || $i == ($num-1))
			{
				// Ev. togli placeholder per posizioni non elaborate
				for( $j = $oapn; $j < ($oapp+1); $j++ )
				{
					$oa_sub="_" . str_pad($j, 2, "0", STR_PAD_LEFT);
					$str=str_replace("?OA_NOPCI" . $oa_sub, '', $str);
					$str=str_replace("?OA_DENOMINAZIONE" . $oa_sub, '', $str);
					$str=str_replace("?OA_QUANTITA" . $oa_sub, '', $str);
					$str=str_replace("?OA_MISALTEZZA" . $oa_sub, '', $str);
					$str=str_replace("?OA_MISLARGHEZZA" . $oa_sub, '', $str);
					$str=str_replace("?OA_MISPROFONDITA" . $oa_sub, '', $str);
					$str=str_replace("?OA_MISPESO" . $oa_sub, '', $str);
					$str=str_replace("?VOL" . $oa_sub, '', $str);
				}
				// dati testata
				$str=str_replace("?A_DENOMINAZIONE", Tools::cleanForSVG($a_denominazione), $str);
				$str=str_replace("?A_NOPCI", Tools::cleanForSVG($a_nopci), $str);
				$str=str_replace("?A_COMUNE", Tools::cleanForSVG($a_comune), $str);
				$str=str_replace("?A_CAT", Tools::cleanForSVG($a_cat), $str);
				$str=str_replace("?A_INDICAZIONIRIFUGIO", Tools::remNL(Tools::cleanForSVG($a_indrifugio)), $str);
				// dati footer
				$str=str_replace("?DATA", date("d.m.Y"), $str);
				$str=str_replace("?PAGINA", $lastpage, $str);
				$str=str_replace("?TOTPAGINA", $totpages, $str);
				
				// crea file temp SVG
				$outFileName = "PBC_ListaEvacuazione_" . str_pad($page, 4, "0", STR_PAD_LEFT);
				file_put_contents($outFilePath . $outFileName . ".svg", $str, LOCK_EX);
				
				// add a page
				$pdf->AddPage();
				$pdf->SetPrintHeader(false);
				$pdf->SetPrintFooter(false);
				$pdf->setMargins(0, 0, 0, true);
				$pdf->setPageOrientation('L', false, 0);		
				$pdf->ImageSVG($file=$outFilePath . $outFileName . ".svg", $x=0, $y=0, $w=1052, $h=745, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
				
				// elimina SVG temp
				unlink($outFilePath . $outFileName . ".svg");

				// reload clean svg
				$str=file_get_contents($svgfilename);
				$lastpage=$page;
				$oapn=0;
			}


			$oapn++;
			$i++;
		}

		// aggiungi PDF totali
		$pdf->AddPage();
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->setMargins(0, 0, 0, true);
		$pdf->setPageOrientation('L', false, 0);
		$totstr = file_get_contents($svgfilename_tot);
		$totstr=str_replace("?A_DENOMINAZIONE", Tools::cleanForSVG($a_denominazione), $totstr);
		$totstr=str_replace("?A_NOPCI", Tools::cleanForSVG($a_nopci), $totstr);
		$totstr=str_replace("?A_COMUNE", Tools::cleanForSVG($a_comune), $totstr);
		$totstr=str_replace("?A_CAT", Tools::cleanForSVG($a_cat), $totstr);
		$totstr=str_replace("?A_INDICAZIONIRIFUGIO", Tools::remNL(Tools::cleanForSVG($a_indrifugio)), $totstr);
		$totstr = str_replace("?TOTALEPEZZI", $tot_pezzi, $totstr);
		$totstr = str_replace("?TOTALEPESO", $tot_peso, $totstr);
		$totstr = str_replace("?TOTALEVOLUME", Tools::cleanForSVG(Tools::volCM3toM3($tot_vol_cm)), $totstr);
		$totstr = str_replace("?DATA", date("d.m.Y"), $totstr);
		$totstr = str_replace("?PAGINA", $totpages, $totstr);
		$totstr = str_replace("?TOTPAGINA", $totpages, $totstr);
		file_put_contents($outFilePath . "PBC_ListaEvacuazione_TOT.svg", $totstr, LOCK_EX);
		$pdf->ImageSVG($file=$outFilePath . "PBC_ListaEvacuazione_TOT.svg", $x=0, $y=0, $w=1052, $h=745, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
		unlink($outFilePath . "PBC_ListaEvacuazione_TOT.svg");
		// chiudi PDF e scrivi
		$pdf->Output($outHomePath . "05_ListaEvacuazione" . "" . ".pdf", 'FD');
		// elimina cartella temp
		rmdir($outFilePath);
		// chiudi conn mysql
		$mysqli->close();
	}
	
}
