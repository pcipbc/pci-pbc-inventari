<?php
require_once('class.Stampa.php');
require_once('tcpdf_include.php');
require_once('class.Tools.php');

class FoglioCondotta extends Stampa
{
	public function createPDF($archid)
	{
		set_time_limit(0);
		// file svg da elaborare e stampare
		$svgfilename = "svg/01_foglio_condotta.svg";

		// connessione db
		$dbhost = "localhost";
		$dbuser = "pbc";
		$dbpass = "manuela";
		$dbname = "pci_pbc_inventari";
		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		$mysqli->set_charset("utf8");
		$result = $mysqli->query("select * from ARCHITETTURA left outer join CONTATTO on CONTATTO.IDPROP = ARCHITETTURA.ID and CONTATTO.TIPO='CUSTODE' left outer join CONTATTO c on c.IDPROP = ARCHITETTURA.ID and c.TIPO='PROPRIETARIO' WHERE ARCHITETTURA.ID='" . $archid . "'");
		
		$num = $result->num_rows;
		

		// 
		$rndName = Tools::rndString(4);
		$outHomePath = "/tmp/";
		$outFilePath = $outHomePath . "PBC_" . $rndName . "/";
		mkdir($outFilePath);

		// gen pdf
		$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false, true);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->SetMargins(0, 0, 0, true);
		$pdf->setPageOrientation('', false, 0);	
		$pdf->SetFont('lato', '', 12);

		// itera su OA
		$i=0;
		$page=0;
		
		if($num > 0) {
			while ($row = $result->fetch_assoc())  // $num
			{
				$page++;
	
				$a_cap=Tools::cleanForSVG($row["CAP"]);
				$p_cap=Tools::cleanForSVG($row["CAP"]);
				$r_cap=Tools::cleanForSVG($row["CAP"]);
				$scudi = Tools::cleanForSVG($row["SCUDI"]);
				if( $scudi == 'NONE') $scudi = 'Nessuno';
				if( $a_cap == '0' ) $a_cap = '';
				if( $p_cap == '0' ) $p_cap = '';
				if( $r_cap == '0' ) $r_cap = '';
				$docsicr="No";
				if( $row["DOCUMENTAZIONE"])
					$docsicr="Si";
	
				// sost campi
				$str=file_get_contents($svgfilename);
				$str=str_replace("?A_DENOMINAZIONE", Tools::cleanForSVG($row["DENOMINAZIONE"]), $str);
				$str=str_replace("?A_NOPCI", Tools::cleanForSVG($row["NOPCI"]), $str);
				$str=str_replace("?A_CAT", Tools::cleanForSVG($row["CATEGORIA"]), $str);
				$str=str_replace("?CAP", $a_cap, $str);
				$str=str_replace("?COMUNE", Tools::cleanForSVG($row["COMUNE"]), $str);
				$str=str_replace("?INDIRIZZO", Tools::cleanForSVG($row["INDIRIZZO"]), $str);
				$str=str_replace("?COORDINATE", Tools::cleanForSVG($row["COORDINATE"]), $str);
				$str=str_replace("?NUMEROMAPPALE", Tools::cleanForSVG($row["FONDORFD"]), $str);
				// proprietario
				$str=str_replace("?PROP_COGNOME", Tools::cleanForSVG($row["COGNOME"]), $str);
				$str=str_replace("?PROP_NOME", Tools::cleanForSVG($row["NOME"]), $str);
				$str=str_replace("?PROP_INDIRIZZO", Tools::cleanForSVG($row["INDIRIZZO"]), $str);
				$str=str_replace("?PROP_COMUNE", Tools::cleanForSVG($row["DOMICILIO"]), $str);
				$str=str_replace("?PROP_CAP", $p_cap, $str);
				$str=str_replace("?PROP_TEL_CASA", Tools::cleanForSVG($row["TELCASA"]), $str);
				$str=str_replace("?PROP_TEL_UFF", Tools::cleanForSVG($row["TELUFF"]), $str);
				$str=str_replace("?PROP_TEL_CELL", Tools::cleanForSVG($row["TELNAT"]), $str);
				$str=str_replace("?PROP_FAX", Tools::cleanForSVG($row["FAX"]), $str);
				$str=str_replace("?PROP_EMAIL", Tools::cleanForSVG($row["EMAIL"]), $str);
				// custode-responsabile
				$str=str_replace("?RESP_COGNOME", Tools::cleanForSVG($row["COGNOME"]), $str);
				$str=str_replace("?RESP_NOME", Tools::cleanForSVG($row["NOME"]), $str);
				$str=str_replace("?RESP_INDIRIZZO", Tools::cleanForSVG($row["INDIRIZZO"]), $str);
				$str=str_replace("?RESP_COMUNE", Tools::cleanForSVG($row["DOMICILIO"]), $str);
				$str=str_replace("?RESP_CAP", $r_cap, $str);
				$str=str_replace("?RESP_TEL_CASA", Tools::cleanForSVG($row["TELCASA"]), $str);
				$str=str_replace("?RESP_TEL_UFF", Tools::cleanForSVG($row["TELUFF"]), $str);
				$str=str_replace("?RESP_TEL_CELL", Tools::cleanForSVG($row["TELNAT"]), $str);
				$str=str_replace("?RESP_FAX", Tools::cleanForSVG($row["FAX"]), $str);
				$str=str_replace("?RESP_EMAIL", Tools::cleanForSVG($row["EMAIL"]), $str);
	
				$str=str_replace("?SCUDI", $scudi, $str);
				$str=str_replace("?DISTRIBUZIONEPI1", Tools::cleanForSVG($row["DISTRIBUZIONE1"]), $str);
				$str=str_replace("?DISTRIBUZIONEPI2", Tools::cleanForSVG($row["DISTRIBUZIONE2"]), $str);
				$str=str_replace("?DISTRIBUZIONEPI3", Tools::cleanForSVG($row["DISTRIBUZIONE3"]), $str);
				$str=str_replace("?DISTRIBUZIONEPI4", Tools::cleanForSVG($row["DISTRIBUZIONE4"]), $str);
	
				$str=str_replace("?DOCUMENTAZIONESICUREZZAREALIZZATA", $docsicr, $str);
				$str=str_replace("?RESPONSABILE", Tools::cleanForSVG($row["RESPONSABILEPCI"]) , $str);
				$str=str_replace("?RESPEVAC", Tools::cleanForSVG($row["RESPONSABILEEVACUAZIONE"]) , $str);
				$str=str_replace("?RESPPROTEZ", Tools::cleanForSVG($row["RESPONSABILEPROTEZIONE"]) , $str);
	
				$infodep=Tools::toArray(Tools::cleanForSVG($row["DEPOSITO"]), 70, 5);
				for( $f = 0; $f < count($infodep); $f++)
				{
					$str=str_replace("?INFORMAZIONIDEPOSITO_" . ($f+1), $infodep[$f] , $str);	
				}
				$osservaz=Tools::toArray(Tools::cleanForSVG($row["NOTE"]), 95, 9);
				for( $f = 0; $f < count($osservaz); $f++)
				{
					$str=str_replace("?OSSERVAZIONI_" . ($f+1), $osservaz[$f], $str);
				}
	
	
				$str=str_replace("?DATA", date("d.m.Y"), $str);
				$str=str_replace("?PAGINA", $page, $str);
				$str=str_replace("?TOTPAGINA", $num, $str);
	
				$outFileName = "PBC_FoglioCondotta_" . str_pad($page, 4, "0", STR_PAD_LEFT) ;
				file_put_contents($outFilePath . $outFileName . ".svg", $str, LOCK_EX);
	
				$i++;
	
				// add a page
				$pdf->AddPage();
				$pdf->SetPrintHeader(false);
				$pdf->SetPrintFooter(false);
				$pdf->setMargins(0, 0, 0, true);
				$pdf->setPageOrientation('', false, 0);		
				// $pdf->Line(0, 0, 745, 1052, array('width' => 5,  'color' => array(255, 0, 0)) );
				
				$pdf->ImageSVG($file=$outFilePath . $outFileName . ".svg", $x=0, $y=0, $w=745, $h=1052, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
				//Close and output PDF document
				unlink($outFilePath . $outFileName . ".svg");
				
			}			
		}
		$pdf->Output($outHomePath . "01_FoglioCondotta" . "" . ".pdf", 'FD');
		rmdir($outFilePath);
		$mysqli->close();
		echo "Pagine: ",$page,"\n";

		// 
		// $str=str_replace("?A_DENOMINAZIONE", a_denominazione, $str);
	}
	
}


?>
