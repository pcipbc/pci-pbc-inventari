<?php

require_once('class.Stampa.php');
require_once('tcpdf_include.php');
require_once('class.Tools.php');

class OggettiPrioEvacuare extends Stampa
{
	public function createPDF($archid)
	{
		set_time_limit(0);
		// file svg da elaborare e stampare
		$svgfilename = "svg/02_lista_evacuazione_prioritari.svg";

		// connessione db
		$dbhost = "localhost";
		$dbuser = "pbc";
		$dbpass = "manuela";
		$dbname = "pci_pbc_inventari";
		$mysqli = new mysqli($dbhost, $dbuser, $dbpass, $dbname);
		$mysqli->set_charset("utf8");
		$result = $mysqli->query("select *, oa.ID AS oaID, oa.DENOMINAZIONE AS oaDENOMINAZIONE, oa.NOPCI AS oaNOPCI, oa.NOPREC AS oaNOPREC, oa.GENERE AS oaGENERE, oa.NOTE AS oaNOTE from OPERAARTE oa inner join PARTEARCHITETTONICA pa on pa.ID = oa.PARTEARCHITETTONICA_ID inner join ARCHITETTURA a on a.ID = pa.ARCHITETTURA_ID WHERE oa.STATUS='A' and oa.STATO='MOBILE' and oa.PRIORITARIO=1 and a.ID='" . $archid . "' ORDER BY oa.NOPCI");
		
		
		$num = $result->num_rows;
		

		// 
		$rndName = Tools::rndString(4);
		$outHomePath = "/tmp/";
		$outFilePath = $outHomePath . "PBC_" . $rndName . "/";
		mkdir($outFilePath);

		// gen pdf
		$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false, true);
		$pdf->SetPrintHeader(false);
		$pdf->SetPrintFooter(false);
		$pdf->SetMargins(0, 0, 0, true);
		$pdf->setPageOrientation('', false, 0);	
		$pdf->SetFont('lato', '', 12);

		// calc tot pages
		$oapp = 10;
		$totpages = floor($num/$oapp)+($num%$oapp == 0 ?0:1);
		
		// var totali
		$tot_pezzi = 0;
		$tot_peso = 0;
		$tot_vol_cm = 0;

		// itera su OA
		$i=0;
		$page=1;
		$imgMaxSize=530;
		$lastpage=$page;
		$str=file_get_contents($svgfilename);
		$oapn=1;

		if($num > 0) {
			while ($row = $result->fetch_assoc())  // $num
			{
				$page = floor($i/$oapp)+1;
	
				// carica dati da result set
				$a_denominazione = $row["DENOMINAZIONE"];
				$a_nopci = $row["NOPCI"];
				$a_comune = $row["COMUNE"];
				$a_cat = $row["CATEGORIA"];
				$a_indrifugio = $row["INDICAZIONIRIFUGIO"];
	
				$oa_id = $row["oaID"];
				$oa_denominazione = $row["oaDENOMINAZIONE"];
				$oa_nopci = $row["oaNOPCI"];
				$oa_noprec = $row["oaNOPREC"];
				$oa_genere = $row["oaGENERE"];
				$oa_note =  Tools::cleanForSVG($row["oaNOTE"]);
	
	
	
				if( $oa_genere == $oa_denominazione )
					$oa_denominazione = "";
	
				if( $oa_noprec !== '' )
					$oa_nopci = $oa_nopci . "  (" . $oa_noprec . ")";
	
				//echo $oa_id.": ".$oa_nopci." ".$oa_genere.$oa_denominazione." (P".$page.")"."\n";
	
				$oa_sub="_" . str_pad($oapn, 2, "0", STR_PAD_LEFT);
	
				// dati OA elenco
				$str=str_replace("?OA_NOPCI" . $oa_sub, Tools::cleanForSVG($oa_nopci), $str);
				$str=str_replace("?OA_GENERE" . $oa_sub, Tools::cleanForSVG($oa_genere), $str);
				$str=str_replace("?OA_DENOMINAZIONE" . $oa_sub, Tools::cleanForSVG($oa_denominazione, 65), $str);
	
				$note = Tools::toArray($oa_note, 70, 4);
				for( $f = 0; $f < count($note); $f++)
				{
					$str=str_replace("?OA_NOTE" . $oa_sub . "_" . ($f+1), $note[$f], $str);
				}
	
				// new page?
				if( $page != $lastpage || $i == ($num-1))
				{
					// Ev. togli placeholder per posizioni non elaborate
					for( $j = $oapn; $j < ($oapp+1); $j++ )
					{
						$oa_sub="_" . str_pad($j, 2, "0", STR_PAD_LEFT);
	
						$str=str_replace("?OA_NOPCI" . $oa_sub, '', $str);
						$str=str_replace("?OA_GENERE" . $oa_sub, '', $str);
						$str=str_replace("?OA_DENOMINAZIONE" . $oa_sub, '', $str);
						$note = Tools::toArray('', 70, 4);
						for( $f = 0; $f < count($note); $f++)
						{
							$str=str_replace("?OA_NOTE" . $oa_sub . "_" . ($f+1), $note[$f], $str);
						}
					}
	
					// dati testata
					$str=str_replace("?A_DENOMINAZIONE", Tools::cleanForSVG($a_denominazione), $str);
					$str=str_replace("?A_NOPCI", Tools::cleanForSVG($a_nopci), $str);
					$str=str_replace("?A_COMUNE", Tools::cleanForSVG($a_comune), $str);
					$str=str_replace("?A_CAT", Tools::cleanForSVG($a_cat), $str);
	
					// dati footer
					$str=str_replace("?DATA", date("d.m.Y"), $str);
					$str=str_replace("?PAGINA", $lastpage, $str);
					$str=str_replace("?TOTPAGINA", $totpages, $str);
	
					// crea file temp SVG
					$outFileName = "PBC_ListaEvacuazionePrioritari_" . str_pad($page, 4, "0", STR_PAD_LEFT);
					file_put_contents($outFilePath . $outFileName . ".svg", $str, LOCK_EX);
	
					// add a page
					$pdf->AddPage();
					$pdf->SetPrintHeader(false);
					$pdf->SetPrintFooter(false);
					$pdf->setMargins(0, 0, 0, true);
					$pdf->setPageOrientation('P', false, 0);
					$pdf->ImageSVG($file=$outFilePath . $outFileName . ".svg", $x=0, $y=0, $w=745, $h=1052, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
	
					// elimina SVG temp
					unlink($outFilePath . $outFileName . ".svg");
	
					// reload clean svg
					$str=file_get_contents($svgfilename);
					$lastpage=$page;
					$oapn=0;
				}
	
				$oapn++;
				$i++;
			}
		} else {
			
			$pdf->AddPage();
			$pdf->ImageSVG($file="svg/02_lista_evacuazione_prioritari_nessuno.svg", $x=0, $y=0, $w=745, $h=1052, $link='', $align='T', $palign='L', $border=0, $fitonpage=true);
			//$pdf->Write(0, "", '', 0, 'C', true, false, false, 0);
			//$pdf->Write(0, "Nessun prioritario definito.", '', 0, 'C', true, false, false, 0);
		}

		// chiudi PDF e scrivi
		$pdf->Output($outHomePath . "02_ListaEvacuazionePrioritari" . "" . ".pdf", 'FD');
		// elimina cartella temp
		rmdir($outFilePath);
		// chiudi conn mysql
		$mysqli->close();
	}
	
}
