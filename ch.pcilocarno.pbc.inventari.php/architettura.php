<?php
require 'config.php';

$arch_id=$_GET['id'];
$mysqli->set_charset("utf8");
$result = $mysqli->query("SELECT
	A.NOPCI as 'anopci',A.DENOMINAZIONE as 'adenom', A.COMUNE as 'acomune', A.NOUBC as 'anoubc', 
	PA.NOPCI as 'panopci', PA.PIANO as 'papiano', PA.DENOMINAZIONE as 'padenom', 
	OA.NOPCI as 'oanopci', OA.GENERE as 'oagenere', OA.DENOMINAZIONE as 'oadenom',
	OA.EDITED as 'oaedited', OA.ID as 'oaid', (SELECT count(ID) FROM IMMAGINE where IDPROP=OA.ID and STATUS='A') AS 'oacount'
from OPERAARTE OA
	INNER JOIN PARTEARCHITETTONICA PA ON OA.PARTEARCHITETTONICA_ID = PA.ID
	INNER JOIN ARCHITETTURA A ON A.ID = PA.ARCHITETTURA_ID
WHERE
	PA.ARCHITETTURA_ID= '$arch_id' AND OA.STATUS='A'
ORDER BY
	PA.DENOMINAZIONE, PA.PIANO, OA.NOPCI ");

$num = $result->num_rows;

// load arch data
$arch_nopci="";
$arch_denom="";
$arch_comun="";
$arch_noubc="";
$result_a = $mysqli->query("
	SELECT a.NOPCI, a.NOUBC, a.DENOMINAZIONE, a.COMUNE 
	FROM ARCHITETTURA a 
	WHERE a.ID = '$arch_id'");

	while ($row = $result_a->fetch_assoc())
{
	$arch_nopci = $row["NOPCI"];
	$arch_noubc = $row["NOUBC"];
	$arch_denom = $row["DENOMINAZIONE"];
	$arch_comun = $row["COMUNE"];
}

$ultima_pa='$$@@##999##@@$$';

$pai=0;

$mysqli->close();

?>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PCi PBC - Inventari</title>
		<!-- Bootstrap -->
	    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/main.css" rel="stylesheet">
	    <!-- Add jQuery library -->
		<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		
		
		<!-- Add mousewheel plugin (this is optional) -->
		<script type="text/javascript" src="fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
		
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
		<script type="text/javascript" src="fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		
		<!-- Optionally add helpers - button, thumbnail and/or media -->
		<link rel="stylesheet" href="fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
		<script type="text/javascript" src="fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		<script type="text/javascript" src="fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
		
		<link rel="stylesheet" href="fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
		<script type="text/javascript" src="fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	</head>

	<body>

	    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">Cassandra</a>
	        </div>
	        <div class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="index.php"><span class="glyphicon glyphicon-home"></span>&nbsp;Elenco architetture</a></li>
	            <li><a href="stampa.php?id=<?php echo $arch_id?>"><span class="glyphicon glyphicon-print"></span>&nbsp;Scarica i pdf</a></li>
				<li><a href="rapporti.php?id=<?php echo $arch_id?>"><span class="glyphicon glyphicon-file"></span>&nbsp;Rapporti di fine corso</a></li>
	            <li><a href="ricerca.php"><span class="glyphicon glyphicon-search"></span>&nbsp;Ricerca</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </div>

		<div class="page-header">
			<h2>
			<?php echo $arch_nopci ?> 
			<?php echo $arch_denom ?> 
			<?php
			 if($arch_comun <> '')
			 { ?>
				<small><?php echo $arch_comun ?></small>
			 <?php } ?>
			</h2>
		</div>

	    <div class="container">
	    	<?php if ( $num == 0 ) { ?>
	    		<div class='alert alert-warning'>Nessuna OA registrata.</div>
			<?php } else { ?>
		    	<table class="table table-striped table-hover">
					<tbody>
						<?php
						 while ($row = $result->fetch_assoc()) {
							$pa			= $row['panopci'];
							$padenom	= $row['padenom'];
							$papiano	= $row['papiano'];
							$oanopci	= $row['oanopci'];
							$oadenom	= $row['oadenom'];
							$oagenere	= $row['oagenere'];
							$oaedited	= $row['oaedited'];
							$oaid		= $row['oaid'];
							$oacount	= $row['oacount'];
							
							// ricostruisci piano
							if($papiano <> 0){
								$papiano="&nbsp;<small>$papiano. piano</small>";
							}else{
								$papiano='&nbsp;<small>P.T.</small>';
							}
							
							// nuovo gruppo
							$pak=$pa."_".$padenom;
							if( $pak <> $ultima_pa ){ ?>
						<tr class="info">
							<td colspan="4" style="padding-left: 12px;">
								<h4><?php echo $padenom;?><small><?php echo $papiano ?></small></h4>
							</td>
						</tr>
						<?php
						 $ultima_pa=$pak;
						 $pai=0;
						 }
						 $oatxt=$oagenere;
						 if( $oagenere <> $oadenom ){
							$oatxt=$oagenere." - ".$oadenom;
						 }
						 $trclass=$pai%2==0?"odd":"even"; 
						 ?>
						<tr>
						<?php
							echo ($oacount > 0) ? '<td><a class="various fancybox.ajax" href="oadettaglio.php?id='.$oaid.'"><span class="glyphicon glyphicon-eye-open"></span></a></td>' : '<td><span style="color: silver;" class="glyphicon glyphicon-eye-close"></span></td>';
						?>
							
							<td><?php echo $oanopci ?></td>
							<td><?php echo $oatxt ?></td>
							<td><?php echo date("d.m.Y H:i", strtotime($oaedited)) ?></td>
						</tr>
						<?php
						 $pai++;
						 } ?>
					</tbody>
					<tfoot>
						<tr>
						<td colspan="4">
							<i>Totale OA: <?php echo $num; ?></i>
						</td>
						</tr>
					</tfoot>
				</table>
				<br/><br/><br/>
			<?php } ?>
		</div>
	</div>
	<script>
	$(document).ready(function() {
		$(".various").fancybox({
			maxWidth	: 800,
			maxHeight	: 800,
			fitToView	: false,
			width		: '70%',
			height		: '70%',
			autoSize	: false,
			closeClick	: false,
			openEffect	: 'none',
			closeEffect	: 'none'
		});
	});
	</script>
	</body>
</html>
