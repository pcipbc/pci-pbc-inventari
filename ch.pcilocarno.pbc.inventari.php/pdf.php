<?php
	ini_set('memory_limit', '-1');
	ini_set('display_errors', 'Off');
	require('classes/class.00.Copertina.php');
	require('classes/class.01.FoglioCondotta.php');
	require('classes/class.02.OggettiPrioEvacuare.php');
	require('classes/class.03.OggettiPrioProteggere.php');
	require('classes/class.04.FoglioEvacuazione.php');
	require('classes/class.05.ListaEvacuazione.php');
	require('classes/class.06.DettaglioEvacuazione.php');
	require('classes/class.07.ListaProtezione.php');
	require('classes/class.08.DettaglioProtezione.php');
	require('classes/class.09.Panoramiche.php');
	
	/* LOADING DIALOG */
	session_start();
    $processing=true;
    while($processing){
      $_SESSION['downloadstatus']=array("status"=>"pending","message"=>"Processing");
      session_write_close();
      $processing=doPdf();
      session_start();
    }
    $_SESSION['downloadstatus']=array("status"=>"finished","message"=>"Done");
    /* END LOADING DIALOG */
    
	function doPdf() {
		$arch=$_GET['id'];
		$idprt=$_GET['idstampa'];
	
		if($idprt == '00')
		{
			$de = new Copertina;
			$de->createPDF($arch);
		}
		if($idprt == '01')
		{
			$de = new FoglioCondotta;
			$de->createPDF($arch);
		}
		else
		if($idprt == '02')
		{
			$de = new OggettiPrioEvacuare;
			$de->createPdf($arch);
		}
		else
		if($idprt == '03')
		{
			$de = new OggettiPrioProteggere;
			$de->createPdf($arch);
		}
		else
		if($idprt == '04')
		{
			$de = new FoglioEvacuazione;
			$de->createPdf($arch);
		}
		else
		if($idprt == '05')
		{
			$de = new ListaEvacuazione;
			$de->createPDF($arch);
		}
		else
		if( $idprt == '06')
		{
			$de = new DettaglioEvacuazione;
			$de->createPDF($arch);
		}
		else
		if( $idprt == '07')
		{
			$de = new ListaProtezione;
			$de->createPDF($arch);
		}
		else
		if( $idprt == '08')
		{
			$de = new DettaglioProtezione;
			$de->createPDF($arch);
		}
		else
		if( $idprt == '09')
		{
			$de = new Panoramiche;
			$de->createPDF($arch);
		}
		else
			echo "Codice ".$idprt." non conosciuto.";
	}