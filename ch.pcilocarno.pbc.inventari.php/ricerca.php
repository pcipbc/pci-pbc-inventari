<?php
	include 'config.php';

	
	if(isset($_GET['mode'])) {
	// CHECK PARAMETERS
	$query = "";
	$q_s = '(OA.ID LIKE "%'.$_POST['campo_libero'].'%" OR OA.AUTORE LIKE "%'.$_POST['campo_libero'].'%" OR OA.NOPCI LIKE "%'.$_POST['campo_libero'].'%" OR OA.DENOMINAZIONE LIKE "%'.$_POST['campo_libero'].'%" OR OA.MATERIATECNICA LIKE "%'.$_POST['campo_libero'].'%" OR OA.ED1DESCRIZIONE LIKE "%'.$_POST['campo_libero'].'%")';
	
	
	if($_POST['tipo'] !== "") {
		$q_s .= ' AND (OA.STATO LIKE "'.$_POST['tipo'].'")';
	}
	if($_POST['genere'] !== "") {
		$q_s .= ' AND (OA.GENERE LIKE "'.$_POST['genere'].'")';
	}
	if($_POST['comune'] !== "") {
		$q_s .= ' AND (A.COMUNE LIKE "'.$_POST['comune'].'")';
	}
    $mysqli->set_charset("utf8");
	$result = $mysqli->query('SELECT 
				OA.NOPCI, OA.DENOMINAZIONE, OA.QTAIMMAGINI, A.COMUNE, A.DEFINITIVO, OA.ID, OA.EDITED as "edited"
			FROM
			OPERAARTE OA
			INNER JOIN PARTEARCHITETTONICA PA ON OA.PARTEARCHITETTONICA_ID = PA.ID
			INNER JOIN ARCHITETTURA A ON A.ID = PA.ARCHITETTURA_ID
			where '.$q_s.' AND OA.STATUS="A" ORDER BY A.COMUNE, A.DENOMINAZIONE ASC');
			
	//echo $query;

	//mysql_query("SET NAMES 'utf8'");

	$num = $result->num_rows;
	}
	
	// POPULATE DROPDOWNS
	
	// COMUNI
	$r_drop1=$mysqli->query("SELECT COMUNE FROM ARCHITETTURA GROUP BY COMUNE ORDER BY COMUNE");
	
	// STATO OGGETTO
	$r_drop2=$mysqli->query("SELECT STATO FROM OPERAARTE GROUP BY STATO ORDER BY STATO");
	
	// GENERE OGGETTO
	$r_drop3=$mysqli->query("SELECT GENERE FROM OPERAARTE GROUP BY GENERE ORDER BY GENERE");

$mysqli->close();
?>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title>PCi PBC - Ricerca</title>
		<!-- Bootstrap -->
	    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	    <link href="css/main.css" rel="stylesheet">
	    <link rel="stylesheet" href="css/bootstrap-select.css">
	    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="js/bootstrap-select.js"></script>
		<!-- Add mousewheel plugin (this is optional) -->
		<script type="text/javascript" src="fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
		
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
		<script type="text/javascript" src="fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		
		<!-- Optionally add helpers - button, thumbnail and/or media -->
		<link rel="stylesheet" href="fancybox/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
		<script type="text/javascript" src="fancybox/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
		<script type="text/javascript" src="fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
		
		<link rel="stylesheet" href="fancybox/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
		<script type="text/javascript" src="fancybox/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
	</head>

	<body>

	    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
	      <div class="container-fluid">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
	            <span class="sr-only">Toggle navigation</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="#">Cassandra</a>
	        </div>
	        <div class="collapse navbar-collapse">
	          <ul class="nav navbar-nav">
	            <li><a href="index.php"><span class="glyphicon glyphicon-home"></span>&nbsp;Elenco architetture</a></li>
	            <li class="active"><a href="#"><span class="glyphicon glyphicon-search"></span>&nbsp;Ricerca</a></li>
	          </ul>
	        </div><!--/.nav-collapse -->
	      </div>
	    </div>

		<div class="page-header">
			<h2>Architetture</h2>
		</div>

	    <div class="container">
	    	<form method="POST" action="?mode=search">
					<div class="form-group">
						<input type="text" name="campo_libero" placeholder="Cerca testo" class="form-control">
					</div>
	    		Comune: <select name="comune" title="Seleziona il comune" class="selectpicker show-tick" data-live-search="true">
	    			
	    			<?PHP
					while ($row = $r_drop1->fetch_assoc()) {
						echo '<option value="'.$row['COMUNE'].'">'.$row['COMUNE'].'</option>';  
					}
	    			
	    		?>
	    		</select>
	    		Tipo: <select name="tipo" title="Seleziona il tipo" class="selectpicker show-tick" data-live-search="true">
	    		<?PHP
					while ($row = $r_drop2->fetch_assoc()) {
						echo '<option value="'.$row['STATO'].'">'.$row['STATO'].'</option>';  
					}
	    			
	    		?>
	    		</select>
	    		Genere: <select name="genere" title="Seleziona il genere OA" class="selectpicker show-tick" data-live-search="true">
	    		<?PHP
					while ($row = $r_drop3->fetch_assoc()) {
						echo '<option value="'.$row['GENERE'].'">'.$row['GENERE'].'</option>';  
					}
	    			
	    		?>
	    		</select>
	    		<input type="hidden" name="search" value="true">
	    		<input class="btn btn-default" type="submit" value="Cerca!">
	    	</form>
	    	
	    	
	    	
	    	<?php if(isset($_GET['mode'])) { 
		    	
		    	echo'
		    	<div class="alert alert-warning"><i>Totale OA: '.$num.'</i></div>
	    	<table class="table table-striped table-hover">
	    		<thead>
	    		<tr>
	    			<th></th>
					<th>No PCi</th>
					<th>Denominazione</th>
					<th>Comune</th>
					<th>Ultima modifica</th>
				</tr>
				</thead>
				<tbody>';
				if($num < 1){
					echo "<tr><td colspan='5'><center>----- Nessuna risultato -----</center></td></tr>";
				 } else{
					$i=0;
					while ($row = $result->fetch_assoc()) {
			
						$nopci=$row["NOPCI"];
						$denominazione=$row["DENOMINAZIONE"];
						$comune=$row["COMUNE"];
						$id=$row["ID"];
						$def = $row["DEFINITIVO"];
						$edited = $row["edited"];
						$nimg = $row["QTAIMMAGINI"];
						if( $nopci == '' ) {
							$nopci="&nbsp;";
						}
						$i++;
						echo "<tr>
							<td>";
						echo $def ? "<span class='glyphicon glyphicon-check' />":"&nbsp;";
						echo '</td>
							<td>'.$nopci.'</td>
							<td>';
						echo ($nimg > 0) ? '<a class="various fancybox.ajax" href="oadettaglio.php?id='.$id.'">'.$denominazione.'</a>' : $denominazione;
						//echo ($nopci !== "") ? '<a class="various fancybox.ajax" href="oadettaglio.php?id='.$id.'">'.$denominazione.'</a>' : $denominazione;
						echo '</td>
							<td>'.$comune.'</td>
							<td>
								'.date("d.m.Y / H:i", strtotime($edited)).'
							</td>
						</tr>';						
					}
				}
				echo "
				</tbody>
	    	</table>";
	    	};
	    	?>
	    </div><!-- /.container -->
	    <script>
		    $(document).ready(function() {
		    	$('.selectpicker').selectpicker();
				$(".various").fancybox({
					maxWidth	: 800,
					maxHeight	: 800,
					fitToView	: false,
					width		: '70%',
					height		: '70%',
					autoSize	: false,
					closeClick	: false,
					openEffect	: 'none',
					closeEffect	: 'none'
				});
			});
	    </script>
	</body>
</html>
<?php
	$mysqli->close();
?>
