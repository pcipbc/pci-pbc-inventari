/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.program.Program;
import org.eclipse.ui.progress.UIJob;

import ch.pcilocarno.pbc.inventari.Activator;

public class FileUtils {

	static File lastSelectedFile = null;

	/**
	 * @return
	 */
	public static File getLastSelectedFile() {
		return lastSelectedFile;
	}

	/**
	 * @param lastSelected
	 */
	public static void setSelectedFile(File lastSelected) {
		lastSelectedFile = lastSelected;
	}

	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public static final String getExtension(String fileName) {
		if (fileName == null)
			return "";
		int dotidx = fileName.lastIndexOf('.');
		if (dotidx < 0)
			return "";

		return fileName.substring(dotidx + 1);
	}

	/**
	 * 
	 * @param fileName
	 * @return
	 */
	public static final String getFileName(String fileName) {
		if (fileName == null)
			return "";
		int dotidx = fileName.lastIndexOf('.');
		if (dotidx < 0)
			return fileName;

		return fileName.substring(0, dotidx);
	}

	/**
	 * @param path
	 * @throws Exception
	 */
	public static final void openFile(String path) throws Exception {

		URL url = FileLocator.find(Activator.getDefault().getBundle(), new Path(path), new HashMap<>());

		Program.launch(new File(FileLocator.toFileURL(url).getPath()).getCanonicalPath());
	}

	/**
	 * @param content
	 * @param friendlyName
	 * @param extension
	 * @param open
	 * @return the temp file written
	 * @throws IOException
	 */
	public static final File writeTmpFile(byte[] content, String friendlyName, String extension, boolean open)
			throws IOException {
		Assert.isTrue(friendlyName != null | extension != null, "FriendlyName and Extension can't be both null.");

		// crt file
		final File f;
		if (friendlyName != null)
			f = new File(System.getProperty("java.io.tmpdir"), friendlyName + "." + extension.toLowerCase());
		else
			f = File.createTempFile("ediesis", extension.toLowerCase());

		f.deleteOnExit();

		// write file contents
		FileOutputStream fos = new FileOutputStream(f);
		fos.write(content);
		fos.close();
		fos = null;
		Log.info("Written file %s", f.getAbsoluteFile());

		// open file
		if (open)
			new UIJob("Open report file (UI)") {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					try {
						Program.launch(f.getCanonicalPath());
						return Status.OK_STATUS;
					} catch (IOException ioe) {
						return new Status(IStatus.ERROR, Activator.PLUGIN_ID,
								"Trying to invoke open on file " + f.getAbsolutePath() + ": " + ioe);
					}
				}
			}.schedule();

		return f;

	}
}
