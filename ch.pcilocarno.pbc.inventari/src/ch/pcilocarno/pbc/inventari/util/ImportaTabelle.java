/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ch.pcilocarno.pbc.inventari.data.model.Tabella;
import ch.pcilocarno.pbc.inventari.data.model.enums.TipoTabella;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;

/**
 * ch.pcilocarno.pbc.inventari.ripresa.ImportaTabelle
 * 
 * @author elvis on Sep 10, 2009
 * @version @@version@@ build @@build@@
 */
public class ImportaTabelle {
	final File home;

	int filesImp = 0;

	public ImportaTabelle(String homePath) {
		this.home = new File(homePath == null ? "/home/elvis/Development/pci/pbc/tabelle" : homePath);
	}

	public void run() {
		Log.info("Importando files trovati in %s", home.getAbsolutePath());
		importFile("AUTORI.txt", TipoTabella.AUTORI, 4);
		importFile("COLLOCAZIONE.txt", TipoTabella.COLLOCAZIONE, 2);
		importFile("GENERE.txt", TipoTabella.GENERE, 2);
		importFile("GENEREGRUPPO.txt", TipoTabella.GENERE_GRUPPO, 2);
		importFile("INTERVENTO.txt", TipoTabella.INTERVENTO, 2);
		importFile("MATERIATECNICA.txt", TipoTabella.MATERIAETECNICA, 2);
		importFile("PARTEARCH.txt", TipoTabella.PARTEARCH, 2);
		importFile("POSIZIONEELDI.txt", TipoTabella.ELDIST_POSIZIONE, 2);
		importFile("TECNICAELDI.txt", TipoTabella.ELDIST_TECNICA, 2);
		importFile("TIPOELDI.txt", TipoTabella.ELDIST_TIPO, 2);
		importFile("MATERIALE.txt", TipoTabella.MATERIALE, 2);
		Log.info("Importazione terminata. Importato %s files.", filesImp);
	}

	/**
	 * @param file
	 * @param expectedElements
	 * @return
	 * @throws IOException
	 */
	List<String[]> readFileContent(File file, int expectedElements) throws IOException {
		BufferedReader in = new BufferedReader(new FileReader(file));
		List<String[]> result = new ArrayList<String[]>();
		String line;
		while ((line = in.readLine()) != null) {
			String[] pline = line.split(";");
			if (pline.length != expectedElements) // non standard line?
			{
				Log.error(null, "Non expected line found: %s", line);
			}

			result.add(pline);
		}
		in.close();
		return result;
	}

	/**
	 * @param tipo
	 * @param elements
	 * @return
	 */
	Tabella parse(TipoTabella tipo, String[] elements) {
		Integer code = 0;
		try {
			code = Integer.parseInt(elements[0]);
		} catch (NumberFormatException nfe) {
			Log.error(null, "Formatting integer code: %s (code assumed as ZERO)", elements[0]);
		}

		// per AUTORI salva dati suppl.
		if (tipo == TipoTabella.AUTORI) {
			Tabella t = new Tabella(tipo, code, elements[1].trim());
			return t;
		}
		return new Tabella(tipo, code, Tools.properCase(elements[1].trim()));
	}

	/**
	 * @param fileName
	 * @param tipo
	 * @param expectedLineElements
	 */
	void importFile(String fileName, TipoTabella tipo, int expectedLineElements) {
		File file = new File(home, fileName);
		if (!file.exists()) {
			Log.error(null, "File %s does not exist!", fileName);
			return;
		}
		if (!file.canRead() || !file.isFile())
			throw new RuntimeException("Cannot access file " + file.getAbsolutePath());

		try {
			Log.debug("Reading file %s", file.getAbsolutePath());
			List<String[]> fileC = readFileContent(file, expectedLineElements);
			PersistenceFactory.Tremote().TRbegin();
			for (String[] each : fileC) {
				Tabella t = parse(tipo, each);
				boolean exists = PersistenceFactory.Tremote().exists(t);
				if (exists) {
					Log.debug("Tabella esiste già: %s (%s)", t.getValore(), t.getTipo());
					continue;
				}
				t = PersistenceFactory.Tremote().saveOrUpdate(t);
				Log.debug("Tabella importata: %s (%s)", t.getValore(), t.getTipo());
			}
			PersistenceFactory.Tremote().TRcommit();
			filesImp++;
		} catch (Exception e) {
			Log.error(e, "Importing file %s of type %s", fileName, tipo.toString());
		}
	}
}
