/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

public class ByteArrayInOutStream extends ByteArrayOutputStream
{
	/**
	 * Creates a new ByteArrayInOutStream. The buffer capacity is initially 32 bytes, though its size increases if necessary.
	 */
	public ByteArrayInOutStream()
	{
		super();
	}

	/**
	 * Creates a new ByteArrayInOutStream, with a buffer capacity of the specified size, in bytes.
	 * 
	 * @param size
	 *            the initial size.
	 * @exception IllegalArgumentException
	 *                if size is negative.
	 */
	public ByteArrayInOutStream(int size)
	{
		super(size);
	}

	/**
	 * Creates a new ByteArrayInputStream that uses the internal byte array buffer of this ByteArrayInOutStream instance as its buffer array. The initial value of pos is set to zero and the initial value of count is the number of bytes that can be read from the byte array. The buffer array is not copied. This instance
	 * of ByteArrayInOutStream can not be used anymore after calling this method.
	 * 
	 * @return the ByteArrayInputStream instance
	 */
	public ByteArrayInputStream getInputStream()
	{
		// create new ByteArrayInputStream that respect the current count
		ByteArrayInputStream in = new ByteArrayInputStream(this.buf);

		// set the buffer of the ByteArrayOutputStream
		// to null so it can't be altered anymore
		this.buf = null;

		return in;
	}
}
