/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import ch.pcilocarno.pbc.inventari.data.model.OperaArte;
import ch.pcilocarno.pbc.inventari.data.model.ParteArchitettonica;
import ch.pcilocarno.pbc.inventari.data.model.enums.StatoConservazione;
import ch.pcilocarno.pbc.inventari.data.model.enums.StatoOpera;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;

public class CSVimport implements Runnable {
	final File csvfile;

	final SimpleDateFormat dformatter = new SimpleDateFormat("dd.MM.yyyy");

	/**
	 * @param csvfile_
	 */
	public CSVimport(File csvfile_) {
		this.csvfile = csvfile_;
	}

	/**
	 * @return
	 * @throws IOException
	 */
	final Iterable<CSVRecord> readCSV() throws IOException {
		CSVFormat csvf = CSVFormat.DEFAULT//
				.withDelimiter(';')//
				.withQuote('"')//
				.withSkipHeaderRecord()//
				.withHeader("NOPCI", "NOPREC", "STATO", "AUTHOR", "CREATED", "NOUBC", "GENERE", "DENOMINAZIONE",
						"CONSFORMALE", "OSSER", "ARTISTA", "DATAZIONE", "NOTATECNICA", "PA", "UBICAZIONE",
						"PRIORITARIO", "PEZZI", "PESO", "MISALTCC", "MISLARCC", "MISPROCC", "FOTO", "ORE", "PERSONE",
						"ATTREZZI", "MATERIALE", "PROT");

		// CSVParser parser = new CSVParser(in, csvf);
		return CSVParser.parse(csvfile, Charset.forName("ISO-8859-1"), csvf).getRecords();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		try {
			Iterable<CSVRecord> records = readCSV();
			checkCanImport(records);
			for (CSVRecord r : records)
				importRecord(r);
		} catch (Exception ex) {
			Log.error(ex, "Running CSVimport");
			throw new RuntimeException(ex.getMessage(), ex);
		}
	}

	private void checkCanImport(Iterable<CSVRecord> records) {
		List<String> nopci = new LinkedList<String>();
		for (CSVRecord r : records) {
			if (nopci.contains(Tools.ftrim(r.get("NOPCI")))) //
				throw new RuntimeException("Sono state trovate più OA con lo stesso numero PCi (NoPCi: "
						+ Tools.ftrim(r.get("NOPCI")) + ").");
			nopci.add(Tools.ftrim(r.get("NOPCI")));
		}
	}

	private final Date parseDate(String dt) throws ParseException {
		if (Tools.isEmpty(dt)) {
			return new Date(System.currentTimeMillis());
		}

		return dformatter.parse(dt);
	}

	/**
	 * @param r
	 * @throws Exception
	 */
	private void importRecord(CSVRecord r) throws Exception {
		// trova PA
		ParteArchitettonica pa = PersistenceFactory.PAlocal().getByName(r.get("PA"), true);

		OperaArte oa = new OperaArte(pa);
		oa.setNoPci(Tools.toInt(Tools.ftrim(r.get("NOPCI"))));
		oa.setNoPrec(Tools.ftrim(r.get("NOPREC")));
		oa.setStato(StatoOpera.valueOf(r.get("STATO")));
		oa.setAuthor(Tools.ftrim(r.get("AUTHOR")));
		oa.setCreated(parseDate(r.get("CREATED")));
		oa.setNoUbc(Tools.cut(Tools.ftrim(r.get("NOUBC")), 32));
		oa.setGenere(Tools.cut(Tools.ftrim(r.get("GENERE")), 64));
		oa.setDenominazione(Tools.cut(Tools.ftrim(r.get("DENOMINAZIONE")), 128));
		if (oa.getDenominazione().isEmpty())
			oa.setDenominazione(Tools.cut(oa.getGenere(), 128));
		oa.setConsFormale(StatoConservazione.parse(r.get("CONSFORMALE")));
		oa.setConsStrutturale(oa.getConsFormale());
		oa.setNote(Tools.cut(Tools.ftrim(r.get("OSSER")), 512));
		oa.setAutore(Tools.cut(Tools.ftrim(r.get("ARTISTA")), 128));
		oa.setDatazione(Tools.cut(Tools.ftrim(r.get("DATAZIONE")), 32));
		oa.setPciOsservazioni(Tools.cut(Tools.ftrim(r.get("NOTATECNICA")), 512));
		oa.setCollocazione(Tools.cut(Tools.ftrim(r.get("UBICAZIONE")), 64));
		oa.setPrioritario(Tools.toBoolean(r.get("PRIORITARIO")));
		oa.setQuantita(Tools.toInt(r.get("PEZZI")));

		oa.setMisPeso(Tools.toInt(r.get("PESO")));
		oa.setMisAltezza(Tools.toInt(r.get("MISALTCC")));
		oa.setMisLarghezza(Tools.toInt(r.get("MISLARCC")));
		oa.setMisProfondita(Tools.toInt(r.get("MISPROCC")));

		// oa.setMisPeso(oa.getMisCcPeso());
		// oa.setMisAltezza(oa.getMisCcAltezza());
		// oa.setMisLarghezza(oa.getMisCcLarghezza());
		// oa.setMisProfondita(oa.getMisCcProfondita());

		oa.setFoto(Tools.cut(Tools.ftrim(r.get("FOTO")), 64));
		oa.setPciOre(Tools.toBigD(r.get("ORE").replaceAll(",", ".")));
		oa.setPciPersone(Tools.toInt(r.get("PERSONE")));
		oa.setPciAttrezzi(Tools.cut(Tools.ftrim(r.get("ATTREZZI")), 512));
		oa.setPciMateriale(Tools.cut(Tools.ftrim(r.get("MATERIALE")), 512));
		oa.setPciModelloProtezione(Tools.cut(Tools.ftrim(r.get("PROT")), 256));

		PersistenceFactory.OAlocal().saveOrUpdate(oa);
	}
}
