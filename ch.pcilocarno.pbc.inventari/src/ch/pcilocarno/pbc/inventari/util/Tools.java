/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.jface.fieldassist.ContentProposal;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.fieldassist.ControlDecoration;
import org.eclipse.jface.fieldassist.FieldDecoration;
import org.eclipse.jface.fieldassist.FieldDecorationRegistry;
import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalListener;
import org.eclipse.jface.fieldassist.IContentProposalListener2;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.jface.fieldassist.IControlContentAdapter;
import org.eclipse.jface.fieldassist.SimpleContentProposalProvider;
import org.eclipse.jface.fieldassist.TextContentAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;

import ch.pcilocarno.pbc.inventari.beans.DatePeriod;
import ch.pcilocarno.pbc.inventari.data.TabellaService;
import ch.pcilocarno.pbc.inventari.data.model.enums.TipoTabella;

/**
 * ch.pcilocarno.pbc.inventari.util.Tools
 * 
 * @author elvis on Sep 9, 2009
 * @version @@version@@ build @@build@@
 */
public abstract class Tools {

	public static final List<DatePeriod> buildPeriods(List<LocalDate> dates, boolean addToday, boolean addAnydate) {
		List<DatePeriod> result = new LinkedList<>();

		if (addToday)
			result.add(new DatePeriod(LocalDate.now(), LocalDate.now()));

		if (addAnydate)
			result.add(new DatePeriod(DatePeriod.MIN, DatePeriod.MAX));

		long MAX_EMPTY_DAYS = 2L;

		DatePeriod current = new DatePeriod();
		LocalDate lastDate = null;
		for (LocalDate date : dates) {

			if (lastDate == null)
				lastDate = date;

			if (current.getFrom() == null)
				current.setFrom(date);

			current.setTo(lastDate);

			long daysBetween = ChronoUnit.DAYS.between(lastDate, date);

			if (Math.abs(daysBetween) > MAX_EMPTY_DAYS) {
				result.add(current);
				current = new DatePeriod();
				current.setFrom(date);
			}
			lastDate = date;
		}
		if (current.getTo() == null)
			current.setTo(lastDate);

		if (current.getFrom() != null && current.getTo() != null)
			result.add(current);

		result.sort(new Comparator<DatePeriod>() {
			@Override
			public int compare(DatePeriod o1, DatePeriod o2) {
				return o1.getFrom().compareTo(o2.getFrom()) * -1;
			}
		});

		return result;
	}

	/**
	 * 
	 * @param b
	 * @return
	 */
	public static final byte[] toPrimitive(Byte[] b) {
		if (b == null || b.length == 0)
			return new byte[0];

		byte[] result = new byte[b.length];
		for (int i = 0; i < b.length; i++) {
			result[i] = b[i].byteValue();
		}
		return result;
	}

	/**
	 * 
	 * @param b
	 * @return
	 */
	public static final Byte[] toObject(byte[] b) {
		if (b == null || b.length == 0)
			return new Byte[0];

		Byte[] result = new Byte[b.length];
		for (int i = 0; i < b.length; i++) {
			result[i] = Byte.valueOf(b[i]);

		}
		return result;
	}

	/**
	 * @param val
	 * @param deft
	 * @return
	 */
	public static final <T> T ifNull(T val, T deft) {
		if (val == null)
			return deft;
		return val;
	}

	/**
	 * @param s
	 * @return
	 */
	public static final boolean isEmpty(String s) {
		if (s == null)
			return true;
		return ftrim(s).isEmpty();
	}

	/**
	 * @param ms
	 * @return
	 */
	public static final String getMillisHR(final long ms) {
		if (ms < 1000)
			return ms + " ms";

		long m = ms % 1000;
		long s = (ms - m) / 1000;

		if (ms < 60000) //
			return s + " s " + m + " ms";

		s = s % 60;
		long n = (((ms - m) / 1000) - (s)) / 60;

		return n + " min " + s + " s " + m + " ms";

	}

	/**
	 * @param array
	 * @return
	 */
	public static final <T> T getFirst(T[] array) {
		if (array == null)
			return null;
		if (array.length == 0)
			return null;
		return array[0];
	}

	/**
	 * @param s
	 * @param len
	 * @return
	 */
	public static final String cut(final String s, int len) {
		if (s == null)
			return "";
		if (s.isEmpty())
			return "";
		if (s.length() <= len)
			return s;

		return s.substring(0, len - 1);
	}

	/**
	 * @param s
	 * @param len
	 * @return
	 */
	public static final String fixLen(final String s, int len) {
		String start = s;
		if (start == null)
			start = "";

		if (start.length() >= len)
			return start.substring(0, len);

		int rem = len - start.length();
		for (int i = 0; i < rem; i++) {
			start += " ";
		}

		return start;
	}

	/**
	 * @param len
	 * @return
	 */
	public static final String getRandomString(int len) {
		String alfab = "abcdefghijklmnopqrstuvwxyz";
		String result = "";
		Random rnd = new Random();
		for (int i = 0; i < len; i++) {
			int r = rnd.nextInt(alfab.length());
			result += alfab.toCharArray()[r];
		}
		return result;
	}

	/**
	 * @param text
	 * @return
	 */
	public static final String properCase(String text) {
		return text.substring(0, 1).toUpperCase() + text.substring(1).toLowerCase();
	}

	/**
	 * @param text
	 * @return
	 */
	public static final String forLike(String text) {
		if (text == null || text.isEmpty())
			return "%";
		if (text.indexOf("%") > 0)
			return text;
		else
			return "%" + text + "%";
	}

	/**
	 * @param s
	 * @return
	 */
	public static final Integer toInt(String s) {
		if (s.isEmpty())
			return 0;

		return Integer.parseInt(s);
	}

	public static final boolean toBoolean(String s) {
		if (s == null)
			throw new NullPointerException("String s is null");
		if ("true".equalsIgnoreCase(s))
			return true;
		if ("vero".equalsIgnoreCase(s))
			return true;
		if ("1".equalsIgnoreCase(s))
			return true;

		if ("false".equalsIgnoreCase(s))
			return false;
		if ("falso".equalsIgnoreCase(s))
			return false;
		if ("0".equalsIgnoreCase(s))
			return false;

		throw new IllegalArgumentException("Cannot parse " + s + " to Boolean.");
	}

	/**
	 * @param s
	 * @return
	 */
	public static final BigDecimal toBigD(String s) {
		if (s.isEmpty())
			return BigDecimal.ZERO;

		return new BigDecimal(s);
	}

	/**
	 * Checks if two Objects are equal in a null-safe mode.
	 * 
	 * @param o1
	 * @param o2
	 * @return
	 */
	public static final boolean equals(Object o1, Object o2) {
		if (o1 == null && o2 == null)
			return true;
		if (o1 == null && o2 != null)
			return false;

		return o1.equals(o2);
	}

	public static final String ftrim(String s) {
		if (s == null)
			return "";
		return s.replaceAll("\\s+", " ").trim();
	}

	/**
	 * @param field
	 * @param tipoTabella
	 * @param propAccStyle
	 */
	public static final void installContentProposalAsync(final Text field, final TipoTabella tipoTabella,
			final int propAccStyle, final boolean allowMultiple) {
		if (field == null || field.isDisposed())
			return;
		final String[] choices = TabellaService.getValues(tipoTabella);

		if (choices == null || choices.length == 0) {
			Log.warning("Nessuna lista valori trovata per %s", tipoTabella.toString());
			return;
		}

		Display.getCurrent().asyncExec(new Runnable() {
			@Override
			public void run() {
				if (field == null || field.isDisposed())
					return;
				installContentProposalAdapter(field, choices, propAccStyle, true, allowMultiple);
			}
		});
	}

	/**
	 * @param control
	 * @param choices
	 * @param propAccStyle
	 * @param addDeco
	 * @param allowMultiple
	 * @return
	 */
	private static ContentProposalAdapter installContentProposalAdapter(final Control control, String[] choices,
			int propAccStyle, boolean addDeco, boolean allowMultiple) {
		try {
			IControlContentAdapter cca = new TextContentAdapter();
			char[] autoActivationCharacters = null;
			KeyStroke keyStroke = KeyStroke.getInstance("Ctrl+Space");
			ContentProposalAdapter adapter = new ContentProposalAdapter(control, cca,
					getContentProposalProvider(choices, allowMultiple), keyStroke, autoActivationCharacters);

			adapter.setAutoActivationDelay(100);
			adapter.setPropagateKeys(true);
			adapter.setFilterStyle(ContentProposalAdapter.FILTER_NONE);
			adapter.setProposalAcceptanceStyle(propAccStyle);

			if (addDeco) {
				ControlDecoration fdec = new ControlDecoration(control, SWT.LEFT | SWT.TOP);
				FieldDecoration standardDecoration = FieldDecorationRegistry.getDefault()
						.getFieldDecoration(FieldDecorationRegistry.DEC_CONTENT_PROPOSAL);
				fdec.setImage(standardDecoration.getImage());
				fdec.setDescriptionText("Assistente disponibile via Ctrl+Barra spaziatrice");
				fdec.setShowOnlyOnFocus(true);
				fdec.show();
			}

			if (allowMultiple) {
				adapter.addContentProposalListener(new IContentProposalListener() {
					@Override
					public void proposalAccepted(IContentProposal proposal) {
						String oldtext = control.getData("oldtext") == null ? "" : (String) control.getData("oldtext");
						if (oldtext.isEmpty()) {
							((Text) control).setText(proposal.getContent());
							return;
						}
						String[] elts = oldtext.split(",");
						String result = "";
						for (String each : elts) {
							if (!result.isEmpty())
								result += ", ";
							result += each.trim();
						}
						result = result.substring(0, result.length() - elts[elts.length - 1].length());
						result += (result.isEmpty() ? "" : " ") + proposal.getContent();
						((Text) control).setText(result);
						((Text) control).setSelection(((Text) control).getText().length());
					}

				});
				adapter.addContentProposalListener(new IContentProposalListener2() {
					@Override
					public void proposalPopupOpened(ContentProposalAdapter adapter) {
						control.setData("oldtext", ((Text) control).getText());
					}

					@Override
					public void proposalPopupClosed(ContentProposalAdapter adapter) {
					}
				});
			}
			return adapter;
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException("Adding content proposal: " + e.toString());
		}
	}

	/**
	 * @param choices
	 * @param allowMultiple
	 * @return
	 */
	private static IContentProposalProvider getContentProposalProvider(final String[] choices,
			final boolean allowMultiple) {
		SimpleContentProposalProvider cp = new SimpleContentProposalProvider(choices) {
			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public IContentProposal[] getProposals(String contents, int position) {
				if (allowMultiple) {
					String[] elements = contents.split(",");
					contents = elements[elements.length - 1].trim();
				}

				ArrayList list = new ArrayList();
				for (int i = 0; i < choices.length; i++)
					if (choices[i].toUpperCase().indexOf(contents.toUpperCase()) >= 0) //
						list.add(new ContentProposal(choices[i]));
				return (IContentProposal[]) list.toArray(new IContentProposal[list.size()]);
			}
		};
		cp.setFiltering(true);
		return cp;
	}

}
