/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Widget;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.statushandlers.StatusManager;

import ch.pcilocarno.pbc.inventari.Activator;

public class UIhelper {

	public static final int STYLE_LABEL = SWT.LEFT;

	private static final ImageRegistry imgReg = new ImageRegistry();

	/**
	 * @param w
	 */
	public static final void dispose(Widget w) {
		if (w == null)
			return; // is null
		if (w.isDisposed())
			return; // is disposed
		w.dispose();
		w = null;
	}

	/**
	 * @param path
	 * @return
	 */
	public static final Image getImage(String path) {
		return getImageDescrInternal(path).createImage();
	}

	/**
	 * internal method to load an image descriptor from a full path (Relative to the
	 * current abstract pulgin)
	 * 
	 * @param fullPath
	 *            Image full path
	 * @return
	 */
	private static final ImageDescriptor getImageDescrInternal(String fullPath) {
		ImageDescriptor imgd = imgReg.getDescriptor(fullPath);
		if (imgd == null) // not initialized
			imgReg.put(fullPath, AbstractUIPlugin.imageDescriptorFromPlugin(Activator.PLUGIN_ID, fullPath));

		if (imgReg.getDescriptor(fullPath) == null) //
			Log.error(null, "Cannot instantiate image %s", fullPath);

		return imgReg.getDescriptor(fullPath);
	}

	/**
	 * @param parent
	 * @param title
	 * @param message
	 */
	public static void openInfoDialog(Shell parent, String title, String message) {
		MessageDialog.openInformation(parent, title, message);
	}

	/**
	 * @param parent
	 * @param title
	 * @param message
	 * @param okButtonText
	 * @return
	 */
	public static boolean openConfirmDialog(Shell parent, String title, String message, String okButtonText) {
		MessageDialog dialog = new MessageDialog(parent, title, null, message, MessageDialog.QUESTION,
				new String[] { okButtonText, "Cancel" }, 1);
		return dialog.open() == MessageDialog.OK;
	}

	/**
	 * @param shell
	 * @param message
	 * @param th
	 */
	public static void reportException(final String message, final Throwable th) {
		final IStatus status = new Status(IStatus.ERROR, Activator.PLUGIN_ID, message, th);
		StatusManager.getManager().handle(status, StatusManager.LOG | StatusManager.SHOW);
		// StatusManager.getManager().handle(status, StatusManager.SHOW);
	}
}
