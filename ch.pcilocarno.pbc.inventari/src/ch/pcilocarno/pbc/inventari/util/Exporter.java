/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;

import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.Utils;
import ch.pcilocarno.pbc.inventari.data.model.Allegato;
import ch.pcilocarno.pbc.inventari.data.model.Architettura;
import ch.pcilocarno.pbc.inventari.data.model.Contatto;
import ch.pcilocarno.pbc.inventari.data.model.Immagine;
import ch.pcilocarno.pbc.inventari.data.model.LocalHistory;
import ch.pcilocarno.pbc.inventari.data.model.OperaArte;
import ch.pcilocarno.pbc.inventari.data.model.ParteArchitettonica;
import ch.pcilocarno.pbc.inventari.data.model.Rapporto;
import ch.pcilocarno.pbc.inventari.data.model.enums.HistoryRecordType;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.model.enums.TipoContatto;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceTools;
import ch.pcilocarno.pbc.inventari.util.export.ExportAction;
import ch.pcilocarno.pbc.inventari.util.export.ExportLog;
import ch.pcilocarno.pbc.inventari.util.export.ExportReport;

public class Exporter {
	String SYNC = "sync";

	String nl = System.getProperty("line.separator");

	SimpleDateFormat df = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss.SSS");

	ExportReport report;

	public Exporter() {
		this.report = new ExportReport("");
	}

	public void dispose() {
		df = null;
		report = null;
	}

	public ExportReport getReport() {
		return report;
	}

	/**
	 * Esporta la banca dati locale verso la centralizzata
	 * 
	 * @throws Exception
	 */
	public final void sync() throws Exception {
		System.gc();

		if (!PersistenceTools.remoteDbAccessible()) //
		{
			getReport().setMessage("!Impossibile connettersi alla banca dati centrale!");
			return;
		}

		//
		final long start = System.currentTimeMillis();

		PersistenceFactory.Aremote().TRbegin();
		try {
			// count remote entities.
			getReport().before_pa_count = PersistenceFactory.PAremote()
					.countByArch(Session.INSTANCE.getArchitetturaUUID()).intValue();
			getReport().before_oa_count = PersistenceFactory.OAremote()
					.countByArch(Session.INSTANCE.getArchitetturaUUID()).intValue();
			getReport().before_img_count = PersistenceFactory.IMGremote()
					.countByArch(Session.INSTANCE.getArchitetturaUUID()).intValue();
			getReport().before_att_count = PersistenceFactory.ATTremote()
					.countByArch(Session.INSTANCE.getArchitetturaUUID()).intValue();
			getReport().before_rep_count = PersistenceFactory.REPremote()
					.countByArch(Session.INSTANCE.getArchitetturaUUID()).intValue();

			// load/check A
			Architettura a = PersistenceFactory.Alocal().findById(Session.INSTANCE.getArchitetturaUUID());
			if (a.getId() == null)
				throw new Exception("Architettura not found... ?!?");

			// export A
			syncA(a);

			// load local PA
			List<ParteArchitettonica> lclPas = PersistenceFactory.PAlocal().findByArch(a.getId());

			// aggiungi PA cancellate alla lista PA, anche le cancellate devono essere
			// sincronizzate!
			List<ParteArchitettonica> lclDelPas = PersistenceFactory.PAlocal().findByArch(a.getId(), RowStatus.C);
			lclPas.addAll(lclDelPas);

			// export PA
			for (ParteArchitettonica pa : lclPas) {
				List<OperaArte> lclOas = PersistenceFactory.OAlocal().findByPaText(pa.getId(), null);

				// aggiungi OA cancellate alla lista OA, anche le cancellate devono essere
				// sincronizzate!
				List<OperaArte> lclDelOas = PersistenceFactory.OAlocal().findByPaText(pa.getId(), null, RowStatus.C);
				lclOas.addAll(lclDelOas);

				syncPA(pa, !lclOas.isEmpty());

				// export OAs
				for (OperaArte oa : lclOas) {
					syncOA(oa);
				}
			}

			// export contacts
			for (Contatto ct : PersistenceFactory.Clocal().findAll()) {
				syncC(ct);
			}

			// export allegati
			for (Allegato att : PersistenceFactory.ATTlocal().findAll())
				syncAtt(att);

			// export Rapporti
			for( Rapporto rep : PersistenceFactory.REPlocal().findAll()) {
				syncRapport(rep);
			}
			// controlli post
			// count
			getReport().after_pa_count = PersistenceFactory.PAremote()
					.countByArch(Session.INSTANCE.getArchitetturaUUID()).intValue();
			getReport().after_oa_count = PersistenceFactory.OAremote()
					.countByArch(Session.INSTANCE.getArchitetturaUUID()).intValue();
			getReport().after_img_count = PersistenceFactory.IMGremote()
					.countByArch(Session.INSTANCE.getArchitetturaUUID()).intValue();
			getReport().after_att_count = PersistenceFactory.ATTremote()
					.countByArch(Session.INSTANCE.getArchitetturaUUID()).intValue();
			getReport().after_rep_count = PersistenceFactory.REPremote()
					.countByArch(Session.INSTANCE.getArchitetturaUUID()).intValue();

			PersistenceFactory.Aremote().TRcommit();
			PersistenceFactory.LHlocal().saveOrUpdate(new LocalHistory(HistoryRecordType.SYNC, a));
		} catch (Exception e) {
			Log.error(e, "Error exporting to db.");
			PersistenceFactory.Aremote().TRrollback();
			throw new Exception("Errore di sincronizzazione: " + e.getMessage(), e);
		}

		getReport().time_costed = System.currentTimeMillis() - start;
		System.gc();
	}

	/**
	 * @param rep
	 */
	void syncRapport(Rapporto rep) throws Exception{
		Rapporto rmtRep = PersistenceFactory.REPremote().findById(rep.getId());

		if (rmtRep == null) {
			// crea nuovo
			rmtRep = new Rapporto(rep.getArchitettura());
			rmtRep.setId(rep.getId());
			getReport().rep_created++;
			
			getReport().addLog(new ExportLog(ExportAction.CRT, Rapporto.class, DateUtils.formatDateTime(rep.getCreated())+ "",
					rep.getNome(), "Nuovo RAPPORTO locale."));
		} else {
			// aggiorna?
			final Date ilmdate = rep.getEdited();
			final Date irmdate = rmtRep.getEdited();

			if (ilmdate.after(irmdate)) {
				getReport().addLog(new ExportLog(ExportAction.UPD, Rapporto.class, "", rep.getNome(),
						"Data modifica diversa: " + df.format(irmdate) + " <> " + df.format(ilmdate)));
				getReport().c_updated++;
				
			} else if (ilmdate.before(irmdate)) {
				getReport().addLog(new ExportLog(ExportAction.ERR, Rapporto.class, "", rep.getNome(),
						"Data modifica server posteriore a locale: " + df.format(irmdate) + " > "
								+ df.format(ilmdate)));
				return;
				
			} else if (ilmdate.equals(irmdate)) {
				getReport().addLog(
						new ExportLog(ExportAction.IGN, Rapporto.class, "", rep.getNome(), "Nessuna modifica locale."));
				return;
				
			}
		}

		Utils.copyValues(rep, rmtRep);

		rmtRep.setEditor(rep.getEditor());
		rmtRep.setEdited(rep.getEdited());
		rmtRep.setArchitettura(rep.getArchitettura());
		rmtRep.setStatus(rep.getStatus());
		rmtRep.setContenuto(rep.getContenuto());
		rmtRep.setNome(rep.getNome());

		PersistenceFactory.REPremote().saveOrUpdate(rmtRep, false);

		// dispose
		rmtRep = null;
		rep = null;
	}

	/**
	 * @param img
	 * @param report
	 * @throws Exception
	 */
	void syncIMG(Immagine img) throws Exception {
		if (img.getIdProp() == null) {
			Log.warning("Trovata immagine senza padre (%s.%s)", img.getNome(), img.getEstensione());
			try {
				byte[] c = img.getContenuto();
				FileUtils.writeTmpFile(c, img.getNome(), img.getEstensione(), false);
				c = null;
				img = null;
				return;
			} catch (Exception ex) {
				Log.error(ex, "Locally storing orphan Image!");
			}
		}

		Immagine rmtImg = PersistenceFactory.IMGremote().findById(img.getId());

		if (rmtImg == null) {
			rmtImg = new Immagine(img.getIdProp());
			rmtImg.setId(img.getId());
			getReport().img_created++;
		} else {
			final Date ilmdate = img.getEdited();
			final Date irmdate = rmtImg.getEdited();

			if (ilmdate.after(irmdate)) {
				getReport().addLog(new ExportLog(ExportAction.UPD, Immagine.class, "", img.getNome(),
						"Data modifica diversa: " + df.format(irmdate) + " <> " + df.format(ilmdate)));
				getReport().img_updated++;
			} else if (ilmdate.before(irmdate)) {
				getReport().addLog(new ExportLog(ExportAction.ERR, Immagine.class, "", img.getNome(),
						"Data modifica server posteriore a locale: SRV = " + df.format(irmdate) + " > LCL = "
								+ df.format(ilmdate)));
				return;
			} else if (ilmdate.equals(irmdate)) {
				getReport().addLog(
						new ExportLog(ExportAction.IGN, Immagine.class, "", img.getNome(), "Nessuna modifica locale."));
				return;
			}
		}

		Utils.copyValues(img, rmtImg);
		rmtImg.setEditor(img.getEditor());
		rmtImg.setEdited(img.getEdited());
		rmtImg.setContenuto(img.getContenuto());
		rmtImg.setEstensione(img.getEstensione());
		rmtImg.setNome(img.getNome());
		rmtImg.setIdProp(img.getIdProp());
		rmtImg.setStatus(img.getStatus());
		rmtImg.setDidascalia(Tools.ifNull(img.getDidascalia(), ""));

		PersistenceFactory.IMGremote().saveOrUpdate(rmtImg, false);
		rmtImg = null;
		img = null;
	}

	/**
	 * @param pa
	 * @param report
	 * @param hasOAs
	 * @throws Exception
	 */
	void syncPA(ParteArchitettonica pa, boolean hasOAs) throws Exception {
		if (pa.getNote() == null)
			pa.setNote(""); // FIXME workaround remove in 1.4 [1.4.0]
		ParteArchitettonica rmtPa = PersistenceFactory.PAremote().findById(pa.getId());

		// se la PA non esiste e localmente non vi sono OA esplicitamente
		// istanzia una nuova PA e salvala
		boolean crt = false;
		boolean sync = false;
		if (rmtPa == null) {
			if (!hasOAs) {
				rmtPa = new ParteArchitettonica(pa.getArchitettura());
				rmtPa.setId(pa.getId());
				getReport().pa_created++;
				crt = true;
				sync = true;
				getReport().addLog(new ExportLog(ExportAction.CRT, ParteArchitettonica.class, pa.getNoPci(),
						pa.getDenominazione(), "Nuova PA locale (senza OA)."));
			} else {
				// do not explicit save the PA, sub entities will do that
				// implicitly
				getReport().pa_created++;
				getReport().addLog(new ExportLog(ExportAction.CRT, ParteArchitettonica.class, pa.getNoPci(),
						pa.getDenominazione(), "Nuova PA locale."));
				return;
			}
		} else { // update
			final Date parmdate = rmtPa.getEdited();
			final Date palmdate = pa.getEdited();

			if (palmdate.after(parmdate)) {
				getReport().pa_updated++;
				sync = true;
				if (!crt) // already counted above
					getReport().addLog(new ExportLog(ExportAction.UPD, ParteArchitettonica.class, pa.getNoPci(),
							pa.getDenominazione(),
							"Data modifica diversa: " + df.format(parmdate) + " <> " + df.format(palmdate)));
			} else if (palmdate.before(parmdate)) {
				getReport().addLog(new ExportLog(ExportAction.ERR, ParteArchitettonica.class, pa.getNoPci(),
						pa.getDenominazione(), "Data modifica server posteriore a locale: " + df.format(parmdate)
								+ " > " + df.format(palmdate)));
				sync = false;
			} else if (palmdate.equals(parmdate)) {
				getReport().addLog(new ExportLog(ExportAction.IGN, ParteArchitettonica.class, pa.getNoPci(),
						pa.getDenominazione(), "Nessuna modifica locale."));
				sync = false;
			}
		}
		if (sync) {
			Utils.copyValues(pa, rmtPa);
			rmtPa.setEdited(pa.getEdited());
			rmtPa.setEditor(pa.getEditor());
			rmtPa = PersistenceFactory.PAremote().saveOrUpdate(rmtPa, false);
		}

		for (Immagine img : PersistenceFactory.IMGlocal().findByIDprop(pa.getId(), null)) {
			syncIMG(img);
			img = null;
		}

		pa = null;
		rmtPa = null;
	}

	/**
	 * @param a
	 * @return
	 * @throws Exception
	 */
	Architettura syncA(Architettura a) throws Exception {
		Architettura remoteA = PersistenceFactory.Aremote().findById(a.getId());

		if (remoteA != null) {
			final Date armdate = remoteA.getEdited();
			final Date almdate = a.getEdited();
			if (!armdate.equals(almdate)) // exists,update
			{
				Utils.copyValues(a, remoteA);
				remoteA = PersistenceFactory.Aremote().saveOrUpdate(remoteA);
				getReport().a_updated++;
				getReport()
						.addLog(new ExportLog(ExportAction.UPD, Architettura.class, a.getNoPci(), a.getDenominazione(),
								"Data modifica diversa: " + df.format(armdate) + " <> " + df.format(almdate)));
			} else {
				getReport().addLog(new ExportLog(ExportAction.IGN, Architettura.class, a.getNoPci(),
						a.getDenominazione(), "Nessuna modifica locale."));
			}
		} else {
			// do not explicit save the A, sub entities will do that implicitly
			a = PersistenceFactory.Aremote().saveOrUpdate(a, false);
			getReport().addLog(new ExportLog(ExportAction.CRT, Architettura.class, a.getNoPci(), a.getDenominazione(),
					"Nuova Architettura locale."));
			getReport().a_created++;
		}

		for (Immagine img : PersistenceFactory.IMGlocal().findByIDprop(a.getId(), null)) {
			syncIMG(img);
			img = null;
		}

		return remoteA;
	}

	/**
	 * @param oa
	 * @param report
	 * @throws Exception
	 */
	void syncOA(OperaArte oa) throws Exception {
		if (oa.getNote() == null)
			oa.setNote(""); // FIXME workaround remove in 1.4 [1.4.0]

		OperaArte rmtOa = PersistenceFactory.OAremote().findById(oa.getId());

		// if (rmtOa != null && rmtOa.getEdited().after(oa.getEdited()))
		// continue;

		boolean oacrt = false;
		boolean sync = false;
		if (rmtOa == null) //
		{
			rmtOa = new OperaArte(null);
			rmtOa.setId(oa.getId());
			getReport().oa_created++;
			oacrt = true;
			getReport().addLog(new ExportLog(ExportAction.CRT, OperaArte.class, oa.getNoPci() + "",
					oa.getDenominazione(), "Nuova OA locale."));
			sync = true;
		} else {
			final Date oalmdate = oa.getEdited();
			final Date oarmdate = rmtOa.getEdited();

			if (oalmdate.after(oarmdate)) {
				// update
				getReport().oa_updated++;
				if (!oacrt) //
					getReport().addLog(
							new ExportLog(ExportAction.UPD, OperaArte.class, oa.getNoPci() + "", oa.getDenominazione(),
									"Data modifica diversa: " + df.format(oarmdate) + " <> " + df.format(oalmdate)));
				sync = true;
			} else if (oalmdate.equals(oarmdate)) {
				// are same ignore
				getReport().addLog(new ExportLog(ExportAction.IGN, OperaArte.class, oa.getNoPci() + "",
						oa.getDenominazione(), "Nessuna modifica locale."));
				sync = false;
			} else if (oalmdate.before(oarmdate)) {
				// error: on server newer as local
				getReport().addLog(new ExportLog(ExportAction.ERR, OperaArte.class, oa.getNoPci() + "",
						oa.getDenominazione(), "Data modifica server posteriore a locale: " + df.format(oarmdate)
								+ " > " + df.format(oalmdate)));
				sync = false;
			}
		}

		if (sync) {
			Utils.copyValues(oa, rmtOa);
			rmtOa.setEdited(oa.getEdited());
			rmtOa.setEditor(oa.getEditor());

			PersistenceFactory.OAremote().saveOrUpdate(rmtOa, false);
		}

		for (Immagine img : PersistenceFactory.IMGlocal().findByIDprop(oa.getId(), null)) {
			syncIMG(img);
			img = null;
		}

		oa = null;
		rmtOa = null;
	}

	void syncAtt(Allegato a) throws Exception {
		Allegato rmtAtt = PersistenceFactory.ATTremote().findById(a.getId());

		if (rmtAtt == null) {
			// crea nuovo
			rmtAtt = new Allegato(a.getIdProp());
			rmtAtt.setId(a.getId());
			getReport().att_created++;
		} else {
			// aggiorna?
			final Date ilmdate = a.getEdited();
			final Date irmdate = rmtAtt.getEdited();

			if (ilmdate.after(irmdate)) {
				getReport().addLog(new ExportLog(ExportAction.UPD, Allegato.class, "", a.getNome(),
						"Data modifica diversa: " + df.format(irmdate) + " <> " + df.format(ilmdate)));
				getReport().c_updated++;
			} else if (ilmdate.before(irmdate)) {
				getReport().addLog(new ExportLog(ExportAction.ERR, Allegato.class, "", a.getNome(),
						"Data modifica server posteriore a locale: " + df.format(irmdate) + " > "
								+ df.format(ilmdate)));
				return;
			} else if (ilmdate.equals(irmdate)) {
				getReport().addLog(
						new ExportLog(ExportAction.IGN, Allegato.class, "", a.getNome(), "Nessuna modifica locale."));
				return;
			}
		}

		Utils.copyValues(a, rmtAtt);

		rmtAtt.setEditor(a.getEditor());
		rmtAtt.setEdited(a.getEdited());
		rmtAtt.setIdProp(a.getIdProp());
		rmtAtt.setStatus(a.getStatus());
		rmtAtt.setContenuto(a.getContenuto());
		rmtAtt.setNome(a.getNome());

		PersistenceFactory.ATTremote().saveOrUpdate(rmtAtt, false);

		// dispose
		rmtAtt = null;
		a = null;
	}

	/**
	 * @param ct
	 * @param report
	 * @throws Exception
	 */
	void syncC(Contatto ct) throws Exception {
		Contatto rmtCt = PersistenceFactory.Cremote().findById(ct.getId());

		if (rmtCt == null) {
			rmtCt = new Contatto(ct.getIdProp(), TipoContatto.GENERICO);
			rmtCt.setId(ct.getId());
			getReport().c_created++;
		} else {
			final Date ilmdate = ct.getEdited();
			final Date irmdate = rmtCt.getEdited();

			if (ilmdate.after(irmdate)) {
				getReport().addLog(new ExportLog(ExportAction.UPD, Contatto.class, "", ct.getNome(),
						"Data modifica diversa: " + df.format(irmdate) + " <> " + df.format(ilmdate)));
				getReport().c_updated++;
			} else if (ilmdate.before(irmdate)) {
				getReport().addLog(new ExportLog(ExportAction.ERR, Contatto.class, "", ct.getNome(),
						"Data modifica server posteriore a locale: " + df.format(irmdate) + " > "
								+ df.format(ilmdate)));
				return;
			} else if (ilmdate.equals(irmdate)) {
				getReport().addLog(
						new ExportLog(ExportAction.IGN, Contatto.class, "", ct.getNome(), "Nessuna modifica locale."));
				return;
			}
		}

		Utils.copyValues(ct, rmtCt);
		rmtCt.setEditor(ct.getEditor());
		rmtCt.setEdited(ct.getEdited());
		rmtCt.setIdProp(ct.getIdProp());
		rmtCt.setStatus(ct.getStatus());

		PersistenceFactory.Cremote().saveOrUpdate(rmtCt, false);

		rmtCt = null;
		ct = null;
	}
}
