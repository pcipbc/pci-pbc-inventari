/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityTransaction;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Shell;

import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.Allegato;
import ch.pcilocarno.pbc.inventari.data.model.Architettura;
import ch.pcilocarno.pbc.inventari.data.model.Contatto;
import ch.pcilocarno.pbc.inventari.data.model.Immagine;
import ch.pcilocarno.pbc.inventari.data.model.LocalHistory;
import ch.pcilocarno.pbc.inventari.data.model.OperaArte;
import ch.pcilocarno.pbc.inventari.data.model.ParteArchitettonica;
import ch.pcilocarno.pbc.inventari.data.model.Rapporto;
import ch.pcilocarno.pbc.inventari.data.model.Tabella;
import ch.pcilocarno.pbc.inventari.data.model.enums.HistoryRecordType;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceTools;
import ch.pcilocarno.pbc.inventari.dialogs.ImportDialog;

public class Importer {

	public static final List<String> selectAtoImport(Shell shell) {
		// open import dialog
		ImportDialog dia = new ImportDialog(shell);
		if (dia.open() != ImportDialog.OK)
			return new ArrayList<>(0);

		return dia.selectedA;
	}

	public static final void importTabelle() {
		// importa tabelle
		for (Tabella each : PersistenceFactory.Tremote().findAll()) {
			if (each.getStatus() != RowStatus.A)
				continue; // only active
			PersistenceFactory.Tremote().detach(each);
			PersistenceFactory.Tlocal().saveOrUpdate(each, false);
		}
	}

	public static final void importA(final String Auuid) {
		EntityTransaction TR = PersistenceFactory.Alocal().TRget();
		final boolean internalTR = !TR.isActive();

		if (internalTR)
			TR.begin();

		try {
			// importa immagini, contatti e allegati
			importImmagine(Auuid);
			importContatto(Auuid);
			importAllegato(Auuid);
			importRapporti(Auuid);

			// importa PA
			for (ParteArchitettonica pa : PersistenceFactory.PAremote().findByArch(Auuid)) {
				PersistenceFactory.PAremote().detach(pa);
				PersistenceFactory.PAlocal().saveOrUpdate(pa, false);

				importImmagine(pa.getId());
				importContatto(pa.getId());
				importAllegato(pa.getId());
			}

			// importa OA
			for (OperaArte oa : PersistenceFactory.OAremote().findByArchitettura(Auuid)) {
				PersistenceFactory.OAremote().detach(oa);
				PersistenceFactory.OAlocal().saveOrUpdate(oa, false);

				importImmagine(oa.getId());
				importContatto(oa.getId());
				importAllegato(oa.getId());
			}

			// importa A
			Architettura a = PersistenceFactory.Alocal().findById(Auuid);
			if (a == null) //
			{
				a = PersistenceFactory.Aremote().findById(Auuid);
				PersistenceFactory.Aremote().detach(a);
				a = PersistenceFactory.Alocal().saveOrUpdate(a, false);
			}

			// store LocalHistory
			PersistenceFactory.LHlocal().saveOrUpdate(new LocalHistory(HistoryRecordType.IMPORT, a));
		} catch (Exception ex) {
			if (internalTR)
				TR.rollback();
			throw ex;
		}
	}


	/**
	 * @param shell
	 */
	public static final void doImport(Shell shell) {
		// local A already present, pick first
		Architettura a_ = PersistenceFactory.Alocal().getFirst();
		if (a_ != null) {
			Session.INSTANCE.setArchitetturaUUID(a_.getId());
			return;
		}

		// db connection configured?
		if (!PersistenceTools.remoteDbConfigured()) //
			if (MessageDialog.openQuestion(shell, "Connessione non configurata",
					"La connessione con il server centralizzato non è stata configurata.\n\nImpostare i parametri del collegamento ora?\n(File > Preferenze per impostare la connessione)")) //
				return;

		// ask user what to import
		final List<String> selectedA = selectAtoImport(shell);
		if (selectedA.isEmpty())
			return; // no A selected

		// local optimizations
		PersistenceTools.optimizeLocalDB();

		importTabelle();

		// Get and start transaction
		EntityTransaction TR = PersistenceFactory.Alocal().TRget();
		TR.begin();

		try {
			for (String Auuid : selectedA) {
				importA(Auuid);
			}
		} catch (Exception ex) {
			TR.rollback();
		}

		// commit
		TR.commit();
	}

	/**
	 * @param IDprop
	 * @return
	 */
	private static void importContatto(final String IDprop) {
		for (Contatto each : PersistenceFactory.Cremote().findByIDprop(IDprop, RowStatus.A)) {
			PersistenceFactory.Cremote().detach(each);
			PersistenceFactory.Clocal().saveOrUpdate(each, false);
		}
	}

	private static final void importAllegato(final String IDprop) {
		for (Allegato each : PersistenceFactory.ATTremote().findByIdprop(IDprop, RowStatus.A)) {
			PersistenceFactory.ATTremote().detach(each);
			PersistenceFactory.ATTlocal().saveOrUpdate(each, false);
		}
	}

	/**
	 * @param IDprop
	 * @return
	 */
	private static void importImmagine(final String IDprop) {
		for (Immagine each : PersistenceFactory.IMGremote().findByIDprop(IDprop, RowStatus.A)) {
			PersistenceFactory.IMGremote().detach(each);
			PersistenceFactory.IMGlocal().saveOrUpdate(each, false);
		}
	}

	/**
	 * @param auuid
	 */
	private static void importRapporti(String auuid) {
		for (Rapporto rapporto : PersistenceFactory.REPremote().findByArch(auuid, RowStatus.A)) {
			PersistenceFactory.REPremote().detach(rapporto);
			PersistenceFactory.REPlocal().saveOrUpdate(rapporto, false);
		}
	}
}
