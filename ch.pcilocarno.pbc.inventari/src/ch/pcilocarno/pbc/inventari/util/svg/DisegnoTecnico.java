/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util.svg;

public enum DisegnoTecnico {

	ABACO("Abaco", "abaco.svg", "Piletta_acquasanta.jpg"), //
	ALTARE("Altare", "altare_SENZA_gradino.svg", "Altare_FonteBattesimale.jpg"), //
	ALTARE_CON_CAPPELLA("Altare con cappella", "altare_con_cappella.svg", "Altare_FonteBattesimale.jpg"), //
	ALTARE_CON_CRIPTA("Altare con cripta", "altare_con_cripta.svg", "Altare_FonteBattesimale.jpg"), //
	ALTARE_GRADINO("Altare con gradino", "altare_gradino.svg", "Altare_FonteBattesimale.jpg"), //
	ALTARE_MAGGIORE("Altare maggiore", "altare_maggiore.svg", "Altare_FonteBattesimale.jpg"), //

	BALAUSTRA_A_ELLE("Balaustra a elle", "balaustra_a_ELLE.svg", "Balaustra.jpg"), //
	BALAUSTRA_A_ESSE("Balaustra a esse", "balaustra_a_ESSE.svg", "Balaustra.jpg"), //
	BALAUSTRA_SEMPLICE("Balaustra semplice", "balaustra_semplice.svg", "Balaustra.jpg"), //
	BALAUSTRA_SINGOLA_E_A_ELLE("Balaustra singola e a elle", "balaustra_singola_e_a_ELLE.svg", "Balaustra.jpg"), //
	BALAUSTRA_TONDEGGIANTE("Balaustra tondeggiante", "balaustra_tondeggiante.svg", "Balaustra.jpg"), //

	DIPINTO_MURALE_QUADRATO("Dipinto murale (quadrato)", "dipinto_murale_quadrato.svg", "Affresco.jpg"), //
	DIPINTO_MURALE_ORIZZONTALE("Dipinto murale (orizzontale)", "dipinto_murale_ORIZZONTALE.svg", "Affresco.jpg"), //
	DIPINTO_MURALE_VERTICALE("Dipinto murale (verticale)", "dipinto_murale_VERTICALE.svg", "Affresco.jpg"), //

	DIPINTO_MURALE_PI_QUADRATO("Dipinto murale (parete intera, quadrato)", "dipinto_murale_parete_intera_quadrato.svg",
			"Affresco_2-PARETE_INTERA.jpg"), //
	DIPINTO_MURALE_PI_ORIZZONTALE("Dipinto murale (parete intera, orizzontale)",
			"dipinto_murale_parete_intera_ORIZZONTALE.svg", "Affresco_2-PARETE_INTERA.jpg"), //
	DIPINTO_MURALE_PI_VERTICALE("Dipinto murale (parete intera, verticale)",
			"dipinto_murale_parete_intera_VERTICALE.svg", "Affresco_2-PARETE_INTERA.jpg"), //

	FONTE_BATTESIMALE("Fonte battesimale", "fonte_battesimale.svg", "Acquasantiera_1.jpg"), //
	FONTE_BATTESIMALE_2("Fonte battesimale (modello 2)", "fonte_battesimale_MODELLO_2.svg", "Acquasantiera_2.jpg"), //
	LAPIDE("Lapide", "lapide.svg", "Affresco.jpg"), //
	LASTRA("Lastra", "lastra.svg", "Altare_FonteBattesimale.jpg"), //
	LAVABO_SAGRESTIA("Lavabo sagrestia", "lavabo_sagrestia.svg", "Piletta_acquasanta.jpg"), //
	NICCHIA_QUADRATA("Nicchia (quadrata)", "nicchia_quadrata.svg", "Nicchia.jpg"), //
	NICCHIA_ORIZZONTALE("Nicchia (orizzontale)", "nicchia_ORIZZONTALE.svg", "Nicchia.jpg"), //
	NICCHIA_VERTICALE("Nicchia (verticale)", "nicchia_VERTICALE.svg", "Nicchia.jpg"), //
	PILETTA("Piletta", "piletta.svg", "Piletta_acquasanta.jpg"), //
	PILETTA_A_MURO("Piletta a muro", "piletta_a_muro.svg", "Piletta_acquasanta.jpg"), //
	STEMMI_A_MURO("Stemmi a muro (come al collegio Papio)", "stemmi_a_muro.svg", null), //
	TABERNACOLO("Tabernacolo murale", "tabernacolo.svg", null), //
	VETRATA_QUADRATA("Vetrata (quadrata)", "vetrata_quadrata.svg", "Vetrata.jpg"), //
	VETRATA_ORIZZONTALE("Vetrata (orizzontale)", "vetrata_ORIZZONTALE.svg", "Vetrata.jpg"), //
	VETRATA_VERTICALE("Vetrata (verticale)", "vetrata_VERTICALE.svg", "Vetrata.jpg"), //

	;

	private final String descrizione;

	private final String immagineDT;

	private final String immagineMP;

	/**
	 * @param descrizione_
	 * @param svgfilename_
	 * @param immagineMP_
	 */
	private DisegnoTecnico(String descrizione_, String svgfilename_, String immagineMP_) {
		this.descrizione = descrizione_;
		this.immagineDT = svgfilename_;
		this.immagineMP = immagineMP_;
	}

	/**
	 * @return
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * @return
	 */
	public String getImmagineDT() {
		return immagineDT;
	}

	/**
	 * @return
	 */
	public String getImmagineMP() {
		return immagineMP;
	}

	public static final String PATH_DT = "/immagini/disegnotecnico/";

	public static final String PATH_MP = "/immagini/modelloprotezione/";
}
