/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class Profile
{
	private static final Map<String, Profile> instances = new HashMap<String, Profile>();

	final static SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss.SSS");

	final String name;

	final long start;

	long lastStep = 0L;

	long step = 0L;

	long end = 0L;

	/**
	 * @param name
	 */
	private Profile(String name)
	{
		this.name = name;
		this.start = System.currentTimeMillis();
		step = start;
		lastStep = start;
	}

	/**
	 * @param name
	 */
	public static void start(final String name)
	{
		if (instances.containsKey(name)) throw new RuntimeException("Profiler already registered.");
		Profile profile = new Profile(name);
		profile.print("Profile started.");
		instances.put(name, profile);
	}

	/**
	 * @param name
	 * @param message
	 */
	public static final void step(final String name, String message)
	{
		if (!instances.containsKey(name)) return; // throw new RuntimeException("Profile named " + name + " not found.");
		instances.get(name).step(message);
	}

	/**
	 * @param message
	 */
	private void step(String message)
	{
		lastStep = step;
		step = System.currentTimeMillis();
		print(message);
	}

	/**
	 * @param name
	 * @return Time this profile took
	 */
	public static long end(final String name)
	{
		if (!instances.containsKey(name)) //
			throw new RuntimeException("Profile named " + name + " not found.");

		Profile p = instances.get(name);
		p.end = System.currentTimeMillis();
		instances.remove(name);
		p.print("Profile ended.");
		return p.end - p.start;
	}

	/**
	 * @param message
	 */
	private void print(String message)
	{
		System.out.println(df.format(new java.util.Date()) + " [" + name.toUpperCase() + "] " + message + " (+" + (step - lastStep) + " ms / " + (step - start) + " ms)");
	}
}
