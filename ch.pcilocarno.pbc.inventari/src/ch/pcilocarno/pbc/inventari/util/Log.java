/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util;

import java.util.Formatter;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

import ch.pcilocarno.pbc.inventari.Activator;

public class Log
{

	static Boolean debug = null;

	/**
	 * @return
	 */
	private static final boolean isDebugActive()
	{
		if (debug == null)
		{
			String sp = System.getProperty("ediesis.debug", "0");
			debug = "1".equals(sp) || "true".equalsIgnoreCase(sp);
			Log.info("Loaded system property ediesis.debug=%s; Thus debug = %s", sp, debug);
		}
		return debug.booleanValue();
	}

	/**
	 * @return
	 */
	private final static ILog getLogger()
	{
		return Activator.getDefault().getLog();
	}

	/**
	 * @param status
	 */
	public final static void log(IStatus status)
	{
		getLogger().log(status);
	}

	/**
	 * @param message
	 *            The massage, placeholder for arg is %s
	 * @param args
	 *            Message arguments
	 * @return
	 */
	private static final String formatMsg(final String message, Object... args)
	{
		Formatter f = new Formatter();
		final String fm = f.format(message, args).toString();
		f.close();
		return fm;
	}

	/**
	 * @param message
	 *            The massage, placeholder for arg is %s
	 * @param args
	 *            Message arguments
	 */
	public static final void debug(final String message, Object... args)
	{
		if (!isDebugActive()) return;
		info(message, args);
	}

	/**
	 * @param message
	 *            The massage, placeholder for arg is %s
	 * @param args
	 *            Message arguments
	 */
	public final static void info(final String message, Object... args)
	{
		log(new Status(IStatus.INFO, Activator.PLUGIN_ID, formatMsg(message, args), null));
	}

	/**
	 * @param ex
	 *            The exception thrown
	 * @param message
	 *            The massage, placeholder for arg is %s
	 * @param args
	 *            Message arguments
	 */
	public final static void error(Exception ex, final String message, Object... args)
	{
		log(new Status(IStatus.ERROR, Activator.PLUGIN_ID, formatMsg(message, args), ex));
	}

	/**
	 * @param message
	 *            The massage, placeholder for arg is %s
	 * @param args
	 *            Message arguments
	 */
	public static final void warning(final String message, Object... args)
	{
		log(new Status(IStatus.WARNING, Activator.PLUGIN_ID, formatMsg(message, args), null));
	}

}
