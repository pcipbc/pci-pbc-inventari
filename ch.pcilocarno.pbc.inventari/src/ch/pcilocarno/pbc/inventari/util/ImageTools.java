/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.eclipse.swt.SWT;

import ch.pcilocarno.pbc.inventari.preferences.PreferencesHelper;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.resizers.configurations.AlphaInterpolation;
import net.coobird.thumbnailator.resizers.configurations.Rendering;
import net.coobird.thumbnailator.resizers.configurations.ScalingMode;

public class ImageTools {

	/**
	 * @param file
	 * @return
	 * @throws IOException
	 */
	public static final byte[] resizeImage(File file) throws IOException {
		Log.debug("Resizing %s", file.getAbsolutePath());

		final int maxImageSize = PreferencesHelper.getImageSize();

		ByteArrayOutputStream os = new ByteArrayOutputStream();

		boolean resize = PreferencesHelper.resizeImages();

		// decide if should resize image, if already smalle don't resize to
		// avoid stretching
		if (resize)
			try {
				FileInputStream fis = new FileInputStream(file);
				BufferedImage bufi = ImageIO.read(fis);
				if (bufi.getHeight() <= maxImageSize && bufi.getWidth() <= maxImageSize)
					resize = false;
				bufi = null;
				fis.close();
				fis = null;
				Log.debug("Based on image width/height resizing? %s", resize);
			} catch (Exception ex) {
				Log.error(null, "Deciding if resize based on image size: %s", ex.toString());
			}

		// Resize
		if (resize) {
			Thumbnails.of(file)//
					.size(maxImageSize, maxImageSize) //
					.useExifOrientation(PreferencesHelper.useExifRotation()) //
					.rendering(Rendering.QUALITY) //
					.alphaInterpolation(AlphaInterpolation.QUALITY) //
					.outputQuality(PreferencesHelper.getResizeQuality()) //
					.keepAspectRatio(true) //
					.outputFormat(FileUtils.getExtension(file.getName())) //
					.scalingMode(ScalingMode.BICUBIC) //
					.toOutputStream(os);

		} else {
			FileInputStream fis = new FileInputStream(file);
			BufferedImage img = ImageIO.read(fis);

			ImageIO.write(img, FileUtils.getExtension(file.getName()), os);
			os.flush();
			fis.close();
			fis = null;
			img = null;
		}

		final byte[] imgContent = os.toByteArray();

		// close outputStream
		try {
			os.close();
			os = null;
		} catch (IOException ioex) {
			/* - ignore - */
		}

		return imgContent;
	}

	/**
	 * @param bi
	 * @return
	 */
	public static BufferedImage rotate90DX(BufferedImage bi) {
		return rotate(bi, SWT.RIGHT);
	}

	/**
	 * @param bi
	 * @return
	 */
	public static BufferedImage rotate90SX(BufferedImage img) {
		return rotate(img, SWT.LEFT);
	}

	public static final BufferedImage rotate(BufferedImage img, final int direction) {
		int w = img.getWidth();
		int h = img.getHeight();

		BufferedImage rot = new BufferedImage(h, w, BufferedImage.TYPE_INT_RGB);

		double theta;
		switch (direction) {
		case SWT.RIGHT:
			theta = Math.PI / 2;
			break;
		case SWT.LEFT:
			theta = -Math.PI / 2;
			break;
		default:
			throw new AssertionError();
		}

		AffineTransform xform = new AffineTransform();
		xform.translate(0.5 * h, 0.5 * w);
		xform.rotate(theta);
		xform.translate(-0.5 * w, -0.5 * h);
		Graphics2D g = rot.createGraphics();
		g.drawImage(img, xform, null);
		g.dispose();

		return rot;
	}

	/**
	 * @param bi
	 * @return
	 */
	public static BufferedImage flipH(BufferedImage bi) {
		int width = bi.getWidth();
		int height = bi.getHeight();

		BufferedImage biFlip = new BufferedImage(width, height, bi.getType());

		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				biFlip.setRGB((width - 1) - i, j, bi.getRGB(i, j));

		return biFlip;
	}

	/**
	 * @param bi
	 * @return
	 */
	public static BufferedImage flipW(BufferedImage bi) {
		int width = bi.getWidth();
		int height = bi.getHeight();

		BufferedImage biFlip = new BufferedImage(width, height, bi.getType());

		for (int i = 0; i < width; i++)
			for (int j = 0; j < height; j++)
				biFlip.setRGB(i, (height - 1) - j, bi.getRGB(i, j));

		return biFlip;
	}
}
