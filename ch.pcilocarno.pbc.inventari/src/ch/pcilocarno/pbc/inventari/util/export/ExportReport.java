/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.util.export;

import java.util.ArrayList;
import java.util.List;

public class ExportReport {

	List<ExportLog> log = new ArrayList<>();

	String message;

	public int before_pa_count = 0;
	public int after_pa_count = 0;

	public int before_oa_count = 0;
	public int after_oa_count = 0;

	public int before_img_count = 0;
	public int after_img_count = 0;

	public int before_att_count = 0;
	public int after_att_count = 0;
	
	public int before_rep_count = 0;
	public int after_rep_count = 0;

	public int a_updated = 0;
	public int a_created = 0;

	public int oa_created = 0;
	public int oa_updated = 0;

	public int pa_created = 0;
	public int pa_updated = 0;

	public int img_created = 0;
	public int img_updated = 0;

	public int c_created = 0;
	public int c_updated = 0;

	public int att_created = 0;
	public int att_updated = 0;

	public int rep_created = 0;
	public int rep_updated = 0;
	
	public long time_costed = 0;

	public ExportReport(String message) {
		log.clear();
		this.message = message;
	}

	public void addLog(ExportLog log_) {
		log.add(log_);
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public List<ExportLog> getLog() {
		return log;
	}

	public void dispose() {
		log.clear();
		log = null;
		message = null;
	}
}
