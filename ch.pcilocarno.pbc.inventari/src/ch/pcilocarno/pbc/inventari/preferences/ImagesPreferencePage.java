/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.preferences;

import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.ComboFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import ch.pcilocarno.pbc.inventari.Activator;

public class ImagesPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage
{
	public ImagesPreferencePage()
	{
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Opzioni per l'aggiunta di immagini");
	}

	public void createFieldEditors()
	{
		addField(new BooleanFieldEditor(PreferenceConstants.P_IMG_AUTORESIZE_DO, "Ridimensiona automaticamente le immagini", getFieldEditorParent()));
		addField(new ComboFieldEditor(PreferenceConstants.P_IMG_AUTORESIZE_SIZE, "Dimensione massima (pixels)", new String[][] { { "XS (800)", "800" }, { "S (1024)", "1024" }, { "M (1280)", "1280" }, { "L (1600)", "1600" }, { "XL (2048)", "2048" } }, getFieldEditorParent()));
	}

	@Override
	public void init(IWorkbench workbench)
	{

	}
}
