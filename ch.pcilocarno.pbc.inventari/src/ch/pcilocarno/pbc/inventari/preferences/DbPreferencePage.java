/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.preferences;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import ch.pcilocarno.pbc.inventari.Activator;

/**
 * This class represents a preference page that is contributed to the Preferences dialog. By subclassing <samp>FieldEditorPreferencePage</samp>, we can use the field support built into JFace that allows us to create a page that is small and knows how to save, restore and apply itself.
 * <p>
 * This page is used to modify preferences only. They are stored in the preference store that belongs to the main plug-in class. That way, preferences can be accessed directly via the preference store.
 */

public class DbPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage
{
	StringFieldEditor url, dbname, server;

	ComboEditor dbtype;

	ModifyListener modli = new ModifyListener()
	{
		@Override
		public void modifyText(ModifyEvent e)
		{
			updateURLfield();
		}
	};

	public DbPreferencePage()
	{
		super(GRID);
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
		setDescription("Dati di connessione per la sincronizzazione con la banca dati centralizzata.");
	}

	/**
	 * Creates the field editors. Field editors are abstractions of the common GUI blocks needed to manipulate various types of preferences. Each field editor knows how to save and restore itself.
	 */
	public void createFieldEditors()
	{
		// addField(new DirectoryFieldEditor(PreferenceConstants.P_PATH, "&Directory preference:", getFieldEditorParent()));
		// addField(new BooleanFieldEditor(PreferenceConstants.P_BOOLEAN, "&An example of a boolean preference", getFieldEditorParent()));
		// addField(new RadioGroupFieldEditor(PreferenceConstants.P_CHOICE, "An example of a multiple-choice preference", 1, new String[][] { { "&Choice 1", "choice1" }, { "C&hoice 2", "choice2" } }, getFieldEditorParent()));
		// addField(new StringFieldEditor(PreferenceConstants.P_STRING, "A &text preference:", getFieldEditorParent()));
		dbtype = new ComboEditor(PreferenceConstants.P_JDBC_DRIVER, "Tipo di &banca dati:", new String[][] { { "Nessuna", "NONE" }, { "MySQL", "com.mysql.jdbc.Driver" } }, getFieldEditorParent());
		dbtype.getCombo().addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				updateURLfield();
			}
		});
		server = new StringFieldEditor(PreferenceConstants.P_JDBC_SERVER, "&Server:", getFieldEditorParent());
		server.getTextControl(getFieldEditorParent()).addModifyListener(modli);

		dbname = new StringFieldEditor(PreferenceConstants.P_JDBC_DB, "Nome &banca dati:", getFieldEditorParent());
		dbname.getTextControl(getFieldEditorParent()).addModifyListener(modli);

		url = new StringFieldEditor(PreferenceConstants.P_JDBC_URL, "&URL di connessione:", getFieldEditorParent());
		url.getTextControl(getFieldEditorParent()).setEnabled(false);

		addField(dbtype);
		addField(server);
		addField(dbname);
		addField(url);
		addField(new StringFieldEditor(PreferenceConstants.P_JDBC_USER, "Nome u&tente:", getFieldEditorParent()));
		addField(new PasswordFieldEditor(PreferenceConstants.P_JDBC_PASSWORD, "Pass&word:", getFieldEditorParent()));
	}

	void updateURLfield()
	{
		String url = getBaseUrl();
		final String srv_ = server.getTextControl(getFieldEditorParent()).getText();
		final String dbn_ = dbname.getTextControl(getFieldEditorParent()).getText();
		final String dbt_ = "mysql";

		url = url.replaceAll("\\$dbtype", dbt_);
		url = url.replaceAll("\\$server", srv_);
		url = url.replaceAll("\\$database", dbn_);

		this.url.getTextControl(getFieldEditorParent()).setText(url);
	}

	private String getBaseUrl()
	{
		if ("MySQL".equals(dbtype.getCombo().getText())) return "jdbc:$dbtype://$server/$database";

		return "UNKNOW";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.IWorkbenchPreferencePage#init(org.eclipse.ui.IWorkbench)
	 */
	public void init(IWorkbench workbench)
	{
	}

}