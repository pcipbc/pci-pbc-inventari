/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.preferences;

import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

/**
 * @author elvis
 * 
 */
public class PasswordFieldEditor extends StringFieldEditor
{

	/**
	 * @param name
	 * @param labelText
	 * @param parent
	 */
	public PasswordFieldEditor(String name, String labelText, Composite parent)
	{
		super(name, labelText, parent);
	}

	/**
	 * @param name
	 * @param labelText
	 * @param width
	 * @param parent
	 */
	public PasswordFieldEditor(String name, String labelText, int width, Composite parent)
	{
		super(name, labelText, width, parent);
	}

	/**
	 * @param name
	 * @param labelText
	 * @param width
	 * @param strategy
	 * @param parent
	 */
	public PasswordFieldEditor(String name, String labelText, int width, int strategy, Composite parent)
	{
		super(name, labelText, width, strategy, parent);
	}

	@Override
	public Text getTextControl(Composite parent)
	{
		Text t = super.getTextControl(parent);
		t.setEchoChar('*');
		return t;
	}
}
