/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchSite;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceTools;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.util.Tools;

public class ASwitchHandler extends AbstractHandler implements IHandler {
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.commands.AbstractHandler#execute(org.eclipse.core.commands.
	 * ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		final IWorkbenchSite site = HandlerUtil.getActiveSite(event);
		IWorkbenchPage page = site.getPage();

		if (page != null)
			page.closeAllEditors(true);

		IEditorPart[] dirtyEditors = page != null ? page.getDirtyEditors() : new IEditorPart[0];
		if (dirtyEditors != null && dirtyEditors.length != 0) {
			MessageDialog.openError(HandlerUtil.getActiveShell(event), "Errore",
					"Salvare tutto prima di proseguire.\nOperazione annullata.");
			return null;
		}

		// connessione configurata?
		if (!PersistenceTools.remoteDbConfigured()) {
			MessageDialog.openError(HandlerUtil.getActiveShell(event), "Errore",
					"La connessione con la banca dati centralizzata non è stata configurata.\nFile > Preferenze per configurarla.\n\nOperazione annullata.");
			return null;
		}

		// controlla connessione con server
		if (!PersistenceTools.remoteDbAccessible()) {
			MessageDialog.openError(HandlerUtil.getActiveShell(event), "Errore",
					"Non è possibile contattare la banca dati centralizzata.\nOperazione annullata.");
			return null;
		}

		final String captcha = Tools.getRandomString(4);
		InputDialog id = new InputDialog(HandlerUtil.getActiveShell(event), "Conferma",
				"Il file locale viene cancellato, perdendo le modifiche non sincronizzate.\n\nLa connessione di  rete con il server è richiesta!\nSi desidera proseguire?\n\nImmettere il seguente codice nel campo di verifica: "
						+ captcha.toUpperCase(),
				"", new IInputValidator() {
					@Override
					public String isValid(String newText) {
						return newText.equalsIgnoreCase(captcha) ? null : "Codice di verifica non valido.";
					}
				});

		if (id.open() != InputDialog.OK)
			return null;

		try {
			PersistenceTools.removeLocalDB();
			PlatformUI.getWorkbench().close();
		} catch (Exception e) {
			Log.error(e, "Exporting to local file and closing app.");
		}
		return null;
	}
}
