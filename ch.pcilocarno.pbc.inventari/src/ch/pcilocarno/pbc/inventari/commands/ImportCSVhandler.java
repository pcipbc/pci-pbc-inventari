/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.commands;

import java.io.File;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IViewReference;
import org.eclipse.ui.PlatformUI;

import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.CSVimport;
import ch.pcilocarno.pbc.inventari.views.IViewListPO;
import ch.pcilocarno.pbc.inventari.views.OperaArteView;
import ch.pcilocarno.pbc.inventari.views.ParteArchitettonicaView;

public class ImportCSVhandler extends AbstractHandler implements IHandler
{
	public static final String ID = "ch.pcilocarno.pbc.inventari.commands.ImportCSV";

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		if (PersistenceFactory.OAlocal().findExtended(Session.INSTANCE.getArchitetturaUUID(), null, "", "", "", "", null, null, false, false, false, false, false).size() > 0) //
			if (!MessageDialog.openConfirm(getShell(), "Continuare?", "L'architettura corrente contiene già delle OA.\nProseguendo con l'import potrebbe risultare con OA doppie.\n\nContinuare comunque?")) //
				return null;

		FileDialog dialog = new FileDialog(getShell(), SWT.OPEN);
		dialog.setFilterExtensions(new String[] { "*.csv;*.CSV", "*.txt;*.TXT", "*" });
		// dialog.setFilterPath("c:\\temp");
		String result = dialog.open();

		// cancel pressed
		if (result == null) return null;

		// file selected import it
		try
		{
			CSVimport ci = new CSVimport(new File(result));
			ci.run();

			MessageDialog.openInformation(getShell(), "Info", "Il file è stato importato correttamente.");
		}
		catch (Exception ex)
		{
			MessageDialog.openError(getShell(), "Errore", ex.getMessage());
		}

		refreshViews();
		return result;
	}

	/**
	 * 
	 */
	private void refreshViews()
	{
		IViewReference[] views = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getViewReferences();
		for (IViewReference view : views)
		{
			if (OperaArteView.ID.equalsIgnoreCase(view.getId()) || ParteArchitettonicaView.ID.equalsIgnoreCase(view.getId())) //
			{
				((IViewListPO) view.getPart(false)).refresh();
			}
		}
	}

	private Shell getShell()
	{
		return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	}
}
