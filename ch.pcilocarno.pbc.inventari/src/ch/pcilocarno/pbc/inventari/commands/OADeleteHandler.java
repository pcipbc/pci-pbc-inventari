/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import ch.pcilocarno.pbc.inventari.data.model.OperaArte;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.editors.input.OAEditorInput;
import ch.pcilocarno.pbc.inventari.views.OperaArteView;

/**
 * ch.pcilocarno.pbc.inventari.editors.handler.OpenEditorHandler
 * 
 * @author elvis on Sep 9, 2009
 * @version @@version@@ build @@build@@
 */
public class OADeleteHandler extends AbstractHandler implements IHandler
{
	public static final String ID = "ch.pcilocarno.pbc.inventari.commands.OADeleteHandler";

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException
	{
		// Get the view
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
		IWorkbenchPage page = window.getActivePage();
		OperaArteView view = (OperaArteView) page.findView(OperaArteView.ID);
		// Get the selection
		ISelection selection = view.getSite().getSelectionProvider().getSelection();
		if (selection != null && selection instanceof IStructuredSelection)
		{
			Object[] objs = ((IStructuredSelection) selection).toArray();

			if (!MessageDialog.openConfirm(window.getShell(), "Conferma", "Si è sicuri di voler cancellare le OA (Opera d'Arte) selezionate? (" + objs.length + ")")) return null;

			// Editors
			IEditorReference[] editorsRefs = page.getEditorReferences();
			for (Object obj : objs)
			{
				// If we had a selection lets delete it
				if (obj != null && obj instanceof OperaArte)
				{
					try
					{
						for (IEditorReference each : editorsRefs)
							if (each.getEditorInput() instanceof OAEditorInput && ((OAEditorInput) each.getEditorInput()).getPO().getId().equals(((OperaArte) obj).getId())) //
								page.closeEditors(new IEditorReference[] { each }, false);
					}
					catch (Exception e)
					{
						/* ignore */
					}
					PersistenceFactory.OAlocal().delete((OperaArte) obj);

				}
			}

			try
			{
				view.refresh();
			}
			catch (Exception e)
			{
			}
		}

		return null;

	}
}
