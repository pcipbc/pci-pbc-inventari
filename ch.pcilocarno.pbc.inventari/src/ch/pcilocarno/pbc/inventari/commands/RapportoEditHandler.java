/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;

import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.Architettura;
import ch.pcilocarno.pbc.inventari.data.model.OperaArte;
import ch.pcilocarno.pbc.inventari.data.model.Rapporto;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.dialogs.RapportoEditDialog;
import ch.pcilocarno.pbc.inventari.editors.OAEditor;
import ch.pcilocarno.pbc.inventari.editors.input.OAEditorInput;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.views.AllegatiView;
import ch.pcilocarno.pbc.inventari.views.RapportiView;

/**
 * @author elvis
 *
 */
public class RapportoEditHandler extends AbstractHandler implements IHandler {
	public static final String ID = "ch.pcilocarno.pbc.inventari.command.ReportEdit";
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.
	 * ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		// Get the view
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindow(event);
		IWorkbenchPage page = window.getActivePage();
		RapportiView view = (RapportiView) page.findView(RapportiView.ID);

		Architettura A = PersistenceFactory.Alocal().findById(Session.INSTANCE.getArchitetturaUUID());
		if (A == null) {
			MessageDialog.openError(HandlerUtil.getActiveShell(event), "Errore", "Nessuna architettura attiva.");
			return null;
		}

		// Get the selection
		ISelection selection = page.getSelection();// view.getSite().getSelectionProvider().getSelection();
		Rapporto rapporto = null;
		
		if (selection != null && selection instanceof IStructuredSelection) {
			for (Object obj : ((IStructuredSelection) selection).toArray()) {
				if (obj != null && obj instanceof Rapporto) {
					rapporto = (Rapporto) obj;
				}
			}
		}
		if( rapporto == null ) {
			return null;
		}
		
		RapportoEditDialog dialog = new RapportoEditDialog(window.getShell(), rapporto);
		if (dialog.open() != Dialog.OK) {
			return null;
		}

		rapporto.setContenuto(dialog.getSelectedContenuto());
		rapporto.setNome(dialog.getSelectedNome());

		rapporto = PersistenceFactory.REPlocal().saveOrUpdate(rapporto);

		// ricarica la view
		try {
			view.refresh();
		} catch (Exception ex) {
		}

		return rapporto;
	}

}
