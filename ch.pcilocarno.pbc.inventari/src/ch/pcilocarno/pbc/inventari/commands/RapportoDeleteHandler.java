/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.commands;

import java.util.Date;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.handlers.HandlerUtil;

import ch.pcilocarno.pbc.inventari.data.model.Rapporto;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.DateUtils;
import ch.pcilocarno.pbc.inventari.util.Tools;
import ch.pcilocarno.pbc.inventari.views.RapportiView;

/**
 * @author elvis
 *
 */
public class RapportoDeleteHandler extends AbstractHandler implements IHandler {

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.
	 * ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {

		// Get the view
		IWorkbenchPage page = HandlerUtil.getActiveWorkbenchWindow(event).getActivePage();
		RapportiView view = (RapportiView) page.findView(RapportiView.ID);

		// Get the selection
		ISelection selection = view.getSite().getSelectionProvider().getSelection();
		if (selection != null && selection instanceof IStructuredSelection) {
			boolean doDelete = false;

			Object selected = ((IStructuredSelection) selection).getFirstElement();
			if (selected != null && selected instanceof Rapporto) {
				Rapporto rapporto = (Rapporto) selected;

				if (DateUtils.daysBetween(rapporto.getCreated(), new Date()) > 7) {
					// ask confirmation!
					final String captcha = Tools.getRandomString(4);
					InputDialog id = new InputDialog(HandlerUtil.getActiveShell(event), "Conferma cancellazione",
							"Immettere il seguente codice per confermare la cancellazione: " + captcha.toUpperCase(),
							"", new IInputValidator() {
								@Override
								public String isValid(String newText) {
									return newText.equalsIgnoreCase(captcha) ? null : "Codice di verifica non valido.";
								}
							});

					doDelete = id.open() == InputDialog.OK;

				} else {
					if (!MessageDialog.openConfirm(page.getWorkbenchWindow().getShell(), "Conferma",
							"Si è sicuri di voler cancellare il rapporto selezionato?"))
						return null;

					doDelete = true;
				}

				if (doDelete) {

					PersistenceFactory.REPlocal().delete(rapporto);

					try {
						view.refresh();
					} catch (Exception e) {
					}
				}
			}

		}

		return null;

	}

}
