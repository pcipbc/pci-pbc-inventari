/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.commands;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;

import ch.pcilocarno.pbc.inventari.Activator;
import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.Architettura;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceTools;
import ch.pcilocarno.pbc.inventari.util.Exporter;
import ch.pcilocarno.pbc.inventari.util.Tools;
import ch.pcilocarno.pbc.inventari.util.UIhelper;
import ch.pcilocarno.pbc.inventari.util.export.ExportLog;
import ch.pcilocarno.pbc.inventari.util.export.ExportReport;

/**
 * @author elvis
 * 
 */
public class ExportHandler extends AbstractHandler implements IHandler {
	static String nl = System.getProperty("line.separator");

	public Shell buildWaitShell() {
		// init shell
		final Shell dialogShell = new Shell(Display.getDefault().getActiveShell(), SWT.APPLICATION_MODAL);

		// get current shell bound
		final Rectangle bounds = Display.getDefault().getActiveShell().getBounds();

		// init waiting shell
		final Rectangle shell_bounds = new Rectangle(0, 0, 460, 220);

		// calc waiting shell bounds
		shell_bounds.x = bounds.x + (bounds.width / 2) - (shell_bounds.width / 2);
		shell_bounds.y = bounds.y + (bounds.height / 2) - (shell_bounds.height / 2);

		// populate waiting shell
		dialogShell.setLayout(new FillLayout(SWT.VERTICAL));
		dialogShell.setBounds(shell_bounds);
		Label iconLabel = new Label(dialogShell, SWT.CENTER);
		iconLabel.setImage(UIhelper.getImage(Icons.HOURGLASS_48));
		Label loadingLabel = new Label(dialogShell, SWT.CENTER);
		loadingLabel.setText("Sincronizzazione in corso, attendere...");

		return dialogShell;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.commands.IHandler#execute(org.eclipse.core.commands.
	 * ExecutionEvent)
	 */
	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchPage page = HandlerUtil.getActiveSite(event).getPage();
		if (page == null)
			return null;

		// controlla che tutti gli editors siano salvati
		if (page.getDirtyEditors().length != 0) //
			page.saveAllEditors(true);
		if (page.getDirtyEditors().length != 0) { // errore salvataggio
			MessageDialog.openError(HandlerUtil.getActiveShell(event), "Errore",
					"Salvare le modifiche prima di proseguire.\nOperazione annullata.");
			return null;
		}

		// connessione configurata?
		if (!PersistenceTools.remoteDbConfigured()) {
			MessageDialog.openError(HandlerUtil.getActiveShell(event), "Errore",
					"La connessione con la banca dati centralizzata non è stata configurata.\nFile > Preferenze per configurarla.\n\nOperazione annullata.");
			return null;
		}

		// controlla connessione con server
		if (!PersistenceTools.remoteDbAccessible()) {
			MessageDialog.openError(HandlerUtil.getActiveShell(event), "Errore",
					"Non è possibile contattare la banca dati centralizzata.\nOperazione annullata.");
			return null;
		}

		Architettura a_ = PersistenceFactory.Alocal().findById(Session.INSTANCE.getArchitetturaUUID());
		// chiedi conferma
		if (!MessageDialog.openConfirm(HandlerUtil.getActiveShell(event), "Conferma",
				"Si è sicuri di voler sincronizzare i dati dell'architettura \n\n" + //
						"\t" + a_.getNoPci() + " " + a_.getDenominazione() + " (" + a_.getComune() + ")" //
						+ "\n\ncon il server centralizzato?")) //
			return null;

		// init the waiting modal
		final Shell waitModal = buildWaitShell();
		waitModal.open();

		Job exportJob = new Job("Esportazione dati.") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Esportazione dati", IProgressMonitor.UNKNOWN);
				try {
					Exporter exporter = new Exporter();
					exporter.sync();
					monitor.done();
					final ExportReport result = exporter.getReport();
					exporter.dispose();
					exporter = null;

					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							waitModal.close();
							waitModal.dispose();
									

							if (result.getMessage().startsWith("!"))
								MessageDialog.openError(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
										"Errore", result.getMessage().substring(1));
							else {
								StringBuffer sb = new StringBuffer();

								sb.append("Architettura" + nl);
								sb.append(
										"   Create " + result.a_created + "; Aggiornate " + result.a_updated + nl);
								sb.append("Parti architettoniche" + nl);
								sb.append("   Create " + result.pa_created + "; Aggiornate " + result.pa_updated
										+ " (" + result.before_pa_count + " -> " + result.after_pa_count + ")"
										+ nl);
								sb.append("Opera d'arte" + nl);
								sb.append("   Create " + result.oa_created + "; Aggiornate " + result.oa_updated
										+ " (" + result.before_oa_count + " -> " + result.after_oa_count + ")"
										+ nl);
								sb.append("Immagine" + nl);
								sb.append("   Create " + result.img_created + "; Aggiornate " + result.img_updated
										+ " (" + result.before_img_count + " -> " + result.after_img_count + ")"
										+ nl);
								sb.append("Contatto" + nl);
								sb.append(
										"   Creati " + result.c_created + "; Aggiornati " + result.c_updated + nl);
								sb.append("Allegati" + nl);
								sb.append("   Creati " + result.att_created + "; Aggiornati " + result.att_updated
										+ nl);
								sb.append("Rapporti" + nl);
								sb.append("   Creati " + result.rep_created + "; Aggiornati " + result.rep_updated
										+ " (" + result.before_rep_count + " -> " + result.after_rep_count + ")"
										+ nl);
								
								sb.append(nl);
								sb.append("Took: " + Tools.getMillisHR(result.time_costed));

								MessageDialog.openInformation(
										PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Sync report",
										sb.toString());
								System.out.println("\n---- Sync done in " + Tools.getMillisHR(result.time_costed));
								for (ExportLog log : result.getLog()) {
									System.out.println(
											log.getAction() + "\t" + Tools.fixLen(log.getPOclass().getSimpleName(), 20)
													+ Tools.fixLen(" ("
															+ (log.getPOnbr() + " " + log.getPOdesc()).trim() + ")", 40)
													+ ": " + log.getMessage());
								}
							}
						}
					});
					
				} catch (Exception e) {
					return new Status(IStatus.ERROR, Activator.PLUGIN_ID, "Errore esportazione dati: " + e.toString(),
							e);
					
				} finally {
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							if( waitModal!=null && !waitModal.isDisposed()) {
								waitModal.close();
								waitModal.dispose();
							}
						}
					});
					
				}
				return Status.OK_STATUS;
			}
		};

		exportJob.setPriority(Job.INTERACTIVE);
		exportJob.setUser(true);
		exportJob.schedule();

		return null;
	}
}
