/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.commands;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityTransaction;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;

import ch.pcilocarno.pbc.inventari.beans.DatePeriod;
import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.Immagine;
import ch.pcilocarno.pbc.inventari.data.model.OperaArte;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.dialogs.CollisionsDialog;
import ch.pcilocarno.pbc.inventari.dialogs.ImportPicturesDialog;
import ch.pcilocarno.pbc.inventari.util.Collision;
import ch.pcilocarno.pbc.inventari.util.Counter;
import ch.pcilocarno.pbc.inventari.util.FileUtils;
import ch.pcilocarno.pbc.inventari.util.ImageTools;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.util.Tools;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

public class OAImportPicturesHandler extends AbstractHandler {
	public static final String ID = "ch.pcilocarno.pbc.inventari.commands.OAImportPictures";

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		ImportPicturesDialog ipdia = new ImportPicturesDialog(getShell());
		if (ipdia.open() != Dialog.OK) // cancel pressed
			return null;

		// selezione
		final File parentDir = ipdia.getSelectedFolder();
		final boolean forcePictureImport = ipdia.getSelectedForcePictureImport();
		final boolean onlyWithoutPictures = ipdia.getSelectedOnlyWithoutPictures();
		final DatePeriod period = ipdia.getSelectedPeriod();

		// carica OA
		final List<OperaArte> oas = PersistenceFactory.OAlocal().findForPictureImport(Session.INSTANCE.getArchitetturaUUID(), period, onlyWithoutPictures);

		// ricerca collisioni
		final List<Collision> collisions = findCollissions(oas);

		if (!collisions.isEmpty()) {
			// se ci sono collisioni e l'import è forzato non si può continuare
			if (forcePictureImport) {
				UIhelper.openInfoDialog(getShell(), "Impossibile continuare", "Non è possibile continuare ad importare le foto poiché ci sono delle collisioni.");
				return null;
			}

			// mostra le collisioni
			CollisionsDialog cdia = new CollisionsDialog(getShell(), collisions);
			if (cdia.open() != Dialog.OK)
				return null; // cancel pressed

		}

		// collect pictures
		final List<File> pictures = new ArrayList<>();
		collectPictures(parentDir, pictures);
		final List<OperaArte> ignored = new LinkedList<>();
		Counter counter = new Counter();
		Job job = new Job("Importa foto") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Importa foto", oas.size());

				// Get and start transaction
				EntityTransaction TR = PersistenceFactory.Alocal().TRget();
				TR.begin();

				OAL: for (OperaArte oa : oas) {
					// should exit?
					if (monitor.isCanceled()) {
						TR.rollback();
						return Status.CANCEL_STATUS;
					}

					// senza foto definita
					if (Tools.isEmpty(oa.getFoto())) {
						ignored.add(oa);
						continue OAL;
					}

					// ignora le OA con collisioni
					for (Collision collision : collisions) {
						if (oa.equals(collision.getOa1()) || oa.equals(collision.getOa2())) {
							ignored.add(oa);
							continue OAL;
						}
					}

					monitor.setTaskName(oa.getNoPci() + " " + oa.getDenominazione() + " (Foto " + oa.getFoto() + ")");

					String[] fotos = parseFotos(oa.getFoto());
					final int maxFotos = fotos.length;

					Set<File> matchers = new TreeSet<>(new Comparator<File>() {
						@Override
						public int compare(File o1, File o2) {
							return o1.getName().compareTo(o2.getName());
						}
					});

					// cerca foto che si chiama esattamente come il contenuto del campo
					for (File f : pictures) {
						for (String foto : fotos) {
							if (FileUtils.getFileName(f.getName()).equalsIgnoreCase(foto)) {
								if (matchers.size() < maxFotos)
									matchers.add(f);
							}
						}
					}

					// fuzzy search
					if (matchers.isEmpty()) { // solo se non ho trovato l'immagine esatta
						for (File f : pictures) {
							for (String foto : fotos)
								if (f.getName().matches("(.*)" + Tools.ftrim(foto) + "(.*)")) {
									if (matchers.size() < maxFotos)
										matchers.add(f);
								}
						}
					}
					Log.debug("(%s) %s: %s --> trovate %s foto.", oa.getNoPci(), oa.getDenominazione(), oa.getFoto(), matchers.size());
					if (matchers.isEmpty()) // nessuna foto trovata
						continue;

					List<Immagine> images = PersistenceFactory.IMGlocal().findByIDprop(oa.getId(), RowStatus.A);
					int idx = 0;

					for (File match : matchers) {
						String fileName = match.getName().substring(0, match.getName().lastIndexOf("."));

						// controlla se esiste già
						boolean exists = false;

						for (Immagine image : images) {
							if (fileName.equalsIgnoreCase(image.getNome())) //
							{
								exists = true;
								if (forcePictureImport) {
									PersistenceFactory.IMGlocal().delete(image);
									exists = false;
								}
							}
						}

						if (!exists) // non esiste, importa
						{
							monitor.subTask("Ridimensionando e importando " + match.getName());
							try {
								byte[] imageContent = ImageTools.resizeImage(match);

								Immagine nu = new Immagine(oa.getId());
								nu.setContenuto(imageContent);
								nu.setEstensione(FileUtils.getExtension(match.getName()));
								nu.setNome(fileName);
								nu.setPrincipale(idx == 0);
								PersistenceFactory.IMGlocal().saveOrUpdate(nu);
								counter.inc();
								Log.debug("Importato foto %s per (%s) %s", fileName, oa.getNoPci(), oa.getDenominazione());
							} catch (Exception ex) {
								Log.error(ex, "Errore importando foto %s", match.getName());
							}
						}
						idx++;
					}
					monitor.worked(1);
				}
				// commit
				TR.commit();
				monitor.done();

				// give user feedback
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						MessageDialog.openInformation(getShell(), getName(),
								"Operazione eseguita con successo.\nImportate " + counter.getCount() + " fotografie.");
					}
				});

				return Status.OK_STATUS;
			}
		};
		job.setUser(true);
		job.setPriority(Job.INTERACTIVE);
		job.schedule();

		return null;
	}

	/**
	 * 
	 * @param onlyWithoutPictures
	 * @param period
	 * @return
	 */
	private List<Collision> findCollissions(List<OperaArte> oas) {
		List<Collision> collisions = new ArrayList<>();

		Map<String, OperaArte> fotos = new LinkedHashMap<>();
		for (OperaArte oa : oas) {
			final String[] f = parseFotos(oa.getFoto());
			for (String each : f) {
				if (!fotos.containsKey(each)) {
					fotos.put(each, oa);
				} else {
					if (fotos.get(each).getId().equals(oa.getId()))
						continue;
					else
						collisions.add(new Collision(oa, fotos.get(each)));
				}
			}
		}
		return collisions;
	}

	private final String[] parseFotos(String foto) {
		List<String> f = new ArrayList<String>();
		for (String each : foto.split("[,\\s:;]")) {
			if (Tools.isEmpty(each))
				continue;
			f.add(Tools.ftrim(each));
		}
		return f.toArray(new String[] {});
	}

	/**
	 * Get the shell
	 * 
	 * @return
	 */
	private Shell getShell() {
		return PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
	}

	private void collectPictures(File parent, List<File> pictures) {
		File[] content = parent.listFiles();
		for (File f : content) {
			if (f.isDirectory()) {
				collectPictures(f, pictures);
				continue;
			}

			final String ext = FileUtils.getExtension(f.getName());
			if (ext.equalsIgnoreCase("JPG") || ext.equalsIgnoreCase("JPEG")) //
				pictures.add(f);
		}
	}
}
