/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.beans;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;

import java.io.File;
import java.io.IOException;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.concurrent.TimeUnit;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.swt.widgets.Display;

import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.ImageTools;
import ch.pcilocarno.pbc.inventari.util.Log;

/**
 * @author elvis
 *
 */
public class ImageEditPoller extends Job {

	final ImageEditInfo imageInfo;
	final Imageviewer imageViewer;
	WatchService service;
	boolean canceling = false;

	/**
	 * 
	 */
	public ImageEditPoller(ImageEditInfo imageInfo_, Imageviewer imageViewer_) {
		super("Attendo ev. modifica esterna dell'immagine.");
		this.imageInfo = imageInfo_;
		this.imageViewer = imageViewer_;
	}
	
	public final String id(){
		return getImageInfo().getImmagine().getId();
	}

	/**
	 * @return the image
	 */
	public ImageEditInfo getImageInfo() {
		return imageInfo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.jobs.Job#canceling()
	 */
	@Override
	protected void canceling() {
		canceling = true;
		stopService();
	}

	private final void stopService() {
		if (service != null)
			try {
				service.close();
			} catch (Exception ex) {
			}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.runtime.jobs.Job#run(org.eclipse.core.runtime.
	 * IProgressMonitor)
	 */
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		final Path path = Paths.get(getImageInfo().getFile().getParentFile().toURI());
		final String filename = getImageInfo().getFile().getName();

		// Sanity check - Check if path is a folder
		try {
			Boolean isFolder = (Boolean) Files.getAttribute(path, "basic:isDirectory", NOFOLLOW_LINKS);
			if (!isFolder) {
				throw new IllegalArgumentException("Path: " + path + " is not a folder");
			}
		} catch (IOException ioe) {
			// Folder does not exists
			ioe.printStackTrace();
		}

		Log.debug("Watching path: %s", path);

		// We obtain the file system of the Path
		FileSystem fs = path.getFileSystem();
		// We create the new WatchService using the new try() block
		try {
			service = fs.newWatchService();
			boolean refreshViewer = false;
			// We register the path to the service
			// We watch for creation events
			path.register(service, ENTRY_MODIFY);

			// Start the infinite polling loop
			while (!canceling) {
				WatchKey key;
				try {
					key = service.poll(25, TimeUnit.MILLISECONDS);
				} catch (InterruptedException e) {
					break;
				} catch (ClosedWatchServiceException cwse) {
					break;
				}
				
				if (key == null) {
					Thread.yield();
					continue;
				}

				for (WatchEvent<?> event : key.pollEvents()) {
					WatchEvent.Kind<?> kind = event.kind();
					// Get the type of the event
					kind = event.kind();
					if (OVERFLOW == kind) {
						continue; // loop
					} else if (ENTRY_MODIFY == kind) {
						// Path was modified
						Path newPath = ((WatchEvent<Path>) event).context();
						if (newPath.endsWith(filename)) {
							saveImmagine(getImageInfo().getFile());
							refreshViewer = true;
						}
					}
				}

				if (refreshViewer)
					refreshViewer();

				if (!key.reset()) {
					break; // loop
				}
			}

		} catch (IOException ioe) {
			Log.error(ioe, "Watching directory for changes.");
		} finally {
			stopService();
		}

		return Status.OK_STATUS;
	}

	private final void saveImmagine(File file) throws IOException {
		try {
			byte[] content = ImageTools.resizeImage(file);
			getImageInfo().getImmagine().setContenuto(content);
			PersistenceFactory.IMGlocal().saveOrUpdate(getImageInfo().getImmagine());
		} catch (Exception ex) {
			/* IGNORE */
		}
	}

	private final void refreshViewer() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				if (imageViewer != null && !imageViewer.isDisposed())
					imageViewer.refresh();
			}
		});
	}
}
