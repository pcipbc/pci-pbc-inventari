/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.beans;

import org.eclipse.swt.widgets.Button;

public class RadioBehavior
{
	final Button[] radios;

	public RadioBehavior(Button[] radios)
	{
		this.radios = radios;
	}

	public void dispose()
	{
	}

	public void deselectAll()
	{
		for (Button each : radios)
		{
			each.setSelection(false);
		}
	}

	public void select(Button b)
	{
		Button fb = null;
		for (Button each : radios)
		{
			each.setSelection(false);
			if (each == b) fb = each;
		}
		if (fb != null) fb.setSelection(true);
	}

	public String getSelected()
	{
		for (Button each : radios)
			if (each.getSelection()) return each.getData("keyv") + "";

		return null;
	}

	public void select(String keyvData)
	{
		deselectAll();
		for (Button each : radios)
		{
			if (each.getData("keyv").toString().equals(keyvData))
			{
				each.setSelection(true);
				return;
			}
		}
	}
}
