/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.beans;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.ListenerList;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.PlatformUI;

import ch.pcilocarno.pbc.inventari.Activator;
import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.data.model.Immagine;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.preferences.PreferencesHelper;
import ch.pcilocarno.pbc.inventari.util.FileUtils;
import ch.pcilocarno.pbc.inventari.util.ImageTools;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

public abstract class Imageviewer extends Composite {

	final List<ImageEditPoller> pollers = new LinkedList<>();

	final public static int BUTTONBAR_NONE = 0;

	final public static int BUTTONBAR_TOP = 2;

	final public static int BUTTONBAR_BOTTOM = 4;

	final public static int CAPTION_FIELD = 8;

	final public static int DUMMY_IMAGE_NONE = 16;

	final public static int DUMMY_IMAGE_SILHOUETTE = 32;

	static final String[] ACCEPTED_EXT = { "*.jpg;*.jpeg;*.png;*.JPG;*.JPEG;*.PNG", "*.jpg", "*.jpeg", "*.png", "*.JPG",
			"*.JPEG", "*.PNG" };

	final int MAX_FILE_SIZE;

	final int STYLE;

	int maxPictures = -1;

	Label add, delete, download, btnspacer, previous, next;

	Text didascalia;

	MenuItem miAdd, miDelete, miOpen, miNext, miPrevious, miRefresh, miRotate90sx, miRotate90dx, miMakePrincipal;

	final List<Immagine> beans = new ArrayList<Immagine>(5);

	Immagine selectedBean;

	Composite buttonsBar;

	private Image currentImage;

	boolean readonly = false;

	Canvas canvas;

	Canvas c;

	GC gc;

	private final ListenerList<Listener> listeners = new ListenerList<Listener>();

	public void addSelectionChangeListener(SelectionChangeListener listener) {
		listeners.add(listener);
	}

	public void addImageAddedListener(ImageAddedListener listener) {
		listeners.add(listener);
	}

	public void addImageRemovedListener(ImageRemovedListener listener) {
		listeners.add(listener);
	}

	public void removeImageAddedListener(ImageAddedListener listener) {
		listeners.remove(listener);
	}

	public void removeSelectionChangeListener(SelectionChangeListener listener) {
		listeners.remove(listener);
	}

	public void removeImageRemovedListener(ImageRemovedListener listener) {
		listeners.remove(listener);
	}

	public void notifySelectionChangeListeners() {
		for (Object each : listeners.getListeners())
			if (each instanceof SelectionChangeListener)
				((SelectionChangeListener) each).selectionChanged();
	}

	public void notifyImageAddedListeners(final String imageName) {
		for (Object each : listeners.getListeners()) {
			if (each instanceof ImageAddedListener) {
				final ImageAddedEvent event = new ImageAddedEvent(imageName);
				((ImageAddedListener) each).handleEvent(event);
			}
		}
	}

	public void notifyImageRemovedListeners(final String imagename) {
		for (Object each : listeners.getListeners()) {
			if (each instanceof ImageRemovedListener) {
				final ImageRemovedEvent event = new ImageRemovedEvent(imagename);
				((ImageRemovedListener) each).handleEvent(event);
			}
		}
	}

	public void addDataChangeListener(DataChangeListener listener) {
		listeners.add(listener);
	}

	public void removeDataChangeListener(DataChangeListener listener) {
		listeners.remove(listener);
	}

	protected void notifyDataChangeListeners() {
		for (Object each : listeners.getListeners())
			if (each instanceof DataChangeListener)
				((DataChangeListener) each).dataChanged();
	}

	/**
	 * Constructor
	 * 
	 * @param parent
	 * @param style
	 */
	public Imageviewer(Composite parent, int style) {
		super(parent, SWT.NONE);
		STYLE = style;
		MAX_FILE_SIZE = 2097152;
		initUI();
	}

	protected String getDummyImagePath() {
		if ((STYLE & DUMMY_IMAGE_NONE) == DUMMY_IMAGE_NONE) //
			return null;

		return null;
	}

	protected Image getDummyImage() {
		if (getDummyImagePath() != null) //
			return adaptImage(Activator.getImageDescriptor(getDummyImagePath()).createImage());

		return null;
	}

	public void refresh() {
		if (isDisposed())
			return;
		setDidascalia("");
		final Immagine sel_ = getSelectedBean();
		reloadBeans();
		if (beans.indexOf(sel_) != -1)
			setSelectedBean(beans.get(beans.indexOf(sel_)));
		else
			selectFirst();

		if (!miAdd.isDisposed()) //
			miAdd.setEnabled(!(getMaxPictures() > 0 && beans.size() >= getMaxPictures()));
	}

	/**
	 * Set the image viewer as readonly (can't add nor delete)
	 */
	public void setReadOnly() {
		readonly = true;
		if (add != null)
			add.setEnabled(false);
		if (delete != null)
			delete.setEnabled(false);

		miAdd.setEnabled(!readonly || getSelectedBean() == null);
		miDelete.setEnabled(!readonly || getSelectedBean() == null);
		miRotate90dx.setEnabled(!readonly || getSelectedBean() == null);
		miRotate90sx.setEnabled(!readonly || getSelectedBean() == null);
		miMakePrincipal.setEnabled(!readonly || getSelectedBean() == null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Widget#dispose()
	 */
	@Override
	public void dispose() {
		selectedBean = null;
		if (beans != null)
			beans.clear();
		if (currentImage != null && !currentImage.isDisposed()) {
			currentImage.dispose();
			currentImage = null;
		}
		UIhelper.dispose(c);
		UIhelper.dispose(canvas);
		if (gc != null && !gc.isDisposed()) {

			gc.dispose();
			gc = null;
		}
		super.dispose();
	}

	/**
	 * @return
	 */
	public Immagine getSelectedBean() {
		return selectedBean;
	}

	/**
	 * @param e
	 * @param select
	 */
	public void addBean(Immagine e, boolean select) {
		beans.add(e);

		if (select) //
			setSelectedBean(e);

	}

	/**
	 * @param e
	 */
	private void setSelectedBean(Immagine e) {
		this.selectedBean = e;

		Display.getCurrent().asyncExec(new Runnable() {

			@Override
			public void run() {
				if (getCanvas().isDisposed())
					return;
				getCanvas().redraw();
			}
		});

		if (delete != null && !delete.isDisposed())
			delete.setEnabled(!readonly && getSelectedBean() != null);
		if (download != null && !download.isDisposed())
			download.setEnabled(getSelectedBean() != null);

		if (getSelectedBean() == null) {
			if (!miDelete.isDisposed()) //
				miDelete.setEnabled(false);
			if (!miOpen.isDisposed()) //
				miOpen.setEnabled(false);
			if (!miRotate90dx.isDisposed()) //
				miRotate90dx.setEnabled(false);
			if (!miRotate90sx.isDisposed()) //
				miRotate90sx.setEnabled(false);
			if (!miMakePrincipal.isDisposed()) //
				miMakePrincipal.setEnabled(false);
			if (!miMakePrincipal.isDisposed()) //
				miMakePrincipal.setSelection(false);
		} else {
			if (!miDelete.isDisposed()) //
				miDelete.setEnabled(!readonly && getSelectedBean() != null);
			if (!miOpen.isDisposed()) //
				miOpen.setEnabled(getSelectedBean() != null);
			if (!miRotate90dx.isDisposed()) //
				miRotate90dx.setEnabled(!readonly && getSelectedBean() != null);
			if (!miRotate90sx.isDisposed()) //
				miRotate90sx.setEnabled(!readonly && getSelectedBean() != null);
			if (!miMakePrincipal.isDisposed()) //
				miMakePrincipal.setEnabled(!readonly && getSelectedBean() != null);
			if (!miMakePrincipal.isDisposed()) //
				miMakePrincipal.setSelection(getSelectedBean().getPrincipale());

			setDidascalia(getSelectedBean().getDidascalia());
		}
		if (!miNext.isDisposed()) //
			miNext.setEnabled(hasNext());
		if (!miPrevious.isDisposed()) //
			miPrevious.setEnabled(hasPrevious());
		if (previous != null) //
			previous.setEnabled(hasPrevious());
		if (next != null) //
			next.setEnabled(hasNext());

		notifySelectionChangeListeners();
	}

	/**
	 * @param text
	 */
	private void setDidascalia(String text) {
		if (didascalia != null && !didascalia.isDisposed()) //
			didascalia.setText(text == null ? "" : text);
	}

	/**
	 * @return
	 */
	public boolean hasNext() {
		if (beans.size() > 1) //
			if (beans.indexOf(getSelectedBean()) < (beans.size() - 1)) //
				return true;
		return false;
	}

	/**
	 * @return
	 */
	public boolean hasPrevious() {
		if (beans.size() > 1) //
			if (beans.indexOf(getSelectedBean()) > 0) //
				return true;
		return false;
	}

	/**
	 * @param image
	 * @param carea
	 * @return <code>null</code> if no resize is needed
	 */
	protected Rectangle calcDimensions(Rectangle image, Rectangle carea) {
		final int original_width = image.width;
		final int original_height = image.height;
		final int bound_width = carea.width;
		final int bound_height = carea.height;
		int new_width = original_width;
		int new_height = original_height;

		// do not enlarge small images (avoid pixelation)
		if (original_width <= bound_width && original_height <= bound_height)
			return null; // no resize needed

		// first check if we need to scale width
		if (original_width > bound_width) {
			// scale width to fit
			new_width = bound_width;
			// scale height to maintain aspect ratio
			new_height = (new_width * original_height) / original_width;
		}

		// then check if we need to scale even with the new height
		if (new_height > bound_height) {
			// scale height to fit instead
			new_height = bound_height;
			// scale width to maintain aspect ratio
			new_width = (new_height * original_width) / original_height;
		}
		// Tools.log("\nImg size = h: %s\tw: %s\nArea size = h: %s\tw: %s\nNew
		// size = h: %s\tw: %s\n", original_height, original_width,
		// bound_height, bound_width, new_height, new_width);

		return new Rectangle(0, 0, new_width, new_height);
	}

	/**
	 * Adapt image's dimensions for the canvas area
	 * 
	 * @param e
	 *            The source image
	 * @return The adapted Image
	 */
	private Image adaptImage(Image e) {
		if (e == null || e.isDisposed())
			return null;

		Rectangle nuImgSize = calcDimensions(e.getBounds(), canvas.getClientArea());
		if (nuImgSize == null)
			return e;

		// scale img
		ImageData imgData = e.getImageData().scaledTo(nuImgSize.width, nuImgSize.height);
		ImageDescriptor imgDescr = ImageDescriptor.createFromImageData(imgData);
		Image scaledE = imgDescr.createImage();
		e.dispose();

		return scaledE;
	}

	/**
	 * 
	 */
	void createButtonBar() {
		buttonsBar = new Composite(this, SWT.NONE);
		buttonsBar.setLayout(
				GridLayoutFactory.fillDefaults().numColumns(8).margins(0, 0).spacing(3, 3).equalWidth(false).create());
		buttonsBar.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		add = new Label(buttonsBar, SWT.LEFT);
		add.setText("");
		add.setToolTipText("Aggiungi");
		add.setImage(UIhelper.getImage(Icons.PLUS_16));
		add.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				clickedAdd();
				super.mouseDown(e);
			}
		});

		delete = new Label(buttonsBar, UIhelper.STYLE_LABEL);
		delete.setText("");
		delete.setToolTipText("Cancella");
		delete.setImage(UIhelper.getImage(Icons.CROSS_16));
		delete.setEnabled(false);
		delete.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				clickedDelete();
				super.mouseDown(e);
			}
		});

		download = new Label(buttonsBar, UIhelper.STYLE_LABEL);
		download.setText("");
		download.setToolTipText("Apri");
		download.setImage(UIhelper.getImage(Icons.EYE_16));
		download.setEnabled(false);
		download.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				clickedDownload();
				super.mouseDown(e);
			}
		});

		btnspacer = new Label(buttonsBar, UIhelper.STYLE_LABEL);
		btnspacer.setText("");
		btnspacer.setBackground(getParent().getBackground());
		btnspacer.setImage(UIhelper.getImage(Icons.EMPTY_16));

		previous = new Label(buttonsBar, UIhelper.STYLE_LABEL);
		previous.setText("");
		previous.setToolTipText("Precedente");
		previous.setImage(UIhelper.getImage(Icons.LEFT_16));
		previous.setEnabled(false);
		previous.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				clickedPrevious();
				super.mouseDown(e);
			}
		});
		next = new Label(buttonsBar, UIhelper.STYLE_LABEL);
		next.setText("");
		next.setToolTipText("Prossimo");
		next.setImage(UIhelper.getImage(Icons.RIGHT_16));
		next.setEnabled(false);
		next.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDown(MouseEvent e) {
				clickedNext();
				super.mouseDown(e);
			}
		});
	}

	/**
	 * 
	 */
	void reloadBeans() {
		beans.clear();
		beans.addAll(loadBeans());
		notifyDataChangeListeners();
	}

	/**
	 * @return the canvas
	 */
	public Canvas getCanvas() {
		return canvas;
	}

	/**
	 * 
	 */
	void createCanvas() {
		canvas = new Canvas(this, SWT.NONE);
		canvas.setLayoutData(new GridData(GridData.FILL_BOTH));
		canvas.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event e) {
				if (getSelectedBean() != null) //
					canvas.redraw();
			}
		});

		canvas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				if (getSelectedBean() != null) //
					clickedDownload();
				else
					//
					clickedAdd();
			}
		});

		canvas.addListener(SWT.Paint, new Listener() {
			@Override
			public void handleEvent(final Event e) {
				// dispose current image, if any
				if (currentImage != null && !currentImage.isDisposed()) {
					currentImage.dispose();
					currentImage = null;
				}

				// nothing selected and nothing to display
				if (getSelectedBean() == null && getDummyImage() == null) //
					return;

				currentImage = getSelectedBean() == null ? getDummyImage() : adaptImage(getSelectedBeanImage());
				if (currentImage == null)
					return;//

				c = (Canvas) e.widget;
				GC gc = e.gc;

				if (gc == null || gc.isDisposed())
					return;
				if (c == null || c.isDisposed())
					return;

				gc.setClipping(c.getClientArea());
				gc.fillRectangle(c.getClientArea());

				gc.drawImage(currentImage, 0, 0);

				Rectangle rect = currentImage.getBounds();
				Rectangle client = c.getClientArea();

				int marginWidth = client.width - rect.width;
				if (marginWidth > 0) {
					gc.fillRectangle(rect.width, 0, marginWidth, client.height);
				}

				int marginHeight = client.height - rect.height;
				if (marginHeight > 0) {
					gc.fillRectangle(0, rect.height, client.width, marginHeight);
				}

			}
		});

		canvas.setMenu(initCanvasPopup());
	}

	/**
	 * @return
	 */
	private Menu initCanvasPopup() {

		Menu popup = new Menu(getCanvas().getShell(), SWT.POP_UP);

		miAdd = new MenuItem(popup, SWT.PUSH);
		miAdd.setText("Aggiungi");
		miAdd.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clickedAdd();
			}
		});

		miMakePrincipal = new MenuItem(popup, SWT.CHECK);
		miMakePrincipal.setText("Principale");
		miMakePrincipal.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clickedMakePricipal();
			}
		});

		miDelete = new MenuItem(popup, SWT.PUSH);
		miDelete.setText("Elimina");
		miDelete.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clickedDelete();
			}
		});

		miOpen = new MenuItem(popup, SWT.PUSH);
		miOpen.setText("Apri");
		miOpen.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clickedDownload();
			}
		});

		new MenuItem(popup, SWT.SEPARATOR);

		miRotate90sx = new MenuItem(popup, SWT.PUSH);
		miRotate90sx.setText("Ruota verso sinistra");
		miRotate90sx.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clickedRotateSx();
			}
		});

		miRotate90dx = new MenuItem(popup, SWT.PUSH);
		miRotate90dx.setText("Ruota verso destra");
		miRotate90dx.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clickedRotateDx();
			}
		});
		new MenuItem(popup, SWT.SEPARATOR);

		miNext = new MenuItem(popup, SWT.PUSH);
		miNext.setText("Prossimo");
		miNext.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clickedNext();
			}
		});
		miPrevious = new MenuItem(popup, SWT.PUSH);
		miPrevious.setText("Precedente");
		miPrevious.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clickedPrevious();
			}
		});
		miRefresh = new MenuItem(popup, SWT.PUSH);
		miRefresh.setText("Ricarica");
		miRefresh.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				clickedRefresh();
			}
		});
		return popup;
	}

	public void clickedMakePricipal() {
		if (getSelectedBean() == null)
			return;
		try {
			String currentID = getSelectedBean().getId();
			List<Immagine> images = loadBeans();

			if (!getSelectedBean().getPrincipale()) {
				for (Immagine i : images) {
					i.setPrincipale(currentID.equals(i.getId()));
					PersistenceFactory.IMGlocal().saveOrUpdate(i);
				}
			} else {
				boolean set = false;
				for (Immagine i : images) {
					if (i.getId().equals(currentID))
						i.setPrincipale(false);
					else {
						if (!set) {
							i.setPrincipale(true);
							set = true;
						} else
							continue;
					}

					PersistenceFactory.IMGlocal().saveOrUpdate(i);
				}
			}
		} catch (Exception ex) {
			Log.error(ex, "Setting an image as principal.");
		}

		refresh();
	}

	/**
	 * 
	 */
	public void clickedRefresh() {
		refresh();
	}

	public void clickedRotateSx() {
		rotate(SWT.LEFT);
	}

	public void clickedRotateDx() {
		rotate(SWT.RIGHT);
	}

	/**
	 * @param direction
	 *            one of <code>SWT.LEFT</code> or <code>SWT.RIGHT</code>
	 */
	protected void rotate(final int direction) {
		if (getSelectedBean() == null)
			return;

		new Job("Ruota immagine") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				byte[] imgcontent = getBeanImage(getSelectedBean());
				try {
					InputStream in = new ByteArrayInputStream(imgcontent);
					BufferedImage img = ImageIO.read(in);
					in.close();

					if (direction == SWT.LEFT)
						img = ImageTools.rotate90SX(img);
					else if (direction == SWT.RIGHT)
						img = ImageTools.rotate90DX(img);
					else
						throw new Exception("Unknow direction");

					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ImageIO.write(img, getSelectedBean().getEstensione().toLowerCase(), baos);
					baos.flush();

					byte[] rotated_imgcontent = baos.toByteArray();
					baos.close();

					getSelectedBean().setContenuto(rotated_imgcontent);
					final Immagine updated = PersistenceFactory.IMGlocal().saveOrUpdate(getSelectedBean());

					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							refresh();
							setSelectedBean(updated);
						}
					});
					rotated_imgcontent = null;
				} catch (Exception exc) {
					return new Status(IStatus.ERROR, Activator.PLUGIN_ID, "Ruotando un'immagine", exc);
				}
				imgcontent = null;

				return Status.OK_STATUS;
			}
		}.schedule();
	}

	/**
	 * 
	 */
	public void clickedPrevious() {
		int bi = beans.indexOf(getSelectedBean());
		if (bi == 0)
			return; // is the first
		setSelectedBean(beans.get(bi - 1));
	}

	/**
	 * 
	 */
	public void clickedNext() {
		int bi = beans.indexOf(getSelectedBean());
		if (bi == beans.size() - 1)
			return; // is the last
		setSelectedBean(beans.get(bi + 1));
	}

	/**
	 * initialize UI
	 */
	private void initUI() {
		setLayout(new GridLayout(1, false));
		if ((STYLE & BUTTONBAR_TOP) == BUTTONBAR_TOP) //
			createButtonBar();

		createCanvas();

		if ((STYLE & BUTTONBAR_BOTTOM) == BUTTONBAR_BOTTOM) //
			createButtonBar();

		if ((STYLE & CAPTION_FIELD) == CAPTION_FIELD) //
			createCaptionField();
	}

	public final Text getCaptionField() {
		return didascalia;
	}

	/**
	 * 
	 */
	protected void createCaptionField() {
		didascalia = new Text(this, SWT.MULTI | SWT.WRAP | SWT.V_SCROLL | SWT.BORDER);
		GridData bfgd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		bfgd.heightHint = 35;
		didascalia.setLayoutData(bfgd);
		didascalia.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				doSaveCaption();
			}
		});
	}

	public void doSaveCaption() {
		if (getSelectedBean() == null)
			return;

		getSelectedBean().setDidascalia(didascalia.getText());

		final Immagine updated = PersistenceFactory.IMGlocal().saveOrUpdate(getSelectedBean());

		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				refresh();
				setSelectedBean(updated);
			}
		});
	}

	/**
	 * download button clicked
	 */
	public void clickedDownload() {
		if (getSelectedBean() == null)
			return;

		try {
			byte[] selContent = getBeanImage(getSelectedBean());
			File tempFile = FileUtils.writeTmpFile(selContent, getBeanId(getSelectedBean()),
					getBeanType(getSelectedBean()), true);
			startAndRegisterPoller(new ImageEditPoller(new ImageEditInfo(tempFile, getSelectedBean()), this));
			selContent = null;
		} catch (Exception e) {
			Log.error(e, "Opening image.");
		}
	}

	private final boolean doRegisterImageEditPoller() {
		return true;
	}

	private final void startAndRegisterPoller(ImageEditPoller poller) {
		if (!doRegisterImageEditPoller())
			return;
		if (poller == null)
			return;

		boolean pollerRegistered = false;
		for (ImageEditPoller p_ : pollers) {
			if (poller.id().equals(p_.id())) {
				pollerRegistered = true;
				break;
			}
		}

		if (!pollerRegistered) {
			Log.debug("New poller registered (id: %s)", poller.id());
			pollers.add(poller);
			poller.schedule();
		}
	}

	public final void stopAndDeregisterPollers() {
		Log.debug("Stopping and deregistering pollers.");
		for (ImageEditPoller poller : pollers) {
			Log.debug("Deregistering and canceling poller %s.", poller.id());
			poller.cancel();
		}
	}

	/**
	 * @param shell
	 * @return
	 * @throws ClientException
	 */
	File[] selectImageFile(Shell shell) {
		FileDialog dia = new FileDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
				SWT.OPEN | SWT.MULTI);
		dia.setFilterExtensions(new String[] { "*.jpg;*.jpeg;*.png;*.JPG;*.JPEG;*PNG", "*.*" });
		dia.setFilterNames(new String[] { "Immagini", "Tutti i files" });
		dia.setFileName(
				FileUtils.getLastSelectedFile() == null ? null : FileUtils.getLastSelectedFile().getAbsolutePath());
		final String selected = dia.open();
		if (selected == null)
			return null;

		File selectedF = new File(selected);
		String[] selectedPaths = dia.getFileNames();
		File[] files = new File[selectedPaths.length];
		for (int i = 0; i < selectedPaths.length; i++) {
			final String filename = selectedPaths[i];
			File imgf = new File(selectedF.getParentFile(), filename);
			FileUtils.setSelectedFile(imgf);

			// controllo dimensione
			if (!PreferencesHelper.resizeImages() && imgf.length() > MAX_FILE_SIZE) //
				throw new RuntimeException(
						"File troppo grande! La dimensione massima è " + MAX_FILE_SIZE / 1024 / 1024 + " MB");

			// controlla estensione
			String ext = FileUtils.getExtension(imgf.getName());
			boolean found = false;
			for (String each : ACCEPTED_EXT) {
				found = ext.equalsIgnoreCase(FileUtils.getExtension(each));
				if (found)
					break;
			}
			if (!found)
				continue;

			files[i] = imgf;
		}

		return files;
	}

	public int getImageCount() {
		return beans.size();
	}

	public int getImageIndex() {
		if (selectedBean == null)
			return -1;
		return beans.indexOf(selectedBean) + 1;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.swt.widgets.Control#setBackground(org.eclipse.swt.graphics
	 * .Color)
	 */
	@Override
	public void setBackground(Color color) {
		super.setBackground(color);
		canvas.setBackground(color);
		if (buttonsBar != null)
			buttonsBar.setBackground(color);
		if (add != null)
			add.setBackground(color);
		if (delete != null)
			delete.setBackground(color);
		if (download != null)
			download.setBackground(color);
	}

	/**
	 * @param maxPictures
	 *            the maxPictures to set
	 */
	public void setMaxPictures(int maxPictures) {
		this.maxPictures = maxPictures;
	}

	/**
	 * @return the maxPictures
	 */
	public int getMaxPictures() {
		return maxPictures;
	}

	/**
	 * button add clicked event
	 */
	public void clickedAdd() {
		if (!canAdd())
			return;

		if (getMaxPictures() > 0 && beans.size() >= getMaxPictures()) //
		{
			UIhelper.openInfoDialog(getShell(), "Impossibile continuare",
					"Non è possibile aggiungere più di " + getMaxPictures() + " immagini.");
			return;
		}

		// select file
		final File[] selected_imgs = selectImageFile(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell());
		if (selected_imgs == null || selected_imgs.length == 0)
			return;

		new Job("Ridimensiona e salva foto") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				try {
					int i = 0;
					for (final File selected_img : selected_imgs) {

						// imageName
						final String imageName = selected_img.getName();
						final byte[] imgByte = ImageTools.resizeImage(selected_img);

						// image to OutpuStream

						final Immagine nu = saveSelectedImage(imageName, imgByte);
						final boolean islast = i == (selected_imgs.length - 1);

						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								notifyImageAddedListeners(imageName);
								if (islast) {
									refresh();
									setSelectedBean(nu);
								}
							}
						});

						i++;
					}
				} catch (Exception ex) {
					return new Status(IStatus.ERROR, Activator.PLUGIN_ID, "Ridimensionando e salvando un'immagine.",
							ex);
				}
				return Status.OK_STATUS;
			}
		}.schedule();

	}

	protected boolean canAdd() {
		return true;
	}

	/**
	 * Get selected bean's image
	 * 
	 * @return
	 */
	public Image getSelectedBeanImage() {
		return getImage(getBeanImage(getSelectedBean()));
	}

	/**
	 * Extract image from bean
	 * 
	 * @param content
	 * @return
	 */
	public Image getImage(byte[] content) {
		return new Image(Display.getCurrent(), new ByteArrayInputStream(content));
	}

	/**
	 * Select first image in the combo
	 */
	public void selectFirst() {
		if (beans.size() == 0) {
			setSelectedBean(null);
			return;
		}
		setSelectedBean(beans.get(0));
	}

	/**
	 * Select last image in the combo
	 */
	public void selectLast() {
		if (beans.size() == 0)
			return;
		setSelectedBean(beans.get(beans.size() - 1));
	}

	/**
	 * button delete clicked event
	 */
	public void clickedDelete() {
		final String filename = getSelectedBean().getNome() + "." + getSelectedBean().getEstensione();
		if (!UIhelper.openConfirmDialog(getShell(), "Conferma", "Desideri eliminare l'immagine " + filename + "?",
				"Elimina"))
			return;

		try {
			deleteBean();
			notifyImageRemovedListeners(filename);
			refresh();
		} catch (Exception e) {
			UIhelper.reportException("Deleting ArticlePic.", e);
		}
	}

	public static String getImageType(final String filename) {
		String ext = FileUtils.getExtension(filename);
		if ("JPG".equalsIgnoreCase(ext) || "JPEG".equalsIgnoreCase(ext)) //
			return "jpeg";
		if ("PNG".equalsIgnoreCase(ext)) //
			return "png";
		if ("GIF".equalsIgnoreCase(ext)) //
			return "gif";
		if ("TIFF".equalsIgnoreCase(ext)) //
			return "tiff";

		return "jpeg";
	}

	/**
	 * Save selected image
	 * 
	 * @param name
	 * @param ifc
	 */
	protected abstract Immagine saveSelectedImage(String name, byte[] ifc);

	/**
	 * Get bean's text (image description)
	 * 
	 * @param bean
	 *            The bean
	 * @return
	 */
	protected String getBeanText(Immagine bean) {
		return bean.getNome();
	}

	/**
	 * Get bean's id
	 * 
	 * @param bean
	 *            The bean
	 * @return Bean's ID
	 */
	protected final String getBeanId(Immagine bean) {
		return bean.getId();
	}

	/**
	 * Get bean's type (image type)
	 * 
	 * @param bean
	 * @return
	 */
	protected final String getBeanType(Immagine bean) {
		return bean.getEstensione();
	}

	/**
	 * Load beans
	 * 
	 * @return
	 */
	protected abstract List<Immagine> loadBeans();

	/**
	 * Get'bean's image
	 * 
	 * @param bean
	 *            The bean
	 * @return Image's byte[]
	 */
	protected final byte[] getBeanImage(Immagine bean) {
		return bean.getContenuto();
	}

	/**
	 * Delete selected bean
	 * 
	 * @throws Exception
	 */
	protected final void deleteBean() throws Exception {
		PersistenceFactory.IMGlocal().delete(getSelectedBean());
	}

}
