/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.beans;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

import ch.pcilocarno.pbc.inventari.util.Tools;

/**
 * ch.pcilocarno.pbc.inventari.beans.ComboObj
 * 
 * @author elvis on Sep 10, 2009
 * @version @@version@@ build @@build@@
 */
public abstract class ComboObj<T> extends Combo
{

	private final Map<T, String> entries = new LinkedHashMap<T, String>();

	/*
	 * Abstract methods
	 */
	/**
	 * populate the combo box
	 */
	protected abstract void populate();

	/**
	 * Constructor
	 * 
	 * @param parent
	 * @param style
	 */
	public ComboObj(Composite parent, int style)
	{
		super(parent, style);
		preInit();
		init();
		postInit();
	}

	@Override
	protected void checkSubclass()
	{
	}

	/**
	 * method called just before init()
	 */
	protected void preInit()
	{
	}

	/**
	 * method called after init()
	 */
	protected void postInit()
	{
	}

	/**
	 * initializes combo box content
	 */
	protected void init()
	{
		refresh(true);
	}

	/**
	 * get Combo entries
	 * 
	 * @return
	 */
	private synchronized Map<T, String> getEntries()
	{
		return this.entries;
	}

	/**
	 * refresh combo choices
	 * 
	 * @param force
	 */
	public void refresh(boolean force)
	{
		if (entries == null || force)
		{
			removeAll();
			populate();
		}
	}

	public void removeAll()
	{
		if (getItemCount() > 0) remove(0, getItemCount() - 1);
		getEntries().clear();
	}

	public void setSelectedItem(final T item)
	{
		if (!getEntries().containsKey(null)) //
			Assert.isNotNull(item, "Setting a NULL item as selected on Combo"); //$NON-NLS-1$

		int index = 0;
		Iterator<T> items = getEntries().keySet().iterator();
		while (items.hasNext())
		{
			if (Tools.equals(item, items.next()))
			{
				select(index);
				notifyListeners(SWT.Selection, new Event());
				return;
			}
			index++; // increment index
		}
	}

	public void addItem(T item)
	{
		addItem(item, item.toString());
	}

	public void addItem(T item, String label)
	{
		if (getItemIndex(item) >= 0) return; // already added same item
		getEntries().put(item, label);
		add(label);
	}

	public void addNullitem(String label)
	{
		addItem(null, label);
	}

	public T getSelectedItem()
	{
		if (getSelectionIndex() < 0) return null;
		return (T) getEntries().keySet().toArray()[getSelectionIndex()];
	}

	public int getItemIndex(T item)
	{
		if (getEntries().isEmpty()) return -1;
		int index = 0;

		for (T each : getEntries().keySet())
		{
			if (each == null && item == null) return index;
			if (each != null && each.equals(item)) return index;
			index++;
		}
		return -1;
	}

}
