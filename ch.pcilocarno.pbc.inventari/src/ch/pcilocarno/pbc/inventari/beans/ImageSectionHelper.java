/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.beans;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ImageHyperlink;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;

import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.data.model.Immagine;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

/**
 * @author elvis
 * 
 */
public abstract class ImageSectionHelper {
	final Section section;

	final FormToolkit toolkit;

	final boolean doAddCaptionField;

	ImageHyperlink addHL, prevHL, nextHL, deleteHL, openHL;

	Imageviewer imgviewer;

	Composite client;

	/**
	 * @param section
	 */
	public ImageSectionHelper(Section section, FormToolkit toolkit, boolean doAddCaptionField) {
		this.section = section;
		this.toolkit = toolkit;
		this.doAddCaptionField = doAddCaptionField;
		build();
	}

	/**
	 * 
	 */
	public void dispose() {
		UIhelper.dispose(imgviewer);
		UIhelper.dispose(addHL);
		UIhelper.dispose(prevHL);
		UIhelper.dispose(nextHL);
		UIhelper.dispose(deleteHL);
		UIhelper.dispose(openHL);
		UIhelper.dispose(client);
	}

	void build() {
		TableWrapData twd = new TableWrapData(TableWrapData.FILL_GRAB);
		twd.heightHint = 350;

		section.setText("Immagini");
		section.setExpanded(true);
		section.setLayoutData(twd);

		Composite toolbar = new Composite(section, SWT.NONE);
		RowLayout layout = new RowLayout(SWT.HORIZONTAL);
		layout.marginLeft = 0;
		layout.marginRight = 0;
		layout.spacing = 0;
		layout.marginTop = 0;
		layout.marginBottom = 0;
		toolbar.setLayout(layout);
		section.setTextClient(toolbar);

		addHL = new ImageHyperlink(toolbar, SWT.CENTER);
		addHL.setBackgroundImage(section.getBackgroundImage());
		addHL.setToolTipText("Aggiungi immagine");
		addHL.setImage(UIhelper.getImage(Icons.PLUS_16));
		addHL.addHyperlinkListener(new HyperlinkAdapter() {
			@Override
			public void linkActivated(HyperlinkEvent e) {
				imgviewer.clickedAdd();
			}
		});

		openHL = new ImageHyperlink(toolbar, SWT.CENTER);
		openHL.setBackgroundImage(section.getBackgroundImage());
		openHL.setToolTipText("Apri immagine");
		openHL.setImage(UIhelper.getImage(Icons.EYE_16));
		openHL.addHyperlinkListener(new HyperlinkAdapter() {
			@Override
			public void linkActivated(HyperlinkEvent e) {
				imgviewer.clickedDownload();
			}
		});

		deleteHL = new ImageHyperlink(toolbar, SWT.CENTER);
		deleteHL.setBackgroundImage(section.getBackgroundImage());
		deleteHL.setToolTipText("Cancella immagine");
		deleteHL.setImage(UIhelper.getImage(Icons.CROSS_16));
		deleteHL.addHyperlinkListener(new HyperlinkAdapter() {
			@Override
			public void linkActivated(HyperlinkEvent e) {
				imgviewer.clickedDelete();
			}
		});
		ImageHyperlink spacerHL = new ImageHyperlink(toolbar, SWT.CENTER);
		spacerHL.setBackgroundImage(section.getBackgroundImage());
		spacerHL.setImage(UIhelper.getImage(Icons.EMPTY_16));

		prevHL = new ImageHyperlink(toolbar, SWT.CENTER);
		prevHL.setBackgroundImage(section.getBackgroundImage());
		prevHL.setToolTipText("Precedente");
		prevHL.setImage(UIhelper.getImage(Icons.LEFT_16));
		prevHL.addHyperlinkListener(new HyperlinkAdapter() {
			@Override
			public void linkActivated(HyperlinkEvent e) {
				imgviewer.clickedPrevious();
			}
		});
		prevHL.setEnabled(false);

		nextHL = new ImageHyperlink(toolbar, SWT.CENTER);
		nextHL.setBackgroundImage(section.getBackgroundImage());
		nextHL.setToolTipText("Prossima");
		nextHL.setImage(UIhelper.getImage(Icons.RIGHT_16));
		nextHL.addHyperlinkListener(new HyperlinkAdapter() {
			@Override
			public void linkActivated(HyperlinkEvent e) {
				imgviewer.clickedNext();
			}
		});

		client = toolkit.createComposite(section, SWT.NONE);
		// client.setLayoutData(GridDataFactory.fillDefaults().hint(SWT.DEFAULT,
		// 350).create());
		client.setLayout(new FillLayout());

		// define style
		int style = Imageviewer.BUTTONBAR_NONE;
		if (doAddCaptionField)
			style |= Imageviewer.CAPTION_FIELD;

		imgviewer = new Imageviewer(client, style) {

			@Override
			protected Immagine saveSelectedImage(String name, byte[] ifc) {
				return saveSelectedImage_(name, ifc);
			}

			@Override
			protected List<Immagine> loadBeans() {
				return loadBeans_();
			}

			@Override
			protected boolean canAdd() {
				return canAdd_();
			}
		};
		imgviewer.addDataChangeListener(new DataChangeListener() {

			@Override
			public void dataChanged() {
				updateSectionImageText();
			}
		});

		imgviewer.addSelectionChangeListener(new SelectionChangeListener() {

			@Override
			public void selectionChanged() {
				enableNavHL();
				if (!openHL.isDisposed()) //
					openHL.setEnabled(imgviewer.getSelectedBean() != null);
				if (!deleteHL.isDisposed()) //
					deleteHL.setEnabled(imgviewer.getSelectedBean() != null);
				updateSectionImageText();
			}
		});

		toolkit.paintBordersFor(client);
		section.setClient(client);
	}

	public Imageviewer getImgviewer() {
		return imgviewer;
	}

	public Composite getClient() {
		return client;
	}

	void updateSectionImageText() {
		if (section == null || section.isDisposed())
			return;
		String sectionTxt = "Immagine ";
		int imgi = imgviewer.getImageIndex();

		if (imgi > -1) //
			sectionTxt += imgi + "/" + imgviewer.getImageCount();

		if (imgviewer.getSelectedBean() != null && imgviewer.getSelectedBean().getPrincipale()) //
			sectionTxt += " (Principale)";

		section.setText(sectionTxt.trim());
	}

	void enableNavHL() {
		if (imgviewer.isDisposed())
			return;
		prevHL.setEnabled(imgviewer.hasPrevious());
		nextHL.setEnabled(imgviewer.hasNext());
	}

	protected boolean canAdd_() {
		return true;
	}

	protected abstract Immagine saveSelectedImage_(String name, byte[] ifc);

	protected abstract List<Immagine> loadBeans_();
}
