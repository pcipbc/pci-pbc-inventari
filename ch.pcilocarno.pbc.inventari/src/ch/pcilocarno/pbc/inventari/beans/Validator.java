/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.beans;

import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;

public class Validator {

	public static String validate(Control control, String controlName, boolean mandatory, Class<?> contentClass) {
		if (control instanceof Text) {
			Text fld = (Text) control;
			if (mandatory && fld.getText().trim().isEmpty())
				return "Il campo " + controlName + " è obbligatorio.";

			if (contentClass == Integer.class) {
				try {
					Integer.parseInt(fld.getText());
				} catch (NumberFormatException nfe) {
					return "Il campo " + controlName + " è numerico (intero).";
				}
			}
		}

		return null;
	}
}
