/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.beans;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

public class ExceptionDialog extends TitleAreaDialog {

	Throwable th;

	String message;

	Text textArea;

	Label lblMessage, lblMessage2;

	/**
	 * Constructor
	 */
	public ExceptionDialog(Shell shell, Throwable t, String message) {
		super(shell);
		this.th = t;
		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#create()
	 */
	@Override
	public void create() {
		super.create();

		setTitle("Errore");
		setMessage(message != null ? message : th.getCause().toString());
		setTitleImage(UIhelper.getImage(Icons.WARNING_32));

		getButton(IDialogConstants.OK_ID).setText("Dettagli »»");
		getButton(IDialogConstants.CANCEL_ID).setText("Chiudi");
		getButton(IDialogConstants.OK_ID).setFocus();
		try {
			lblMessage.setText("" + (message != null ? message : th.getMessage()));
			lblMessage2.setText("" + (message != null ? th.getMessage() : th.getClass().getName()));
			textArea.append("MESSAGE: " + message + "\n\n");
			textArea.append("" + th.getMessage() + "\n");
			textArea.append("" + th.getClass().getName() + "\n");
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw, true);
			th.printStackTrace(pw);
			pw.flush();
			sw.flush();
			textArea.append(sw.toString());
			textArea.setEditable(false);
			textArea.setSelection(0, 0);
		} catch (Exception e) {
		}

		getShell().setSize(getShell().getSize().x, getShell().getSize().y - 150);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.TitleAreaDialog#createDialogArea(org.eclipse
	 * .swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		final Composite area = new Composite(parent, SWT.NONE);
		area.setLayoutData(new GridData(GridData.FILL_BOTH));
		area.setLayout(new FormLayout());

		lblMessage = new Label(area, UIhelper.STYLE_LABEL);
		FormData fd = new FormData();
		fd.top = new FormAttachment(0, 5);
		fd.left = new FormAttachment(0, 5);
		fd.right = new FormAttachment(99, -5);
		lblMessage.setLayoutData(fd);

		lblMessage2 = new Label(area, UIhelper.STYLE_LABEL);
		fd = new FormData();
		fd.top = new FormAttachment(lblMessage, 5);
		fd.left = new FormAttachment(0, 5);
		fd.right = new FormAttachment(99, -5);
		lblMessage2.setLayoutData(fd);

		textArea = new Text(area, SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		fd = new FormData();
		fd.top = new FormAttachment(lblMessage2, 10);
		fd.left = new FormAttachment(0, 5);
		fd.right = new FormAttachment(99, -5);
		fd.bottom = new FormAttachment(99, -5);
		fd.height = 150;
		textArea.setLayoutData(fd);
		textArea.setVisible(false);

		return area;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		textArea.setVisible(!textArea.getVisible());
		getButton(IDialogConstants.OK_ID).setText(textArea.getVisible() ? "«« Less" : "More »»");
		if (textArea.getVisible())
			getShell().setSize(getShell().getSize().x, getShell().getSize().y + 150);
		else
			getShell().setSize(getShell().getSize().x, getShell().getSize().y - 150);
		return;
	}

}
