/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.beans;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;

/**
 * @author elvis
 *
 */
public class DatePeriod {
	LocalDate from;
	LocalDate to;

	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");;
	public static final LocalDate MIN = LocalDate.of(1900, Month.JANUARY, 1);
	public static final LocalDate MAX = LocalDate.of(2199, Month.DECEMBER, 31);

	public DatePeriod() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		LocalDate today = LocalDate.now();

		if (today.equals(from) && today.equals(to))
			return "Oggi";
		if (from.equals(MIN) && to.equals(MAX))
			return "In qualsiasi data";

		return from.format(formatter) + " - " + to.format(formatter) + " " + hint();
	}

	private String hint() {
		LocalDate today = LocalDate.now();
		if (today.isAfter(from) && !today.isAfter((getTo())))
			return "(Questa settimana)";

		return "";
	}

	public DatePeriod(LocalDate from, LocalDate to) {
		this.from = from;
		this.to = to;
	}

	/**
	 * @param from
	 *            the from to set
	 */
	public void setFrom(LocalDate from) {
		this.from = from;
	}

	/**
	 * @param to
	 *            the to to set
	 */
	public void setTo(LocalDate to) {
		this.to = to;
	}

	/**
	 * @return the from
	 */
	public LocalDate getFrom() {
		return from;
	}

	/**
	 * @return the to
	 */
	public LocalDate getTo() {
		return to;
	}

}
