/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari;

/**
 * @author elvis
 *
 */
public class Icons {

	private static final String BASE_FOLDER_16 = "/icons/16/";
	private static final String BASE_FOLDER_32 = "/icons/32/";
	private static final String BASE_FOLDER_48 = "/icons/48/";
	// 48
	public static final String HOURGLASS_48 = BASE_FOLDER_48 + "hourglass.png";
	// 32
	public static final String A_32 = BASE_FOLDER_32 + "a.png";
	public static final String CLOCK_HISTORY_32 = BASE_FOLDER_32 + "clock-history.png";
	public static final String IMPORT_32 = BASE_FOLDER_32 + "import.png";
	public static final String INFORMATION_32 = BASE_FOLDER_32 + "information.png";
	public static final String PA_32 = BASE_FOLDER_32 + "pa.png";
	public static final String PICTURE_IMPORT_32 = BASE_FOLDER_32 + "picture-import.png";
	public static final String PICTURES_QUESTION_32 = BASE_FOLDER_32 + "pictures-question.png";
	public static final String WARNING_32 = BASE_FOLDER_32 + "warning.png";

	// 16
	public static final String A_16 = BASE_FOLDER_16 + "a.png";
	public static final String CAMERA_16 = BASE_FOLDER_16 + "camera.png";
	public static final String CLOCK_HISTORY_16 = BASE_FOLDER_16 + "clock-history.png";
	public static final String CROSS_16 = BASE_FOLDER_16 + "cross.png";
	public static final String DATABASE_REFRESH_16 = BASE_FOLDER_16 + "database-refresh.png";
	public static final String DISK_16 = BASE_FOLDER_16 + "disk.png";
	public static final String DOCUMENT_16 = BASE_FOLDER_16 + "document.png";
	public static final String DOCUMENTS_16 = BASE_FOLDER_16 + "documents.png";
	public static final String DOOR_OPEN_16 = BASE_FOLDER_16 + "door-open.png";
	public static final String EDIT_16 = BASE_FOLDER_16 + "edit.png";
	public static final String EMPTY_16 = BASE_FOLDER_16 + "empty.png";
	public static final String EXCEL_16 = BASE_FOLDER_16 + "excel.png";
	public static final String EXCLAMATION_SMALL_16 = BASE_FOLDER_16 + "exclamation-small.png";
	public static final String EXPORT_16 = BASE_FOLDER_16 + "export.png";
	public static final String EYE_16 = BASE_FOLDER_16 + "eye.png";
	public static final String IMPORT_16 = BASE_FOLDER_16 + "import.png";
	public static final String INFORMATION_16 = BASE_FOLDER_16 + "information.png";
	public static final String LEFT_16 = BASE_FOLDER_16 + "left.png";
	public static final String LOCK_SMALL_16 = BASE_FOLDER_16 + "lock-small.png";
	public static final String MAGNIFIER_16 = BASE_FOLDER_16 + "magnifier.png";
	public static final String MEASURES_16 = BASE_FOLDER_16 + "measures.png";
	public static final String MOVE_16 = BASE_FOLDER_16 + "move.png";
	public static final String OA_16 = BASE_FOLDER_16 + "oa.png";
	public static final String PA_16 = BASE_FOLDER_16 + "pa.png";
	public static final String PAPER_CLIP_16 = BASE_FOLDER_16 + "paper-clip.png";
	public static final String PDF_16 = BASE_FOLDER_16 + "pdf.png";
	public static final String PICTURE_SMALL_16 = BASE_FOLDER_16 + "picture-small.png";
	public static final String PLUS_16 = BASE_FOLDER_16 + "plus.png";
	public static final String REFRESH_16 = BASE_FOLDER_16 + "refresh.png";
	public static final String RIGHT_16 = BASE_FOLDER_16 + "right.png";
	public static final String TICK_SMALL_16 = BASE_FOLDER_16 + "tick-small.png";
	public static final String WARNING_SMALL_16 = BASE_FOLDER_16 + "warning-small.png";
	public static final String WRENCH_16 = BASE_FOLDER_16 + "wrench.png";
	public static final String ENDASH = BASE_FOLDER_16 + "endash.png";
}
