/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.editors;

import java.math.BigDecimal;
import java.text.MessageFormat;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.IMessageManager;

public class Helper {
	final IMessageManager mm;

	int errorCount = 0;

	/**
	 * @param messageManager
	 */
	public Helper(IMessageManager messageManager) {
		this.mm = messageManager;
		mm.removeAllMessages();
		errorCount = 0;
	}

	/**
	 * @param id
	 * @param msg
	 * @param control
	 */
	protected void addError(String id, String msg, Control control) {
		mm.addMessage(id, msg, null, IMessageProvider.ERROR, control);
		errorCount++;
	}

	/**
	 * @param fld
	 * @param fldText
	 * @param minVal
	 * @param maxVal
	 */
	protected void validateFieldInteger(Text fld, String fldText, int minVal, int maxVal) {
		int value;
		try {
			value = Integer.parseInt(fld.getText());
		} catch (NumberFormatException nfe) {
			addError(fldText, MessageFormat.format("Il contenuto del campo {0} dev''essere numerico.", fldText), fld);
			return;
		}

		if (value < minVal) //
			addError(fldText,
					MessageFormat.format("Il valore del campo {1} dev''essere maggiore di {0}", minVal, fldText), fld);

		if (value > maxVal)
			addError(fldText,
					MessageFormat.format("Il valore del campo {1} dev''essere minore di {0}", minVal, fldText), fld);

	}

	/**
	 * @param fld
	 * @param fldText
	 * @param minVal
	 * @param maxVal
	 * @param canBeZero
	 */
	protected void validateFieldDecimal(Text fld, String fldText, BigDecimal minVal, BigDecimal maxVal,
			boolean canBeZero) {
		BigDecimal value;
		try {
			value = new BigDecimal(fld.getText());
		} catch (NumberFormatException nfe) {
			addError(fldText, MessageFormat.format("Il contenuto del campo {0} dev''essere numerico.", fldText), fld);
			return;
		}

		if (!canBeZero && value.compareTo(BigDecimal.ZERO) == 0) //
			addError(fldText, MessageFormat.format("Il valore del campo {0} non può essere zero.", fldText), fld);

		if (value.compareTo(minVal) < 0) //
			addError(fldText,
					MessageFormat.format("Il valore del campo {1} dev''essere maggiore di {0}", minVal, fldText), fld);

		if (value.compareTo(maxVal) > 0) //
			addError(fldText,
					MessageFormat.format("Il valore del campo {1} dev''essere minore di {0}", minVal, fldText), fld);

	}

	protected void validateFieldMandatory(Text fld, String fldText) {
		if (fld.getText().trim().isEmpty())
			addError(fldText, MessageFormat.format("Il campo {0} è obbligatorio.", fldText), fld);
	}
}
