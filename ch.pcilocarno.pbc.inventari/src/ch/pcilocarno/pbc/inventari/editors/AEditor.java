/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.editors;

import java.math.BigDecimal;
import java.util.List;

import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;

import ch.pcilocarno.pbc.inventari.beans.ImageAddedListener;
import ch.pcilocarno.pbc.inventari.beans.ImageSectionHelper;
import ch.pcilocarno.pbc.inventari.data.model.Architettura;
import ch.pcilocarno.pbc.inventari.data.model.Immagine;
import ch.pcilocarno.pbc.inventari.data.model.enums.CategoriaBene;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.model.enums.Scudi;
import ch.pcilocarno.pbc.inventari.data.model.enums.TipoContatto;
import ch.pcilocarno.pbc.inventari.data.persistence.IPersistence;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.FileUtils;
import ch.pcilocarno.pbc.inventari.util.Tools;

/**
 * @author elvis
 * 
 */
public class AEditor extends EditorAdapter<Architettura> {
	public static final String ID = "ch.pcilocarno.pbc.inventari.editors.AEditor";

	Section imgSection;

	Text fNoPci, fNoUbc, fDenominazione, fFoto, fIndirizzo, fComune, fCap, fFondoRfd;

	Text fCoordinate;

	Text fBibliografia;

	Button fDocumentazione;

	Combo fCategoria, fScudi;

	ContactPanel custode, proprietario;

	ImageSectionHelper imgSectionHelper;

	// section PCi
	Text fResponsabilePci, fResponsabileEvacuazione, fResponsabileProtezione, fDistribuzione1, fDistribuzione2,
			fDistribuzione3, fDistribuzione4, fNote;

	// section evacuazione
	Text fIndicazioniRifugio, fDeposito, fCollabTrasporto, fMaterialeImballaggio, fMezziTrasporto, fOsservazioniEvac,
			fQtaMezzi, fQtaPersone, fQtaOre;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.EditorAdapter#dispose()
	 */
	@Override
	public void dispose() {
		imgSectionHelper.getImgviewer().stopAndDeregisterPollers();
		imgSectionHelper.dispose();
		imgSection.dispose();
		super.dispose();
	}

	@Override
	String getSourceViewID() {
		return null;
	}

	@Override
	IPersistence<Architettura> getDao() {
		return PersistenceFactory.Alocal();
	}

	@Override
	protected void initContent() {
		createSectionBase();
		createSectionImage();
		createSectionProprietario();
		createSectionCustode();
		createSectionEvacuazione();
		createSectionPci();
	}

	Layout getSectionClientLayout() {
		GridLayout gl = new GridLayout(2, false);
		gl.horizontalSpacing = 10;
		return gl;
	}

	Object getSectionLayoutData() {
		return new TableWrapData(TableWrapData.FILL_GRAB);
	}

	void createSectionProprietario() {
		Section section = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		section.setText("Proprietario");
		section.setExpanded(true);
		section.setLayoutData(getSectionLayoutData());

		proprietario = new ContactPanel(section);
		proprietario.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		proprietario.setModifyListener(modListener);

		getToolkit().adapt(proprietario);

		getToolkit().paintBordersFor(proprietario);

		// getToolkit().paintBordersFor(client);
		section.setClient(proprietario);
		applySectionLayoutDataWorkaround(section);
	}

	void createSectionPci() {
		Section section = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		section.setText("PCi");
		section.setExpanded(true);
		section.setLayoutData(getSectionLayoutData());

		Composite client = getToolkit().createComposite(section, SWT.NONE);
		client.setLayout(getSectionClientLayout());

		GridData lgd = new GridData();
		lgd.widthHint = 90;
		lgd.verticalAlignment = SWT.TOP;
		GridData fgd = new GridData(SWT.FILL, SWT.FILL, true, false);

		// Text fResponsabilePci, fDistribuzione1, fDistribuzione2,
		// fDistribuzione3, fDistribuzione4, fNote;

		getToolkit().createLabel(client, "Resp. PCi").setLayoutData(lgd);
		fResponsabilePci = getToolkit().createText(client, "");
		fResponsabilePci.setLayoutData(fgd);
		fResponsabilePci.setTextLimit(256);
		fResponsabilePci.addModifyListener(modListener);

		getToolkit().createLabel(client, "Resp. Evac.").setLayoutData(lgd);
		fResponsabileEvacuazione = getToolkit().createText(client, "");
		fResponsabileEvacuazione.setLayoutData(fgd);
		fResponsabileEvacuazione.setTextLimit(256);
		fResponsabileEvacuazione.addModifyListener(modListener);

		getToolkit().createLabel(client, "Resp. Prot.").setLayoutData(lgd);
		fResponsabileProtezione = getToolkit().createText(client, "");
		fResponsabileProtezione.setLayoutData(fgd);
		fResponsabileProtezione.setTextLimit(256);
		fResponsabileProtezione.addModifyListener(modListener);

		getToolkit().createLabel(client, "Distribuzione 1").setLayoutData(lgd);
		fDistribuzione1 = getToolkit().createText(client, "");
		fDistribuzione1.setLayoutData(fgd);
		fDistribuzione1.setTextLimit(256);
		fDistribuzione1.addModifyListener(modListener);

		getToolkit().createLabel(client, "Distribuzione 2").setLayoutData(lgd);
		fDistribuzione2 = getToolkit().createText(client, "");
		fDistribuzione2.setLayoutData(fgd);
		fDistribuzione2.setTextLimit(256);
		fDistribuzione2.addModifyListener(modListener);

		getToolkit().createLabel(client, "Distribuzione 3").setLayoutData(lgd);
		fDistribuzione3 = getToolkit().createText(client, "");
		fDistribuzione3.setLayoutData(fgd);
		fDistribuzione3.setTextLimit(256);
		fDistribuzione3.addModifyListener(modListener);

		getToolkit().createLabel(client, "Distribuzione 4").setLayoutData(lgd);
		fDistribuzione4 = getToolkit().createText(client, "");
		fDistribuzione4.setLayoutData(fgd);
		fDistribuzione4.setTextLimit(256);
		fDistribuzione4.addModifyListener(modListener);

		getToolkit().createLabel(client, "Note").setLayoutData(lgd);
		fNote = getToolkit().createText(client, "", SWT.MULTI);
		fNote.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, false).span(1, 1)
				.hint(SWT.DEFAULT, 70).create());
		fNote.setTextLimit(2048);
		fNote.addModifyListener(modListener);

		getToolkit().paintBordersFor(client);
		section.setClient(client);
		applySectionLayoutDataWorkaround(section);
	}

	void createSectionEvacuazione() {
		Section section = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		section.setText("Evacuazione");
		section.setExpanded(true);
		section.setLayoutData(getSectionLayoutData());

		Composite client = getToolkit().createComposite(section, SWT.NONE);
		client.setLayout(getSectionClientLayout());

		GridData lgd = new GridData();
		lgd.widthHint = 90;
		lgd.verticalAlignment = SWT.TOP;
		GridData fgd = new GridData(SWT.FILL, SWT.FILL, true, false);

		getToolkit().createLabel(client, "Ind. rifugio").setLayoutData(lgd);
		fIndicazioniRifugio = getToolkit().createText(client, "", SWT.MULTI);
		fIndicazioniRifugio.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, false)
				.span(1, 1).hint(SWT.DEFAULT, 70).create());
		fIndicazioniRifugio.setTextLimit(2048);
		fIndicazioniRifugio.addModifyListener(modListener);

		getToolkit().createLabel(client, "Mat. imball.").setLayoutData(lgd);
		fMaterialeImballaggio = getToolkit().createText(client, "", SWT.MULTI);
		fMaterialeImballaggio.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, false)
				.span(1, 1).hint(SWT.DEFAULT, 70).create());
		fMaterialeImballaggio.setTextLimit(2048);
		fMaterialeImballaggio.addModifyListener(modListener);

		getToolkit().createLabel(client, "Mezzi trasp.").setLayoutData(lgd);
		fMezziTrasporto = getToolkit().createText(client, "");
		fMezziTrasporto.setLayoutData(fgd);
		fMezziTrasporto.setTextLimit(256);
		fMezziTrasporto.addModifyListener(modListener);

		getToolkit().createLabel(client, "Trasp. in collab.").setLayoutData(lgd);
		fCollabTrasporto = getToolkit().createText(client, "");
		fCollabTrasporto.setLayoutData(fgd);
		fCollabTrasporto.setTextLimit(256);
		fCollabTrasporto.addModifyListener(modListener);

		getToolkit().createLabel(client, "Qtà mezzi").setLayoutData(lgd);
		fQtaMezzi = getToolkit().createText(client, "");
		fQtaMezzi.setLayoutData(fgd);
		fQtaMezzi.setTextLimit(4);
		fQtaMezzi.addModifyListener(modListener);

		getToolkit().createLabel(client, "Qtà persone").setLayoutData(lgd);
		fQtaPersone = getToolkit().createText(client, "");
		fQtaPersone.setLayoutData(fgd);
		fQtaPersone.setTextLimit(4);
		fQtaPersone.addModifyListener(modListener);

		getToolkit().createLabel(client, "Qtà ore").setLayoutData(lgd);
		fQtaOre = getToolkit().createText(client, "");
		fQtaOre.setLayoutData(fgd);
		fQtaOre.setTextLimit(4);
		fQtaOre.addModifyListener(modListener);

		getToolkit().createLabel(client, "Deposito").setLayoutData(lgd);
		fDeposito = getToolkit().createText(client, "");
		fDeposito.setLayoutData(fgd);
		fDeposito.setTextLimit(256);
		fDeposito.addModifyListener(modListener);

		getToolkit().createLabel(client, "Osservazioni").setLayoutData(lgd);
		fOsservazioniEvac = getToolkit().createText(client, "", SWT.MULTI);
		fOsservazioniEvac.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, false)
				.hint(SWT.DEFAULT, 70).create());
		fOsservazioniEvac.setTextLimit(2048);
		fOsservazioniEvac.addModifyListener(modListener);
		getToolkit().paintBordersFor(client);
		
		
		
		section.setClient(client);
		applySectionLayoutDataWorkaround(section);
	}

	void createSectionCustode() {
		Section section = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		section.setText("Custode");
		section.setExpanded(true);
		section.setLayoutData(getSectionLayoutData());

		custode = new ContactPanel(section);
		custode.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		custode.setModifyListener(modListener);

		getToolkit().adapt(custode);
		getToolkit().paintBordersFor(custode);

		section.setClient(custode);
		applySectionLayoutDataWorkaround(section);
	}

	void createSectionBase() {
		Section section = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		section.setText("Dati base");
		section.setExpanded(true);
		section.setLayoutData(getSectionLayoutData());

		Composite client = getToolkit().createComposite(section, SWT.NONE);
		client.setLayout(getSectionClientLayout());

		GridData lgd = new GridData();
		lgd.widthHint = 90;
		GridData fgd = new GridData(SWT.FILL, SWT.FILL, true, false);

		getToolkit().createLabel(client, "No. PCi").setLayoutData(lgd);
		fNoPci = getToolkit().createText(client, "");
		fNoPci.setLayoutData(fgd);
		fNoPci.setTextLimit(32);

		getToolkit().createLabel(client, "No UBC").setLayoutData(lgd);
		fNoUbc = getToolkit().createText(client, "");
		fNoUbc.setLayoutData(fgd);
		fNoUbc.setTextLimit(32);

		getToolkit().createLabel(client, "Denominazione").setLayoutData(lgd);
		fDenominazione = getToolkit().createText(client, "");
		fDenominazione.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fDenominazione.setTextLimit(256);

		getToolkit().createLabel(client, "Categoria").setLayoutData(lgd);

		Composite combos = getToolkit().createComposite(client);
		combos.setLayout(GridLayoutFactory.fillDefaults().equalWidth(false).numColumns(3).margins(0, 0).create());
		combos.setLayoutData(
				GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).indent(0, 0).create());
		fCategoria = new Combo(combos, SWT.BORDER | SWT.READ_ONLY);
		fCategoria.add("");
		fCategoria.add(CategoriaBene.A.toString());
		fCategoria.add(CategoriaBene.B.toString());
		fCategoria.add(CategoriaBene.C.toString());
		// fCategoria.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fCategoria.select(0);
		getToolkit().createLabel(combos, "Scudi").setLayoutData(lgd);
		fScudi = new Combo(combos, SWT.BORDER | SWT.READ_ONLY);
		fScudi.add("");
		fScudi.add(Scudi.SINGOLO.toString());
		fScudi.add(Scudi.TRIPLO.toString());
		fScudi.select(0);

		getToolkit().createLabel(client, "Foto").setLayoutData(lgd);
		fFoto = getToolkit().createText(client, "");
		fFoto.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fFoto.setTextLimit(128);

		getToolkit().createLabel(client, "Indirizzo").setLayoutData(lgd);
		fIndirizzo = getToolkit().createText(client, "");
		fIndirizzo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fIndirizzo.setTextLimit(64);

		getToolkit().createLabel(client, "Cap").setLayoutData(lgd);
		fCap = getToolkit().createText(client, "");
		fCap.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fCap.setTextLimit(9);

		getToolkit().createLabel(client, "Comune").setLayoutData(lgd);
		fComune = getToolkit().createText(client, "");
		fComune.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fComune.setTextLimit(64);

		getToolkit().createLabel(client, "Fondo RFD").setLayoutData(lgd);
		fFondoRfd = getToolkit().createText(client, "");
		fFondoRfd.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fFondoRfd.setTextLimit(32);

		getToolkit().createLabel(client, "Coordinate").setLayoutData(lgd);
		fCoordinate = getToolkit().createText(client, "");
		fCoordinate.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fCoordinate.setTextLimit(64);

		GridData bfgd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		bfgd.heightHint = 70;

		getToolkit().createLabel(client, "Bibliografia").setLayoutData(lgd);
		fBibliografia = getToolkit().createText(client, "", SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
		fBibliografia.setLayoutData(bfgd);
		fBibliografia.setTextLimit(512);

		getToolkit().createLabel(client, "").setLayoutData(lgd);
		fDocumentazione = getToolkit().createButton(client, "Documentazione presente", SWT.CHECK);
		fDocumentazione.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		fNoPci.addModifyListener(modListener);
		fNoUbc.addModifyListener(modListener);
		fDenominazione.addModifyListener(modListener);
		fCategoria.addModifyListener(modListener);
		fScudi.addModifyListener(modListener);
		fFoto.addModifyListener(modListener);
		fIndirizzo.addModifyListener(modListener);
		fCap.addModifyListener(modListener);
		fComune.addModifyListener(modListener);
		fFondoRfd.addModifyListener(modListener);
		fCoordinate.addModifyListener(modListener);

		fBibliografia.addModifyListener(modListener);
		fDocumentazione.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				beanChanged();
				validate();
			}
		});
		getToolkit().paintBordersFor(client);
		section.setClient(client);
		applySectionLayoutDataWorkaround(section);
	}

	void createSectionImage() {

		imgSection = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		imgSectionHelper = new ImageSectionHelper(imgSection, getToolkit(), true) {
			@Override
			protected Immagine saveSelectedImage_(String name, byte[] ifc) {
				Immagine nu = new Immagine(beanGet().getId());
				nu.setContenuto(ifc);
				nu.setNome(name.substring(0, name.lastIndexOf(".")));
				nu.setEstensione(FileUtils.getExtension(name));
				return PersistenceFactory.IMGlocal().saveOrUpdate(nu);
			}

			@Override
			protected List<Immagine> loadBeans_() {
				return PersistenceFactory.IMGlocal().findByIDprop(beanGet().getId(), RowStatus.A);
			}
		};

		imgSectionHelper.getImgviewer().addImageAddedListener(new ImageAddedListener() {
			@Override
			public void imageAdded() {
				if (fFoto.getText().indexOf(imagename) == -1) //
					fFoto.setText(fFoto.getText() + (fFoto.getText().isEmpty() ? "" : ", ") + imagename);
			}
		});
		applySectionLayoutDataWorkaround(imgSection);
	}

	@Override
	protected void checkFields(Helper hlp) {
		hlp.validateFieldInteger(fQtaMezzi, "Qtà mezzi", 0, 999999);
		hlp.validateFieldInteger(fQtaPersone, "Qtà persone", 0, 999999);
		hlp.validateFieldDecimal(fQtaOre, "Qtà ore", BigDecimal.ZERO, new BigDecimal("99999.99"), true);

	}

	@Override
	public String getTitleLong() {
		return beanGet().getNoPci() + " " + beanGet().getDenominazione() + ", " + beanGet().getComune();
	}

	@Override
	public String getTitleShort() {
		return beanGet().getDenominazione();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		fNoPci.setFocus();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.IBeanEditor#beanFromFields()
	 */
	@Override
	public void beanFromFields() {
		beanGet().setNoPci(fNoPci.getText());
		beanGet().setNoUbc(fNoUbc.getText());
		beanGet().setDenominazione(fDenominazione.getText());
		beanGet().setFoto(fFoto.getText());
		beanGet().setIndirizzo(fIndirizzo.getText());
		beanGet().setCap(Integer.parseInt(fCap.getText()));
		beanGet().setCategoria(
				fCategoria.getText().isEmpty() ? CategoriaBene.NONE : CategoriaBene.valueOf(fCategoria.getText()));
		beanGet().setScudi(fScudi.getText().isEmpty() ? Scudi.NONE : Scudi.valueOf(fScudi.getText()));
		beanGet().setComune(fComune.getText());
		beanGet().setFondoRfd(fFondoRfd.getText());
		beanGet().setCoordinate(fCoordinate.getText());
		beanGet().setDocumentazione(fDocumentazione.getSelection());
		beanGet().setBibliografia(fBibliografia.getText());

		beanGet().setResponsabilePCi(fResponsabilePci.getText());
		beanGet().setDistribuzione1(fDistribuzione1.getText());
		beanGet().setDistribuzione2(fDistribuzione2.getText());
		beanGet().setDistribuzione3(fDistribuzione3.getText());
		beanGet().setDistribuzione4(fDistribuzione4.getText());
		beanGet().setNote(fNote.getText());

		beanGet().setIndicazioniRifugio(fIndicazioniRifugio.getText());
		beanGet().setMaterialeImballaggio(fMaterialeImballaggio.getText());
		beanGet().setMezziTrasporto(fMezziTrasporto.getText());
		beanGet().setOsservazioniEvac(fOsservazioniEvac.getText());
		beanGet().setQtaMezzi(Integer.parseInt(fQtaMezzi.getText()));
		beanGet().setQtaPersone(Integer.parseInt(fQtaPersone.getText()));
		beanGet().setQtaOre(Tools.toBigD(fQtaOre.getText()));

		beanGet().setDeposito(fDeposito.getText());
		beanGet().setResponsabileEvacuazione(fResponsabileEvacuazione.getText());
		beanGet().setResponsabileProtezione(fResponsabileProtezione.getText());
		beanGet().setCollabTrasporto(fCollabTrasporto.getText());
		imgSectionHelper.getImgviewer().doSaveCaption();
	}

	@Override
	protected void saveSubBeans() {
		PersistenceFactory.Clocal().saveOrUpdate(custode.getContatto());
		PersistenceFactory.Clocal().saveOrUpdate(proprietario.getContatto());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.IBeanEditor#beanToFields()
	 */
	@Override
	public void beanToFields() {
		fNoPci.setText(beanGet().getNoPci());
		fNoUbc.setText(beanGet().getNoUbc());
		fDenominazione.setText(beanGet().getDenominazione());
		fFoto.setText(beanGet().getFoto());
		fIndirizzo.setText(beanGet().getIndirizzo());
		fCap.setText(beanGet().getCap() + "");
		fComune.setText(beanGet().getComune());
		fFondoRfd.setText(beanGet().getFondoRfd());
		fCoordinate.setText(beanGet().getCoordinate());
		fDocumentazione.setSelection(beanGet().getDocumentazione());
		fBibliografia.setText(beanGet().getBibliografia());

		fCategoria.setText(beanGet().getCategoria() == CategoriaBene.NONE ? "" : beanGet().getCategoria().toString());
		fScudi.setText(beanGet().getScudi() == Scudi.NONE ? "" : beanGet().getScudi().toString());

		custode.beanSet(PersistenceFactory.Clocal().findByIdPropTipo(beanGet().getId(), TipoContatto.CUSTODE));
		custode.beanToFields();

		proprietario
				.beanSet(PersistenceFactory.Clocal().findByIdPropTipo(beanGet().getId(), TipoContatto.PROPRIETARIO));
		proprietario.beanToFields();

		fResponsabilePci.setText(beanGet().getResponsabilePCi());
		fDistribuzione1.setText(beanGet().getDistribuzione1());
		fDistribuzione2.setText(beanGet().getDistribuzione2());
		fDistribuzione3.setText(beanGet().getDistribuzione3());
		fDistribuzione4.setText(beanGet().getDistribuzione4());
		fNote.setText(beanGet().getNote());

		fIndicazioniRifugio.setText(beanGet().getIndicazioniRifugio());
		fMaterialeImballaggio.setText(beanGet().getMaterialeImballaggio());
		fMezziTrasporto.setText(beanGet().getMezziTrasporto());
		fOsservazioniEvac.setText(beanGet().getOsservazioniEvac());
		fQtaMezzi.setText(beanGet().getQtaMezzi().toString());
		fQtaPersone.setText(beanGet().getQtaPersone().toString());
		fQtaOre.setText(beanGet().getQtaOre().toPlainString());

		fDeposito.setText(beanGet().getDeposito());
		fResponsabileEvacuazione.setText(beanGet().getResponsabileEvacuazione());
		fResponsabileProtezione.setText(beanGet().getResponsabileProtezione());
		fCollabTrasporto.setText(beanGet().getCollabTrasporto());

		imgSectionHelper.getImgviewer().refresh();
	}

}
