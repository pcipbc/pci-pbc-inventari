/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.editors;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ch.pcilocarno.pbc.inventari.data.model.Contatto;

public class ContactPanel extends Composite implements IBeanEditor<Contatto>
{
	Contatto bean;

	Text fNome, fCognome, fIndirizzo, fIndirizzo2, fCap, fDomicilio, fTelCasa, fTelUff, fNatel, fFax, fEmail;

	public ContactPanel(Composite parent)
	{
		super(parent, SWT.NONE);
		init();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.swt.widgets.Control#setBackground(org.eclipse.swt.graphics.Color)
	 */
	@Override
	public void setBackground(Color color)
	{
		super.setBackground(color);
		Control[] cc = this.getChildren();
		for (Control c : cc)
			if (c instanceof Label) ((Label) c).setBackground(color);
	}

	public void setModifyListener(ModifyListener moli)
	{
		fNome.addModifyListener(moli);
		fCognome.addModifyListener(moli);
		fIndirizzo.addModifyListener(moli);
		fIndirizzo2.addModifyListener(moli);
		fCap.addModifyListener(moli);
		fDomicilio.addModifyListener(moli);
		fTelCasa.addModifyListener(moli);
		fTelUff.addModifyListener(moli);
		fNatel.addModifyListener(moli);
		fFax.addModifyListener(moli);
		fEmail.addModifyListener(moli);
	}

	protected void init()
	{
		GridLayout gl = new GridLayout(2, false);
		gl.horizontalSpacing = 10;
		setLayout(gl);

		GridData lgd = new GridData();
		lgd.widthHint = 90;

		Label label = new Label(this, SWT.NONE);
		label.setText("Nome");
		label.setLayoutData(lgd);
		fNome = new Text(this, SWT.SINGLE|SWT.BORDER);
		fNome.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(this, SWT.NONE).setText("Cognome");
		fCognome = new Text(this, SWT.SINGLE|SWT.BORDER);
		fCognome.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(this, SWT.NONE).setText("Indirizzo");
		fIndirizzo = new Text(this, SWT.SINGLE|SWT.BORDER);
		fIndirizzo.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(this, SWT.NONE).setText("Supplemento");
		fIndirizzo2 = new Text(this, SWT.SINGLE|SWT.BORDER);
		fIndirizzo2.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(this, SWT.NONE).setText("CAP");
		fCap = new Text(this, SWT.SINGLE|SWT.BORDER);
		fCap.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(this, SWT.NONE).setText("Domicilio");
		fDomicilio = new Text(this, SWT.SINGLE|SWT.BORDER);
		fDomicilio.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(this, SWT.NONE).setText("Tel. casa");
		fTelCasa = new Text(this, SWT.SINGLE|SWT.BORDER);
		fTelCasa.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(this, SWT.NONE).setText("Tel. uff.");
		fTelUff = new Text(this, SWT.SINGLE|SWT.BORDER);
		fTelUff.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(this, SWT.NONE).setText("Cellulare");
		fNatel = new Text(this, SWT.SINGLE|SWT.BORDER);
		fNatel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(this, SWT.NONE).setText("Fax");
		fFax = new Text(this, SWT.SINGLE|SWT.BORDER);
		fFax.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(this, SWT.NONE).setText("E-mail");
		fEmail = new Text(this, SWT.SINGLE|SWT.BORDER);
		fEmail.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	}

	@Override
	public void beanChanged()
	{
	}

	@Override
	public void beanFromFields()
	{
		beanGet().setCap(Integer.parseInt(fCap.getText().isEmpty() ? "0" : fCap.getText()));
		beanGet().setCognome(fCognome.getText());
		beanGet().setDomicilio(fDomicilio.getText());
		beanGet().setEmail(fEmail.getText());
		beanGet().setFax(fFax.getText());
		beanGet().setIndirizzo(fIndirizzo.getText());
		beanGet().setIndirizzo2(fIndirizzo2.getText());
		beanGet().setNome(fNome.getText());
		beanGet().setTelCasa(fTelCasa.getText());
		beanGet().setTelNat(fNatel.getText());
		beanGet().setTelUff(fTelUff.getText());
	}

	@Override
	public Contatto beanGet()
	{
		return bean;
	}

	/**
	 * @return
	 */
	public Contatto getContatto()
	{
		if (!beanIsValid()) throw new RuntimeException("Dati caontatto non validi.");
		beanFromFields();
		return beanGet();
	}

	@Override
	public boolean beanIsValid()
	{
		return true;
	}

	@Override
	public void beanSet(Contatto bean)
	{
		this.bean = bean;
	}

	@Override
	public void beanToFields()
	{
		fNome.setText(beanGet().getNome());
		fCognome.setText(beanGet().getCognome());
		fCap.setText(beanGet().getCap() + "");
		fDomicilio.setText(beanGet().getDomicilio());
		fEmail.setText(beanGet().getEmail());
		fFax.setText(beanGet().getFax());
		fIndirizzo.setText(beanGet().getIndirizzo());
		fIndirizzo2.setText(beanGet().getIndirizzo2());
		fNatel.setText(beanGet().getTelNat());
		fTelCasa.setText(beanGet().getTelCasa());
		fTelUff.setText(beanGet().getTelUff());
	}

	@Override
	public void beanUnchanged()
	{
	}

}
