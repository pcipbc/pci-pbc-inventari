/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.editors;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IPropertyListener;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;
import org.eclipse.ui.part.EditorPart;

import ch.pcilocarno.pbc.inventari.Activator;
import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.data.persistence.IPersistence;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistentObject;
import ch.pcilocarno.pbc.inventari.dialogs.RecordInfoDialog;
import ch.pcilocarno.pbc.inventari.editors.input.IEditorInputPO;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.views.IViewListPO;

/**
 * @author elvis
 * 
 * @param <B>
 */
public abstract class EditorAdapter<B extends PersistentObject> extends EditorPart implements IBeanEditor<B> {
	private boolean beanChanged = false, initialized = false, beanValid = false, enableListeners = true;

	private FormToolkit toolkit;

	private ScrolledForm form;

	// private MessageManager msgManager;

	Action saveAction, infoAction;

	protected ModifyListener modListener = new ModifyListener() {
		@Override
		public void modifyText(ModifyEvent e) {
			beanChanged();
		}
	};

	protected FocusAdapter focusLostValidate = new FocusAdapter() {
		@Override
		public void focusLost(FocusEvent e) {
			super.focusLost(e);
			validate();
		}
	};

	protected ModifyListener valListener = new ModifyListener() {
		@Override
		public void modifyText(ModifyEvent e) {
			validate();
		}
	};

	protected FocusAdapter focusGainedSelectAll = new FocusAdapter() {
		@Override
		public void focusGained(FocusEvent e) {
			((Text) e.widget).selectAll();
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#dispose()
	 */
	@Override
	public void dispose() {
		modListener = null;
		focusGainedSelectAll = null;
		saveAction = null;
		infoAction = null;

		if (getForm() != null)
			getForm().dispose();
		if (getToolkit() != null)
			getToolkit().dispose();

		super.dispose();
	}

	protected boolean areListersEnabled() {
		return enableListeners;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.IBeanEditor#beanChanged()
	 */
	@Override
	final public void beanChanged() {
		beanChanged = true;
		firePropertyChange(PROP_DIRTY);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.IBeanEditor#beanIsValid()
	 */
	@Override
	public boolean beanIsValid() {
		return this.beanValid;
	}

	/**
	 * 
	 */
	final void validate() {
		if (!initialized)
			return;
		Helper hlp = new Helper(getForm().getMessageManager());
		checkFields(hlp);
		beanValid = hlp.errorCount == 0;
	}

	/**
	 * @param hlp
	 */
	abstract protected void checkFields(Helper hlp);

	/**
	 * @return
	 */
	public ScrolledForm getForm() {
		return form;
	}

	/**
	 * @return
	 */
	public FormToolkit getToolkit() {
		return toolkit;
	}

	/**
	 * @param bean
	 */
	@Override
	public void beanSet(B bean) {
		beanToFields();
		beanUnchanged();
	}

	@Override
	public B beanGet() {
		return (B) ((IEditorInputPO) getEditorInput()).getPO();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.IBeanEditor#beanUnchanged()
	 */
	@Override
	public void beanUnchanged() {
		beanChanged = false;
		firePropertyChange(PROP_DIRTY);
	}

	abstract IPersistence<B> getDao();

	abstract String getSourceViewID();

	@Override
	public void doSave(IProgressMonitor monitor) {
		validate();
		if (!beanIsValid())
			return;
		beanFromFields();

		B saved = getDao().saveOrUpdate(beanGet());

		saveSubBeans();

		updateBean(saved);
		beanUnchanged();
		refreshSourceViews();

		updateTitle();
		postSave();
	}

	protected final void updateBean(B saved) {
		IEditorInput input = getEditorInput();
		if (input instanceof IEditorInputPO)
			((IEditorInputPO) input).setPO(saved);
		setInputWithNotify(input);
		beanSet(saved);
	}

	protected final void refreshSourceViews() {
		try {
			if (getSourceViewID() != null) {
				IViewPart view = getSite().getPage().findView(getSourceViewID());
				if (view != null && view instanceof IViewListPO) {
					((IViewListPO) view).refresh();
				}
			}
		} catch (Exception e) {
			Log.error(e, "Refreshing OA view");
		}
	}

	protected void postSave() {
	}

	protected void saveSubBeans() {
	}

	/**
	 * 
	 */
	final void updateTitle() {
		form.setText(getTitleLong());
		setPartName(getTitleShort());
	}

	/**
	 * @return
	 */
	public String getTitleShort() {
		if (beanGet().getId() == null) // isnew
			return "Nuova " + beanGet().getClass().getSimpleName();

		return beanGet().getId();
	}

	/**
	 * @return
	 */
	public String getTitleLong() {
		return getTitleShort();
	}

	@Override
	public void doSaveAs() {
		throw new RuntimeException("Unallowed to call doSaveAs.");
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
		setInput(input);
	}

	@Override
	public boolean isDirty() {
		return beanChanged;
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	/**
	 * @return
	 */
	protected Layout getBodyLayout() {
		TableWrapLayout formBodyLayout = new TableWrapLayout();
		formBodyLayout.numColumns = 2;
		formBodyLayout.makeColumnsEqualWidth = true;
		formBodyLayout.horizontalSpacing = 5;
		formBodyLayout.verticalSpacing = 5;

		return formBodyLayout;
	}

	protected void initActions() {
		infoAction = new Action("Informazioni", Activator.getImageDescriptor(Icons.INFORMATION_16)) {
			@Override
			public void run() {
				new RecordInfoDialog(getSite().getShell(), beanGet()).open();
			}
		};

		saveAction = new Action("Salva", Activator.getImageDescriptor(Icons.DISK_16)) {
			@Override
			public void run() {
				doSave(new NullProgressMonitor());
			}
		};
		saveAction.setEnabled(false);
		addPropertyListener(new IPropertyListener() {
			@Override
			public void propertyChanged(Object source, int propId) {
				if (propId == PROP_DIRTY) //
					saveAction.setEnabled(isDirty());
			}
		});

	}

	@Override
	public void createPartControl(Composite parent) {
		initialized = false;
		toolkit = new FormToolkit(parent.getDisplay());
		form = toolkit.createScrolledForm(parent);
		// msgManager = new MessageManager(getForm());
		// msgManager.setAutoUpdate(false);

		toolkit.decorateFormHeading(getForm().getForm());
		getForm().getBody().setLayout(getBodyLayout());
		getForm().setText(getTitleLong());
		// toolbar
		initActions();
		initToolbar(getForm().getToolBarManager());

		getForm().getToolBarManager().add(infoAction);
		getForm().getToolBarManager().add(new Separator());
		getForm().getToolBarManager().add(saveAction);
		getForm().getToolBarManager().update(true);

		updateTitle();

		initContent();

		postInitContent();

		beanToFields();
		if (beanGet().getId() != null) //
			beanUnchanged();
		initialized = true;
	}

	protected void applySectionLayoutDataWorkaround(Section section) {
		if (section.getLayoutData() instanceof TableWrapData) //
			((TableWrapData) section.getLayoutData()).maxWidth = section.getSize().x;
	}

	/**
	 * 
	 */
	protected void postInitContent() {

	}

	/**
	 * @param toolBarManager
	 */
	protected void initToolbar(IToolBarManager toolBarManager) {

	}

	/**
	 * 
	 */
	protected void initContent() {

	}

	@Override
	public void setFocus() {

	}

}
