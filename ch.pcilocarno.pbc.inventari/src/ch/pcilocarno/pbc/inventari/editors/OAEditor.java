/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.editors;

import static ch.pcilocarno.pbc.inventari.util.Tools.installContentProposalAsync;

import java.math.BigDecimal;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Layout;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.progress.UIJob;

import ch.pcilocarno.pbc.inventari.Activator;
import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.beans.ComboObj;
import ch.pcilocarno.pbc.inventari.beans.ImageAddedListener;
import ch.pcilocarno.pbc.inventari.beans.ImageRemovedListener;
import ch.pcilocarno.pbc.inventari.beans.ImageSectionHelper;
import ch.pcilocarno.pbc.inventari.beans.RadioBehavior;
import ch.pcilocarno.pbc.inventari.data.model.Immagine;
import ch.pcilocarno.pbc.inventari.data.model.OperaArte;
import ch.pcilocarno.pbc.inventari.data.model.enums.Livello;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.model.enums.StatoConservazione;
import ch.pcilocarno.pbc.inventari.data.model.enums.StatoOpera;
import ch.pcilocarno.pbc.inventari.data.model.enums.TipoTabella;
import ch.pcilocarno.pbc.inventari.data.persistence.IPersistence;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.dialogs.EditSVGdialog;
import ch.pcilocarno.pbc.inventari.util.FileUtils;
import ch.pcilocarno.pbc.inventari.util.Tools;
import ch.pcilocarno.pbc.inventari.views.OperaArteView;

public class OAEditor extends EditorAdapter<OperaArte> {
	public static final String ID = "ch.pcilocarno.pbc.inventari.editors.OAEditor";

	ImageSectionHelper imgSectionHelper;

	RadioBehavior radioBeTipo, radioBeScForm, radioBeScStrutt;

	private Text fDenominazione, fAltraDenominazione, fGenere, fGenereGruppo, fQuantita, fCollocazione;

	private Text fNoPci, fNoUbc, fFoto;

	private Text fMateriaTecnica, fMisAlt, fMisLarg, fMisProf, fMisPeso, fMisCcAlt, fMisCcLarg, fMisCcProf, fMisCcPeso;

	private Text fAutore, fDatazione;

	private Text fPciOsservazioni, fPciPersone, fPciModProtezione, fPciMateriale, fPciAttrezzi, fPciNote, fNoPrec;

	private ComboObj<BigDecimal> fPciOre;

	private Text fED1descrizione, fED1posizione, fED1tecnica, fED1tipo;

	private Text fED2descrizione, fED2posizione, fED2tecnica, fED2tipo;

	private Text fED3descrizione, fED3posizione, fED3tecnica, fED3tipo;

	private Spinner fLivello;

	private Button fStatoMobile, fStatoImmobile, fPrioritario;

	private Button fDaVerificare;

	private Text fDaVerificareNote;

	private Section sezioneElDist;

	Section imgSection, sezioneDaVerificare;

	Action aggiungiDTaction;

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.EditorAdapter#dispose()
	 */
	@Override
	public void dispose() {
		imgSectionHelper.dispose();
		sezioneDaVerificare.dispose();
		imgSection.dispose();
		sezioneDaVerificare.dispose();
		imgSectionHelper.getImgviewer().stopAndDeregisterPollers();

		super.dispose();
	}

	@Override
	protected void initToolbar(IToolBarManager toolBarManager) {
		aggiungiDTaction = new Action("Aggiungi disegno tecnico", Activator.getImageDescriptor(Icons.MEASURES_16)) {
			@Override
			public void run() {

				if (isDirty()) // salva editor
					doSave(new NullProgressMonitor());
				if (isDirty()) // non ha salvato
					return;

				EditSVGdialog dia = new EditSVGdialog(getSite().getShell());
				if (dia.open() != Dialog.OK)
					return;

				if (dia.getCreatedFile() != null) {
					Immagine nu = new Immagine(beanGet().getId());
					try {
						nu.setContenuto(dia.getCreatedFile());
					} catch (Exception e) {
						e.printStackTrace();
					}
					nu.setNome("DisegnoTecnico");
					nu.setEstensione("jpg");

					nu = PersistenceFactory.IMGlocal().saveOrUpdate(nu);
				}

				if (dia.getModelloProtezione() != null) {
					Immagine nu = new Immagine(beanGet().getId());
					try {
						nu.setContenuto(dia.getModelloProtezione());
					} catch (Exception e) {
						e.printStackTrace();
					}
					nu.setNome("ModelloProtezione");
					nu.setEstensione("jpg");

					nu = PersistenceFactory.IMGlocal().saveOrUpdate(nu);
				}

				updateBean(PersistenceFactory.OAlocal().findById(beanGet().getId()));
				beanUnchanged();

				imgSectionHelper.getImgviewer().refresh();
				imgSectionHelper.getImgviewer().selectLast();
			}
		};
		aggiungiDTaction.setEnabled(false);
		toolBarManager.add(aggiungiDTaction);
	}

	@Override
	protected void checkFields(Helper hlp) {
		hlp.validateFieldInteger(fNoPci, "No. PCI", 1, Integer.MAX_VALUE);
		if (hlp.errorCount == 0) {
			if (!PersistenceFactory.OAlocal().canSave(beanGet(), Tools.toInt(fNoPci.getText()))) {
				hlp.addError("DUPLICATEKEY", "Un oggetto con lo stesso numero esiste!", fNoPci);

			}
		}

		hlp.validateFieldMandatory(fDenominazione, "Denominazione");
		hlp.validateFieldMandatory(fGenere, "Genere");
		hlp.validateFieldInteger(fQuantita, "Quantità", 0, 999999);
		if (radioBeTipo.getSelected() == null) //
			hlp.addError("", "Il campo tipo è obbligatorio.", null);

		if (hlp.errorCount == 0) {
			hlp.validateFieldInteger(fMisAlt, "Altezza (Mis.)", 0, 999999);
			hlp.validateFieldInteger(fMisLarg, "Largh. (Mis.)", 0, 999999);
			hlp.validateFieldInteger(fMisProf, "Prof. (Mis.)", 0, 999999);
			hlp.validateFieldInteger(fMisPeso, "Peso (Mis.)", 0, 999999);
			hlp.validateFieldInteger(fMisCcAlt, "Altezza (Mis. Cc)", 0, 999999);
			hlp.validateFieldInteger(fMisCcLarg, "Largh. (Mis.Cc)", 0, 999999);
			hlp.validateFieldInteger(fMisCcProf, "Prof. (Mis.Cc)", 0, 999999);
			hlp.validateFieldInteger(fMisCcPeso, "Peso (Mis.Cc)", 0, 999999);

			hlp.validateFieldInteger(fPciPersone, "Persone", 0, 999);
			// hlp.validateFieldDecimal(fPciOre, "Ore", BigDecimal.ZERO, new
			// BigDecimal("999.99"), true);
		}

	}

	@Override
	protected void initContent() {
		createSectionBase();

		createSectionImage();

		createSectionDesc();

		createSectionPCi();

		createSectionElDist();

		createSectionToverify();

		validate();
	}

	@Override
	protected void postInitContent() {
		enableAggiuntiDTaction(beanGet().getId() != null);
	}

	void createSectionImage() {
		imgSection = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		imgSectionHelper = new ImageSectionHelper(imgSection, getToolkit(), false) {
			@Override
			protected boolean canAdd_() {
				return beanGet().getId() != null;
			}

			@Override
			protected Immagine saveSelectedImage_(String name, byte[] ifc) {
				Immagine nu = new Immagine(beanGet().getId());
				nu.setContenuto(ifc);
				nu.setNome(name.substring(0, name.lastIndexOf(".")));
				nu.setEstensione(FileUtils.getExtension(name));
				nu.setPrincipale(PersistenceFactory.IMGlocal().countByIdProp(beanGet().getId()) == 0);
				return PersistenceFactory.IMGlocal().saveOrUpdate(nu);
			}

			@Override
			protected List<Immagine> loadBeans_() {
				List<Immagine> imgs = PersistenceFactory.IMGlocal().findByIDprop(beanGet().getId(), RowStatus.A);

				return imgs;
			}
		};

		imgSectionHelper.getImgviewer().addImageAddedListener(new ImageAddedListener() {
			@Override
			public void imageAdded() {
				if (fFoto.getText().indexOf(imagename) == -1) //
				{
					fFoto.setText(fFoto.getText() + (fFoto.getText().isEmpty() ? "" : ", ") + imagename);
					beanChanged();
				}
				refreshSourceViews();
			}
		});

		imgSectionHelper.getImgviewer().addImageRemovedListener(new ImageRemovedListener() {
			@Override
			public void imageRemoved() {
				refreshSourceViews();
			}
		});
		applySectionLayoutDataWorkaround(imgSection);
	}

	/**
	 * 
	 */
	void createSectionToverify() {
		sezioneDaVerificare = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR | Section.TWISTIE);
		sezioneDaVerificare.setText("Note militi");
		sezioneDaVerificare.setToolTipText("");
		sezioneDaVerificare.setExpanded(false);
		sezioneDaVerificare.setLayoutData(getSectionLayoutData());

		Composite client = getToolkit().createComposite(sezioneDaVerificare, SWT.NONE);
		client.setLayout(new GridLayout(1, false));

		// Label and fields layout Data
		GridData lgd = new GridData();
		lgd.widthHint = 90;
		GridData bfgd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		bfgd.heightHint = 35;

		// add fields and labels
		fDaVerificare = getToolkit().createButton(client, "Da verificare", SWT.CHECK);
		fDaVerificare.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				beanChanged();
			}
		});
		fDaVerificareNote = getToolkit().createText(client, "", SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
		fDaVerificareNote.setLayoutData(bfgd);
		fDaVerificareNote.addModifyListener(modListener);
		fDaVerificareNote.setTextLimit(512);

		getToolkit().paintBordersFor(client);
		sezioneDaVerificare.setClient(client);
		applySectionLayoutDataWorkaround(sezioneDaVerificare);
	}

	@Override
	public String getTitleShort() {
		return beanGet().getId() == null ? "Nuova OA" : (beanGet().getNoPci() + " - " + beanGet().getGenere());
	}

	@Override
	public String getTitleLong() {
		return beanGet().getId() == null ? "Nuova Opera d'Arte (OA)"
				: beanGet().getDenominazione() + " (" + beanGet().getParteArchitettonica().getDenominazione()
						+ " - piano " + beanGet().getParteArchitettonica().getPiano() + ")";
	}

	void changedTipo() {
		boolean immobile = StatoOpera.IMMOBILE.toString().equals(radioBeTipo.getSelected());
		fPciOre.setEnabled(immobile);
		fPciPersone.setEnabled(immobile);
		fPciModProtezione.setEnabled(immobile);
		enableAggiuntiDTaction(immobile);
	}

	void enableAggiuntiDTaction(boolean enable) {
		if (aggiungiDTaction == null)
			return;
		aggiungiDTaction.setEnabled(beanGet().getId() == null ? false : enable);
	}

	@Override
	public void beanToFields() {
		fCollocazione.setText(beanGet().getCollocazione());
		fGenere.setText(beanGet().getGenere());
		fDenominazione.setText(beanGet().getDenominazione());
		fQuantita.setText(beanGet().getQuantita().toString());

		fNoUbc.setText(beanGet().getNoUbc());
		fFoto.setText(beanGet().getFoto());
		fAltraDenominazione.setText(beanGet().getAltraDenominazione());
		fGenereGruppo.setText(beanGet().getGenereGruppo());
		fQuantita.setText(beanGet().getQuantita().toString());
		radioBeTipo.select(beanGet().getStato().toString());
		fLivello.setSelection(
				beanGet().getLivello() == Livello.LIV0 ? 0 : (beanGet().getLivello() == Livello.LIV1 ? 1 : 2));
		// desc
		fMisAlt.setText(beanGet().getMisAltezza().toString());
		fMisLarg.setText(beanGet().getMisLarghezza().toString());
		fMisProf.setText(beanGet().getMisProfondita().toString());
		fMisPeso.setText(beanGet().getMisPeso().toString());
		fMisCcAlt.setText(beanGet().getMisCcAltezza().toString());
		fMisCcLarg.setText(beanGet().getMisCcLarghezza().toString());
		fMisCcProf.setText(beanGet().getMisCcProfondita().toString());
		fMisCcPeso.setText(beanGet().getMisCcPeso().toString());
		fMateriaTecnica.setText(beanGet().getMateriaTecnica());
		fAutore.setText(beanGet().getAutore());
		fDatazione.setText(beanGet().getDatazione());
		radioBeScForm.select(beanGet().getConsFormale().toString());
		radioBeScStrutt.select(beanGet().getConsStrutturale().toString());
		// el.ti desc.
		fED1tipo.setText(beanGet().getEd1Tipo());
		fED1descrizione.setText(beanGet().getEd1Descrizione());
		fED1tecnica.setText(beanGet().getEd1Tecnica());
		fED1posizione.setText(beanGet().getEd1Posizione());
		fED2tipo.setText(beanGet().getEd2Tipo());
		fED2descrizione.setText(beanGet().getEd2Descrizione());
		fED2tecnica.setText(beanGet().getEd2Tecnica());
		fED2posizione.setText(beanGet().getEd2Posizione());
		fED3tipo.setText(beanGet().getEd3Tipo());
		fED3descrizione.setText(beanGet().getEd3Descrizione());
		fED3tecnica.setText(beanGet().getEd3Tecnica());
		fED3posizione.setText(beanGet().getEd3Posizione());

		if (beanGet().getId() != null && (beanGet().getEd1Tipo().length() > 0 || beanGet().getEd2Tipo().length() > 0
				|| beanGet().getEd3Tipo().length() > 0)) //
			sezioneElDist.setExpanded(true);

		// pci
		fPciOsservazioni.setText(beanGet().getPciOsservazioni());
		fPciOre.setSelectedItem(beanGet().getPciOre());
		// fPciOre.setText(beanGet().getPciOre().toPlainString());
		fPciPersone.setText(beanGet().getPciPersone().toString());
		fPciModProtezione.setText(beanGet().getPciModelloProtezione());
		fPciMateriale.setText(beanGet().getPciMateriale());
		fPciAttrezzi.setText(beanGet().getPciAttrezzi());
		fPciNote.setText(beanGet().getNote() == null ? "" : beanGet().getNote());
		fNoPrec.setText(beanGet().getNoPrec());
		fPrioritario.setSelection(beanGet().getPrioritario());

		fDaVerificare.setSelection(beanGet().getDaVerificare());
		fDaVerificareNote.setText(beanGet().getDaVerificareNote());
		if (beanGet().getDaVerificare() || !beanGet().getDaVerificareNote().isEmpty()) //
			sezioneDaVerificare.setExpanded(true);

		fNoPci.setText(beanGet().getNoPci() == null ? "" : beanGet().getNoPci().toString());
		if (fPciOre.getSelectedItem() == null)
			fPciOre.select(0);
		// if (beanGet().getId() == null && beanGet().getNoPci() !=null) //
		// fNoPci.setText(beanGet().getNoPci() + "");
		changedTipo();

		new Job("Refresh Image") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				// if (Dao.IMG.loadOAimages(beanGet()).size() > 0) //
				new UIJob(Display.getDefault(), "") {

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						try {
							imgSectionHelper.getImgviewer().refresh();
						} catch (Exception ex) {
							ex.printStackTrace();
						}
						return Status.OK_STATUS;
					}
				}.schedule();
				return Status.OK_STATUS;
			}
		}.schedule();
	}

	@Override
	public void beanFromFields() {
		// Base
		beanGet().setAltraDenominazione(fAltraDenominazione.getText());
		beanGet().setAutore(fAutore.getText());
		beanGet().setDenominazione(fDenominazione.getText());
		beanGet().setFoto(fFoto.getText());
		beanGet().setGenere(fGenere.getText());
		beanGet().setGenereGruppo(fGenereGruppo.getText());
		beanGet().setMateriaTecnica(fMateriaTecnica.getText());
		beanGet().setNoPci(Tools.toInt(fNoPci.getText()));
		beanGet().setNoUbc(fNoUbc.getText());
		beanGet().setQuantita(Integer.parseInt(fQuantita.getText()));
		beanGet().setStato(StatoOpera.valueOf(radioBeTipo.getSelected()));

		// Descr
		beanGet().setMisAltezza(Integer.parseInt(fMisAlt.getText()));
		beanGet().setMisLarghezza(Integer.parseInt(fMisLarg.getText()));
		beanGet().setMisProfondita(Integer.parseInt(fMisProf.getText()));
		beanGet().setMisPeso(Integer.parseInt(fMisPeso.getText()));
		beanGet().setMisCcAltezza(Integer.parseInt(fMisCcAlt.getText()));
		beanGet().setMisCcLarghezza(Integer.parseInt(fMisCcLarg.getText()));
		beanGet().setMisCcProfondita(Integer.parseInt(fMisCcProf.getText()));
		beanGet().setMisCcPeso(Integer.parseInt(fMisCcPeso.getText()));
		beanGet().setMateriaTecnica(fMateriaTecnica.getText());
		beanGet().setAutore(fAutore.getText());
		beanGet().setDatazione(fDatazione.getText());
		beanGet().setConsFormale(StatoConservazione.valueOf(radioBeScForm.getSelected()));
		beanGet().setConsStrutturale(StatoConservazione.valueOf(radioBeScStrutt.getSelected()));

		// El.ti Dist
		beanGet().setEd1Tipo(fED1tipo.getText());
		beanGet().setEd1Descrizione(fED1descrizione.getText());
		beanGet().setEd1Posizione(fED1posizione.getText());
		beanGet().setEd1Tecnica(fED1tecnica.getText());

		beanGet().setEd2Tipo(fED2tipo.getText());
		beanGet().setEd2Descrizione(fED2descrizione.getText());
		beanGet().setEd2Posizione(fED2posizione.getText());
		beanGet().setEd2Tecnica(fED2tecnica.getText());

		beanGet().setEd3Tipo(fED3tipo.getText());
		beanGet().setEd3Descrizione(fED3descrizione.getText());
		beanGet().setEd3Posizione(fED3posizione.getText());
		beanGet().setEd3Tecnica(fED3tecnica.getText());

		// Locat
		beanGet().setCollocazione(fCollocazione.getText());
		beanGet().setLivello(fLivello.getSelection() == 0 ? Livello.LIV0
				: (fLivello.getSelection() == 1 ? Livello.LIV1 : Livello.LIV2));

		// PCi
		beanGet().setPciOsservazioni(fPciOsservazioni.getText());
		beanGet().setPciMateriale(fPciMateriale.getText());
		beanGet().setPciAttrezzi(fPciAttrezzi.getText());
		beanGet().setNote(fPciNote.getText());
		beanGet().setNoPrec(fNoPrec.getText());
		beanGet().setPrioritario(fPrioritario.getSelection());

		if (StatoOpera.IMMOBILE.toString().equals(radioBeTipo.getSelected())) {
			beanGet().setPciOre(fPciOre.getSelectedItem());
			beanGet().setPciPersone(Tools.toInt(fPciPersone.getText()));
			beanGet().setPciModelloProtezione(fPciModProtezione.getText());
		} else {
			beanGet().setPciOre(BigDecimal.ZERO);
			beanGet().setPciPersone(0);
			beanGet().setPciModelloProtezione("");
		}

		beanGet().setDaVerificare(fDaVerificare.getSelection());
		beanGet().setDaVerificareNote(fDaVerificareNote.getText());
	}

	void createSectionDesc() {
		// // base
		Section desc = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		desc.setText("Dati descrittivi");
		desc.setExpanded(true);
		desc.setLayoutData(getSectionLayoutData());

		Composite client = getToolkit().createComposite(desc, SWT.NONE);
		client.setLayout(getSectionClientLayout());
		GridData lgd = new GridData();
		lgd.widthHint = 90;
		GridData fgd = new GridData(SWT.FILL, SWT.CENTER, true, false);

		// add fields and labels
		Label l = getToolkit().createLabel(client, "Misure (cm)");
		l.setLayoutData(lgd);
		Composite misure = getMisureComposite(client);
		misure.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		l = getToolkit().createLabel(client, "Misure cc");
		l.setLayoutData(lgd);
		Composite misureCc = getMisureCcComposite(client);
		misureCc.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		l = getToolkit().createLabel(client, "Mat. e tecnica");
		l.setLayoutData(lgd);
		fMateriaTecnica = getToolkit().createText(client, "");
		fMateriaTecnica.setLayoutData(fgd);
		fMateriaTecnica.addModifyListener(modListener);
		fMateriaTecnica.setTextLimit(128);
		installContentProposalAsync(fMateriaTecnica, TipoTabella.MATERIAETECNICA,
				ContentProposalAdapter.PROPOSAL_IGNORE, true);

		l = getToolkit().createLabel(client, "Autore");
		l.setLayoutData(lgd);
		fAutore = getToolkit().createText(client, "");
		fAutore.setLayoutData(fgd);
		fAutore.addModifyListener(modListener);
		installContentProposalAsync(fAutore, TipoTabella.AUTORI, ContentProposalAdapter.PROPOSAL_REPLACE, false);
		fAutore.setTextLimit(128);

		l = getToolkit().createLabel(client, "Datazione");
		l.setLayoutData(lgd);
		fDatazione = getToolkit().createText(client, "");
		fDatazione.setLayoutData(fgd);
		fDatazione.addModifyListener(modListener);
		fDatazione.setTextLimit(32);

		l = getToolkit().createLabel(client, "St. cons. form.");
		l.setLayoutData(lgd);
		l.setToolTipText("Stato di conservazione formale");

		Composite radiosSCF = getToolkit().createComposite(client);
		Button scf_buono = getToolkit().createButton(radiosSCF, "Buono", SWT.RADIO);
		Button scf_discr = getToolkit().createButton(radiosSCF, "Discreto", SWT.RADIO);
		Button scf_pessi = getToolkit().createButton(radiosSCF, "Pessimo", SWT.RADIO);
		scf_buono.setData("keyv", StatoConservazione.BUONO.toString());
		scf_discr.setData("keyv", StatoConservazione.DISCRETO.toString());
		scf_pessi.setData("keyv", StatoConservazione.PESSIMO.toString());
		GridLayoutFactory.fillDefaults().numColumns(3).equalWidth(true).margins(0, 0).spacing(0, 0).applyTo(radiosSCF);
		radioBeScForm = new RadioBehavior(new Button[] { scf_buono, scf_discr, scf_pessi });
		SelectionListener radioSel = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				radioBeScForm.select((Button) e.widget);
				beanChanged();
				validate();
			}
		};
		scf_buono.addSelectionListener(radioSel);
		scf_discr.addSelectionListener(radioSel);
		scf_pessi.addSelectionListener(radioSel);

		l = getToolkit().createLabel(client, "St. cons. strutt.");
		l.setToolTipText("Stato di conservazione strutturale");
		l.setLayoutData(lgd);
		Composite radiosSCS = getToolkit().createComposite(client);
		Button scs_buono = getToolkit().createButton(radiosSCS, "Buono", SWT.RADIO);
		Button scs_discr = getToolkit().createButton(radiosSCS, "Discreto", SWT.RADIO);
		Button scs_pessi = getToolkit().createButton(radiosSCS, "Pessimo", SWT.RADIO);
		scs_buono.setData("keyv", StatoConservazione.BUONO.toString());
		scs_discr.setData("keyv", StatoConservazione.DISCRETO.toString());
		scs_pessi.setData("keyv", StatoConservazione.PESSIMO.toString());
		GridLayoutFactory.fillDefaults().numColumns(3).equalWidth(true).margins(0, 0).spacing(0, 0).applyTo(radiosSCS);
		radioBeScStrutt = new RadioBehavior(new Button[] { scs_buono, scs_discr, scs_pessi });
		SelectionListener radioSel2 = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				radioBeScStrutt.select((Button) e.widget);
				beanChanged();
				validate();
			}
		};
		scs_buono.addSelectionListener(radioSel2);
		scs_discr.addSelectionListener(radioSel2);
		scs_pessi.addSelectionListener(radioSel2);

		// set section
		getToolkit().paintBordersFor(client);
		desc.setClient(client);
		applySectionLayoutDataWorkaround(desc);
	}

	Composite getMisureComposite(Composite parent) {
		GridData fgd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		fgd.widthHint = 10;
		Composite misure = getToolkit().createComposite(parent);
		misure.setLayoutData(fgd);
		GridLayout misLayout = new GridLayout(8, false);
		misLayout.marginWidth = 1;
		misure.setLayout(misLayout);
		getToolkit().createLabel(misure, "A");
		fMisAlt = getToolkit().createText(misure, "");
		fMisAlt.setLayoutData(fgd);
		fMisAlt.setTextLimit(6);

		getToolkit().createLabel(misure, "L");
		fMisLarg = getToolkit().createText(misure, "");
		fMisLarg.setLayoutData(fgd);
		fMisLarg.setTextLimit(6);

		getToolkit().createLabel(misure, "P");
		fMisProf = getToolkit().createText(misure, "");
		fMisProf.setLayoutData(fgd);
		fMisProf.setTextLimit(6);

		getToolkit().createLabel(misure, "Kg");
		fMisPeso = getToolkit().createText(misure, "");
		fMisPeso.setLayoutData(fgd);
		fMisPeso.setTextLimit(6);

		fMisAlt.addModifyListener(modListener);
		fMisLarg.addModifyListener(modListener);
		fMisProf.addModifyListener(modListener);
		fMisPeso.addModifyListener(modListener);

		fMisAlt.addFocusListener(focusGainedSelectAll);
		fMisLarg.addFocusListener(focusGainedSelectAll);
		fMisProf.addFocusListener(focusGainedSelectAll);
		fMisPeso.addFocusListener(focusGainedSelectAll);

		getToolkit().paintBordersFor(misure);
		return misure;
	}

	Composite getMisureCcComposite(Composite parent) {

		GridData fgd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		fgd.widthHint = 10;
		Composite misure = getToolkit().createComposite(parent);
		misure.setLayoutData(fgd);
		GridLayout misLayout = new GridLayout(8, false);
		misLayout.marginWidth = 1;
		misure.setLayout(misLayout);
		getToolkit().createLabel(misure, "A");
		fMisCcAlt = getToolkit().createText(misure, "");
		fMisCcAlt.setLayoutData(fgd);
		fMisCcAlt.setTextLimit(6);

		getToolkit().createLabel(misure, "L");
		fMisCcLarg = getToolkit().createText(misure, "");
		fMisCcLarg.setLayoutData(fgd);
		fMisCcLarg.setTextLimit(6);

		getToolkit().createLabel(misure, "P");
		fMisCcProf = getToolkit().createText(misure, "");
		fMisCcProf.setLayoutData(fgd);
		fMisCcProf.setTextLimit(6);

		getToolkit().createLabel(misure, "Kg");
		fMisCcPeso = getToolkit().createText(misure, "");
		fMisCcPeso.setLayoutData(fgd);
		fMisCcPeso.setTextLimit(6);

		fMisCcAlt.addModifyListener(modListener);
		fMisCcLarg.addModifyListener(modListener);
		fMisCcProf.addModifyListener(modListener);
		fMisCcPeso.addModifyListener(modListener);

		fMisCcAlt.addFocusListener(focusGainedSelectAll);
		fMisCcLarg.addFocusListener(focusGainedSelectAll);
		fMisCcProf.addFocusListener(focusGainedSelectAll);
		fMisCcPeso.addFocusListener(focusGainedSelectAll);

		getToolkit().paintBordersFor(misure);
		return misure;
	}

	void createSectionPCi() {
		Section section = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		section.setText("Dati PCi");
		section.setExpanded(true);
		section.setLayoutData(getSectionLayoutData());

		Composite client = getToolkit().createComposite(section, SWT.NONE);
		client.setLayout(getSectionClientLayout());

		// Label and fields layout Data
		GridData lgd = new GridData();
		lgd.widthHint = 90;
		GridData fgd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		GridData bfgd = new GridData(SWT.FILL, SWT.CENTER, true, false);
		bfgd.heightHint = 35;

		// add fields and labels
		Label l = getToolkit().createLabel(client, "Osservazioni");
		l.setLayoutData(lgd);
		fPciOsservazioni = getToolkit().createText(client, "", SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
		fPciOsservazioni.setLayoutData(bfgd);
		fPciOsservazioni.addModifyListener(modListener);
		fPciOsservazioni.setTextLimit(512);

		l = getToolkit().createLabel(client, "Ore");
		l.setLayoutData(lgd);
		fPciOre = new ComboObj<BigDecimal>(client, SWT.SINGLE | SWT.READ_ONLY) {
			@Override
			protected void populate() {
				addItem(new BigDecimal("0.00"), "Scegliere un valore");
				addItem(new BigDecimal("0.50"), "Mezz'ora");
				addItem(new BigDecimal("1.00"), "Un'ora");
				addItem(new BigDecimal("1.50"), "Un'ora e mezza");
				addItem(new BigDecimal("2"), "Due ore");
				addItem(new BigDecimal("2.50"), "Due ore e mezza");
				addItem(new BigDecimal("3.00"), "Tre ore");
				addItem(new BigDecimal("4.00"), "Quattro ore");
				addItem(new BigDecimal("5.00"), "Cinque ore");
				addItem(new BigDecimal("6.00"), "Sei ore");
				addItem(new BigDecimal("8.00"), "Otto ore");
				addItem(new BigDecimal("10.00"), "Dieci ore");
				addItem(new BigDecimal("12.00"), "Dodici ore");
			}
		};
		// fPciOre = getToolkit().createText(client, "");
		getToolkit().adapt(fPciOre, true, true);
		fPciOre.setBackground(client.getBackground());
		fPciOre.setLayoutData(fgd);
		fPciOre.addModifyListener(modListener);
		// fPciOre.setTextLimit(3);

		l = getToolkit().createLabel(client, "Persone");
		l.setLayoutData(lgd);
		fPciPersone = getToolkit().createText(client, "");
		fPciPersone.setLayoutData(fgd);
		fPciPersone.addModifyListener(modListener);
		fPciPersone.setTextLimit(3);

		l = getToolkit().createLabel(client, "Modello di prot.");
		l.setLayoutData(lgd);
		fPciModProtezione = getToolkit().createText(client, "");
		fPciModProtezione.setLayoutData(fgd);
		fPciModProtezione.addModifyListener(modListener);
		fPciModProtezione.setTextLimit(256);

		l = getToolkit().createLabel(client, "Materiale");
		l.setLayoutData(lgd);
		fPciMateriale = getToolkit().createText(client, "", SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
		fPciMateriale.setLayoutData(bfgd);
		fPciMateriale.addModifyListener(modListener);
		fPciMateriale.setTextLimit(512);
		Tools.installContentProposalAsync(fPciMateriale, TipoTabella.MATERIALE, ContentProposalAdapter.PROPOSAL_IGNORE,
				true);

		l = getToolkit().createLabel(client, "Attrezzi");
		l.setLayoutData(lgd);
		fPciAttrezzi = getToolkit().createText(client, "", SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
		fPciAttrezzi.setLayoutData(bfgd);
		fPciAttrezzi.addModifyListener(modListener);
		fPciAttrezzi.setTextLimit(512);
		Tools.installContentProposalAsync(fPciAttrezzi, TipoTabella.ATTREZZI, ContentProposalAdapter.PROPOSAL_IGNORE,
				true);

		getToolkit().createLabel(client, "Note").setLayoutData(lgd);
		fPciNote = getToolkit().createText(client, "", SWT.MULTI | SWT.WRAP | SWT.V_SCROLL);
		fPciNote.setLayoutData(bfgd);
		fPciNote.addModifyListener(modListener);
		fPciNote.setTextLimit(512);

		getToolkit().createLabel(client, "No Prec.").setLayoutData(lgd);
		fNoPrec = getToolkit().createText(client, "");
		fNoPrec.setLayoutData(bfgd);
		fNoPrec.addModifyListener(modListener);
		fNoPrec.setTextLimit(32);

		getToolkit().createLabel(client, "").setLayoutData(lgd);
		fPrioritario = getToolkit().createButton(client, "Prioritario", SWT.CHECK);
		fPrioritario.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				if (!areListersEnabled())
					return;
				beanChanged();
				validate();
			}
		});
		getToolkit().createLabel(client, "").setLayoutData(lgd); // SPACER
		getToolkit().createLabel(client, "").setLayoutData(lgd); // SPACER
		// set section
		getToolkit().paintBordersFor(client);
		section.setClient(client);
		applySectionLayoutDataWorkaround(section);
	}

	void createSectionElDist() {
		sezioneElDist = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR | Section.TWISTIE);
		sezioneElDist.setText("Elementi distintivi");
		sezioneElDist.setToolTipText(
				"Un elemento distintivo può essere, per esempio, un'incisione. È possibile riportare al massimo due elementi distintivi per oggetto.	");
		sezioneElDist.setExpanded(false);
		sezioneElDist.setLayoutData(getSectionLayoutData());

		Composite client = getToolkit().createComposite(sezioneElDist, SWT.NONE);
		client.setLayout(getSectionClientLayout());

		// Label and fields layout Data
		GridData lgd = new GridData();
		lgd.widthHint = 90;
		GridData fgd = new GridData(SWT.FILL, SWT.CENTER, true, false);

		// addfields and labels
		Label label = getToolkit().createLabel(client, "(1) Tipo", SWT.NONE);
		label.setLayoutData(lgd);
		fED1tipo = getToolkit().createText(client, "");
		fED1tipo.setTextLimit(64);
		fED1tipo.setLayoutData(fgd);
		fED1tipo.addModifyListener(modListener);

		label = getToolkit().createLabel(client, "(1) Descrizione", SWT.NONE);
		label.setLayoutData(lgd);
		fED1descrizione = getToolkit().createText(client, "");
		fED1descrizione.setLayoutData(fgd);
		fED1descrizione.setTextLimit(256);
		fED1descrizione.addModifyListener(modListener);

		label = getToolkit().createLabel(client, "(1) Posizione", SWT.NONE);
		label.setLayoutData(lgd);
		fED1posizione = getToolkit().createText(client, "");
		fED1posizione.setTextLimit(64);
		fED1posizione.setLayoutData(fgd);
		fED1posizione.addModifyListener(modListener);

		label = getToolkit().createLabel(client, "(1) Tecnica", SWT.NONE);
		label.setLayoutData(lgd);
		fED1tecnica = getToolkit().createText(client, "");
		fED1tecnica.setTextLimit(64);
		fED1tecnica.setLayoutData(fgd);
		fED1tecnica.addModifyListener(modListener);

		getToolkit().createLabel(client, "", SWT.NONE).setLayoutData(lgd);
		getToolkit().createLabel(client, "", SWT.NONE).setLayoutData(fgd);

		label = getToolkit().createLabel(client, "(2) Tipo", SWT.NONE);
		label.setLayoutData(lgd);
		fED2tipo = getToolkit().createText(client, "");
		fED2tipo.setTextLimit(64);
		fED2tipo.setLayoutData(fgd);
		fED2tipo.addModifyListener(modListener);

		label = getToolkit().createLabel(client, "(2) Descrizione", SWT.NONE);
		label.setLayoutData(lgd);
		fED2descrizione = getToolkit().createText(client, "");
		fED2descrizione.setLayoutData(fgd);
		fED2descrizione.setTextLimit(256);
		fED2descrizione.addModifyListener(modListener);

		label = getToolkit().createLabel(client, "(2) Posizione", SWT.NONE);
		label.setLayoutData(lgd);
		fED2posizione = getToolkit().createText(client, "");
		fED2posizione.setTextLimit(64);
		fED2posizione.setLayoutData(fgd);
		fED2posizione.addModifyListener(modListener);

		label = getToolkit().createLabel(client, "(2) Tecnica", SWT.NONE);
		label.setLayoutData(lgd);
		fED2tecnica = getToolkit().createText(client, "");
		fED2tecnica.setTextLimit(64);
		fED2tecnica.setLayoutData(fgd);
		fED2tecnica.addModifyListener(modListener);

		getToolkit().createLabel(client, "", SWT.NONE).setLayoutData(lgd);
		getToolkit().createLabel(client, "", SWT.NONE).setLayoutData(fgd);

		getToolkit().createLabel(client, "(3) Tipo", SWT.NONE).setLayoutData(lgd);
		fED3tipo = getToolkit().createText(client, "");
		fED3tipo.setTextLimit(64);
		fED3tipo.setLayoutData(fgd);
		fED3tipo.addModifyListener(modListener);

		getToolkit().createLabel(client, "(3) Descrizione", SWT.NONE).setLayoutData(lgd);
		fED3descrizione = getToolkit().createText(client, "");
		fED3descrizione.setLayoutData(fgd);
		fED3descrizione.setTextLimit(256);
		fED3descrizione.addModifyListener(modListener);

		getToolkit().createLabel(client, "(3) Posizione", SWT.NONE).setLayoutData(lgd);
		fED3posizione = getToolkit().createText(client, "");
		fED3posizione.setTextLimit(64);
		fED3posizione.setLayoutData(fgd);
		fED3posizione.addModifyListener(modListener);

		getToolkit().createLabel(client, "(3) Tecnica", SWT.NONE).setLayoutData(lgd);
		fED3tecnica = getToolkit().createText(client, "");
		fED3tecnica.setTextLimit(64);
		fED3tecnica.setLayoutData(fgd);
		fED3tecnica.addModifyListener(modListener);

		// add content proposals
		installContentProposalAsync(fED1posizione, TipoTabella.ELDIST_POSIZIONE,
				ContentProposalAdapter.PROPOSAL_REPLACE, true);
		installContentProposalAsync(fED1tipo, TipoTabella.ELDIST_TIPO, ContentProposalAdapter.PROPOSAL_REPLACE, false);
		installContentProposalAsync(fED1tecnica, TipoTabella.ELDIST_TECNICA, ContentProposalAdapter.PROPOSAL_REPLACE,
				true);

		installContentProposalAsync(fED2posizione, TipoTabella.ELDIST_POSIZIONE,
				ContentProposalAdapter.PROPOSAL_REPLACE, true);
		installContentProposalAsync(fED2tipo, TipoTabella.ELDIST_TIPO, ContentProposalAdapter.PROPOSAL_REPLACE, false);
		installContentProposalAsync(fED2tecnica, TipoTabella.ELDIST_TECNICA, ContentProposalAdapter.PROPOSAL_REPLACE,
				true);

		installContentProposalAsync(fED3posizione, TipoTabella.ELDIST_POSIZIONE,
				ContentProposalAdapter.PROPOSAL_REPLACE, true);
		installContentProposalAsync(fED3tipo, TipoTabella.ELDIST_TIPO, ContentProposalAdapter.PROPOSAL_REPLACE, false);
		installContentProposalAsync(fED3tecnica, TipoTabella.ELDIST_TECNICA, ContentProposalAdapter.PROPOSAL_REPLACE,
				true);

		// set section
		getToolkit().paintBordersFor(client);
		sezioneElDist.setClient(client);
		applySectionLayoutDataWorkaround(sezioneElDist);
	}

	Layout getSectionClientLayout() {
		GridLayout gl = new GridLayout(2, false);
		gl.horizontalSpacing = 10;
		return gl;
	}

	Object getSectionLayoutData() {
		return new TableWrapData(TableWrapData.FILL_GRAB);
	}

	void createSectionBase() {
		Section section = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		section.setText("Dati base");
		section.setExpanded(true);
		section.setLayoutData(getSectionLayoutData());

		Composite client = getToolkit().createComposite(section, SWT.NONE);
		client.setLayout(getSectionClientLayout());

		GridData lgd = new GridData();
		lgd.widthHint = 90;
		GridData fgd = new GridData(SWT.FILL, SWT.FILL, true, false);

		Label label = getToolkit().createLabel(client, "No. PCi");
		label.setLayoutData(lgd);
		fNoPci = getToolkit().createText(client, "");
		fNoPci.setLayoutData(fgd);
		fNoPci.addModifyListener(modListener);
		fNoPci.setTextLimit(32);
		Menu menu = new Menu(fNoPci);
		fNoPci.setMenu(menu);

		MenuItem nextNbr = new MenuItem(menu, SWT.PUSH);
		nextNbr.setText("Prossimo numero");
		nextNbr.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				final Integer next = PersistenceFactory.OAlocal().getNextNbr();
				if (next == null)
					return;

				Display.getCurrent().asyncExec(new Runnable() {
					@Override
					public void run() {
						fNoPci.setText("" + next);
					}
				});
			}
		});
		label = getToolkit().createLabel(client, "No. UBC", SWT.NONE);
		label.setLayoutData(lgd);
		fNoUbc = getToolkit().createText(client, "");
		fNoUbc.setLayoutData(fgd);
		fNoUbc.addModifyListener(modListener);
		fNoUbc.setTextLimit(32);

		label = getToolkit().createLabel(client, "Foto");
		label.setLayoutData(lgd);
		fFoto = getToolkit().createText(client, "");
		fFoto.setLayoutData(fgd);
		fFoto.addModifyListener(modListener);
		fFoto.setTextLimit(64);

		label = getToolkit().createLabel(client, "Genere", SWT.NONE);
		label.setLayoutData(lgd);
		fGenere = getToolkit().createText(client, "");
		fGenere.setLayoutData(fgd);
		fGenere.addModifyListener(modListener);
		fGenere.setTextLimit(64);
		installContentProposalAsync(fGenere, TipoTabella.GENERE, ContentProposalAdapter.PROPOSAL_REPLACE, false);

		label = getToolkit().createLabel(client, "Denominazione", SWT.NONE);
		label.setLayoutData(lgd);
		fDenominazione = getToolkit().createText(client, "");
		fDenominazione.setLayoutData(fgd);
		fDenominazione.addModifyListener(modListener);
		fDenominazione.setTextLimit(128);
		fDenominazione.addFocusListener(new FocusAdapter() {
			@Override
			public void focusGained(org.eclipse.swt.events.FocusEvent e) {
				if (fDenominazione.getText().trim().length() == 0)
					fDenominazione.setText(fGenere.getText());
				fDenominazione.selectAll();
			}
		});

		label = getToolkit().createLabel(client, "Altra denom.", SWT.NONE);
		label.setLayoutData(lgd);
		fAltraDenominazione = getToolkit().createText(client, "");
		fAltraDenominazione.setLayoutData(fgd);
		fAltraDenominazione.setTextLimit(256);
		fAltraDenominazione.addModifyListener(modListener);

		label = getToolkit().createLabel(client, "Gruppo", SWT.NONE);
		label.setLayoutData(lgd);
		fGenereGruppo = getToolkit().createText(client, "");
		fGenereGruppo.setLayoutData(fgd);
		fGenereGruppo.addModifyListener(modListener);
		fGenereGruppo.setTextLimit(64);
		installContentProposalAsync(fGenereGruppo, TipoTabella.GENERE_GRUPPO, ContentProposalAdapter.PROPOSAL_REPLACE,
				false);

		label = getToolkit().createLabel(client, "Quantità", SWT.NONE);
		label.setLayoutData(lgd);
		fQuantita = getToolkit().createText(client, "");
		fQuantita.setLayoutData(fgd);
		fQuantita.addModifyListener(modListener);
		fQuantita.setTextLimit(4);
		fQuantita.addFocusListener(focusGainedSelectAll);

		label = getToolkit().createLabel(client, "Tipo", SWT.NONE);
		label.setLayoutData(lgd);

		Composite statoRadios = getToolkit().createComposite(client);
		fStatoMobile = getToolkit().createButton(statoRadios, "Mobile", SWT.RADIO);
		fStatoImmobile = getToolkit().createButton(statoRadios, "Immobile", SWT.RADIO);
		fStatoMobile.setData("keyv", StatoOpera.MOBILE.toString());
		fStatoImmobile.setData("keyv", StatoOpera.IMMOBILE.toString());
		GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(true).margins(0, 0).spacing(0, 0)
				.applyTo(statoRadios);
		radioBeTipo = new RadioBehavior(new Button[] { fStatoMobile, fStatoImmobile });
		SelectionListener radioSel = new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				radioBeTipo.select((Button) e.widget);
				beanChanged();
				changedTipo();
				validate();
			}
		};
		fStatoImmobile.addSelectionListener(radioSel);
		fStatoMobile.addSelectionListener(radioSel);

		label = getToolkit().createLabel(client, "Collocazione", SWT.NONE);
		label.setLayoutData(lgd);
		fCollocazione = getToolkit().createText(client, "");
		fCollocazione.setLayoutData(fgd);
		fCollocazione.setTextLimit(64);
		fCollocazione.addModifyListener(modListener);
		installContentProposalAsync(fCollocazione, TipoTabella.COLLOCAZIONE, ContentProposalAdapter.PROPOSAL_REPLACE,
				true);

		label = getToolkit().createLabel(client, "Livello", SWT.NONE);
		label.setLayoutData(lgd);

		// crtLivello(client);
		Composite lc = getToolkit().createComposite(client);
		// lc.setLayout(new GridLayout(2, false));
		lc.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fLivello = new Spinner(lc, SWT.WRAP);
		fLivello.setLayoutData(new GridData(25, SWT.DEFAULT));
		final Label expl = getToolkit().createLabel(lc, "0 - 1.5 m", SWT.SINGLE | SWT.WRAP);
		expl.setLayoutData(new GridData(100, SWT.DEFAULT));
		fLivello.setMinimum(0);
		fLivello.setMaximum(2);
		fLivello.setSelection(0);
		fLivello.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				expl.setText(" " + (fLivello.getSelection() == 0 ? "0 - 1.5 m"
						: (fLivello.getSelection() == 1 ? "1.5 - 3 m" : "oltre 3 m")));
				beanChanged();
			}
		});
		GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(false).extendedMargins(0, 0, 0, 0).margins(0, 0)
				.spacing(5, 5).applyTo(lc);
		getToolkit().paintBordersFor(client);

		section.setClient(client);
		applySectionLayoutDataWorkaround(section);
	}

	@Override
	public void setFocus() {
		if (beanGet().getId() == null && beanGet().getNoPci() == null) //
			fNoPci.setFocus();
		else
			fGenere.setFocus();
	}

	@Override
	IPersistence<OperaArte> getDao() {
		return PersistenceFactory.OAlocal();
	}

	@Override
	String getSourceViewID() {
		return OperaArteView.ID;
	}
}
