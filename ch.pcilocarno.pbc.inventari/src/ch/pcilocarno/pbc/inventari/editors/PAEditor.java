/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.editors;

import java.util.List;

import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;

import ch.pcilocarno.pbc.inventari.beans.ImageAddedListener;
import ch.pcilocarno.pbc.inventari.beans.ImageSectionHelper;
import ch.pcilocarno.pbc.inventari.data.model.Immagine;
import ch.pcilocarno.pbc.inventari.data.model.ParteArchitettonica;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.model.enums.TipoTabella;
import ch.pcilocarno.pbc.inventari.data.persistence.IPersistence;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.FileUtils;
import ch.pcilocarno.pbc.inventari.util.Tools;
import ch.pcilocarno.pbc.inventari.views.ParteArchitettonicaView;

/**
 * @author elvis
 * 
 */
public class PAEditor extends EditorAdapter<ParteArchitettonica> {
	public static final String ID = "ch.pcilocarno.pbc.inventari.editors.PAEditor";

	Section sectionImage, sectionBase;

	ImageSectionHelper imgSectionHelper;

	Text fNoPci, fNoUbc, fDenominazione, fFoto, fPiano, fAltraDenominazione;

	@Override
	protected void initContent() {
		createSectionBase();
		createSectionImage();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.EditorAdapter#dispose()
	 */
	@Override
	public void dispose() {
		imgSectionHelper.getImgviewer().stopAndDeregisterPollers();
		imgSectionHelper.dispose();
		sectionImage.dispose();
		sectionBase.dispose();
		super.dispose();
	}

	void createSectionImage() {
		sectionImage = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		imgSectionHelper = new ImageSectionHelper(sectionImage, getToolkit(), true) {
			@Override
			protected Immagine saveSelectedImage_(String name, byte[] ifc) {
				Immagine nu = new Immagine(beanGet().getId());
				nu.setContenuto(ifc);
				nu.setNome(name.substring(0, name.lastIndexOf(".")));
				nu.setEstensione(FileUtils.getExtension(name));
				return PersistenceFactory.IMGlocal().saveOrUpdate(nu);
			}

			@Override
			protected List<Immagine> loadBeans_() {
				return PersistenceFactory.IMGlocal().findByIDprop(beanGet().getId(), RowStatus.A);
			}
		};

		imgSectionHelper.getImgviewer().addImageAddedListener(new ImageAddedListener() {
			@Override
			public void imageAdded() {
				if (fFoto.getText().indexOf(imagename) == -1) //
					fFoto.setText(fFoto.getText() + (fFoto.getText().isEmpty() ? "" : ", ") + imagename);
			}
		});
		applySectionLayoutDataWorkaround(sectionImage);
	}

	/**
	 * 
	 */
	void createSectionBase() {
		sectionBase = getToolkit().createSection(getForm().getBody(), Section.TITLE_BAR);
		sectionBase.setText("Dati base");
		sectionBase.setExpanded(true);
		sectionBase.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB));

		Composite fields = getToolkit().createComposite(sectionBase, SWT.NONE);
		GridLayout gl = new GridLayout(2, false);
		gl.horizontalSpacing = 10;
		fields.setLayout(gl);

		GridData lgd = new GridData();
		lgd.widthHint = 90;
		GridData fgd = new GridData(SWT.FILL, SWT.FILL, true, false);

		getToolkit().createLabel(fields, "Denominazione").setLayoutData(lgd);
		fDenominazione = getToolkit().createText(fields, "");
		fDenominazione.setLayoutData(fgd);
		fDenominazione.setTextLimit(64);

		Tools.installContentProposalAsync(fDenominazione, TipoTabella.PARTEARCH,
				ContentProposalAdapter.PROPOSAL_REPLACE, true);

		getToolkit().createLabel(fields, "Piano").setLayoutData(lgd);
		fPiano = getToolkit().createText(fields, "");
		fPiano.setTextLimit(4);
		fPiano.setLayoutData(fgd);

		getToolkit().createLabel(fields, "Altra denom.").setLayoutData(lgd);
		fAltraDenominazione = getToolkit().createText(fields, "");
		fAltraDenominazione.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fAltraDenominazione.setTextLimit(64);

		getToolkit().createLabel(fields, "No. PCi").setLayoutData(lgd);
		fNoPci = getToolkit().createText(fields, "");
		fNoPci.setLayoutData(fgd);
		fNoPci.setTextLimit(32);

		getToolkit().createLabel(fields, "No UBC").setLayoutData(lgd);
		fNoUbc = getToolkit().createText(fields, "");
		fNoUbc.setLayoutData(fgd);
		fNoUbc.setTextLimit(32);

		getToolkit().createLabel(fields, "Foto").setLayoutData(lgd);
		fFoto = getToolkit().createText(fields, "");
		fFoto.setLayoutData(fgd);
		fFoto.setTextLimit(64);

		fDenominazione.addModifyListener(modListener);
		fPiano.addModifyListener(modListener);
		fAltraDenominazione.addModifyListener(modListener);
		fNoPci.addModifyListener(modListener);
		fNoUbc.addModifyListener(modListener);
		fFoto.addModifyListener(modListener);

		getToolkit().createLabel(fields, "").setLayoutData(lgd);
		getToolkit().createLabel(fields, "").setLayoutData(fgd);
		getToolkit().createLabel(fields, "").setLayoutData(lgd);
		getToolkit().createLabel(fields, "").setLayoutData(fgd);
		getToolkit().createLabel(fields, "").setLayoutData(lgd);
		getToolkit().createLabel(fields, "").setLayoutData(fgd);

		getToolkit().paintBordersFor(fields);

		sectionBase.setClient(fields);
		applySectionLayoutDataWorkaround(sectionBase);
	}

	@Override
	protected void checkFields(Helper hlp) {
		hlp.validateFieldMandatory(fDenominazione, "Denominazione");
		hlp.validateFieldInteger(fPiano, "Piano", -99, 99);
	}

	@Override
	public String getTitleLong() {
		return beanGet().getId() == null ? "Nuova Parte Architettonica (PA)"
				: beanGet().getNoPci() + " " + beanGet().getDenominazione() + ", Piano " + beanGet().getPiano();
	}

	@Override
	public String getTitleShort() {
		if (beanGet().getId() == null)
			return "Nuova PA";
		else
			return beanGet().getDenominazione();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		fDenominazione.setFocus();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.IBeanEditor#beanFromFields()
	 */
	@Override
	public void beanFromFields() {
		beanGet().setNoPci(fNoPci.getText());
		beanGet().setNoUbc(fNoUbc.getText());
		beanGet().setDenominazione(fDenominazione.getText());
		beanGet().setAltraDenominazione(fAltraDenominazione.getText());
		beanGet().setPiano(Integer.parseInt(fPiano.getText()));
		beanGet().setFoto(fFoto.getText());
		imgSectionHelper.getImgviewer().doSaveCaption();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.IBeanEditor#beanToFields()
	 */
	@Override
	public void beanToFields() {
		fNoPci.setText(beanGet().getNoPci());
		fNoUbc.setText(beanGet().getNoUbc());
		fDenominazione.setText(beanGet().getDenominazione());
		fAltraDenominazione.setText(beanGet().getAltraDenominazione());
		fFoto.setText(beanGet().getFoto());
		fPiano.setText(beanGet().getPiano().toString());

		imgSectionHelper.getImgviewer().refresh();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.EditorAdapter#getSourceViewID()
	 */
	@Override
	String getSourceViewID() {
		return ParteArchitettonicaView.ID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.editors.EditorAdapter#getDao()
	 */
	@Override
	IPersistence<ParteArchitettonica> getDao() {
		return PersistenceFactory.PAlocal();
	}
}
