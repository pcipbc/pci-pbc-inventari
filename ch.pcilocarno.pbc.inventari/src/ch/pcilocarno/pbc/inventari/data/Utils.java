/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data;

import java.lang.reflect.Method;

import ch.pcilocarno.pbc.inventari.data.persistence.PersistentObject;

public class Utils
{
	/**
	 * @param o1
	 * @param o2
	 * @return
	 */
	public static final boolean equals(PersistentObject o1, PersistentObject o2)
	{
		if (o1 == null || o2 == null) throw new NullPointerException();
		if (o1.getId() == null && o2.getId() == null) return true;
		if (o1.getId() == null && o2.getId() != null) return false;
		return o1.getId().equals(o2.getId());
	}

	/**
	 * @param src
	 * @param dest
	 * @throws Exception
	 */
	public static final void copyValues(PersistentObject src, PersistentObject dest) throws Exception
	{
		if (src == null || dest == null) throw new IllegalArgumentException("src or dest classes can't be null.");
		if (src.getClass() != dest.getClass()) throw new IllegalArgumentException("Trying to copy values between different classes. [src=" + src.getClass().getName() + ", dest=" + dest.getClass().getName() + "]");
		String[] ignoreMethods = new String[] { "getClass", "getId", "getVersion" };
		//
		Method[] srcGetMethods = src.getClass().getMethods();
		for (Method m : srcGetMethods)
		{
			if (!m.getName().startsWith("get")) continue;
			boolean ignore = false;
			for (String im : ignoreMethods)
				if (m.getName().equals(im)) ignore = true;
			if (ignore) continue;

			Object result = m.invoke(src, new Object[0]);
			Method sm = dest.getClass().getMethod("s" + m.getName().substring(1), m.getReturnType());
			if (sm == null) throw new RuntimeException("Cannot find method s" + m.getName().substring(1) + " on class " + dest.getClass().getName());
			sm.invoke(dest, result);
			sm = null;
			result = null;
		}
	}
}
