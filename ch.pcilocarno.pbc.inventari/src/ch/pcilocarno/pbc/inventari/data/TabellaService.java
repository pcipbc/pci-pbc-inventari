/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data;

import java.util.LinkedHashMap;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import ch.pcilocarno.pbc.inventari.data.model.enums.TipoTabella;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;

/**
 * ch.pcilocarno.pbc.inventari.data.TabellaService
 * 
 * @author elvis on Sep 10, 2009
 * @version @@version@@ build @@build@@
 */
public class TabellaService
{
	static final Map<TipoTabella, String[]> tabValuesMap = new LinkedHashMap<TipoTabella, String[]>(TipoTabella.values().length);

	private TabellaService()
	{
	}

	/**
	 * @param tipo
	 * @return
	 */
	public static final String[] getValues(TipoTabella tipo)
	{
		if (!tabValuesMap.containsKey(tipo)) //
			tabValuesMap.put(tipo, PersistenceFactory.Tlocal().loadValoriByTipo(tipo));

		return tabValuesMap.get(tipo);

	}

	/**
	 * 
	 */
	public static final void reload()
	{
		tabValuesMap.clear();
		loadTables();
	}

	/**
	 * 
	 */
	public static final void loadTables()
	{
		Job j = new Job("Carica dati tabelle comuni in memoria.")
		{

			@Override
			protected IStatus run(IProgressMonitor monitor)
			{
				for (TipoTabella each : TipoTabella.values())
					getValues(each);
				return Status.OK_STATUS;
			}
		};
		j.setPriority(Job.LONG);
		j.schedule();
	}
}
