/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data;

import java.util.List;

import org.eclipse.core.runtime.ListenerList;

import ch.pcilocarno.pbc.inventari.data.ISessionListener.Event;
import ch.pcilocarno.pbc.inventari.data.model.Architettura;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;

/**
 * ch.pcilocarno.pbc.inventari.data.Session
 * 
 * @author elvis on Sep 9, 2009
 * @version @@version@@ build @@build@@
 */
public class Session {

	public static final Session INSTANCE = new Session();

	String username;

	String architetturaUUID;

	boolean definitivo = false;

	final ListenerList<ISessionListener> listeners = new ListenerList<ISessionListener>();

	/**
	 * Private constructor<br>
	 * Use <code>INSTANCE</code> instead
	 */
	private Session() {
	}

	public void addListener(ISessionListener listener) {
		listeners.add(listener);
		// Tools.log("Added listener to session on event %s [hash=%s]",
		// listener.getEvent().toString(), listener.hashCode() + "");
	}

	public void removeListener(ISessionListener listener) {
		listeners.remove(listener);
		// Tools.log("Removed listener from session on event %s [hash=%s]",
		// listener.getEvent().toString(), listener.hashCode() + "");
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *            the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the architetturaUUID
	 */
	public String getArchitetturaUUID() {
		if (architetturaUUID == null) {
			List<Architettura> a = PersistenceFactory.Alocal().findAllByStatusText("");
			if (a == null || a.size() == 0)
				return "";
			architetturaUUID = a.get(0).getId();
		}
		return architetturaUUID;
	}

	protected void setDefinitivo(final boolean definitivo_) {
		this.definitivo = definitivo_;
	}

	public boolean getDefinitivo() {
		return this.definitivo;
	}

	void updateDefinitivo() {
		try {
			// Attenzione: le architetture non sono ancora disponibili localmente!
			setDefinitivo(PersistenceFactory.Alocal().findById(architetturaUUID).getDefinitivo());
		} catch (Exception ex) {
		}
	}

	/**
	 * @param uuid
	 */
	public void setArchitetturaUUID(final String uuid) {
		this.architetturaUUID = uuid;
		updateDefinitivo();

		notifyListeners(Event.A_CHANGE);
	}

	private void notifyListeners(Event event) {
		for (Object each : listeners.getListeners()) {
			if (((ISessionListener) each).getEvent() == event) //
				((ISessionListener) each).handle();
		}
	}
}
