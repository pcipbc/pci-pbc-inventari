/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import ch.pcilocarno.pbc.inventari.data.model.enums.CategoriaBene;
import ch.pcilocarno.pbc.inventari.data.model.enums.Scudi;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistentObject;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;

/**
 * ch.ediesis.pci.pbc.inventari.data.domain.Architettura
 * 
 * @author elvis on Sep 8, 2009
 * @version @@version@@ build @@build@@
 */
@Entity
@NamedQueries({
//
		@NamedQuery(name = Queries.A_FIND_ALL_BY_STATUS_TEXT, query = "select a from Architettura a where a.status=:rs and (a.denominazione like :text OR a.noPci like :text OR a.comune like :text) order by a.comune, a.noPci"), //
		@NamedQuery(name = Queries.A_FIND_ALL_BY_STATUS, query = "select a from Architettura a where a.status=:rs") //
//
})
public class Architettura extends PersistentObject
{
	private static final long serialVersionUID = 8174058011739266016L;

	@Column(length = 32, nullable = false)
	private String noPci = "";

	@Column(length = 32, nullable = false)
	private String noUbc = "";

	@Column(length = 256, nullable = false)
	private String denominazione = "";

	@Enumerated(EnumType.STRING)
	@Column(length = 4, nullable = false)
	private CategoriaBene categoria = CategoriaBene.NONE;

	@Enumerated(EnumType.STRING)
	@Column(length = 8, nullable = false)
	private Scudi scudi = Scudi.NONE;

	@Column(length = 128, nullable = false)
	private String foto = "";

	@Column(length = 64, nullable = false)
	private String indirizzo = "";

	@Column(length = 64, nullable = false)
	private String comune = "";

	@Column(nullable = true)
	private Integer cap = 0;

	@Column(length = 32, nullable = false)
	private String fondoRfd = "";

	@Column(length = 64, nullable = false)
	private String coordinate = "";

	@Column
	private Boolean documentazione = Boolean.FALSE;

	@Column(length = 512, nullable = false)
	private String bibliografia = "";

	// @OneToMany(mappedBy = "architettura", cascade = CascadeType.ALL, targetEntity = Contatto.class, fetch = FetchType.EAGER, orphanRemoval = true)
	// @MapKey(name = "tipo")
	// private Map<TipoContatto, Contatto> contatti = new HashMap<TipoContatto, Contatto>(2);

	// @OneToOne(fetch = FetchType.EAGER, optional = true, cascade = CascadeType.ALL)
	// @JoinColumn(nullable = true)
	// private Contatto proprietario;
	//
	// @OneToOne(fetch = FetchType.EAGER, optional = true, cascade = CascadeType.ALL)
	// @JoinColumn(nullable = true)
	// private Contatto custode;

	@Column(length = 2048, nullable = false)
	private String note = "";

	@Column(length = 256, nullable = false)
	private String responsabilePCi = "";

	@Column(length = 256, nullable = false)
	private String responsabileEvacuazione = "";

	@Column(length = 256, nullable = false)
	private String responsabileProtezione = "";

	@Column(length = 256, nullable = false)
	private String distribuzione1 = "";

	@Column(length = 256, nullable = false)
	private String distribuzione2 = "";

	@Column(length = 256, nullable = false)
	private String distribuzione3 = "";

	@Column(length = 256, nullable = false)
	private String distribuzione4 = "";

	@Column(length = 256, nullable = false)
	private String indicazioniRifugio = "";

	@Column(length = 256, nullable = false)
	private String materialeImballaggio = "";

	@Column(length = 256, nullable = false)
	private String mezziTrasporto = "";

	@Column(length = 2048, nullable = false)
	private String osservazioniEvac = "";

	@Column(nullable = false)
	private Integer qtaMezzi = 0;

	@Column(nullable = false)
	private Integer qtaPersone = 0;

	@Column(nullable = false)
	private BigDecimal qtaOre = BigDecimal.ZERO;

	@Column(nullable = false)
	private Boolean definitivo = Boolean.FALSE;

	@Column(nullable = false, length = 256)
	private String deposito = "";

	@Column(nullable = false, length = 2048)
	private String collabTrasporto = "";

	@OneToMany(mappedBy = "architettura")
	private List<ParteArchitettonica> partiArchitettoniche;

	/**
	 * 
	 */
	public Architettura()
	{
	}

	@Override
	public String toString()
	{
		return getNoPci() + " " + getDenominazione();
	}

	public String getNote()
	{
		return note;
	}

	public void setNote(String note)
	{
		this.note = note;
	}

	/**
	 * @return the noPci
	 */
	public String getNoPci()
	{
		return noPci;
	}

	/**
	 * @param noPci
	 *            the noPci to set
	 */
	public void setNoPci(String noPci)
	{
		this.noPci = noPci;
	}

	/**
	 * @return the noUbc
	 */
	public String getNoUbc()
	{
		return noUbc;
	}

	/**
	 * @param noUbc
	 *            the noUbc to set
	 */
	public void setNoUbc(String noUbc)
	{
		this.noUbc = noUbc;
	}

	/**
	 * @return the denominazione
	 */
	public String getDenominazione()
	{
		return denominazione;
	}

	/**
	 * @param denominazione
	 *            the denominazione to set
	 */
	public void setDenominazione(String denominazione)
	{
		this.denominazione = denominazione;
	}

	/**
	 * @return the categoria
	 */
	public CategoriaBene getCategoria()
	{
		return categoria;
	}

	/**
	 * @param categoria
	 *            the categoria to set
	 */
	public void setCategoria(CategoriaBene categoria)
	{
		this.categoria = categoria;
	}

	/**
	 * @return the foto
	 */
	public String getFoto()
	{
		return foto;
	}

	/**
	 * @param foto
	 *            the foto to set
	 */
	public void setFoto(String foto)
	{
		this.foto = foto;
	}

	/**
	 * @return the indirizzo
	 */
	public String getIndirizzo()
	{
		return indirizzo;
	}

	/**
	 * @param indirizzo
	 *            the indirizzo to set
	 */
	public void setIndirizzo(String indirizzo)
	{
		this.indirizzo = indirizzo;
	}

	/**
	 * @return the comune
	 */
	public String getComune()
	{
		return comune;
	}

	/**
	 * @param comune
	 *            the comune to set
	 */
	public void setComune(String comune)
	{
		this.comune = comune;
	}

	/**
	 * @return the cap
	 */
	public Integer getCap()
	{
		return cap;
	}

	/**
	 * @param cap
	 *            the cap to set
	 */
	public void setCap(Integer cap)
	{
		this.cap = cap;
	}

	/**
	 * @return the fondoRfd
	 */
	public String getFondoRfd()
	{
		return fondoRfd;
	}

	/**
	 * @param fondoRfd
	 *            the fondoRfd to set
	 */
	public void setFondoRfd(String fondoRfd)
	{
		this.fondoRfd = fondoRfd;
	}

	/**
	 * @return the documentazione
	 */
	public Boolean getDocumentazione()
	{
		return documentazione;
	}

	/**
	 * @param documentazione
	 *            the documentazione to set
	 */
	public void setDocumentazione(Boolean documentazione)
	{
		this.documentazione = documentazione;
	}

	/**
	 * @return the bibliografia
	 */
	public String getBibliografia()
	{
		return bibliografia;
	}

	/**
	 * @param bibliografia
	 *            the bibliografia to set
	 */
	public void setBibliografia(String bibliografia)
	{
		this.bibliografia = bibliografia;
	}

	/**
	 * @return
	 */
	public String getCoordinate()
	{
		return coordinate;
	}

	/**
	 * @param coordinate
	 */
	public void setCoordinate(String coordinate)
	{
		this.coordinate = coordinate;
	}

	/**
	 * @return the scudi
	 */
	public Scudi getScudi()
	{
		return scudi;
	}

	/**
	 * @param scudi
	 *            the scudi to set
	 */
	public void setScudi(Scudi scudi)
	{
		this.scudi = scudi;
	}

	/**
	 * @return the responsabilePCi
	 */
	public String getResponsabilePCi()
	{
		return responsabilePCi;
	}

	/**
	 * @param responsabilePCi
	 *            the responsabilePCi to set
	 */
	public void setResponsabilePCi(String responsabilePCi)
	{
		this.responsabilePCi = responsabilePCi;
	}

	/**
	 * @return the distribuzione1
	 */
	public String getDistribuzione1()
	{
		return distribuzione1;
	}

	/**
	 * @param distribuzione1
	 *            the distribuzione1 to set
	 */
	public void setDistribuzione1(String distribuzione1)
	{
		this.distribuzione1 = distribuzione1;
	}

	/**
	 * @return the distribuzione2
	 */
	public String getDistribuzione2()
	{
		return distribuzione2;
	}

	/**
	 * @param distribuzione2
	 *            the distribuzione2 to set
	 */
	public void setDistribuzione2(String distribuzione2)
	{
		this.distribuzione2 = distribuzione2;
	}

	/**
	 * @return the distribuzione3
	 */
	public String getDistribuzione3()
	{
		return distribuzione3;
	}

	/**
	 * @param distribuzione3
	 *            the distribuzione3 to set
	 */
	public void setDistribuzione3(String distribuzione3)
	{
		this.distribuzione3 = distribuzione3;
	}

	/**
	 * @return the distribuzione4
	 */
	public String getDistribuzione4()
	{
		return distribuzione4;
	}

	/**
	 * @param distribuzione4
	 *            the distribuzione4 to set
	 */
	public void setDistribuzione4(String distribuzione4)
	{
		this.distribuzione4 = distribuzione4;
	}

	/**
	 * @return the indicazioniRifugio
	 */
	public String getIndicazioniRifugio()
	{
		return indicazioniRifugio;
	}

	/**
	 * @param indicazioniRifugio
	 *            the indicazioniRifugio to set
	 */
	public void setIndicazioniRifugio(String indicazioniRifugio)
	{
		this.indicazioniRifugio = indicazioniRifugio;
	}

	/**
	 * @return the materialeImballaggio
	 */
	public String getMaterialeImballaggio()
	{
		return materialeImballaggio;
	}

	/**
	 * @param materialeImballaggio
	 *            the materialeImballaggio to set
	 */
	public void setMaterialeImballaggio(String materialeImballaggio)
	{
		this.materialeImballaggio = materialeImballaggio;
	}

	/**
	 * @return the mezziTrasporto
	 */
	public String getMezziTrasporto()
	{
		return mezziTrasporto;
	}

	/**
	 * @param mezziTrasporto
	 *            the mezziTrasporto to set
	 */
	public void setMezziTrasporto(String mezziTrasporto)
	{
		this.mezziTrasporto = mezziTrasporto;
	}

	/**
	 * @return the osservazioniEvac
	 */
	public String getOsservazioniEvac()
	{
		return osservazioniEvac;
	}

	/**
	 * @param osservazioniEvac
	 *            the osservazioniEvac to set
	 */
	public void setOsservazioniEvac(String osservazioniEvac)
	{
		this.osservazioniEvac = osservazioniEvac;
	}

	/**
	 * @return the qtaMezzi
	 */
	public Integer getQtaMezzi()
	{
		return qtaMezzi;
	}

	/**
	 * @param qtaMezzi
	 *            the qtaMezzi to set
	 */
	public void setQtaMezzi(Integer qtaMezzi)
	{
		this.qtaMezzi = qtaMezzi;
	}

	/**
	 * @return the qtaPersone
	 */
	public Integer getQtaPersone()
	{
		return qtaPersone;
	}

	/**
	 * @param qtaPersone
	 *            the qtaPersone to set
	 */
	public void setQtaPersone(Integer qtaPersone)
	{
		this.qtaPersone = qtaPersone;
	}

	/**
	 * @return the qtaOre
	 */
	public BigDecimal getQtaOre()
	{
		return qtaOre;
	}

	/**
	 * @param qtaOre
	 *            the qtaOre to set
	 */
	public void setQtaOre(BigDecimal qtaOre)
	{
		this.qtaOre = qtaOre;
	}

	/**
	 * @return the definitivo
	 */
	public Boolean getDefinitivo()
	{
		return definitivo;
	}

	/**
	 * @param definitivo
	 *            the definitivo to set
	 */
	public void setDefinitivo(Boolean definitivo)
	{
		this.definitivo = definitivo;
	}

	/**
	 * @return the partiArchitettoniche
	 */
	public List<ParteArchitettonica> getPartiArchitettoniche()
	{
		return partiArchitettoniche;
	}

	/**
	 * @param partiArchitettoniche
	 *            the partiArchitettoniche to set
	 */
	public void setPartiArchitettoniche(List<ParteArchitettonica> partiArchitettoniche)
	{
		this.partiArchitettoniche = partiArchitettoniche;
	}

	/**
	 * @return the responsabileProtezione
	 */
	public String getResponsabileProtezione()
	{
		return responsabileProtezione;
	}

	/**
	 * @param responsabileProtezione
	 *            the responsabileProtezione to set
	 */
	public void setResponsabileProtezione(String responsabileProtezione)
	{
		this.responsabileProtezione = responsabileProtezione;
	}

	/**
	 * @return the responsabileEvacuazione
	 */
	public String getResponsabileEvacuazione()
	{
		return responsabileEvacuazione;
	}

	/**
	 * @param responsabileEvacuazione
	 *            the responsabileEvacuazione to set
	 */
	public void setResponsabileEvacuazione(String responsabileEvacuazione)
	{
		this.responsabileEvacuazione = responsabileEvacuazione;
	}

	/**
	 * @return the deposito
	 */
	public String getDeposito()
	{
		return deposito;
	}

	/**
	 * @param deposito
	 *            the deposito to set
	 */
	public void setDeposito(String deposito)
	{
		this.deposito = deposito;
	}

	/**
	 * @return the collabTrasporto
	 */
	public String getCollabTrasporto()
	{
		return collabTrasporto;
	}

	/**
	 * @param collabTrasporto
	 *            the collabTrasporto to set
	 */
	public void setCollabTrasporto(String collabTrasporto)
	{
		this.collabTrasporto = collabTrasporto;
	}

}
