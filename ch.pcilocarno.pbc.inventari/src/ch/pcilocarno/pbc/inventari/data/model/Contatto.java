/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import ch.pcilocarno.pbc.inventari.data.model.enums.TipoContatto;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistentObject;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;

/**
 * ch.ediesis.pci.pbc.inventari.data.domain.Contatto
 * 
 * @author elvis on Sep 8, 2009
 * @version @@version@@ build @@build@@
 */
@NamedQueries({
//
		@NamedQuery(name = Queries.C_FIND_BY_IDPROP_TIPO, query = "select i from Contatto i where i.idProp=:idprop and i.status=:rs and i.tipo=:tipo"),//
		@NamedQuery(name = Queries.C_FIND_BY_IDPROP, query = "select i from Contatto i where i.idProp=:idprop and i.status=:rs"),//
		@NamedQuery(name = Queries.C_FIND_ALL, query = "select i from Contatto i")
//
})
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "idProp", "tipo" }))
@Entity
public class Contatto extends PersistentObject
{
	private static final long serialVersionUID = 8590587561556770606L;

	@Column(length = 36, nullable = false)
	private String idProp;

	@Enumerated(EnumType.STRING)
	@Column(length = 32)
	private TipoContatto tipo;

	@Column(nullable = false, length = 64)
	private String nome = "";

	@Column(length = 64, nullable = false)
	private String cognome = "";

	@Column(length = 64, nullable = false)
	private String indirizzo = "";

	@Column(length = 64, nullable = false)
	private String indirizzo2 = "";

	@Column(nullable = false)
	private Integer cap = 0;

	@Column(length = 64, nullable = false)
	private String domicilio = "";

	@Column(length = 32, nullable = false)
	private String telCasa = "";

	@Column(length = 32, nullable = false)
	private String telUff = "";

	@Column(length = 32, nullable = false)
	private String telNat = "";

	@Column(length = 32, nullable = false)
	private String fax = "";

	@Column(length = 64, nullable = false)
	private String email = "";

	/**
	 * Empty constructor
	 */
	protected Contatto()
	{
	}

	/**
	 * Default constructor
	 * 
	 * @param architettura
	 * @param tipo
	 */
	public Contatto(Architettura architettura, TipoContatto tipo)
	{
		this.tipo = tipo;
		this.idProp = architettura.getId();
	}

	/**
	 * @param propUUID
	 * @param tipo
	 */
	public Contatto(final String propUUID, final TipoContatto tipo)
	{
		this.idProp = propUUID;
		this.tipo = tipo;
	}

	/**
	 * @return the nome
	 */
	public String getNome()
	{
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome)
	{
		this.nome = nome;
	}

	/**
	 * @return the cognome
	 */
	public String getCognome()
	{
		return cognome;
	}

	/**
	 * @param cognome
	 *            the cognome to set
	 */
	public void setCognome(String cognome)
	{
		this.cognome = cognome;
	}

	/**
	 * @return the indirizzo
	 */
	public String getIndirizzo()
	{
		return indirizzo;
	}

	/**
	 * @param indirizzo
	 *            the indirizzo to set
	 */
	public void setIndirizzo(String indirizzo)
	{
		this.indirizzo = indirizzo;
	}

	/**
	 * @return the indirizzo2
	 */
	public String getIndirizzo2()
	{
		return indirizzo2;
	}

	/**
	 * @param indirizzo2
	 *            the indirizzo2 to set
	 */
	public void setIndirizzo2(String indirizzo2)
	{
		this.indirizzo2 = indirizzo2;
	}

	/**
	 * @return the cap
	 */
	public Integer getCap()
	{
		return cap;
	}

	/**
	 * @param cap
	 *            the cap to set
	 */
	public void setCap(Integer cap)
	{
		this.cap = cap;
	}

	/**
	 * @return the domicilio
	 */
	public String getDomicilio()
	{
		return domicilio;
	}

	/**
	 * @param domicilio
	 *            the domicilio to set
	 */
	public void setDomicilio(String domicilio)
	{
		this.domicilio = domicilio;
	}

	/**
	 * @return the telCasa
	 */
	public String getTelCasa()
	{
		return telCasa;
	}

	/**
	 * @param telCasa
	 *            the telCasa to set
	 */
	public void setTelCasa(String telCasa)
	{
		this.telCasa = telCasa;
	}

	/**
	 * @return the telUff
	 */
	public String getTelUff()
	{
		return telUff;
	}

	/**
	 * @param telUff
	 *            the telUff to set
	 */
	public void setTelUff(String telUff)
	{
		this.telUff = telUff;
	}

	/**
	 * @return the telNat
	 */
	public String getTelNat()
	{
		return telNat;
	}

	/**
	 * @param telNat
	 *            the telNat to set
	 */
	public void setTelNat(String telNat)
	{
		this.telNat = telNat;
	}

	/**
	 * @return the fax
	 */
	public String getFax()
	{
		return fax;
	}

	/**
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax)
	{
		this.fax = fax;
	}

	/**
	 * @return the email
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email)
	{
		this.email = email;
	}

	/**
	 * @return the tipo
	 */
	public TipoContatto getTipo()
	{
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(TipoContatto tipo)
	{
		this.tipo = tipo;
	}

	/**
	 * @return the idProp
	 */
	public String getIdProp()
	{
		return idProp;
	}

	/**
	 * @param idProp
	 *            the idProp to set
	 */
	public void setIdProp(String idProp)
	{
		this.idProp = idProp;
	}

}
