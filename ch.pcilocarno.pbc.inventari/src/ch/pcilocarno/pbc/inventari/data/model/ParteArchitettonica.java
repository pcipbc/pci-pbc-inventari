/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import ch.pcilocarno.pbc.inventari.data.persistence.PersistentObject;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;

/**
 * ch.ediesis.pci.pbc.inventari.data.domain.ParteArchitettonica
 * 
 * @author elvis on Sep 8, 2009
 * @version @@version@@ build @@build@@
 */
@Entity
@NamedQueries({
//
		@NamedQuery(name = Queries.PA_FIND_ALL, query = "select p from ParteArchitettonica p"), //
		@NamedQuery(name = Queries.PA_FIND_BY_ARCH, query = "select p from ParteArchitettonica p where p.architettura.id = :archid and p.status=:rs order by p.denominazione, p.piano, p.noPci"), //
		@NamedQuery(name = Queries.PA_COUNT_BY_ARCH, query = "select count(p) from ParteArchitettonica p where p.architettura.id = :archid"), //
		@NamedQuery(name = Queries.PA_GET_BY_NAME, query = "select p from ParteArchitettonica p where p.denominazione = :denom and p.status=:rs") //
})
public class ParteArchitettonica extends PersistentObject
{
	private static final long serialVersionUID = 5532040627609928449L;

	@ManyToOne(optional = false, cascade = CascadeType.REFRESH)
	Architettura architettura;

	@Column(length = 32, nullable = false)
	String noPci = "";

	@Column(length = 32, nullable = false)
	String noUbc = "";

	@Column(length = 64, nullable = false)
	String denominazione = "";

	@Column
	Integer piano = 0;

	@Column(length = 64, nullable = false)
	String altraDenominazione = "";

	@Column(length = 64, nullable = false)
	String foto = "";

	@Column(length = 512, nullable = false)
	private String note = "";

	@OneToMany(mappedBy = "parteArchitettonica")
	private List<OperaArte> opereArte;

	protected ParteArchitettonica()
	{
	}

	public ParteArchitettonica(Architettura architettura_)
	{
		this();
		setArchitettura(architettura_);
	}

	@Override
	public String toString()
	{
		return getDenominazione() + ", piano " + getPiano() + (getNoPci().isEmpty() ? "" : (" - " + getNoPci()));
	}

	/**
	 * @return the architettura
	 */
	public Architettura getArchitettura()
	{
		return architettura;
	}

	/**
	 * @param architettura
	 *            the architettura to set
	 */
	public void setArchitettura(Architettura architettura)
	{
		this.architettura = architettura;
	}

	/**
	 * @return the noPci
	 */
	public String getNoPci()
	{
		return noPci;
	}

	/**
	 * @param noPci
	 *            the noPci to set
	 */
	public void setNoPci(String noPci)
	{
		this.noPci = noPci;
	}

	/**
	 * @return the noUbc
	 */
	public String getNoUbc()
	{
		return noUbc;
	}

	/**
	 * @param noUbc
	 *            the noUbc to set
	 */
	public void setNoUbc(String noUbc)
	{
		this.noUbc = noUbc;
	}

	/**
	 * @return the denominazione
	 */
	public String getDenominazione()
	{
		return denominazione;
	}

	/**
	 * @param denominazione
	 *            the denominazione to set
	 */
	public void setDenominazione(String denominazione)
	{
		this.denominazione = denominazione;
	}

	/**
	 * @return the piano
	 */
	public Integer getPiano()
	{
		return piano;
	}

	/**
	 * @param piano
	 *            the piano to set
	 */
	public void setPiano(Integer piano)
	{
		this.piano = piano;
	}

	/**
	 * @return the altraDenominazione
	 */
	public String getAltraDenominazione()
	{
		return altraDenominazione;
	}

	/**
	 * @param altraDenominazione
	 *            the altraDenominazione to set
	 */
	public void setAltraDenominazione(String altraDenominazione)
	{
		this.altraDenominazione = altraDenominazione;
	}

	/**
	 * @return the foto
	 */
	public String getFoto()
	{
		return foto;
	}

	/**
	 * @param foto
	 *            the foto to set
	 */
	public void setFoto(String foto)
	{
		this.foto = foto;
	}

	public String getNote()
	{
		return note;
	}

	public void setNote(String note)
	{
		this.note = note;
	}

	/**
	 * @return the opereArte
	 */
	public List<OperaArte> getOpereArte()
	{
		return opereArte;
	}

	/**
	 * @param opereArte
	 *            the opereArte to set
	 */
	public void setOpereArte(List<OperaArte> opereArte)
	{
		this.opereArte = opereArte;
	}

}
