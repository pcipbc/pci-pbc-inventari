/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import ch.pcilocarno.pbc.inventari.data.model.enums.TipoTabella;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistentObject;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;

/**
 * ch.ediesis.pci.pbc.inventari.data.domain.Tabella
 * 
 * @author elvis on Sep 8, 2009
 * @version @@version@@ build @@build@@
 */
@Entity
@NamedQueries({
//
		@NamedQuery(name = Queries.TAB_FIND_ALL, query = "select t from Tabella t"), //
		@NamedQuery(name = Queries.TAB_FIND_BY_TIPO, query = "select t from Tabella t where t.status=:rs and t.tipo=:t order by t.valore"), //
		@NamedQuery(name = Queries.TAB_FIND_BY_TIPO_CODUBC, query = "SELECT t from Tabella t WHERE t.tipo=:t and t.codiceUbc=:c")
//
})
public class Tabella extends PersistentObject
{
	private static final long serialVersionUID = 2279704728287298365L;

	@Enumerated(EnumType.STRING)
	@Column(length = 16, nullable = false, updatable = false)
	private TipoTabella tipo;

	@Column(nullable = true)
	private Integer codiceUbc;

	@Column(length = 64)
	private String valore;

	/**
	 * 
	 */
	protected Tabella()
	{
	}

	/**
	 * @param tipo
	 * @param codice
	 * @param valore
	 */
	public Tabella(TipoTabella tipo, Integer codiceUbc, String valore)
	{
		this();
		setTipo(tipo);
		setCodiceUbc(codiceUbc);
		if (valore.length() <= 64)
			setValore(valore);
		else
			setValore(valore.substring(0, 64));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return getTipo() + " | " + getValore() + " (Cod. UBC: " + getCodiceUbc() == null ? "-" : getCodiceUbc() + ")";
	}

	/**
	 * @return the tipo
	 */
	public TipoTabella getTipo()
	{
		return tipo;
	}

	/**
	 * @param tipo
	 *            the tipo to set
	 */
	public void setTipo(TipoTabella tipo)
	{
		this.tipo = tipo;
	}

	/**
	 * @return the valore
	 */
	public String getValore()
	{
		return valore;
	}

	/**
	 * @param valore
	 *            the valore to set
	 */
	public void setValore(String valore)
	{
		this.valore = valore;
	}

	/**
	 * @return the codiceUbc
	 */
	public Integer getCodiceUbc()
	{
		return codiceUbc;
	}

	/**
	 * @param codiceUbc
	 *            the codiceUbc to set
	 */
	public void setCodiceUbc(Integer codiceUbc)
	{
		this.codiceUbc = codiceUbc;
	}

}
