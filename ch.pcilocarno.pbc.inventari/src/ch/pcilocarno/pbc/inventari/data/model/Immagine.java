/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import ch.pcilocarno.pbc.inventari.data.persistence.PersistentObject;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;

@Entity
@NamedQueries({
//
		@NamedQuery(name = Queries.IMG_FIND_ALL, query = "select i from Immagine i"), //
		@NamedQuery(name = Queries.IMG_COUNT_BY_ARCH, query = "select count(img) from Immagine img where img.idProp in (select pa.id from ParteArchitettonica pa where pa.architettura.id=:archid) or img.idProp in (select oa.id from OperaArte oa where oa.parteArchitettonica.architettura.id = :archid) or img.idProp = :archid") //
//
})
public class Immagine extends PersistentObject
{
	private static final long serialVersionUID = -7318546847501520956L;

	@Column(length = 36, nullable = false)
	String idProp;

	@Column
	String nome = "";

	@Column
	String estensione = "";

	@Column
	Boolean principale = Boolean.FALSE;

	@Basic(fetch = FetchType.LAZY)
	@Column(length = 5242880)
	@Lob
	private byte[] contenuto;

	@Column(length = 512)
	String didascalia = "";

	protected Immagine()
	{
	}

	/**
	 * @param idProprietario
	 */
	public Immagine(String idProprietario)
	{
		super();
		setIdProp(idProprietario);
	}

	public String getIdProp()
	{
		return idProp;
	}

	public void setIdProp(String idProp)
	{
		this.idProp = idProp;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public String getEstensione()
	{
		return estensione;
	}

	public void setEstensione(String estensione)
	{
		this.estensione = estensione;
	}

	public byte[] getContenuto()
	{
		return contenuto;
	}

	public void setContenuto(byte[] contenuto)
	{
		this.contenuto = contenuto;
	}

	/**
	 * @return the principale
	 */
	public Boolean getPrincipale()
	{
		return principale == null ? Boolean.FALSE : principale;
	}

	/**
	 * @param principale
	 *            the principale to set
	 */
	public void setPrincipale(Boolean principale)
	{
		this.principale = principale;
	}

	/**
	 * @return the didascalia
	 */
	public String getDidascalia()
	{
		return didascalia;
	}

	/**
	 * @param didascalia
	 *            the didascalia to set
	 */
	public void setDidascalia(String didascalia)
	{
		this.didascalia = didascalia;
	}

}
