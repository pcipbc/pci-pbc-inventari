/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import ch.pcilocarno.pbc.inventari.data.persistence.PersistentObject;

/**
 * ch.pcilocarno.pbc.inventari.data.model.Allegato
 * 
 * @author elvis on Sep 9, 2009
 * @version @@version@@ build @@build@@
 */
@Entity
@NamedQueries({ //
		@NamedQuery(name = "findByIdprop", query = "select a from Allegato a where a.status=:rs and a.idProp = :idprop order by a.nome") //
})
public class Allegato extends PersistentObject {
	private static final long serialVersionUID = -7707758052485326777L;

	@Column(updatable = false, length = 36, nullable = false)
	private String idProp;

	@Column(length = 64)
	private String nome;

	@Column(length = 8)
	private String estensione = "";

	@Lob
	private Byte[] contenuto;

	protected Allegato() {
		super();
	}

	/**
	 * 
	 * @param parentUuid
	 */
	public Allegato(final String parentUuid) {
		super();
		setIdProp(parentUuid);
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the idProp
	 */
	public String getIdProp() {
		return idProp;
	}

	/**
	 * @param idProp
	 *            the idProp to set
	 */
	public void setIdProp(String idProp) {
		this.idProp = idProp;
	}

	/**
	 * @return the estensione
	 */
	public String getEstensione() {
		return estensione;
	}

	/**
	 * @param estensione
	 *            the estensione to set
	 */
	public void setEstensione(String estensione) {
		this.estensione = estensione;
	}

	/**
	 * @return the contenuto
	 */
	public Byte[] getContenuto() {
		return contenuto;
	}

	/**
	 * @param contenuto
	 *            the contenuto to set
	 */
	public void setContenuto(Byte[] contenuto) {
		this.contenuto = contenuto;
	}

}
