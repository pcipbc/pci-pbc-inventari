/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.ManyToOne;

import ch.pcilocarno.pbc.inventari.data.persistence.PersistentObject;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;

/**
 * ch.pcilocarno.pbc.inventari.data.model.Rapporto
 * 
 * @author elvis on Sep 9, 2009
 * @version @@version@@ build @@build@@
 */
@Entity
@NamedQueries({
		@NamedQuery(name = Queries.REP_FIND_ALL, query = "select r from Rapporto r"), //
		@NamedQuery(name = Queries.REP_FIND_BY_ARCH, query = "select r from Rapporto r where r.architettura.id = :archid and r.status=:rs order by r.created DESC"), //
		@NamedQuery(name = Queries.REP_COUNT_BY_ARCH, query = "select count(r) from Rapporto r where r.architettura.id = :archid"), //
		@NamedQuery(name = Queries.REP_GET_BY_NAME, query = "select r from Rapporto r where r.nome = :nome and r.status=:rs order by r.created DESC") //
})
public class Rapporto extends PersistentObject {
	private static final long serialVersionUID = -7707758052485326777L;

	@ManyToOne(optional = false, cascade = CascadeType.REFRESH)
	Architettura architettura;

	@Column(length = 64)
	private String nome = "";

	@Column(length = 4096)
	private String contenuto = "";

	protected Rapporto() {
		super();
	}

	public Rapporto(Architettura architettura) {
		this();
		this.architettura = architettura;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the contenuto
	 */
	public String getContenuto() {
		return contenuto;
	}

	/**
	 * @param contenuto the contenuto to set
	 */
	public void setContenuto(String contenuto) {
		this.contenuto = contenuto;
	}

	/**
	 * @return the architettura
	 */
	public Architettura getArchitettura() {
		return architettura;
	}

	/**
	 * @param architettura the architettura to set
	 */
	public void setArchitettura(Architettura architettura) {
		this.architettura = architettura;
	}
}
