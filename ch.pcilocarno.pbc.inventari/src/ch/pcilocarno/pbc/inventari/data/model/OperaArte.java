/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.model;

import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import ch.pcilocarno.pbc.inventari.data.model.enums.Livello;
import ch.pcilocarno.pbc.inventari.data.model.enums.StatoConservazione;
import ch.pcilocarno.pbc.inventari.data.model.enums.StatoOpera;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistentObject;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;
import ch.pcilocarno.pbc.inventari.util.Tools;

/**
 * ch.ediesis.pci.pbc.inventari.data.domain.OperaArte
 * 
 * @author elvis on Sep 8, 2009
 * @version @@version@@ build @@build@@
 */
@Entity
@NamedQueries({
		//
		@NamedQuery(name = Queries.OA_FIND_BY_PA_TEXT, query = "SELECT oa from OperaArte oa WHERE oa.status=:rs AND oa.parteArchitettonica.id=:paid AND oa.denominazione LIKE :text ORDER BY oa.noPci ASC"), //
		@NamedQuery(name = Queries.OA_FIND_BY_A, query = "select oa from OperaArte oa WHERE oa.status=:rs AND oa.parteArchitettonica.architettura.id=:aid"), //
		@NamedQuery(name = Queries.OA_COUNT_BY_A, query = "select count(oa) from OperaArte oa where oa.parteArchitettonica.architettura.id=:archid") //
		//

})
public class OperaArte extends PersistentObject {
	private static final long serialVersionUID = 6966147966307565410L;

	@ManyToOne(optional = false, cascade = CascadeType.REFRESH)
	private ParteArchitettonica parteArchitettonica;

	@Column(length = 32, nullable = false)
	private Integer noPci;

	@Column(length = 32, nullable = false)
	private String noPrec = "";

	@Column(length = 32, nullable = false)
	private String noUbc;

	@Column(length = 64, nullable = false)
	private String foto;

	@Column(length = 128, nullable = false)
	private String denominazione;

	@Column(length = 256, nullable = false)
	private String altraDenominazione;

	@Column(length = 64, nullable = false)
	private String genere;

	@Column(length = 64, nullable = false)
	private String genereGruppo;

	@Enumerated(EnumType.STRING)
	@Column(length = 32, nullable = false)
	private StatoOpera stato;

	@Column
	private Integer quantita;

	@Column(length = 64, nullable = false)
	private String collocazione;

	@Enumerated(EnumType.STRING)
	@Column(length = 32, nullable = false)
	private Livello livello;

	@Column(length = 128, nullable = false)
	private String materiaTecnica;

	private Integer misAltezza, misLarghezza, misProfondita, misPeso;

	private Integer misCcAltezza, misCcLarghezza, misCcProfondita, misCcPeso;

	@Column(length = 128, nullable = false)
	private String autore;

	@Column(length = 32, nullable = false)
	private String datazione;

	@Enumerated(EnumType.STRING)
	@Column(length = 32, nullable = false)
	private StatoConservazione consFormale;

	@Enumerated(EnumType.STRING)
	@Column(length = 32, nullable = false)
	private StatoConservazione consStrutturale;

	@Column(length = 64, nullable = false)
	private String ed1Tipo, ed1Tecnica, ed1Posizione;

	@Column(length = 64, nullable = false)
	private String ed2Tipo, ed2Tecnica, ed2Posizione;

	@Column(length = 64, nullable = false)
	private String ed3Tipo, ed3Tecnica, ed3Posizione;

	@Column(length = 256, nullable = false)
	private String ed1Descrizione, ed2Descrizione, ed3Descrizione;

	@Column(length = 512, nullable = false)
	private String pciOsservazioni;

	@Column(nullable = false)
	private Integer pciPersone;

	@Column(nullable = false, precision = 5, scale = 2)
	private BigDecimal pciOre;

	@Column(length = 256, nullable = false)
	private String pciModelloProtezione;

	@Column(length = 512, nullable = false)
	private String pciMateriale = "", pciAttrezzi = "";

	@Column(length = 512, nullable = false)
	private String note = "";

	@Column(nullable = false)
	private Boolean prioritario = Boolean.FALSE;

	@Column(nullable = false)
	private Boolean daVerificare = Boolean.FALSE;

	@Column(nullable = false, length = 512)
	private String daVerificareNote = "";

	protected OperaArte() {
		setAltraDenominazione("");
		setAutore("");
		setCollocazione("");
		setLivello(Livello.LIV0);
		setDatazione("");
		setDenominazione("");
		setEd1Descrizione("");
		setEd1Posizione("");
		setEd1Tecnica("");
		setEd1Tipo("");
		setEd2Descrizione("");
		setEd2Posizione("");
		setEd2Tecnica("");
		setEd2Tipo("");
		setEd3Descrizione("");
		setEd3Posizione("");
		setEd3Tecnica("");
		setEd3Tipo("");
		setFoto("");
		setGenere("");
		setGenereGruppo("");
		setMateriaTecnica("");
		setMisAltezza(0);
		setMisLarghezza(0);
		setMisPeso(0);
		setMisProfondita(0);
		setMisCcAltezza(0);
		setMisCcLarghezza(0);
		setMisCcPeso(0);
		setMisCcProfondita(0);
		setNoUbc("");
		setPciModelloProtezione("");
		setPciOre(BigDecimal.ZERO);
		setPciPersone(0);
		setPciOsservazioni("");
		setQuantita(1);
		setConsFormale(StatoConservazione.BUONO);
		setConsStrutturale(StatoConservazione.BUONO);
		setStato(StatoOpera.MOBILE);
	}

	public OperaArte(ParteArchitettonica pa) {
		this();
		setParteArchitettonica(pa);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (obj instanceof OperaArte) {
			return Tools.equals(((OperaArte) obj).getId(), getId());
		}
		return false;
	}

	/**
	 * @return the parteArchitettonica
	 */
	public ParteArchitettonica getParteArchitettonica() {
		return parteArchitettonica;
	}

	/**
	 * @param parteArchitettonica
	 *            the parteArchitettonica to set
	 */
	public void setParteArchitettonica(ParteArchitettonica parteArchitettonica) {
		this.parteArchitettonica = parteArchitettonica;
	}

	/**
	 * @return the noPci
	 */
	public Integer getNoPci() {
		return noPci;
	}

	/**
	 * @param noPci
	 *            the noPci to set
	 */
	public void setNoPci(Integer noPci) {
		this.noPci = noPci;
	}

	/**
	 * @return the noUbc
	 */
	public String getNoUbc() {
		return noUbc;
	}

	/**
	 * @param noUbc
	 *            the noUbc to set
	 */
	public void setNoUbc(String noUbc) {
		this.noUbc = noUbc;
	}

	/**
	 * @return the foto
	 */
	public String getFoto() {
		return foto;
	}

	/**
	 * @param foto
	 *            the foto to set
	 */
	public void setFoto(String foto) {
		this.foto = foto;
	}

	/**
	 * @return the denominazione
	 */
	public String getDenominazione() {
		return denominazione;
	}

	/**
	 * @param denominazione
	 *            the denominazione to set
	 */
	public void setDenominazione(String denominazione) {
		this.denominazione = denominazione;
	}

	/**
	 * @return the altraDenominazione
	 */
	public String getAltraDenominazione() {
		return altraDenominazione;
	}

	/**
	 * @param altraDenominazione
	 *            the altraDenominazione to set
	 */
	public void setAltraDenominazione(String altraDenominazione) {
		this.altraDenominazione = altraDenominazione;
	}

	/**
	 * @return the genere
	 */
	public String getGenere() {
		return genere;
	}

	/**
	 * @param genere
	 *            the genere to set
	 */
	public void setGenere(String genere) {
		this.genere = genere;
	}

	/**
	 * @return the genereGruppo
	 */
	public String getGenereGruppo() {
		return genereGruppo;
	}

	/**
	 * @param genereGruppo
	 *            the genereGruppo to set
	 */
	public void setGenereGruppo(String genereGruppo) {
		this.genereGruppo = genereGruppo;
	}

	/**
	 * @return the stato
	 */
	public StatoOpera getStato() {
		return stato;
	}

	/**
	 * @param stato
	 *            the stato to set
	 */
	public void setStato(StatoOpera stato) {
		this.stato = stato;
	}

	/**
	 * @return the quantita
	 */
	public Integer getQuantita() {
		return quantita == null ? 0 : quantita;
	}

	/**
	 * @param quantita
	 *            the quantita to set
	 */
	public void setQuantita(Integer quantita) {
		this.quantita = quantita;
	}

	/**
	 * @return the collocazione
	 */
	public String getCollocazione() {
		return collocazione;
	}

	/**
	 * @param collocazione
	 *            the collocazione to set
	 */
	public void setCollocazione(String collocazione) {
		this.collocazione = collocazione;
	}

	/**
	 * @return the materiaTecnica
	 */
	public String getMateriaTecnica() {
		return materiaTecnica;
	}

	/**
	 * @param materiaTecnica
	 *            the materiaTecnica to set
	 */
	public void setMateriaTecnica(String materiaTecnica) {
		this.materiaTecnica = materiaTecnica;
	}

	/**
	 * @return the misAltezza
	 */
	public Integer getMisAltezza() {
		return misAltezza;
	}

	/**
	 * @param misAltezza
	 *            the misAltezza to set
	 */
	public void setMisAltezza(Integer misAltezza) {
		this.misAltezza = misAltezza;
	}

	/**
	 * @return the misLarghezza
	 */
	public Integer getMisLarghezza() {
		return misLarghezza;
	}

	/**
	 * @param misLarghezza
	 *            the misLarghezza to set
	 */
	public void setMisLarghezza(Integer misLarghezza) {
		this.misLarghezza = misLarghezza;
	}

	/**
	 * @return the misProfondita
	 */
	public Integer getMisProfondita() {
		return misProfondita;
	}

	/**
	 * @param misProfondita
	 *            the misProfondita to set
	 */
	public void setMisProfondita(Integer misProfondita) {
		this.misProfondita = misProfondita;
	}

	/**
	 * @return the misPeso
	 */
	public Integer getMisPeso() {
		return misPeso;
	}

	/**
	 * @param misPeso
	 *            the misPeso to set
	 */
	public void setMisPeso(Integer misPeso) {
		this.misPeso = misPeso;
	}

	/**
	 * @return the misCcAltezza
	 */
	public Integer getMisCcAltezza() {
		return misCcAltezza;
	}

	/**
	 * @param misCcAltezza
	 *            the misCcAltezza to set
	 */
	public void setMisCcAltezza(Integer misCcAltezza) {
		this.misCcAltezza = misCcAltezza;
	}

	/**
	 * @return the misCcLarghezza
	 */
	public Integer getMisCcLarghezza() {
		return misCcLarghezza;
	}

	/**
	 * @param misCcLarghezza
	 *            the misCcLarghezza to set
	 */
	public void setMisCcLarghezza(Integer misCcLarghezza) {
		this.misCcLarghezza = misCcLarghezza;
	}

	/**
	 * @return the misCcProfondita
	 */
	public Integer getMisCcProfondita() {
		return misCcProfondita;
	}

	/**
	 * @param misCcProfondita
	 *            the misCcProfondita to set
	 */
	public void setMisCcProfondita(Integer misCcProfondita) {
		this.misCcProfondita = misCcProfondita;
	}

	/**
	 * @return the misCcPeso
	 */
	public Integer getMisCcPeso() {
		return misCcPeso;
	}

	/**
	 * @param misCcPeso
	 *            the misCcPeso to set
	 */
	public void setMisCcPeso(Integer misCcPeso) {
		this.misCcPeso = misCcPeso;
	}

	/**
	 * @return the autore
	 */
	public String getAutore() {
		return autore;
	}

	/**
	 * @param autore
	 *            the autore to set
	 */
	public void setAutore(String autore) {
		this.autore = autore;
	}

	/**
	 * @return the datazione
	 */
	public String getDatazione() {
		return datazione;
	}

	/**
	 * @param datazione
	 *            the datazione to set
	 */
	public void setDatazione(String datazione) {
		this.datazione = datazione;
	}

	/**
	 * @return the consFormale
	 */
	public StatoConservazione getConsFormale() {
		return consFormale;
	}

	/**
	 * @param consFormale
	 *            the consFormale to set
	 */
	public void setConsFormale(StatoConservazione consFormale) {
		this.consFormale = consFormale;
	}

	/**
	 * @return the consStrutturale
	 */
	public StatoConservazione getConsStrutturale() {
		return consStrutturale;
	}

	/**
	 * @param consStrutturale
	 *            the consStrutturale to set
	 */
	public void setConsStrutturale(StatoConservazione consStrutturale) {
		this.consStrutturale = consStrutturale;
	}

	/**
	 * @return the ed1Tipo
	 */
	public String getEd1Tipo() {
		return ed1Tipo;
	}

	/**
	 * @param ed1Tipo
	 *            the ed1Tipo to set
	 */
	public void setEd1Tipo(String ed1Tipo) {
		this.ed1Tipo = ed1Tipo;
	}

	/**
	 * @return the ed1Tecnica
	 */
	public String getEd1Tecnica() {
		return ed1Tecnica;
	}

	/**
	 * @param ed1Tecnica
	 *            the ed1Tecnica to set
	 */
	public void setEd1Tecnica(String ed1Tecnica) {
		this.ed1Tecnica = ed1Tecnica;
	}

	/**
	 * @return the ed1Posizione
	 */
	public String getEd1Posizione() {
		return ed1Posizione;
	}

	/**
	 * @param ed1Posizione
	 *            the ed1Posizione to set
	 */
	public void setEd1Posizione(String ed1Posizione) {
		this.ed1Posizione = ed1Posizione;
	}

	/**
	 * @return the ed2Tipo
	 */
	public String getEd2Tipo() {
		return ed2Tipo;
	}

	/**
	 * @param ed2Tipo
	 *            the ed2Tipo to set
	 */
	public void setEd2Tipo(String ed2Tipo) {
		this.ed2Tipo = ed2Tipo;
	}

	/**
	 * @return the ed2Tecnica
	 */
	public String getEd2Tecnica() {
		return ed2Tecnica;
	}

	/**
	 * @param ed2Tecnica
	 *            the ed2Tecnica to set
	 */
	public void setEd2Tecnica(String ed2Tecnica) {
		this.ed2Tecnica = ed2Tecnica;
	}

	/**
	 * @return the ed2Posizione
	 */
	public String getEd2Posizione() {
		return ed2Posizione;
	}

	/**
	 * @param ed2Posizione
	 *            the ed2Posizione to set
	 */
	public void setEd2Posizione(String ed2Posizione) {
		this.ed2Posizione = ed2Posizione;
	}

	/**
	 * @return the ed1Descrizione
	 */
	public String getEd1Descrizione() {
		return ed1Descrizione;
	}

	/**
	 * @param ed1Descrizione
	 *            the ed1Descrizione to set
	 */
	public void setEd1Descrizione(String ed1Descrizione) {
		this.ed1Descrizione = ed1Descrizione;
	}

	/**
	 * @return the ed2Descrizione
	 */
	public String getEd2Descrizione() {
		return ed2Descrizione;
	}

	/**
	 * @param ed2Descrizione
	 *            the ed2Descrizione to set
	 */
	public void setEd2Descrizione(String ed2Descrizione) {
		this.ed2Descrizione = ed2Descrizione;
	}

	/**
	 * @return the pciOsservazioni
	 */
	public String getPciOsservazioni() {
		return pciOsservazioni;
	}

	/**
	 * @param pciOsservazioni
	 *            the pciOsservazioni to set
	 */
	public void setPciOsservazioni(String pciOsservazioni) {
		this.pciOsservazioni = pciOsservazioni;
	}

	/**
	 * @return the pciOre
	 */
	public BigDecimal getPciOre() {
		return pciOre;
	}

	/**
	 * @param pciOre
	 *            the pciOre to set
	 */
	public void setPciOre(BigDecimal pciOre) {
		this.pciOre = pciOre;
	}

	/**
	 * @return the pciPersone
	 */
	public Integer getPciPersone() {
		return pciPersone;
	}

	/**
	 * @param pciPersone
	 *            the pciPersone to set
	 */
	public void setPciPersone(Integer pciPersone) {
		this.pciPersone = pciPersone;
	}

	/**
	 * @return the pciModelloProtezione
	 */
	public String getPciModelloProtezione() {
		return pciModelloProtezione;
	}

	/**
	 * @param pciModelloProtezione
	 *            the pciModelloProtezione to set
	 */
	public void setPciModelloProtezione(String pciModelloProtezione) {
		this.pciModelloProtezione = pciModelloProtezione;
	}

	/**
	 * @return the pciMateriale
	 */
	public String getPciMateriale() {
		return pciMateriale;
	}

	/**
	 * @param pciMateriale
	 *            the pciMateriale to set
	 */
	public void setPciMateriale(String pciMateriale) {
		this.pciMateriale = pciMateriale;
	}

	/**
	 * @return the pciAttrezzi
	 */
	public String getPciAttrezzi() {
		return pciAttrezzi;
	}

	/**
	 * @param pciAttrezzi
	 *            the pciAttrezzi to set
	 */
	public void setPciAttrezzi(String pciAttrezzi) {
		this.pciAttrezzi = pciAttrezzi;
	}

	/**
	 * @return the livello
	 */
	public Livello getLivello() {
		return livello;
	}

	/**
	 * @param livello
	 *            the livello to set
	 */
	public void setLivello(Livello livello) {
		this.livello = livello;
	}

	public String getEd3Tipo() {
		return ed3Tipo;
	}

	public void setEd3Tipo(String ed3Tipo) {
		this.ed3Tipo = ed3Tipo;
	}

	public String getEd3Tecnica() {
		return ed3Tecnica;
	}

	public void setEd3Tecnica(String ed3Tecnica) {
		this.ed3Tecnica = ed3Tecnica;
	}

	public String getEd3Posizione() {
		return ed3Posizione;
	}

	public void setEd3Posizione(String ed3Posizione) {
		this.ed3Posizione = ed3Posizione;
	}

	public String getEd3Descrizione() {
		return ed3Descrizione;
	}

	public void setEd3Descrizione(String ed3Descrizione) {
		this.ed3Descrizione = ed3Descrizione;
	}

	public Boolean getPrioritario() {
		return prioritario;
	}

	public void setPrioritario(Boolean prioritario) {
		this.prioritario = prioritario;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	/**
	 * @return the daVerificare
	 */
	public Boolean getDaVerificare() {
		return daVerificare;
	}

	/**
	 * @param daVerificare
	 *            the daVerificare to set
	 */
	public void setDaVerificare(Boolean daVerificare) {
		this.daVerificare = daVerificare;
	}

	/**
	 * @return the daVerificareNote
	 */
	public String getDaVerificareNote() {
		return daVerificareNote;
	}

	/**
	 * @param daVerificareNote
	 *            the daVerificareNote to set
	 */
	public void setDaVerificareNote(String daVerificareNote) {
		this.daVerificareNote = daVerificareNote;
	}

	/**
	 * @return the noPrec
	 */
	public String getNoPrec() {
		return noPrec;
	}

	/**
	 * @param noPrec
	 *            the noPrec to set
	 */
	public void setNoPrec(String noPrec) {
		this.noPrec = noPrec;
	}

}
