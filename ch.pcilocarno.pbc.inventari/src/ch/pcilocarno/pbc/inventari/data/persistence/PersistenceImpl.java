/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.eclipse.persistence.jpa.PersistenceProvider;

import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;

/**
 * @author elvis
 * 
 */
public abstract class PersistenceImpl<E extends IPersistentObject> implements IPersistence<E> {

	/**
	 * @return
	 */
	protected final EntityManager em() {
		EntityManager _em = PersistenceFactory.getEM(getPUname());
		if (_em == null) {
			_em = new PersistenceProvider().createEntityManagerFactory(getPUname(), getPUproperties())
					.createEntityManager();
			PersistenceFactory.putEM(getPUname(), _em);
		}
		if (!_em.isOpen()) {
			PersistenceFactory.clearEM(getPUname());
			return em();
		}

		return _em;
	}

	/**
	 * @return
	 */
	protected Map<String, Object> getPUproperties() {
		return new HashMap<String, Object>(0);
	}

	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	protected Class<E> getEclass() {
		return (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}

	/**
	 * Detach an entity from the PersistenceContext
	 * 
	 * @param entity
	 */
	public final void detach(E entity) {
		em().detach(entity);
	}

	@Override
	public E findById(String id) {
		return em().find(getEclass(), id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.pcilocarno.pbc.inventari.data.persistence.IPersistence#save(java.lang.
	 * Object)
	 */
	@Override
	public E save(E entity) {
		em().persist(entity);
		return entity;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.data.persistence.IPersistence#update(java.
	 * lang.Object)
	 */
	@Override
	public E update(E entity) {
		return em().merge(entity);
	}

	/**
	 * @param entity
	 * @return
	 */
	@Override
	public E saveOrUpdate(E entity) throws RuntimeException {
		return saveOrUpdate(entity, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.data.persistence.IPersistence#saveOrUpdate(
	 * java.lang.Object)
	 */
	@Override
	public E saveOrUpdate(E entity, boolean setUpdateData) throws RuntimeException{

		final boolean TRwasActive = em().getTransaction().isActive();
		
		if (!TRwasActive)
			em().getTransaction().begin();

		try {
			if (entity.getId() == null) {
				setDataINSERT(entity);
				entity = save(entity);
			} else {
				setDataUPDATE(entity, setUpdateData);
				entity = update(entity);
			}

		} catch (Exception ex) {
			
			if (!TRwasActive)
				em().getTransaction().rollback();
			
			throw ex;
		}
		
		if (!TRwasActive)
			em().getTransaction().commit();

		return entity;
	}

	/**
	 * @param entity
	 */
	protected final void setDataINSERT(E entity) {
		if (entity.getId() == null) //
			entity.setId(UUID.randomUUID().toString());

		if (entity.getAuthor() == null) //
			entity.setAuthor(Session.INSTANCE.getUsername());

		if (entity.getCreated() == null) //
			entity.setCreated(PersistenceTools.nowNoMillis());

		if (entity.getStatus() == null) //
			entity.setStatus(RowStatus.A);

		setDataUPDATE(entity);
	}

	/**
	 * @param entity
	 */
	protected final void setDataUPDATE(E entity) {
		setDataUPDATE(entity, true);
	}

	/**
	 * @param entity
	 * @param setdata
	 */
	protected final void setDataUPDATE(E entity, boolean setdata) {
		if (!setdata)
			return;
		entity.setEdited(PersistenceTools.nowNoMillis());
		entity.setEditor(Session.INSTANCE.getUsername());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.data.persistence.IPersistence#delete(java.
	 * lang.Object)
	 */
	@Override
	public void delete(E entity)  throws RuntimeException{
		entity.setStatus(RowStatus.C);
		saveOrUpdate(entity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.data.persistence.IPersistence#destroy(java.
	 * lang.Object)
	 */
	@Override
	public void destroy(E entity) {
		em().remove(entity);
	}

	// ***** * * * * * * * * * *
	// transaction control methods
	public final void TRbegin() {
		em().getTransaction().begin();
	}

	public final void TRcommit() {
		em().getTransaction().commit();
	}

	public final void TRrollback() {
		em().getTransaction().rollback();
	}

	public final EntityTransaction TRget() {
		return em().getTransaction();
	}

	public final void flush() {
		em().flush();
	}

	public final void clear() {
		em().clear();
	}

	// abstract methods
	protected abstract String getPUname();

}
