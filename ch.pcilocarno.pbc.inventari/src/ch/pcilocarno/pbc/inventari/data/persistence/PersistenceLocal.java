/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence;

import java.util.HashMap;
import java.util.Map;

import ch.pcilocarno.pbc.inventari.util.Log;

/**
 * @author elvis
 * 
 */
public abstract class PersistenceLocal<E extends PersistentObject> extends PersistenceImpl<E> {
	/**
	 * @return
	 */
	public abstract String[] indexes();

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.pcilocarno.pbc.inventari.data.persistence.PersistenceImpl#getPUname()
	 */
	@Override
	protected String getPUname() {
		return "localPU";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.data.persistence.PersistenceImpl#
	 * getPUproperties()
	 */
	@Override
	protected Map<String, Object> getPUproperties() {
		Log.debug("Local DB path: %s", PersistenceTools.getDBpath());

		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put("javax.persistence.jdbc.url", //
				"jdbc:h2:" + PersistenceTools.getDBpath() + ";CACHE_SIZE=64;PAGE_SIZE=8");

		// properties.put(PersistenceUnitProperties.TARGET_DATABASE, "Auto");
		// properties.put(PersistenceUnitProperties.JDBC_DRIVER,
		// "org.h2.Driver");
		// properties.put(PersistenceUnitProperties.JDBC_USER, "sa");
		// properties.put(PersistenceUnitProperties.JDBC_PASSWORD, "");
		//
		// properties.put(PersistenceUnitProperties.DDL_DATABASE_GENERATION,
		// PersistenceUnitProperties.CREATE_OR_EXTEND);
		// properties.put(PersistenceUnitProperties.DDL_GENERATION_MODE,
		// PersistenceUnitProperties.DDL_DATABASE_GENERATION);
		// properties.put(PersistenceUnitProperties.CLASSLOADER,
		// this.getClass().getClassLoader());
		// properties.put("eclipselink.logging.level", "FINE"); // was FINE
		// properties.put("eclipselink.logging.timestamp", "false");
		// properties.put("eclipselink.logging.session", "false");
		// properties.put("eclipselink.logging.thread", "false");

		return properties;
	}
}
