/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence.local;

import java.util.List;

import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.ParteArchitettonica;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;
import ch.pcilocarno.pbc.inventari.util.Log;

public class ParteArchitettonicaPersistenceLocal extends PersistenceLocal<ParteArchitettonica> {
	@Override
	public String[] indexes() {
		return new String[] {
				//
				"CREATE INDEX PA_IDX_ARCH ON PARTEARCHITETTONICA(ARCHITETTURA_ID, STATUS)"
				//
		};
	}

	/**
	 * @param architteturaID
	 *            ID Architettura
	 * @return
	 */
	public List<ParteArchitettonica> findByArch(final String architteturaID) {
		return findByArch(architteturaID, RowStatus.A);
	}

	/**
	 * @param architteturaID
	 *            ID Architettura
	 * @param rs
	 *            Row status
	 * @return
	 */
	public List<ParteArchitettonica> findByArch(final String architteturaID, final RowStatus rs) {
		return em().createNamedQuery(Queries.PA_FIND_BY_ARCH, ParteArchitettonica.class)//
				.setParameter("archid", architteturaID)//
				.setParameter("rs", rs)//
				.getResultList();
	}

	public ParteArchitettonica getByName(final String name, final boolean createIfNotFound) {
		final String rname = name == null || name.isEmpty() ? "(non definito)" : name;

		List<ParteArchitettonica> pas = em().createNamedQuery(Queries.PA_GET_BY_NAME, ParteArchitettonica.class)//
				.setParameter("denom", rname)//
				.setParameter("rs", RowStatus.A)//
				.getResultList();

		if (pas.isEmpty() && createIfNotFound) {
			ParteArchitettonica pa = new ParteArchitettonica(
					PersistenceFactory.Alocal().findById(Session.INSTANCE.getArchitetturaUUID()));
			pa.setDenominazione(rname);
			return PersistenceFactory.PAlocal().saveOrUpdate(pa);
		}

		if (pas.size() > 1)
			Log.warning("Trovate più PA con la stessa denominazione. (%s)", name);

		return pas.get(0);
	}
}
