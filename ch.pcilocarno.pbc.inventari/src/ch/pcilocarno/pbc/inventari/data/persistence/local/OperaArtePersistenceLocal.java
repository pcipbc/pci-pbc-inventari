/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence.local;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.Query;

import ch.pcilocarno.pbc.inventari.beans.DatePeriod;
import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.OperaArte;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.model.enums.StatoOpera;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceTools;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.util.Tools;

public class OperaArtePersistenceLocal extends PersistenceLocal<OperaArte> {
	@Override
	public String[] indexes() {
		return new String[] {
				//
				"CREATE INDEX OA_IDX_PA_TEXT ON OPERAARTE(PARTEARCHITETTONICA_ID, DENOMINAZIONE, NOPCI, STATUS)"
				//
		};
	}

	/**
	 * @param parteArchitettonicaId
	 *            ID Parte Architettonica
	 * @param text
	 *            Text, could be null
	 * @return
	 */
	public List<OperaArte> findByPaText(String parteArchitettonicaId, String text) {
		return findByPaText(parteArchitettonicaId, text, RowStatus.A);
	}

	/**
	 * @param parteArchitettonicaId
	 *            ID Parte Architettonica
	 * @param text
	 *            Text, could be null
	 * @param rs
	 *            RowStatus
	 * @return
	 */
	public List<OperaArte> findByPaText(String parteArchitettonicaId, String text, RowStatus rs) {
		return em().createNamedQuery(Queries.OA_FIND_BY_PA_TEXT, OperaArte.class)//
				.setParameter("rs", rs)//
				.setParameter("paid", parteArchitettonicaId)//
				.setParameter("text", Tools.forLike(text)) //
				.getResultList();
	}

	public List<LocalDate> collectCreateDates() {

		List<Date> dates = em().createQuery("select distinct o.created from OperaArte o where o.parteArchitettonica.architettura.id=:aid and o.status=:rs order by o.created ASC", java.util.Date.class) //
				.setParameter("rs", RowStatus.A) //
				.setParameter("aid", Session.INSTANCE.getArchitetturaUUID()) //
				.getResultList();

		Set<LocalDate> result = new LinkedHashSet<>();
		for (Date date : dates) {
			LocalDate ldat = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			result.add(ldat);
		}

		return new LinkedList<>(result);
	}

	/**
	 * @return
	 */
	public Integer getNextNbr() {
		List<OperaArte> oas = em().createQuery("select o from OperaArte o where o.parteArchitettonica.architettura.id=:aid and o.status=:rs order by o.noPci DESC", OperaArte.class)//
				.setParameter("aid", Session.INSTANCE.getArchitetturaUUID())//
				.setParameter("rs", RowStatus.A) //
				.setMaxResults(1) //
				.getResultList();

		// no records found
		OperaArte ultima = oas.size() == 0 ? null : oas.get(0);
		if (ultima == null)
			return 1;

		try {
			return ultima.getNoPci() + 1;
		} catch (NumberFormatException nfe) {
			Log.error(null, "Parsing noPci to integer [noPci = %s].", ultima.getNoPci());
		}

		return null;
	}

	/**
	 * @param oa
	 * @param noPci
	 * @return
	 */
	public boolean canSave(final OperaArte oa, final Integer noPci) {

		List<OperaArte> oas = em().createQuery("select o from OperaArte o where o.id<>:id and o.noPci = :np and o.parteArchitettonica.architettura.id=:aid and o.status=:rs", OperaArte.class)//
				.setParameter("id", oa.getId()) //
				.setParameter("aid", Session.INSTANCE.getArchitetturaUUID()) //
				.setParameter("np", noPci) //
				.setParameter("rs", RowStatus.A) //
				.setMaxResults(1)//
				.getResultList();

		return oas.size() == 0;
	}

	public List<OperaArte> findForPictureImport(String architetturaUUID, DatePeriod insertedPeriod, boolean onlyWithoutPicture) {

		String sql = "select o from OperaArte o " //
				+ "where o.status = :rs " //
				+ "and o.parteArchitettonica.architettura.id= :archuuid " //
				+ "and o.created >= :crtfrom " //
				+ "and o.created <= :crtto " //
				+ (onlyWithoutPicture ? "and (select count(i.id) from Immagine i where i.status=:rs and i.idProp = o.id) = 0 " : "") + " order by o.noPci ";
		return em().createQuery(sql, OperaArte.class) //
				.setParameter("archuuid", architetturaUUID) //
				.setParameter("rs", RowStatus.A) //
				.setParameter("crtfrom", Date.from(insertedPeriod.getFrom().atStartOfDay().toInstant(ZoneOffset.ofHours(1)))) //
				.setParameter("crtto", Date.from(insertedPeriod.getTo().atTime(23, 59, 59).toInstant(ZoneOffset.ofHours(1)))) //
				.getResultList();
	}

	/**
	 * @param noPci
	 * @param noUbc
	 * @param genere
	 * @param denominazione
	 * @param foto
	 * @param stato
	 * @param daverificare
	 * @param withoutPic
	 * @param withoutDimensions
	 * @param withoutMateria
	 * @param withoutPianifPci
	 * @return
	 */
	public List<OperaArte> findExtended(final String architetturaUUID, final Integer noPci, final String noUbc, final String genere, final String denominazione, final String foto, final Date from, final StatoOpera stato, final Boolean daverificare, final boolean withoutPic, final boolean withoutDimensions, final boolean withoutMateria, final boolean withoutPianifPci) {
		String sql = "select o from OperaArte o where o.status=:rs and " + //
				" o.parteArchitettonica.architettura.id= :archuuid" + //
				" and o.noUbc like :noubc " + //
				" and o.genere like :genere " + //
				" and o.denominazione like :denominazione " + //
				" and o.foto like :foto " + //
				(noPci != null ? " and o.noPci = :nopci " : "") + //
				(stato != null ? " and  o.stato = :stato" : "") + //
				(from != null ? " and o.created >= :fromdate" : "") + //
				(daverificare ? " and o.daVerificare = true " : "") + //
				(withoutPic ? " and (select count(i.id) from Immagine i where i.status=:irs and i.idProp=o.id) = 0 " : "") + //
				(withoutDimensions ? " and (o.misAltezza * o.misLarghezza * o.misProfondita = 0 ) " : "") + (withoutMateria ? " and o.materiaTecnica = '' " : "") + //
				(withoutPianifPci ? " and o.stato = :spstato and (o.pciOre = 0 or o.pciPersone=0 or o.pciModelloProtezione = '') " : "") //
		;

		Query q = em().createQuery(sql, OperaArte.class);
		q.setParameter("rs", RowStatus.A);
		if (withoutPic)
			q.setParameter("irs", RowStatus.A);
		q.setParameter("archuuid", architetturaUUID);
		q.setParameter("noubc", PersistenceTools.forLike(noUbc));
		q.setParameter("genere", PersistenceTools.forLike(genere));
		q.setParameter("denominazione", PersistenceTools.forLike(denominazione));
		q.setParameter("foto", PersistenceTools.forLike(foto));
		if (noPci != null)
			q.setParameter("nopci", noPci);
		if (stato != null)
			q.setParameter("stato", stato);
		if (withoutPianifPci)
			q.setParameter("spstato", StatoOpera.IMMOBILE);
		if (from != null)
			q.setParameter("fromdate", from);
		return q.getResultList();
	}
}
