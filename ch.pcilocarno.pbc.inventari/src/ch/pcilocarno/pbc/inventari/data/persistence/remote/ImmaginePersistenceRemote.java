/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence.remote;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import ch.pcilocarno.pbc.inventari.data.model.Immagine;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceRemote;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;
import ch.pcilocarno.pbc.inventari.util.Tools;

public class ImmaginePersistenceRemote extends PersistenceRemote<Immagine> {
	/**
	 * @return
	 */
	public List<Immagine> findAll() {
		return em().createNamedQuery(Queries.IMG_FIND_ALL, Immagine.class) //
				.setParameter("rs", RowStatus.A) //
				.getResultList();
	}

	/**
	 * @param IDprop
	 * @param rs
	 * @param force
	 * @return
	 */
	public List<Immagine> findByIDprop(String IDprop, RowStatus rs) {
		if (Tools.isEmpty(IDprop)) //
			return new ArrayList<>(0);

		Query qry = em().createQuery("select i from Immagine i where i.idProp=:idprop "
				+ (rs == null ? "" : " and i.status=:rs ") + " order by i.principale DESC, i.created ASC",
				Immagine.class);
		qry.setParameter("idprop", IDprop);
		if (rs != null)
			qry.setParameter("rs", rs);

		return qry.getResultList();
	}

	/**
	 * @param architetturaID
	 * @return
	 */
	public Long countByArch(final String architetturaID) {
		return em().createNamedQuery(Queries.IMG_COUNT_BY_ARCH, Long.class) //
				.setParameter("archid", architetturaID)//
				.getSingleResult();
	}
}
