/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.persistence.config.PersistenceUnitProperties;

import ch.pcilocarno.pbc.inventari.Activator;
import ch.pcilocarno.pbc.inventari.preferences.PreferenceConstants;

/**
 * @author elvis
 * 
 */
public abstract class PersistenceRemote<E extends PersistentObject> extends PersistenceImpl<E>
{
	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.data.persistence.PersistenceImpl#getPUname()
	 */
	@Override
	protected String getPUname()
	{
		return "remotePU";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.data.persistence.PersistenceImpl#getPUproperties()
	 */
	@Override
	protected Map<String, Object> getPUproperties()
	{
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PersistenceUnitProperties.TARGET_DATABASE, "Auto");
		properties.put(PersistenceUnitProperties.JDBC_DRIVER, Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.P_JDBC_DRIVER));
		properties.put(PersistenceUnitProperties.JDBC_URL, Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.P_JDBC_URL));
		properties.put(PersistenceUnitProperties.JDBC_USER, Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.P_JDBC_USER));
		properties.put(PersistenceUnitProperties.JDBC_PASSWORD, Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.P_JDBC_PASSWORD));
		// properties.put(PersistenceUnitProperties.JDBC_READ_CONNECTIONS_MIN, "1");
		// properties.put(PersistenceUnitProperties.JDBC_WRITE_CONNECTIONS_MIN, "1");

		properties.put(PersistenceUnitProperties.DDL_DATABASE_GENERATION, PersistenceUnitProperties.CREATE_ONLY);
		properties.put(PersistenceUnitProperties.DDL_GENERATION_MODE, PersistenceUnitProperties.DDL_DATABASE_GENERATION);
		properties.put(PersistenceUnitProperties.CLASSLOADER, this.getClass().getClassLoader());
		properties.put("eclipselink.logging.level", "INFO");
		properties.put("eclipselink.logging.timestamp", "false");
		properties.put("eclipselink.logging.session", "false");
		properties.put("eclipselink.logging.thread", "false");
		properties.put("eclipselink.logging.thread", "false");
		properties.put("eclipselink.jdbc.property.useSSL", "false");
		properties.put("eclipselink.jdbc.property.requireSSL", "false");
		
		return properties;
	}
}
