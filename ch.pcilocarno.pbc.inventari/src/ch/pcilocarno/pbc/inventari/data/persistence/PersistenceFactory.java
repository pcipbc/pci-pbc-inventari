/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;

import ch.pcilocarno.pbc.inventari.data.persistence.local.AllegatoPersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.local.ArchitetturaPersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.local.ContattoPersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.local.ImmaginePersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.local.LocalHistoryPersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.local.OperaArtePersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.local.ParteArchitettonicaPersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.local.RapportoPersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.local.TabellaPersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.remote.AllegatoPersistenceRemote;
import ch.pcilocarno.pbc.inventari.data.persistence.remote.ArchitetturaPersistenceRemote;
import ch.pcilocarno.pbc.inventari.data.persistence.remote.ContattoPersistenceRemote;
import ch.pcilocarno.pbc.inventari.data.persistence.remote.ImmaginePersistenceRemote;
import ch.pcilocarno.pbc.inventari.data.persistence.remote.OperaArtePersistenceRemote;
import ch.pcilocarno.pbc.inventari.data.persistence.remote.ParteArchitettonicaPersistenceRemote;
import ch.pcilocarno.pbc.inventari.data.persistence.remote.RapportoPersistenceRemote;
import ch.pcilocarno.pbc.inventari.data.persistence.remote.TabellaPersistenceRemote;
import ch.pcilocarno.pbc.inventari.util.Log;

public class PersistenceFactory {
	static final Map<String, EntityManager> entityManagers = new HashMap<String, EntityManager>(2);

	private static TabellaPersistenceRemote Tremote = null;

	private static ArchitetturaPersistenceRemote Aremote = null;

	private static ArchitetturaPersistenceLocal Alocal = null;

	private static ContattoPersistenceRemote Cremote = null;

	private static ContattoPersistenceLocal Clocal = null;

	private static TabellaPersistenceLocal Tlocal;

	private static ParteArchitettonicaPersistenceLocal PAlocal;

	private static ParteArchitettonicaPersistenceRemote PAremote;

	private static OperaArtePersistenceLocal OAlocal;

	private static OperaArtePersistenceRemote OAremote;

	private static ImmaginePersistenceLocal IMGlocal;

	private static ImmaginePersistenceRemote IMGremote;

	private static LocalHistoryPersistenceLocal LHlocal;

	private static AllegatoPersistenceLocal ATTlocal;
	private static AllegatoPersistenceRemote ATTremote;

	private static RapportoPersistenceLocal REPlocal;
	private static RapportoPersistenceRemote REPremote;

	/**
	 * @param key
	 * @return
	 */
	public static synchronized EntityManager getEM(final String key) {
		return entityManagers.get(key);
	}

	/**
	 * @param key
	 * @param em
	 */
	public static final synchronized void putEM(final String key, EntityManager em) {
		entityManagers.put(key, em);
	}

	/**
	 * @param key
	 */
	public static final synchronized void clearEM(final String key) {
		try {
			EntityManager em = getEM(key);
			if (em != null) {
				if (em.isOpen()) //
					em.close();
			}
		} catch (Exception ex) {
			Log.debug("Closing EntityManager %s", key);
		}
		entityManagers.remove(key);
	}

	/**
	 * Private
	 */
	private PersistenceFactory() {
	}

	/**
	 * 
	 * @return
	 */
	public static final AllegatoPersistenceLocal ATTlocal() {
		if (ATTlocal == null)
			ATTlocal = new AllegatoPersistenceLocal();
		return ATTlocal;
	}

	/**
	 * 
	 * @return
	 */
	public static final AllegatoPersistenceRemote ATTremote() {
		if (ATTremote == null)
			ATTremote = new AllegatoPersistenceRemote();
		return ATTremote;
	}

	/**
	 * 
	 * @return
	 */
	public static final RapportoPersistenceLocal REPlocal() {
		if (REPlocal == null)
			REPlocal = new RapportoPersistenceLocal();
		return REPlocal;
	}

	/**
	 * 
	 * @return
	 */
	public static final RapportoPersistenceRemote REPremote() {
		if (REPremote == null)
			REPremote = new RapportoPersistenceRemote();
		return REPremote;
	}

	public static final LocalHistoryPersistenceLocal LHlocal() {
		if (LHlocal == null)
			LHlocal = new LocalHistoryPersistenceLocal();
		return LHlocal;
	}

	public static final ArchitetturaPersistenceRemote Aremote() {
		if (Aremote == null)
			Aremote = new ArchitetturaPersistenceRemote();
		return Aremote;
	}

	public static final ArchitetturaPersistenceLocal Alocal() {
		if (Alocal == null)
			Alocal = new ArchitetturaPersistenceLocal();
		return Alocal;
	}

	public static final ContattoPersistenceLocal Clocal() {
		if (Clocal == null)
			Clocal = new ContattoPersistenceLocal();
		return Clocal;
	}

	public static final ContattoPersistenceRemote Cremote() {
		if (Cremote == null)
			Cremote = new ContattoPersistenceRemote();
		return Cremote;
	}

	public static final TabellaPersistenceRemote Tremote() {
		if (Tremote == null)
			Tremote = new TabellaPersistenceRemote();
		return Tremote;
	}

	public static final TabellaPersistenceLocal Tlocal() {
		if (Tlocal == null)
			Tlocal = new TabellaPersistenceLocal();
		return Tlocal;
	}

	public static final ParteArchitettonicaPersistenceLocal PAlocal() {
		if (PAlocal == null)
			PAlocal = new ParteArchitettonicaPersistenceLocal();
		return PAlocal;
	}

	public static final ParteArchitettonicaPersistenceRemote PAremote() {
		if (PAremote == null)
			PAremote = new ParteArchitettonicaPersistenceRemote();
		return PAremote;
	}

	public static final OperaArtePersistenceLocal OAlocal() {
		if (OAlocal == null)
			OAlocal = new OperaArtePersistenceLocal();
		return OAlocal;
	}

	public static final OperaArtePersistenceRemote OAremote() {
		if (OAremote == null)
			OAremote = new OperaArtePersistenceRemote();
		return OAremote;
	}

	public static final ImmaginePersistenceLocal IMGlocal() {
		if (IMGlocal == null)
			IMGlocal = new ImmaginePersistenceLocal();
		return IMGlocal;
	}

	public static final ImmaginePersistenceRemote IMGremote() {
		if (IMGremote == null)
			IMGremote = new ImmaginePersistenceRemote();
		return IMGremote;
	}
}
