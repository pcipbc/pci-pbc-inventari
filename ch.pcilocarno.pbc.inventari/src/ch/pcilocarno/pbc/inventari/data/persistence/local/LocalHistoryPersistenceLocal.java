/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence.local;

import java.util.List;

import ch.pcilocarno.pbc.inventari.data.model.LocalHistory;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceLocal;

/**
 * @author elvis
 * 
 */
public class LocalHistoryPersistenceLocal extends PersistenceLocal<LocalHistory>
{
	@Override
	public String[] indexes()
	{
		return new String[0];
	}

	public List<LocalHistory> findByArch(String archId)
	{
		return em().createQuery("select h from LocalHistory h where h.architettura.id = :archid order by h.created DESC", LocalHistory.class)//
				.setParameter("archid", archId)//
				.getResultList();
	}
}
