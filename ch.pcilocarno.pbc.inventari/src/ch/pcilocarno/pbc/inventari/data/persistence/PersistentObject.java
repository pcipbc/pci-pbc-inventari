/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;

@MappedSuperclass
public abstract class PersistentObject implements IPersistentObject
{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 36)
	private String id;

	// @Version
	private int version = 0;

	@Column(updatable = false, nullable = false)
	private String author;

	@Column(nullable = false)
	private String editor;

	@Column(nullable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date created;

	@Column(nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date edited;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, length = 1)
	private RowStatus status;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		if (!(obj instanceof PersistentObject)) {
			return false;
		}

		if (!obj.getClass().equals(this.getClass())) {
			return false;
		}

		return Objects.equals(((PersistentObject) obj).getId(), getId());
	}

	/**
	 * @return the id
	 */
	@Override
	public String getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	@Override
	public void setId(String id)
	{
		this.id = id;
	}

	/**
	 * @return the version
	 */
	public int getVersion()
	{
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(int version)
	{
		this.version = version;
	}

	/**
	 * @return the author
	 */
	@Override
	public String getAuthor()
	{
		return author;
	}

	/**
	 * @param author
	 *            the author to set
	 */
	@Override
	public void setAuthor(String author)
	{
		this.author = author;
	}

	/**
	 * @return the editor
	 */
	@Override
	public String getEditor()
	{
		return editor;
	}

	/**
	 * @param editor
	 *            the editor to set
	 */
	@Override
	public void setEditor(String editor)
	{
		this.editor = editor;
	}

	/**
	 * @return the created
	 */
	@Override
	public Date getCreated()
	{
		return created;
	}

	/**
	 * @param created
	 *            the created to set
	 */
	@Override
	public void setCreated(Date created)
	{
		this.created = created;
	}

	/**
	 * @return the edited
	 */
	@Override
	public Date getEdited()
	{
		return edited;
	}

	/**
	 * @param edited
	 *            the edited to set
	 */
	@Override
	public void setEdited(Date edited)
	{
		this.edited = edited;
	}

	/**
	 * @return the status
	 */
	@Override
	public RowStatus getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	@Override
	public void setStatus(RowStatus status)
	{
		this.status = status;
	}

}
