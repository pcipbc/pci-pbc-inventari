/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence.local;

import java.util.List;

import ch.pcilocarno.pbc.inventari.data.model.Allegato;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceLocal;

/**
 * @author elvis
 *
 */
public class AllegatoPersistenceLocal extends PersistenceLocal<Allegato> {

	@Override
	public String[] indexes() {
		return new String[] {
				//
				"CREATE INDEX ALLEGATO_IDX_IDPROP ON ALLEGATO(IDPROP, STATUS)"
				//
		};
	}

	/**
	 * 
	 * @param UUID
	 * @return
	 */
	public List<Allegato> findByIdprop(final String UUID) {
		return em()
				.createQuery("select a from Allegato a where a.status=:rs and a.idProp = :idprop order by a.nome",
						Allegato.class) //
				.setParameter("rs", RowStatus.A)//
				.setParameter("idprop", UUID) //
				.getResultList();

	}

	/**
	 * @return
	 */
	public List<Allegato> findAll() {
		return em().createQuery("select a from Allegato a", Allegato.class) //
				.getResultList();
	}
}
