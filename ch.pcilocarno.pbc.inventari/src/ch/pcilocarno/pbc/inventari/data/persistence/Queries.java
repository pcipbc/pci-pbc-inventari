/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence;

public class Queries {
	// Architettura
	public static final String A_FIND_ALL_BY_STATUS = "Architettura.findAllByStatus";
	public static final String A_FIND_ALL_BY_STATUS_TEXT = "Architettura.findAllByStatusText";

	// Contatto
	public static final String C_FIND_ALL = "Contatto.findAll";
	public static final String C_FIND_BY_IDPROP = "Conatto.findByIdProp";
	public static final String C_FIND_BY_IDPROP_TIPO = "Conatto.findByIdPropTipo";

	// ParteArchitettonica
	public static final String PA_FIND_ALL = "ParteArchittetonica.findAll";
	public static final String PA_FIND_BY_ARCH = "ParteArchittetonica.findByArch";
	public static final String PA_COUNT_BY_ARCH = "ParteArchittetonica.countByArch";
	public static final String PA_GET_BY_NAME = "ParteArchittetonica.getByName";

	// OperaArte
	public static final String OA_FIND_BY_PA_TEXT = "OperaArte.findByPaText";
	public static final String OA_FIND_BY_A = "OperaArte.findByArchitettura";
	public static final String OA_COUNT_BY_A = "OperaArte.countByArchitettura";

	// Immagine

	public static final String IMG_FIND_ALL = "Immagine.findAll";
	public static final String IMG_COUNT_BY_ARCH = "Immagine.countByArchitettura";

	// Allegato

	public static final String ATT_FIND_BY_IDPROP = "findByIdprop";

	// Rapporto
	public static final String REP_FIND_ALL = "Rapporto.findAll";
	public static final String REP_FIND_BY_ARCH = "Rapporto.findByArch";
	public static final String REP_COUNT_BY_ARCH = "Rapporto.countByArch";
	public static final String REP_GET_BY_NAME = "Rapporto.getByName";

	// queries
	public static final String TAB_FIND_ALL = "Tabella.findAll";

	public static final String TAB_FIND_BY_TIPO = "Tabella.findByTipo";

	public static final String TAB_FIND_BY_TIPO_CODUBC = "Tabella.findByTipoCodiceUBC";

	private Queries() {
	}
}
