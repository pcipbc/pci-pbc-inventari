/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence.remote;

import java.util.List;

import ch.pcilocarno.pbc.inventari.data.model.Tabella;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.model.enums.TipoTabella;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceRemote;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;

public class TabellaPersistenceRemote extends PersistenceRemote<Tabella> {
	public List<Tabella> findAll() {
		return em().createNamedQuery(Queries.TAB_FIND_ALL, Tabella.class).getResultList();
	}

	/**
	 * @param t
	 * @return
	 */
	public boolean exists(Tabella t) {
		List<Tabella> r = em().createNamedQuery(Queries.TAB_FIND_BY_TIPO_CODUBC, Tabella.class)//
				.setParameter("t", t.getTipo())//
				.setParameter("c", t.getCodiceUbc())//
				.getResultList();

		return r != null && r.size() != 0;
	}

	/**
	 * @param tipo
	 * @param rs
	 * @return
	 */
	public List<Tabella> findByTipo(TipoTabella tipo, RowStatus rs) {
		return em().createNamedQuery(Queries.TAB_FIND_BY_TIPO, Tabella.class)//
				.setParameter("rs", RowStatus.A)//
				.setParameter("t", tipo)//
				.getResultList();
	}

	/**
	 * @param tipo
	 * @return
	 */
	public String[] loadValoriByTipo(TipoTabella tipo) {
		List<Tabella> t = findByTipo(tipo, RowStatus.A);

		String[] result = new String[t.size()];
		for (int i = 0; i < t.size(); i++)
			result[i] = t.get(i).getValore();

		return result;

	}
}
