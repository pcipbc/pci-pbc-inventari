/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence.local;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import ch.pcilocarno.pbc.inventari.data.model.Immagine;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;
import ch.pcilocarno.pbc.inventari.util.Tools;

public class ImmaginePersistenceLocal extends PersistenceLocal<Immagine> {
	@Override
	public String[] indexes() {
		return new String[] {
				//
				"CREATE INDEX IMG_IDX_IDPROP ON IMMAGINE(IDPROP, STATUS)"
				//
		};
	}

	/**
	 * Conta immagini per ID proprietario
	 * 
	 * @param IDprop
	 *            ID proprietario
	 * @return
	 */
	public int countByIdProp(String IDprop) {
		Query qry = em().createQuery("select count(i.id) from Immagine i where i.status=:rs and i.idProp=:idprop");
		qry.setParameter("idprop", IDprop);
		qry.setParameter("rs", RowStatus.A);

		return ((Long) qry.getSingleResult()).intValue();
	}

	/**
	 * @param IDprop
	 * @param rs
	 * @param force
	 * @return
	 */
	public List<Immagine> findByIDprop(String IDprop, RowStatus rs) {
		if (Tools.isEmpty(IDprop)) //
			return new ArrayList<>(0);

		Query qry = em().createQuery("select i from Immagine i where i.idProp=:idprop "
				+ (rs == null ? "" : " and i.status=:rs ") + " order by i.principale DESC, i.created ASC",
				Immagine.class);
		qry.setParameter("idprop", IDprop);

		if (rs != null)
			qry.setParameter("rs", rs);

		return qry.getResultList();
	}

	public List<Immagine> findAll() {
		return em().createNamedQuery(Queries.IMG_FIND_ALL, Immagine.class)
				.getResultList();
	}

}
