/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence.remote;

import java.util.List;

import ch.pcilocarno.pbc.inventari.data.model.Architettura;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceRemote;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceTools;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;

public class ArchitetturaPersistenceRemote extends PersistenceRemote<Architettura> {

	/**
	 * @param text
	 * @return
	 */
	public List<Architettura> findAllByText(String text) {
		text = PersistenceTools.forLike(text);
		return em().createNamedQuery(Queries.A_FIND_ALL_BY_STATUS_TEXT, Architettura.class) //
				.setParameter("rs", RowStatus.A)//
				.setParameter("text", text) //
				.getResultList();
	}

	// QUERY che estrae i dati necessari all'import e la dimensione stimata
	// select ID, NOPCI, DENOMINAZIONE, COMUNE, round( sum(asize)/1024/1024, 0 ) as
	// 'MB' FROM
	// (
	// select a.ID, a.NOPCI, a.DENOMINAZIONE,
	// a.COMUNE,sum(OCTET_LENGTH(i.CONTENUTO)) as asize
	// from IMMAGINE i
	// inner join OPERAARTE oa on oa.ID = i.IDPROP
	// inner join PARTEARCHITETTONICA pa on pa.ID = oa.PARTEARCHITETTONICA_ID
	// inner join ARCHITETTURA a on a.id = pa.ARCHITETTURA_ID
	// where i.STATUS='A'
	// group by a.ID
	// UNION ALL
	// select a.ID, a.NOPCI, a.DENOMINAZIONE, a.COMUNE, sum(LENGTH(i.CONTENUTO)) as
	// asize
	// from IMMAGINE i
	// inner join PARTEARCHITETTONICA pa on pa.ID = i.IDPROP
	// inner join ARCHITETTURA a on a.id = pa.ARCHITETTURA_ID
	// where i.STATUS='A'
	// group by a.ID
	// UNION ALL
	// select a.ID, a.NOPCI, a.DENOMINAZIONE, a.COMUNE,sum(LENGTH(i.CONTENUTO)) as
	// asize
	// from IMMAGINE i
	// inner join ARCHITETTURA a on a.id = i.IDPROP
	// where i.STATUS='A'
	// group by a.ID
	// ) as totals group by ID, NOPCI, DENOMINAZIONE, COMUNE order by COMUNE, NOPCI
}
