/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence.remote;

import java.util.ArrayList;
import java.util.List;

import ch.pcilocarno.pbc.inventari.data.model.Allegato;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceRemote;

/**
 * @author elvis
 *
 */
public class AllegatoPersistenceRemote extends PersistenceRemote<Allegato> {

	/**
	 * @param architetturaID
	 * @return
	 */
	public Long countByArch(final String architetturaID) {
		return (Long) em().createQuery("select count(p) from Allegato p where p.idProp = :archid") //
				.setParameter("archid", architetturaID)//
				.getSingleResult();
	}

	/**
	 * @param iDprop
	 * @param a
	 * @return
	 */
	public List<Allegato> findByIdprop(String iDprop, RowStatus a) {
		// can load?
		if (iDprop == null) //
			return new ArrayList<Allegato>(0);

		return em().createQuery("select i from Allegato i where i.idProp=:idprop and i.status=:rs", Allegato.class)//
				.setParameter("rs", a)//
				.setParameter("idprop", iDprop)//
				.getResultList();
	}

}
