/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence;

import java.io.File;
import java.net.URL;
import java.util.Calendar;

import org.eclipse.core.runtime.FileLocator;
import org.h2.tools.DeleteDbFiles;

import ch.pcilocarno.pbc.inventari.Activator;
import ch.pcilocarno.pbc.inventari.preferences.PreferenceConstants;
import ch.pcilocarno.pbc.inventari.util.Log;

public abstract class PersistenceTools {
	private static final String DBNAME = "CASSANDRA";

	private static final Calendar cal = Calendar.getInstance();

	private static String DBfolder = null;

	private static String DBpath = null;

	public static final void removeLocalDB() {
		try {
			PersistenceFactory.Alocal().deleteDBobjectsAndShutdown();
			Log.info("Deleting DB %s in folder %s", DBNAME, DBfolder);
			DeleteDbFiles.execute(DBfolder, DBNAME, true);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}

	/**
	 * @return
	 */
	public static final String getDBpath() {
		if (DBpath == null) {
			try {
				String configArea = System.getProperty("osgi.configuration.area");
				URL furl = FileLocator.toFileURL(new URL(configArea));

				// db file
				DBfolder = furl.getFile();
				File dbf = new File(furl.getFile(), DBNAME);
				DBpath = dbf.getCanonicalPath();
				Log.debug("Resolved DB path %s", DBpath);
			} catch (Exception ex) {
				DBpath = "pcipbc";
				Log.error(ex, "Resolving database path. Set to default " + DBpath);
			}

			System.getProperties().put("ediesis.DBpath", DBpath);
		}

		return DBpath;
	}

	/**
	 * @return
	 */
	public static final java.sql.Date nowNoMillis() {
		cal.setTimeInMillis(System.currentTimeMillis());
		cal.set(Calendar.MILLISECOND, 0);

		return new java.sql.Date(cal.getTimeInMillis());
	}

	/**
	 * @param text
	 * @return
	 */
	public static final String forLike(String text) {
		if (text == null || text.isEmpty()) //
			return "%";

		if (text.indexOf("%") > 0)
			return text;
		else
			return "%" + text + "%";
	}

	/**
	 * @return
	 */
	public static final boolean remoteDbConfigured() {
		return !Activator.getDefault().getPreferenceStore().getString(PreferenceConstants.P_JDBC_DRIVER).equals("NONE");
	}

	/**
	 * @return
	 */
	public static final boolean remoteDbAccessible() {
		if (!remoteDbConfigured())
			return false; // not configured
		try {
			PersistenceFactory.Aremote().findAllByText("");
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static void optimizeLocalDB() {
		PersistenceFactory.Alocal().TRbegin();
		try {
			for (String crtidx : PersistenceFactory.PAlocal().indexes()) {
				Log.debug("Executing %s", crtidx);
				PersistenceFactory.PAlocal().em().createNativeQuery(crtidx).executeUpdate();
			}

			for (String crtidx : PersistenceFactory.OAlocal().indexes()) {
				Log.debug("Executing %s", crtidx);
				PersistenceFactory.OAlocal().em().createNativeQuery(crtidx).executeUpdate();
			}

			for (String crtidx : PersistenceFactory.IMGlocal().indexes()) {
				Log.debug("Executing %s", crtidx);
				PersistenceFactory.IMGlocal().em().createNativeQuery(crtidx).executeUpdate();
			}

			for (String crtidx : PersistenceFactory.Alocal().indexes()) {
				Log.debug("Executing %s", crtidx);
				PersistenceFactory.Alocal().em().createNativeQuery(crtidx).executeUpdate();
			}

			for (String crtidx : PersistenceFactory.Clocal().indexes()) {
				Log.debug("Executing %s", crtidx);
				PersistenceFactory.Clocal().em().createNativeQuery(crtidx).executeUpdate();
			}

			for (String crtidx : PersistenceFactory.Tlocal().indexes()) {
				Log.debug("Executing %s", crtidx);
				PersistenceFactory.Tlocal().em().createNativeQuery(crtidx).executeUpdate();
			}

			for (String crtidx : PersistenceFactory.ATTlocal().indexes()) {
				Log.debug("Executing %s", crtidx);
				PersistenceFactory.ATTlocal().em().createNativeQuery(crtidx).executeUpdate();
			}

		} catch (Exception ex) {
			Log.error(ex, "Optminizing DB");
		}
		PersistenceFactory.Alocal().TRcommit();
		Log.info("DB optimized!");
	}
}
