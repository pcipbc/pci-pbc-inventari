/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.data.persistence.local;

import java.util.List;

import ch.pcilocarno.pbc.inventari.data.model.Architettura;
import ch.pcilocarno.pbc.inventari.data.model.enums.RowStatus;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceLocal;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceTools;
import ch.pcilocarno.pbc.inventari.data.persistence.Queries;
import ch.pcilocarno.pbc.inventari.util.Log;

public class ArchitetturaPersistenceLocal extends PersistenceLocal<Architettura> {
	@Override
	public String[] indexes() {
		return new String[0];
	}

	public void deleteDBobjectsAndShutdown() {
		try {
			em().getTransaction().begin();
			java.sql.Connection connection = em().unwrap(java.sql.Connection.class);
			connection.createStatement().execute("DROP ALL OBJECTS DELETE FILES");
			em().getTransaction().commit();
			connection.createStatement().execute("SHUTDOWN");

		} catch (Exception ex) {
			Log.error(ex, "Error droppingall objects and shutting down the DB");
		}
		try {
			em().close();
		} catch (Exception ex) {
			Log.error(ex, "Error closing the EntityManager: " + ex.toString());
		}
	}

	/**
	 * @return
	 */
	public Architettura getFirst() {
		List<Architettura> archs = findAll();

		return archs.size() == 0 ? null : archs.get(0);
	}

	/**
	 * @return
	 */
	public List<Architettura> findAll() {
		return em().createNamedQuery(Queries.A_FIND_ALL_BY_STATUS, Architettura.class) //
				.setParameter("rs", RowStatus.A)//
				.getResultList();
	}

	/**
	 * @param text
	 * @return
	 */
	public List<Architettura> findAllByStatusText(String text) {
		return em().createNamedQuery(Queries.A_FIND_ALL_BY_STATUS_TEXT, Architettura.class) //
				.setParameter("rs", RowStatus.A)//
				.setParameter("text", PersistenceTools.forLike(text)) //
				.getResultList();
	}
}
