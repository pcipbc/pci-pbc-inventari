/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari;

import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.menus.WorkbenchWindowControlContribution;

import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.Architettura;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.StringUtils;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

/**
 * @author elvis
 * 
 */
public class ArchitetturaSwitcher extends WorkbenchWindowControlContribution {
	boolean handleSelection = true;

	/**
	 * 
	 */
	public ArchitetturaSwitcher() {
	}

	/**
	 * @param id
	 */
	public ArchitetturaSwitcher(String id) {
		super(id);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.action.ControlContribution#createControl(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected Control createControl(Composite parent) {
		final ComboViewer combo = new ComboViewer(parent, SWT.BORDER | SWT.READ_ONLY);
		combo.setContentProvider(new ArrayContentProvider());
		combo.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return (element instanceof Architettura) ? ((Architettura) element).getDenominazione() + " ("
						+ ((Architettura) element).getComune() + ")" : ("" + element);
			}

			@Override
			public Image getImage(Object element) {
				return ((Architettura) element).getDefinitivo()
						? Activator.getImageDescriptor(Icons.LOCK_SMALL_16).createImage() : null;
			}
		});

		if (StringUtils.isNullOrEmpty(Session.INSTANCE.getArchitetturaUUID())) {
			combo.add("Nessuna Architettura");
			combo.getCombo().select(0);
			combo.getCombo().setEnabled(false);
		} else {
			List<Architettura> arcs = PersistenceFactory.Alocal().findAll();
			combo.getCombo().setEnabled(arcs.size() > 1);
			combo.setInput(arcs);
			combo.setSelection(new StructuredSelection(
					PersistenceFactory.Alocal().findById(Session.INSTANCE.getArchitetturaUUID())), true);

			combo.addSelectionChangedListener(new ISelectionChangedListener() {
				@Override
				public void selectionChanged(SelectionChangedEvent event) {
					if (!handleSelection)
						return;
					if (!canSwitch()) {
						combo.setSelection(new StructuredSelection(
								PersistenceFactory.Alocal().findById(Session.INSTANCE.getArchitetturaUUID())));
						handleSelection = true;
						return;
					}

					Session.INSTANCE.setArchitetturaUUID(
							((Architettura) ((StructuredSelection) event.getSelection()).getFirstElement()).getId());
				}
			});
		}
		return combo.getControl();
	}

	/**
	 * @return
	 */
	private boolean canSwitch() {
		if (PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getDirtyEditors().length > 0) //
		{
			UIhelper.openInfoDialog(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), "Attenzione",
					"Salvare le schede prima di cambiare architettura!");
			handleSelection = false;
			return false;
		}
		return true;
	}

}
