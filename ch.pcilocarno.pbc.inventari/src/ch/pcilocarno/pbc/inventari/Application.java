/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.jface.dialogs.IInputValidator;
import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.util.Importer;
import ch.pcilocarno.pbc.inventari.util.Log;

/**
 * This class controls all aspects of the application's execution
 */
public class Application implements IApplication
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#start(org.eclipse.equinox.app. IApplicationContext)
	 */
	public Object start(IApplicationContext context) throws Exception
	{
		final Display display = PlatformUI.createDisplay();
		try
		{
			// set username
			Session.INSTANCE.setUsername(System.getProperty("ediesis.username", ""));
			if (Session.INSTANCE.getUsername().isEmpty())
			{
				InputDialog id = new InputDialog(display.getActiveShell(), "Nome utente", "Inserire il proprio nome e cognome", "", new IInputValidator()
				{
					@Override
					public String isValid(String newText)
					{
						if (newText.length() == 0) return "È obbligatorio specificare il dato richiesto.";
						return null;
					}

				});
				id.setBlockOnOpen(true);
				if (id.open() != InputDialog.OK) return IApplication.EXIT_OK;
				Session.INSTANCE.setUsername(id.getValue());
			}
			else
				Log.debug("User name %s set from arguments.", Session.INSTANCE.getUsername());

			// Import
			Importer.doImport(display.getActiveShell());

			int returnCode = PlatformUI.createAndRunWorkbench(display, new ApplicationWorkbenchAdvisor());

			if (returnCode == PlatformUI.RETURN_RESTART)
				return IApplication.EXIT_RESTART;
			else
				return IApplication.EXIT_OK;
		}
		finally
		{
			display.dispose();
		}

	}

	/**
	 * @param shell
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.equinox.app.IApplication#stop()
	 */
	public void stop()
	{
		final IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null) return;
		final Display display = workbench.getDisplay();
		display.syncExec(new Runnable()
		{
			public void run()
			{
				if (!display.isDisposed()) workbench.close();
			}
		});
	}
}
