/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.ViewPart;

import ch.pcilocarno.pbc.inventari.beans.TableSorter;
import ch.pcilocarno.pbc.inventari.commands.OAOpenHandler;
import ch.pcilocarno.pbc.inventari.data.model.OperaArte;
import ch.pcilocarno.pbc.inventari.data.model.ParteArchitettonica;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.views.content.OAContentProvider;
import ch.pcilocarno.pbc.inventari.views.label.OALabelProvider;

/**
 * ch.pcilocarno.pbc.inventari.views.OperaArteView
 * 
 * @author elvis on Sep 9, 2009
 * @version @@version@@ build @@build@@
 */
public class OperaArteView extends ViewPart implements IViewListPO<OperaArte> {
	public static final String ID = "ch.pcilocarno.pbc.inventari.views.OperaArteView";

	private TableViewer viewer;

	private String paUuid = null;

	private Text fFilter;

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.
	 * widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {

		parent.setLayout(
				GridLayoutFactory.fillDefaults().numColumns(1).extendedMargins(0, 0, 5, 0).margins(0, 0).create());
		fFilter = new Text(parent, SWT.BORDER | SWT.SINGLE);
		fFilter.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
		fFilter.setMessage("Filtra per denominazione");
		fFilter.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				refresh();
			}
		});
		final TableSorter<OperaArte> sorter = new TableSorter<OperaArte>() {
			@Override
			protected int compare(int col, OperaArte b1, OperaArte b2) {
				switch (col) {
				case 0:
					return b1.getNoPci().compareTo(b2.getNoPci());
				case 1:
					return b1.getGenere().compareTo(b2.getGenere());
				case 2:
					return b1.getDenominazione().compareTo(b2.getDenominazione());
				}
				return 0;
			}
		};
		viewer = new TableViewer(parent, SWT.MULTI | SWT.FULL_SELECTION);
		viewer.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));

		// col headers
		final TableViewerColumn col = new TableViewerColumn(viewer, SWT.NONE);
		col.getColumn().setText("No.");
		col.getColumn().setWidth(60);
		col.getColumn().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				sorter.setColumn(0);
				int dir = viewer.getTable().getSortDirection();
				if (viewer.getTable().getSortColumn() == col.getColumn())
					dir = dir == SWT.DOWN ? SWT.UP : SWT.DOWN;
				else
					dir = SWT.UP;
				viewer.getTable().setSortDirection(dir);
				viewer.getTable().setSortColumn(col.getColumn());
				viewer.refresh(true);
			}
		});
		final TableViewerColumn col1 = new TableViewerColumn(viewer, SWT.NONE);
		col1.getColumn().setText("Genere");
		col1.getColumn().setWidth(150);
		col1.getColumn().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				sorter.setColumn(1);
				int dir = viewer.getTable().getSortDirection();
				if (viewer.getTable().getSortColumn() == col1.getColumn())
					dir = dir == SWT.DOWN ? SWT.UP : SWT.DOWN;
				else
					dir = SWT.UP;
				viewer.getTable().setSortDirection(dir);
				viewer.getTable().setSortColumn(col1.getColumn());
				viewer.refresh(true);
			}
		});
		final TableViewerColumn col2 = new TableViewerColumn(viewer, SWT.NONE);
		col2.getColumn().setText("Denominazione");
		col2.getColumn().setWidth(250);
		col2.getColumn().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				sorter.setColumn(2);
				int dir = viewer.getTable().getSortDirection();
				if (viewer.getTable().getSortColumn() == col2.getColumn())
					dir = dir == SWT.DOWN ? SWT.UP : SWT.DOWN;
				else
					dir = SWT.UP;
				viewer.getTable().setSortDirection(dir);
				viewer.getTable().setSortColumn(col2.getColumn());
				viewer.refresh(true);
			}
		});
		final TableViewerColumn col3 = new TableViewerColumn(viewer, SWT.NONE);
		col3.getColumn().setText("Tipo");
		col3.getColumn().setWidth(100);

		viewer.setContentProvider(new OAContentProvider(viewer));
		viewer.setLabelProvider(new OALabelProvider(false));
		viewer.setComparator(sorter);
		viewer.setUseHashlookup(true);
		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);
		viewer.getTable().setSortDirection(SWT.UP);
		viewer.getTable().setSortColumn(col.getColumn());
		getViewSite().setSelectionProvider(viewer);

		getViewSite().getPage().addSelectionListener(ParteArchitettonicaView.ID, new ISelectionListener() {
			@Override
			public void selectionChanged(IWorkbenchPart part, ISelection selection) {
				if (selection == null || selection.isEmpty())
					setSelectedPA(null);
				else {
					Object selected = ((StructuredSelection) selection).getFirstElement();
					if (selected != null && selected instanceof ParteArchitettonica) //
						setSelectedPA((ParteArchitettonica) selected);
				}
			}
		});
		hookDoubleClickCommand();

		//
		refresh();
	}

	/**
	 * @param pa
	 */
	private void setSelectedPA(ParteArchitettonica pa) {
		if (pa != null && pa.getId().equals(this.paUuid))
			return;
		this.paUuid = pa == null ? null : pa.getId();
		fFilter.setText("");
		setContentDescription(pa == null ? ""
				: (pa.getDenominazione() + (pa.getAltraDenominazione().trim().isEmpty() ? ""
						: (" (" + pa.getAltraDenominazione().trim() + ")"))));
		refresh();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ch.pcilocarno.pbc.inventari.views.IViewListPO#refresh()
	 */
	@Override
	public void refresh() {
		final String filter = fFilter.getText();
		new Job("Carica OA.") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				if (paUuid == null || paUuid.isEmpty()) {
					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {
							if (viewer.getTable().isDisposed())
								return;
							viewer.setInput(new ArrayList<OperaArte>(0));
							viewer.refresh();
						}
					});
					return Status.OK_STATUS;
				}

				final List<OperaArte> pa = PersistenceFactory.OAlocal().findByPaText(paUuid, filter);
				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						if (viewer.getTable().isDisposed())
							return;
						viewer.setInput(pa);
						viewer.refresh();
					}
				});

				return Status.OK_STATUS;
			}
		}.schedule();

	}

	private void hookDoubleClickCommand() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				IHandlerService handlerService = getSite().getService(IHandlerService.class);
				try {
					handlerService.executeCommand(OAOpenHandler.ID, null);
				} catch (Exception ex) {
					throw new RuntimeException(OAOpenHandler.ID + " not found");
				}
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		fFilter.setFocus();
	}

}
