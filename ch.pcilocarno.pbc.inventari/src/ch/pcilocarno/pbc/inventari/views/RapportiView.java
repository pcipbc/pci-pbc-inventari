/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.views;

import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;

import ch.pcilocarno.pbc.inventari.commands.OAOpenHandler;
import ch.pcilocarno.pbc.inventari.commands.RapportoEditHandler;
import ch.pcilocarno.pbc.inventari.data.ArchChangeListener;
import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.Rapporto;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.FileUtils;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.util.Tools;
import ch.pcilocarno.pbc.inventari.views.content.ReportContentProvider;
import ch.pcilocarno.pbc.inventari.views.label.ReportLabelProvider;

/**
 * @author elvis
 *
 */
public class RapportiView extends ViewPart implements IViewListPO<Rapporto> {
	public static final String ID = "ch.pcilocarno.pbc.inventari.views.RapportiView";

	TableViewer tableReports;

	ArchChangeListener alistener = new ArchChangeListener() {
		@Override
		public void handle() {
			refresh();
			getViewSite().getSelectionProvider().setSelection(null);
		}
	};

	@Override
	public void init(IViewSite site) throws PartInitException {
		super.init(site);
		Session.INSTANCE.addListener(alistener);
	}

	@Override
	public void dispose() {
		Session.INSTANCE.removeListener(alistener);
		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.
	 * widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new FillLayout());

		initTablePanel(parent);
		refresh();
	}

	@Override
	public final void refresh() {

		new Job("Carica RAPPORTI.") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				if (tableReports == null || tableReports.getTable().isDisposed())
					return Status.CANCEL_STATUS;

				final List<Rapporto> rapporti = PersistenceFactory.REPlocal()
						.findByArch(Session.INSTANCE.getArchitetturaUUID());

				new UIJob(Display.getDefault(), "Carica RAPPORTI in tabella.") {
					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						if (tableReports == null || tableReports.getTable().isDisposed())
							return Status.CANCEL_STATUS;
						tableReports.setInput(rapporti);
						tableReports.refresh();
						return Status.OK_STATUS;
					}
				}.schedule();
				return Status.OK_STATUS;
			}
		}.schedule(0);
	}
	

	private void hookDoubleClickCommand() {
		tableReports.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				IHandlerService handlerService = getSite().getService(IHandlerService.class);
				try {
					handlerService.executeCommand(RapportoEditHandler.ID, null);
				} catch (Exception ex) {
					throw new RuntimeException(RapportoEditHandler.ID + " not found");
				}
			}
		});
	}

	private void initTablePanel(Composite parent) {
		tableReports = new TableViewer(parent, SWT.SINGLE|SWT.FULL_SELECTION);
		// tableReports.setComparator(sorter);

		// add headers
		final TableViewerColumn col_creat = new TableViewerColumn(tableReports, SWT.NONE);
		col_creat.getColumn().setText("Data creazione");
		col_creat.getColumn().setWidth(150);
		
		final TableViewerColumn col_updat = new TableViewerColumn(tableReports, SWT.NONE);
		col_updat.getColumn().setText("Data aggiornamento");
		col_updat.getColumn().setWidth(150);
		
		final TableViewerColumn col_name = new TableViewerColumn(tableReports, SWT.NONE);
		col_name.getColumn().setText("Nome");
		col_name.getColumn().setWidth(150);

		final TableViewerColumn col_text = new TableViewerColumn(tableReports, SWT.NONE);
		col_text.getColumn().setText("Testo");
		col_text.getColumn().setWidth(500);

		// set contenProvider
		tableReports.setContentProvider(new ReportContentProvider(tableReports));
		// set labelProvider
		tableReports.setLabelProvider(new ReportLabelProvider());

		tableReports.setUseHashlookup(true);
		tableReports.getTable().setHeaderVisible(true);
		tableReports.getTable().setLinesVisible(true);


		// register selection provider
		getViewSite().setSelectionProvider(tableReports);
		hookDoubleClickCommand();
	}


	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {

	}

}
