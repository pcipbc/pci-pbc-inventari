/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.views;

import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;

import ch.pcilocarno.pbc.inventari.data.ArchChangeListener;
import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.Allegato;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.FileUtils;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.util.Tools;
import ch.pcilocarno.pbc.inventari.views.content.AttachmentContentProvider;
import ch.pcilocarno.pbc.inventari.views.label.AttachmentLabelProvider;

/**
 * @author elvis
 *
 */
public class AllegatiView extends ViewPart implements IViewListPO<Allegato> {
	public static final String ID = "ch.pcilocarno.pbc.inventari.views.AllegatiView";

	TableViewer tableAttachments;

	ArchChangeListener alistener = new ArchChangeListener() {
		@Override
		public void handle() {
			refresh();
			getViewSite().getSelectionProvider().setSelection(null);
		}
	};

	@Override
	public void init(IViewSite site) throws PartInitException {
		super.init(site);
		Session.INSTANCE.addListener(alistener);
	}

	@Override
	public void dispose() {
		Session.INSTANCE.removeListener(alistener);
		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.
	 * widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new FillLayout());

		initTablePanel(parent);
		refresh();
	}

	@Override
	public final void refresh() {

		new Job("Carica ALLEGATI.") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				if (tableAttachments == null || tableAttachments.getTable().isDisposed())
					return Status.CANCEL_STATUS;

				final List<Allegato> allegati = PersistenceFactory.ATTlocal()
						.findByIdprop(Session.INSTANCE.getArchitetturaUUID());

				new UIJob(Display.getDefault(), "Carica ALLEGATI in tabella.") {

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor) {
						if (tableAttachments == null || tableAttachments.getTable().isDisposed())
							return Status.CANCEL_STATUS;
						tableAttachments.setInput(allegati);
						tableAttachments.refresh();
						return Status.OK_STATUS;
					}
				}.schedule();
				return Status.OK_STATUS;
			}
		}.schedule(0);
	}

	private void initTablePanel(Composite parent) {
		tableAttachments = new TableViewer(parent);
		// tableAttachments.setComparator(sorter);

		// add headers
		final TableViewerColumn col = new TableViewerColumn(tableAttachments, SWT.NONE);
		col.getColumn().setText("Nome");
		col.getColumn().setWidth(300);

		// set contenProvider
		tableAttachments.setContentProvider(new AttachmentContentProvider(tableAttachments));
		// set labelProvider
		tableAttachments.setLabelProvider(new AttachmentLabelProvider());

		tableAttachments.setUseHashlookup(true);
		tableAttachments.getTable().setHeaderVisible(false);
		tableAttachments.getTable().setLinesVisible(true);

		tableAttachments.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				openAttachment();
			}
		});

		// register selection provider
		getViewSite().setSelectionProvider(tableAttachments);
	}

	private void openAttachment() {
		System.out.println("Open attachment called.");
		if (tableAttachments.getSelection().isEmpty())
			return; // nothing selected
		StructuredSelection selection = (StructuredSelection) tableAttachments.getSelection();
		Allegato selected = (Allegato) selection.getFirstElement();

		try {
			FileUtils.writeTmpFile(Tools.toPrimitive(selected.getContenuto()), selected.getNome(),
					selected.getEstensione(), true);
		} catch (IOException ioex) {
			Log.error(ioex, "Aprendo l'allegato (ID: %s)", selected.getId());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {

	}

}
