/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.views;

import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;

import ch.pcilocarno.pbc.inventari.data.ArchChangeListener;
import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.ParteArchitettonica;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.views.content.PAContentProvider;
import ch.pcilocarno.pbc.inventari.views.label.PALabelProvider;

/**
 * ch.pcilocarno.pbc.inventari.ParteArchitettonicaView
 * 
 * @author elvis on Sep 9, 2009
 * @version @@version@@ build @@build@@
 */
public class ParteArchitettonicaView extends ViewPart implements IViewListPO<ParteArchitettonica>
{
	public static final String ID = "ch.pcilocarno.pbc.inventari.views.ParteArchitettonicaView";

	TableViewer viewer;

	ArchChangeListener alistener = new ArchChangeListener()
	{
		@Override
		public void handle()
		{
			refresh();
			getViewSite().getSelectionProvider().setSelection(null);
		}
	};

	/**
	 * 
	 */
	public ParteArchitettonicaView()
	{
	}

	@Override
	public void init(IViewSite site) throws PartInitException
	{
		super.init(site);
		Session.INSTANCE.addListener(alistener);
	}

	@Override
	public void dispose()
	{
		Session.INSTANCE.removeListener(alistener);
		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite parent)
	{
		parent.setLayout(GridLayoutFactory.fillDefaults().numColumns(1).extendedMargins(0, 0, 0, 0).margins(0, 0).create());

		viewer = new TableViewer(parent, SWT.SINGLE | SWT.FULL_SELECTION);
		viewer.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));

		// columns
		TableViewerColumn col = new TableViewerColumn(viewer, SWT.NONE);
		col.getColumn().setText("Denominazione");
		col.getColumn().setWidth(230);

		col = new TableViewerColumn(viewer, SWT.NONE);
		col.getColumn().setText("Piano");
		col.getColumn().setWidth(50);

		// col = new TableViewerColumn(viewer, SWT.RIGHT);
		// col.getColumn().setText("Qtà OA");
		// col.getColumn().setWidth(70);

		col = new TableViewerColumn(viewer, SWT.NONE);
		col.getColumn().setText("");
		col.getColumn().setWidth(10);

		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);

		viewer.setContentProvider(new PAContentProvider(viewer));
		viewer.setLabelProvider(new PALabelProvider());
		getViewSite().setSelectionProvider(viewer);

		refresh();
	}

	public void refresh()
	{
		new Job("Carica PA.")
		{
			@Override
			protected IStatus run(IProgressMonitor monitor)
			{
				if (viewer == null || viewer.getTable().isDisposed()) return Status.CANCEL_STATUS;
				final List<ParteArchitettonica> pa = PersistenceFactory.PAlocal().findByArch(Session.INSTANCE.getArchitetturaUUID());
				new UIJob(Display.getDefault(), "Carica PA in tabella.")
				{

					@Override
					public IStatus runInUIThread(IProgressMonitor monitor)
					{
						if (viewer == null || viewer.getTable().isDisposed()) return Status.CANCEL_STATUS;
						viewer.setInput(pa);
						viewer.refresh();
						return Status.OK_STATUS;
					}
				}.schedule();
				return Status.OK_STATUS;
			}
		}.schedule(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus()
	{
		viewer.getControl().setFocus();
	}
}
