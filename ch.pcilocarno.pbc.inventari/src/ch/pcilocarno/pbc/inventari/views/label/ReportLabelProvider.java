/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.views.label;

import ch.pcilocarno.pbc.inventari.data.model.Rapporto;
import ch.pcilocarno.pbc.inventari.util.DateUtils;
import ch.pcilocarno.pbc.inventari.util.StringUtils;
import ch.pcilocarno.pbc.inventari.util.Tools;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

/**
 * @author elvis
 *
 */
public class ReportLabelProvider extends LabelProviderAdapter<Rapporto> {

	final int IDX_CREATED = 0;
	final int IDX_UPDATED = 1;
	final int IDX_NAME = 2;
	final int IDX_TEXT = 3;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.pcilocarno.pbc.inventari.views.label.LabelProviderAdapter#getLabel(ch.
	 * pcilocarno.pbc.inventari.data.persistence.PersistentObject, int)
	 */
	@Override
	public String getLabel(Rapporto po, int columnIndex) {
		switch (columnIndex) {
		case IDX_CREATED:
			return DateUtils.formatDateTime(po.getCreated());
		case IDX_UPDATED:
			return DateUtils.formatDateTime(po.getEdited());
		case IDX_NAME:
			return po.getNome();
		case IDX_TEXT:
			return StringUtils.cleanForView(po.getContenuto(), 50);
		}
		return null;
	}

}
