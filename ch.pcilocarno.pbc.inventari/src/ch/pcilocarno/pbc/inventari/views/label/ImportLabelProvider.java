/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.views.label;

import org.eclipse.swt.graphics.Image;

import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.data.model.Architettura;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

public class ImportLabelProvider extends LabelProviderAdapter<Architettura> {

	private static final int IDX_NOPCI = 0;
	private static final int IDX_DENOMINAZIONE = 1;
	private static final int IDX_COMUNE = 2;

	Image lock = UIhelper.getImage(Icons.LOCK_SMALL_16);

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.viewers.BaseLabelProvider#dispose()
	 */
	@Override
	public void dispose() {
		lock.dispose();
		lock = null;
		super.dispose();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.pcilocarno.pbc.inventari.views.label.LabelProviderAdapter#getLabel(ch.
	 * pcilocarno.pbc.inventari.data.persistence.PersistentObject, int)
	 */
	@Override
	public String getLabel(Architettura po, int columnIndex) {
		switch (columnIndex) {
		case IDX_NOPCI:
			return po.getNoPci();
		case IDX_DENOMINAZIONE:
			return po.getDenominazione();
		case IDX_COMUNE:
			return po.getComune();
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.pcilocarno.pbc.inventari.views.label.LabelProviderAdapter#getImage(ch.
	 * pcilocarno.pbc.inventari.data.persistence.PersistentObject, int)
	 */
	@Override
	public Image getImage(Architettura po, int columnIndex) {
		if (columnIndex == IDX_NOPCI && po.getDefinitivo())
			return lock;
		return super.getImage(po, columnIndex);
	}

}
