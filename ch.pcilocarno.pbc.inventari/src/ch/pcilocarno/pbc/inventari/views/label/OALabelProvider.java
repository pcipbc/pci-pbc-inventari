/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.views.label;

import java.math.BigDecimal;

import org.eclipse.swt.graphics.Image;

import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.data.model.OperaArte;
import ch.pcilocarno.pbc.inventari.data.model.enums.StatoOpera;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.Tools;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

/**
 * ch.pcilocarno.pbc.inventari.views.PALabelProvider
 * 
 * @author elvis on Sep 9, 2009
 * @version @@version@@ build @@build@@
 */
public class OALabelProvider extends LabelProviderAdapter<OperaArte> {
	Image tick = UIhelper.getImage(Icons.TICK_SMALL_16);
	Image ouch = UIhelper.getImage(Icons.EXCLAMATION_SMALL_16);
	Image ocio = UIhelper.getImage(Icons.WARNING_SMALL_16);
	Image foto = UIhelper.getImage(Icons.PICTURE_SMALL_16);
	Image endash = UIhelper.getImage(Icons.ENDASH);
	Image empty = UIhelper.getImage(Icons.EMPTY_16);

	final boolean extended;

	/**
	 * @param ext_
	 */
	public OALabelProvider(final boolean ext_) {
		this.extended = ext_;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * ch.pcilocarno.pbc.inventari.views.label.LabelProviderAdapter#getLabel(ch.
	 * pcilocarno.pbc.inventari.data.model.PersistentObject, int)
	 */
	@Override
	public String getLabel(OperaArte po, final int columnIndex) {
		switch (columnIndex) {
		case 0:
			return po.getNoPci() + "";
		case 1:
			return po.getGenere();
		case 2:
			return po.getDenominazione();
		case 3:
			return Tools.properCase(po.getStato().toString());
		case 4:
			return po.getParteArchitettonica().getDenominazione();
		case 5:
			return po.getCollocazione();
		case 6:
			return po.getFoto();
		case 11:
			return extended ? PersistenceFactory.IMGlocal().countByIdProp(po.getId()) + "" : "";
		}
		return null;
	}

	@Override
	public Image getImage(OperaArte po, int columnIndex) {
		switch (columnIndex) {
		case 0:
			return po.getDaVerificare() ? ocio : empty;
		case 2: {

			if (!extended && PersistenceFactory.IMGlocal().countByIdProp(po.getId()) > 0) //
				return foto;
			return null;
		}
		case 7:
			return PersistenceFactory.IMGlocal().countByIdProp(po.getId()) > 0 ? tick : ouch;
		case 8:
			return (po.getMisAltezza() * po.getMisLarghezza() != 0) ? tick : ouch;
		case 9:
			return po.getMateriaTecnica().isEmpty() ? ouch : tick;
		case 10: {
			if (po.getStato() == StatoOpera.MOBILE)
				return endash; // ignora evacuazione

			if (po.getPciOre().equals(BigDecimal.ZERO) || po.getPciPersone() == 0
					|| po.getPciModelloProtezione().isEmpty())
				return ouch;
			return tick;
		}
		}

		return null;
	}

	@Override
	public void dispose() {
		tick.dispose();
		ouch.dispose();
		ocio.dispose();
		foto.dispose();
		endash.dispose();
		empty.dispose();
		tick = null;
		ouch = null;
		ocio = null;
		foto = null;
		endash = null;
		empty = null;
		super.dispose();
	}
}
