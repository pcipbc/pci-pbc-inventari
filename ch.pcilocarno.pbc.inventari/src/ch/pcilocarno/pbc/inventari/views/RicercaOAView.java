/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.views;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.fieldassist.ContentProposalAdapter;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.handlers.IHandlerService;
import org.eclipse.ui.part.ViewPart;

import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.beans.TableSorter;
import ch.pcilocarno.pbc.inventari.commands.OAOpenHandler;
import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.OperaArte;
import ch.pcilocarno.pbc.inventari.data.model.enums.StatoOpera;
import ch.pcilocarno.pbc.inventari.data.model.enums.TipoTabella;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.Tools;
import ch.pcilocarno.pbc.inventari.util.UIhelper;
import ch.pcilocarno.pbc.inventari.views.content.OAContentProvider;
import ch.pcilocarno.pbc.inventari.views.label.OALabelProvider;

/**
 * @author elvis
 * 
 */
public class RicercaOAView extends ViewPart {
	public static final String ID = "ch.pcilocarno.pbc.inventari.views.RicercaOAView";

	// vars
	private static final String ANY = "Qualsiasi";
	private static final String TODAY = "Oggi";
	private static final String LAST_WEEK = "Ultima settimana";

	private TableViewer viewer;

	Text fNoPci, fNoUbc, fGenere, fDenominazione, fFoto;

	Button fDaVerificare, fSenzaFoto, fSenzaMis, fSenzaMateriaTecnica, fSenzaPianifPci;

	Combo fTipo, fImmissione;

	Label lRecordsNo;

	final KeyListener ekl = new KeyAdapter() {
		@Override
		public void keyPressed(KeyEvent e) {
			if (e.keyCode == SWT.CR || e.keyCode == SWT.KEYPAD_CR)
				search();
			else
				super.keyPressed(e);
		}
	};

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.
	 * widgets.Composite)
	 */
	@Override
	public void createPartControl(Composite main) {
		main.setLayout(new GridLayout(2, false));

		Composite filters = new Composite(main, SWT.NONE);
		filters.setLayoutData(new GridData(GridData.FILL_VERTICAL));
		filters.setLayout(new GridLayout(2, false));

		initFiltersPanel(filters);

		initTable(main);
		viewer.getControl()
				.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).create());
	}

	void initFiltersPanel(Composite parent) {
		final int LABEL_WIDTH = 80;
		final int FIELD_WIDTH = 140;

		Label lnopci = new Label(parent, SWT.NONE);
		lnopci.setText("No Pci");
		lnopci.setLayoutData(
				GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).hint(LABEL_WIDTH, SWT.DEFAULT).create());

		fNoPci = new Text(parent, SWT.SINGLE | SWT.BORDER);
		fNoPci.setText("");
		fNoPci.setMessage(ANY);
		fNoPci.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).hint(FIELD_WIDTH, SWT.DEFAULT)
				.grab(true, false).create());

		Label lnoubc = new Label(parent, SWT.NONE);
		lnoubc.setText("No UBC");
		lnoubc.setLayoutData(
				GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).hint(LABEL_WIDTH, SWT.DEFAULT).create());

		fNoUbc = new Text(parent, SWT.SINGLE | SWT.BORDER);
		fNoUbc.setText("");
		fNoUbc.setMessage(ANY);
		fNoUbc.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).hint(FIELD_WIDTH, SWT.DEFAULT)
				.grab(true, false).create());

		Label lgenere = new Label(parent, SWT.NONE);
		lgenere.setText("Genere");
		lgenere.setLayoutData(
				GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).hint(LABEL_WIDTH, SWT.DEFAULT).create());

		fGenere = new Text(parent, SWT.SINGLE | SWT.BORDER);
		fGenere.setText("");
		fGenere.setMessage(ANY);
		fGenere.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false)
				.hint(FIELD_WIDTH, SWT.DEFAULT).create());
		Tools.installContentProposalAsync(fGenere, TipoTabella.GENERE, ContentProposalAdapter.PROPOSAL_REPLACE, true);

		Label ldenom = new Label(parent, SWT.NONE);
		ldenom.setText("Denom.");
		ldenom.setLayoutData(
				GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).hint(LABEL_WIDTH, SWT.DEFAULT).create());

		fDenominazione = new Text(parent, SWT.SINGLE | SWT.BORDER);
		fDenominazione.setText("");
		fDenominazione.setMessage(ANY);
		fDenominazione.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER)
				.hint(FIELD_WIDTH, SWT.DEFAULT).grab(true, false).create());

		Label lfoto = new Label(parent, SWT.NONE);
		lfoto.setText("Foto (Testo)");
		lfoto.setLayoutData(
				GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).hint(LABEL_WIDTH, SWT.DEFAULT).create());

		fFoto = new Text(parent, SWT.SINGLE | SWT.BORDER);
		fFoto.setText("");
		fFoto.setMessage(ANY);
		fFoto.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).hint(FIELD_WIDTH, SWT.DEFAULT)
				.grab(true, false).create());

		Label ldata = new Label(parent, SWT.NONE);
		ldata.setText("Immesse");
		ldata.setLayoutData(
				GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).hint(LABEL_WIDTH, SWT.DEFAULT).create());

		fImmissione = new Combo(parent, SWT.READ_ONLY | SWT.BORDER);
		fImmissione.add(ANY);
		fImmissione.add(TODAY);
		fImmissione.add(LAST_WEEK);
		fImmissione.select(0);
		fImmissione.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER)
				.hint(FIELD_WIDTH, SWT.DEFAULT).grab(true, false).create());

		Label ltipo = new Label(parent, SWT.NONE);
		ltipo.setText("Tipo");
		ltipo.setLayoutData(
				GridDataFactory.fillDefaults().align(SWT.LEFT, SWT.CENTER).hint(LABEL_WIDTH, SWT.DEFAULT).create());
		fTipo = new Combo(parent, SWT.READ_ONLY | SWT.BORDER);
		fTipo.add(ANY);
		fTipo.add(StatoOpera.MOBILE.toString());
		fTipo.add(StatoOpera.IMMOBILE.toString());
		fTipo.select(0);
		fTipo.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).hint(FIELD_WIDTH, SWT.DEFAULT)
				.grab(true, false).create());

		new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL).setLayoutData(GridDataFactory.fillDefaults().span(2, 1)
				.align(SWT.FILL, SWT.CENTER).grab(true, false).indent(SWT.DEFAULT, 10).create());

		fDaVerificare = new Button(parent, SWT.CHECK);
		fDaVerificare.setText("Da verificare");
		fDaVerificare.setLayoutData(
				GridDataFactory.fillDefaults().span(2, 1).align(SWT.FILL, SWT.CENTER).grab(true, false).create());

		fSenzaFoto = new Button(parent, SWT.CHECK);
		fSenzaFoto.setText("Senza foto");
		fSenzaFoto.setLayoutData(
				GridDataFactory.fillDefaults().span(2, 1).align(SWT.FILL, SWT.CENTER).grab(true, false).create());
		// fSenzaFoto.setEnabled(false);

		fSenzaMis = new Button(parent, SWT.CHECK);
		fSenzaMis.setText("Senza misure");
		fSenzaMis.setLayoutData(
				GridDataFactory.fillDefaults().span(2, 1).align(SWT.FILL, SWT.CENTER).grab(true, false).create());

		fSenzaMateriaTecnica = new Button(parent, SWT.CHECK);
		fSenzaMateriaTecnica.setText("Senza mat. e tecnica");
		fSenzaMateriaTecnica.setLayoutData(
				GridDataFactory.fillDefaults().span(2, 1).align(SWT.FILL, SWT.CENTER).grab(true, false).create());

		fSenzaPianifPci = new Button(parent, SWT.CHECK);
		fSenzaPianifPci.setText("Senza pianif.");
		fSenzaPianifPci.setLayoutData(
				GridDataFactory.fillDefaults().span(2, 1).align(SWT.FILL, SWT.CENTER).grab(true, false).create());

		fFoto.addKeyListener(ekl);
		fDenominazione.addKeyListener(ekl);
		fNoPci.addKeyListener(ekl);
		fGenere.addKeyListener(ekl);
		fNoUbc.addKeyListener(ekl);

		Button sb = new Button(parent, SWT.PUSH);
		sb.setText("Trova");
		sb.setImage(UIhelper.getImage(Icons.MAGNIFIER_16));
		sb.setLayoutData(
				GridDataFactory.fillDefaults().span(2, 1).align(SWT.FILL, SWT.CENTER).grab(true, false).create());
		sb.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				search();
			}
		});

		new Label(parent, SWT.SEPARATOR | SWT.HORIZONTAL).setLayoutData(GridDataFactory.fillDefaults().span(2, 1)
				.align(SWT.FILL, SWT.CENTER).indent(SWT.DEFAULT, 10).grab(true, false).create());
		lRecordsNo = new Label(parent, SWT.WRAP);
		lRecordsNo.setLayoutData(
				GridDataFactory.fillDefaults().span(2, 1).align(SWT.FILL, SWT.CENTER).grab(true, false).create());

	}

	/**
	 * Sets a column sortable
	 * 
	 * @param col
	 * @param sorter
	 */
	private void setColumnSortable(final TableViewerColumn col, final int columnIndex,
			final TableSorter<OperaArte> sorter) {
		col.getColumn().addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				sorter.setColumn(columnIndex);
				int dir = viewer.getTable().getSortDirection();
				if (viewer.getTable().getSortColumn() == col.getColumn())
					dir = dir == SWT.DOWN ? SWT.UP : SWT.DOWN;
				else
					dir = SWT.UP;
				viewer.getTable().setSortDirection(dir);
				viewer.getTable().setSortColumn(col.getColumn());
				viewer.refresh(true);
			}
		});
	}

	private void initTable(Composite parent) {
		final TableSorter<OperaArte> sorter = new TableSorter<OperaArte>() {
			@Override
			protected int compare(int col, OperaArte b1, OperaArte b2) {
				switch (col) {
				case 0:
					return b1.getNoPci().compareTo(b2.getNoPci());
				case 1:
					return b1.getGenere().compareTo(b2.getGenere());
				case 2:
					return b1.getDenominazione().compareTo(b2.getDenominazione());
				case 3:
					return b1.getStato().compareTo(b2.getStato());
				case 4:
					return b1.getParteArchitettonica().getDenominazione()
							.compareTo(b2.getParteArchitettonica().getDenominazione());
				case 5:
					return b1.getCollocazione().compareTo(b2.getCollocazione());
				case 6:
					return b1.getFoto().compareTo(b2.getFoto());
				case 11: {
					Integer b1count = PersistenceFactory.IMGlocal().countByIdProp(b1.getId());
					Integer b2count = PersistenceFactory.IMGlocal().countByIdProp(b2.getId());
					return b1count.compareTo(b2count);
				}
				}
				return 0;
			}
		};
		viewer = new TableViewer(parent, SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER);
		viewer.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));
		
		// col headers
		final TableViewerColumn col = new TableViewerColumn(viewer, SWT.NONE);
		col.getColumn().setText("No.");
		col.getColumn().setWidth(60);
		setColumnSortable(col, 0, sorter);

		final TableViewerColumn col1 = new TableViewerColumn(viewer, SWT.NONE);
		col1.getColumn().setText("Genere");
		col1.getColumn().setWidth(160);
		setColumnSortable(col1, 1, sorter);

		final TableViewerColumn col2 = new TableViewerColumn(viewer, SWT.NONE);
		col2.getColumn().setText("Denominazione");
		col2.getColumn().setWidth(220);
		setColumnSortable(col2, 2, sorter);

		final TableViewerColumn col3 = new TableViewerColumn(viewer, SWT.NONE);
		col3.getColumn().setText("Tipo");
		col3.getColumn().setWidth(75);
		setColumnSortable(col3, 3, sorter);

		final TableViewerColumn col4 = new TableViewerColumn(viewer, SWT.NONE);
		col4.getColumn().setText("PA");
		col4.getColumn().setWidth(150);
		setColumnSortable(col4, 4, sorter);

		final TableViewerColumn col5 = new TableViewerColumn(viewer, SWT.NONE);
		col5.getColumn().setText("Collocazione");
		col5.getColumn().setWidth(140);
		setColumnSortable(col5, 5, sorter);

		final TableViewerColumn col6 = new TableViewerColumn(viewer, SWT.NONE);
		col6.getColumn().setText("Foto");
		col6.getColumn().setWidth(100);
		setColumnSortable(col6, 6, sorter);

		final TableViewerColumn colf = new TableViewerColumn(viewer, SWT.NONE);
		colf.getColumn().setText("F");
		colf.getColumn().setToolTipText("Foto (principale)");
		colf.getColumn().setWidth(25);

		final TableViewerColumn col7 = new TableViewerColumn(viewer, SWT.NONE);
		col7.getColumn().setText("D");
		col7.getColumn().setToolTipText("Dimensioni");
		col7.getColumn().setWidth(25);

		final TableViewerColumn colt = new TableViewerColumn(viewer, SWT.NONE);
		colt.getColumn().setText("T");
		colt.getColumn().setToolTipText("Materia e Tecnica");
		colt.getColumn().setWidth(25);

		final TableViewerColumn colp = new TableViewerColumn(viewer, SWT.NONE);
		colp.getColumn().setText("P");
		colp.getColumn().setToolTipText("Pianificazione PCi");
		colp.getColumn().setWidth(25);

		final TableViewerColumn colqf = new TableViewerColumn(viewer, SWT.NONE);
		colqf.getColumn().setText("Qta foto");
		colqf.getColumn().setToolTipText("Quantità di foto");
		colqf.getColumn().setWidth(80);
		setColumnSortable(colqf, 11, sorter);

		
		viewer.setContentProvider(new OAContentProvider(viewer));
		viewer.setLabelProvider(new OALabelProvider(true));
		viewer.setComparator(sorter);
		viewer.setUseHashlookup(true);
		viewer.getTable().setHeaderVisible(true);
		viewer.getTable().setLinesVisible(true);
		viewer.getTable().setSortDirection(SWT.UP);
		viewer.getTable().setSortColumn(col.getColumn());
		getViewSite().setSelectionProvider(viewer);

		hookDoubleClickCommand();

	}

	private void hookDoubleClickCommand() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				IHandlerService handlerService = getSite().getService(IHandlerService.class);
				try {
					handlerService.executeCommand(OAOpenHandler.ID, null);
				} catch (Exception ex) {
					throw new RuntimeException(OAOpenHandler.ID + " not found");
				}
			}
		});
	}

	public void search() {
		final Integer nopci_ = Tools.isEmpty(fNoPci.getText()) ? null : Tools.toInt(fNoPci.getText());
		final String noubc_ = fNoUbc.getText();
		final String gener_ = fGenere.getText();
		final String denom_ = fDenominazione.getText();
		final String foto_ = fFoto.getText();
		final StatoOpera stato_ = getStatoOpera(fTipo.getText());
		final Boolean daverificare_ = fDaVerificare.getSelection();
		final boolean withoutPic = fSenzaFoto.getSelection();
		final boolean withoutDim = fSenzaMis.getSelection();
		final boolean withoutMat = fSenzaMateriaTecnica.getSelection();
		final boolean withoutPia = fSenzaPianifPci.getSelection();
		final Date immesseDal = getFrom(fImmissione.getText());

		new Job("Ricerca OA.") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {

				final List<OperaArte> oas = PersistenceFactory.OAlocal().findExtended(
						Session.INSTANCE.getArchitetturaUUID(), nopci_, noubc_, gener_, denom_, foto_, immesseDal,
						stato_, daverificare_, withoutPic, withoutDim, withoutMat, withoutPia);

				Display.getDefault().asyncExec(new Runnable() {
					@Override
					public void run() {
						if (viewer.getTable().isDisposed())
							return;
						viewer.setInput(oas);
						viewer.refresh();

						lRecordsNo.setText("Trovate " + oas.size() + " OA.");
					}
				});

				return Status.OK_STATUS;
			}
		}.schedule();

	}

	private Date getFrom(String selected) {
		if (ANY.equalsIgnoreCase(selected))
			return null;

		try {
			Calendar c = Calendar.getInstance();
			c.setTime(new Date());
			c.set(Calendar.HOUR, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 1);

			if (TODAY.equalsIgnoreCase(selected)) //
				return c.getTime();

			if (LAST_WEEK.equalsIgnoreCase(selected)) {
				c.add(Calendar.DATE, -7);
				return c.getTime();
			}
		} catch (Exception ex) {
		}
		return null;
	}

	/**
	 * @param selected
	 * @return
	 */
	private StatoOpera getStatoOpera(String selected) {
		try {
			return StatoOpera.valueOf(selected);
		} catch (IllegalArgumentException iae) {

		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
	 */
	@Override
	public void setFocus() {
		fNoPci.setFocus();
	}

}
