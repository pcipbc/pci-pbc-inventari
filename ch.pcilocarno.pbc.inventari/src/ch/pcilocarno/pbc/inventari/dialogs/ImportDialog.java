/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.TraverseEvent;
import org.eclipse.swt.events.TraverseListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import ch.pcilocarno.pbc.inventari.Activator;
import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.data.model.Architettura;
import ch.pcilocarno.pbc.inventari.data.model.Contatto;
import ch.pcilocarno.pbc.inventari.data.model.enums.TipoContatto;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceTools;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.util.UIhelper;
import ch.pcilocarno.pbc.inventari.views.content.ImportContentProvider;
import ch.pcilocarno.pbc.inventari.views.label.ImportLabelProvider;

/**
 * @author elvis
 * 
 */
public class ImportDialog extends TitleAreaDialog {
	Text fFilter;

	private TableViewer viewer;

	public List<String> selectedA = new ArrayList<String>();

	public boolean sLocal;

	public ImportDialog(Shell shell) {
		super(shell);
	}

	@Override
	public void create() {
		super.create();

		setTitle("Importazione architettura");
		setMessage("Scegliere l'architettura da importare e cliccare Importa.");
		setTitleImage(UIhelper.getImage(Icons.IMPORT_32));
		getButton(IDialogConstants.OK_ID).setText("Importa");
		getButton(IDialogConstants.OK_ID).setEnabled(false);
		reload();
	}

	protected void newPressed() {
		ANewDialog dia = new ANewDialog(getShell(), PersistenceTools.remoteDbConfigured());
		if (dia.open() != Window.OK)
			return;

		Architettura a = new Architettura();

		Contatto prop = new Contatto(a, TipoContatto.PROPRIETARIO);
		Contatto cust = new Contatto(a, TipoContatto.CUSTODE);

		a.setDenominazione(dia.sDenominazione);
		a.setComune(dia.sComune);
		a.setNoPci(dia.sNoPbc);

		if (dia.sLocal) {
			a = PersistenceFactory.Alocal().saveOrUpdate(a);
			prop.setIdProp(a.getId());
			cust.setIdProp(a.getId());
			PersistenceFactory.Clocal().saveOrUpdate(prop);
			PersistenceFactory.Clocal().saveOrUpdate(cust);
			selectedA.add(a.getId());
			sLocal = true;
		} else {
			a = PersistenceFactory.Aremote().saveOrUpdate(a);
			prop.setIdProp(a.getId());
			cust.setIdProp(a.getId());
			PersistenceFactory.Clocal().saveOrUpdate(prop);
			PersistenceFactory.Clocal().saveOrUpdate(cust);
			selectedA.add(a.getId());
			sLocal = false;

		}
		super.okPressed();

	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, 998, "Nuova...", false);
		super.createButtonsForButtonBar(parent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == 998)
			newPressed();
		else
			super.buttonPressed(buttonId);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = new Composite(parent, SWT.NONE);
		area.setLayout(new GridLayout(1, false));
		area.setLayoutData(new GridData(GridData.FILL_BOTH));

		fFilter = new Text(area, SWT.BORDER | SWT.SINGLE);
		fFilter.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fFilter.addTraverseListener(new TraverseListener() {

			@Override
			public void keyTraversed(TraverseEvent e) {
				if (e.keyCode == SWT.KEYPAD_CR || e.keyCode == SWT.CR)//
				{
					reload();
					e.doit = false;
				}
			}
		});
		Composite tabArea = new Composite(area, SWT.NONE);
		tabArea.setLayout(new FillLayout());
		GridData tagd = new GridData(GridData.FILL_BOTH);
		tagd.heightHint = 300;
		tabArea.setLayoutData(tagd);
		createViewer(tabArea);

		return area;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		for (Architettura each : getCheckedItems())
			selectedA.add(each.getId());
		super.okPressed();
	}

	/**
	 * @param area
	 */
	private void createViewer(Composite area) {
		viewer = new TableViewer(area, SWT.CHECK | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.HIDE_SELECTION);
		createColumns(viewer);
		viewer.setContentProvider(new ImportContentProvider(viewer));
		viewer.setLabelProvider(new ImportLabelProvider());

		viewer.getTable().addListener(SWT.Selection, new Listener() {
			@Override
			public void handleEvent(Event event) {
				if (event.detail == SWT.CHECK) {
					getButton(IDialogConstants.OK_ID).setEnabled(getCheckedItems().size() > 0);
				}
			}
		});
	}

	/**
	 * @return
	 */
	private List<Architettura> getCheckedItems() {
		List<Architettura> selected = new ArrayList<Architettura>();
		for (TableItem each : viewer.getTable().getItems())
			if (each.getChecked())
				selected.add((Architettura) each.getData());

		return selected;
	}

	/**
	 * This will create the columns for the table
	 * 
	 * @param viewer
	 */
	private void createColumns(TableViewer viewer) {
		String[] titles = { "No. PCi", "Denominazione", "Comune" };
		int[] bounds = { 80, 250, 100 };

		for (int i = 0; i < titles.length; i++) {
			TableViewerColumn column = new TableViewerColumn(viewer, SWT.NONE);
			column.getColumn().setText(titles[i]);
			column.getColumn().setWidth(bounds[i]);
			column.getColumn().setResizable(true);
			column.getColumn().setMoveable(false);
		}
		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
	}

	void reload() {
		setErrorMessage(null);
		if (PersistenceTools.remoteDbConfigured()) //
		{
			try {
				viewer.setInput(PersistenceFactory.Aremote().findAllByText(fFilter.getText()));
			} catch (Exception e) {
				setErrorMessage(
						"Impossibile connettersi alla banca dati configurata. Controllare le impostazioni in File > Preferenze... e riavviare l'applicazione.");
				Log.error(e, "Connecting to db");
			}
		} else {
			setMessage(
					"Banca dati non configurata. Per configurare la connessione File > Preferenze... e riavviare l'applicazione",
					IMessageProvider.WARNING);
			Log.log(new Status(IStatus.INFO, Activator.PLUGIN_ID, "Connessione non configurata."));
		}

	}
}
