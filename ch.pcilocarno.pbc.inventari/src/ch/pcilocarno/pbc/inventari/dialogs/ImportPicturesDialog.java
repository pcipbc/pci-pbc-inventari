/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.dialogs;

import java.io.File;
import java.io.IOException;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DirectoryDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.beans.DatePeriod;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.Tools;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

public class ImportPicturesDialog extends TitleAreaDialog {
	// selection
	private File selectedFolder;
	private boolean selectedForcePictureImport = false;
	private boolean selectedOnlyWithoutPicture = true;
	private DatePeriod selectedPeriod;

	// ui fields
	Text fFolder;

	Button bSelectFolder;
	Button bForcePictureImport;
	Button bOnlyWithoutPicture;
	ComboViewer fPeriod;

	public ImportPicturesDialog(Shell shell) {
		super(shell);
	}

	public File getSelectedFolder() {
		return selectedFolder;
	}

	public boolean getSelectedForcePictureImport() {
		return selectedForcePictureImport;
	}

	public boolean getSelectedOnlyWithoutPictures() {
		return this.selectedOnlyWithoutPicture;
	}

	public DatePeriod getSelectedPeriod() {
		return selectedPeriod;
	}

	@Override
	public void create() {
		super.create();

		setTitle("Importa foto");
		setMessage("Immetti le opzioni e clicca importa per avviare l'importazione delle foto.");
		setTitleImage(UIhelper.getImage(Icons.PICTURE_IMPORT_32));
		getButton(IDialogConstants.OK_ID).setText("Importa");
		setFields();
		enableOkButton();
	}

	private void enableOkButton() {
		boolean enable = true;

		if (selectedFolder == null)
			enable = false;

		if (selectedPeriod == null)
			enable = false;

		getButton(IDialogConstants.OK_ID).setEnabled(enable);
	}

	private final void setFields() {
		fPeriod.getCombo().select(0);
		bOnlyWithoutPicture.setSelection(true);
		enableOkButton();

		this.selectedPeriod = (DatePeriod) ((StructuredSelection) fPeriod.getSelection()).getFirstElement();

	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = new Composite(parent, SWT.NONE);
		area.setLayoutData(new GridData(GridData.FILL_BOTH));
		area.setLayout(GridLayoutFactory.fillDefaults().margins(10, 10).spacing(10, 10).numColumns(3).equalWidth(false).create());

		new Label(area, SWT.SINGLE).setText("Cartella: ");
		fFolder = new Text(area, SWT.SINGLE | SWT.BORDER);
		fFolder.setEditable(false);
		fFolder.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		bSelectFolder = new Button(area, SWT.PUSH);
		bSelectFolder.setText(" \u2026 ");
		bSelectFolder.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectFolder();
			}
		});

		Label lConsider = new Label(area, SWT.SINGLE);
		lConsider.setText("Inserite:");
		lConsider.setLayoutData(GridDataFactory.fillDefaults().align(SWT.BEGINNING, SWT.CENTER).indent(0, 15).create());

		fPeriod = new ComboViewer(area, SWT.READ_ONLY);
		fPeriod.getCombo().setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).indent(0, 15).grab(true, false).span(2, 1).create());
		fPeriod.setContentProvider(ArrayContentProvider.getInstance());
		fPeriod.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				return super.getText(element);
			}
		});
		fPeriod.setInput(Tools.buildPeriods(PersistenceFactory.OAlocal().collectCreateDates(), true, true));
		fPeriod.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				selectedPeriod = (DatePeriod) ((StructuredSelection) fPeriod.getSelection()).getFirstElement();
				enableOkButton();
			}
		});

		new Label(area, SWT.SINGLE).setText("");
		bOnlyWithoutPicture = new Button(area, SWT.CHECK);
		bOnlyWithoutPicture.setText("Solo OA senza foto");
		bOnlyWithoutPicture.setSelection(selectedOnlyWithoutPicture);
		bOnlyWithoutPicture.setLayoutData(GridDataFactory.fillDefaults().indent(0, 20).grab(true, false).span(2, 1).create());
		bOnlyWithoutPicture.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectedOnlyWithoutPicture = ((Button) e.widget).getSelection();
				bForcePictureImport.setEnabled(!selectedOnlyWithoutPicture);

				if (selectedOnlyWithoutPicture) {
					bForcePictureImport.setSelection(false);
				}
			}
		});

		new Label(area, SWT.SINGLE).setText("");
		bForcePictureImport = new Button(area, SWT.CHECK);
		bForcePictureImport.setText("Sovrascrivi immagine esistente");
		bForcePictureImport.setEnabled(false);
		bForcePictureImport.setSelection(selectedForcePictureImport);
		bForcePictureImport.setLayoutData(GridDataFactory.fillDefaults().grab(true, false).span(2, 1).create());
		bForcePictureImport.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				selectedForcePictureImport = ((Button) e.widget).getSelection();
			}
		});

		return area;
	}

	private final void selectFolder() {
		// select folder
		final DirectoryDialog ddia = new DirectoryDialog(getShell(), SWT.OPEN);
		ddia.setText("Scelta cartella");
		ddia.setMessage("Scegli la cartella che contiene le foto da importare.");
		final String result = ddia.open();
		if (result == null) // cancel pressed
			return;

		final File parentDir = new File(result);

		// checks on selected folder
		if (!parentDir.exists()) {
			UIhelper.reportException("Cannot access folder " + result, new IOException("Folder " + result + " doesn't exist."));
			return;
		}

		if (!parentDir.canRead()) {
			UIhelper.reportException("Cannot read folder " + result, new IOException("Folder " + result + " isn't readable."));
			return;
		}

		fFolder.setText(parentDir.getAbsolutePath());
		selectedFolder = parentDir;
		enableOkButton();
	}

}
