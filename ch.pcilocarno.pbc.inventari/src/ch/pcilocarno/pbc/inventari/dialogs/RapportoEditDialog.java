/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.dialogs;

import java.util.Date;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ch.pcilocarno.pbc.inventari.data.model.Rapporto;
import ch.pcilocarno.pbc.inventari.util.DateUtils;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

/**
 * @author elvis
 *
 */
public class RapportoEditDialog extends TitleAreaDialog {
	// const
	private static final int BUTTON_ID_FORCEEDIT = -900;
	// bean
	final Rapporto rapporto;

	// ui fields
	Text fNome;
	Text fContenuto;

	// selection
	String selectedNome;
	String selectedContenuto;

	/**
	 * @param parentShell
	 */
	public RapportoEditDialog(Shell parentShell, Rapporto rapporto) {
		super(parentShell);
		this.rapporto = rapporto;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#create()
	 */
	@Override
	public void create() {
		super.create();
		setTitle(rapporto.getId() == null ? "Nuovo rapporto di fine corso" : "Modifica rapporto di fine corso");
		setMessage("Rapporto di fine corso.");

		enableEdit(canEdit());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.
	 * widgets.Composite)
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		if (!canEdit()) {
			createButton(parent, BUTTON_ID_FORCEEDIT, "Forza modifica", false);
		}
		super.createButtonsForButtonBar(parent);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
	 */
	@Override
	protected void buttonPressed(int buttonId) {
		if (buttonId == BUTTON_ID_FORCEEDIT) {
			enableEdit(true);
		} else {
			super.buttonPressed(buttonId);
		}
	}

	private final void enableEdit(final boolean canEdit) {
		fNome.setEnabled(canEdit);
		fContenuto.setEditable(canEdit);
		getButton(IDialogConstants.OK_ID).setEnabled(canEdit);
		setMessage(canEdit? null : "Il rapporto non è modificabile.", IMessageProvider.WARNING);
	}

	private final boolean canEdit() {
		// nuovo rapporto
		if (rapporto.getId() == null) {
			return true;
		}

		if (DateUtils.daysBetween(rapporto.getCreated(), new Date()) > 7L) {
			return false;
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.TitleAreaDialog#createDialogArea(org.eclipse.swt.
	 * widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = new Composite(parent, SWT.NONE);
		area.setLayoutData(new GridData(GridData.FILL_BOTH));
		area.setLayout(
				GridLayoutFactory.fillDefaults().numColumns(2).equalWidth(false).extendedMargins(5, 5, 5, 5).create());

		new Label(area, UIhelper.STYLE_LABEL).setText("Nome e cognome");

		fNome = new Text(area, SWT.BORDER | SWT.SINGLE);
		fNome.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.CENTER).grab(true, false).create());
		fNome.setTextLimit(64);

		fContenuto = new Text(area, SWT.BORDER | SWT.MULTI);
		fContenuto.setLayoutData(GridDataFactory.fillDefaults().grab(true, true).span(2, 1).minSize(SWT.DEFAULT, 350)
				.align(SWT.FILL, SWT.FILL).create());
		fContenuto.setTextLimit(4096);

		// set content
		fNome.setText(rapporto.getNome());
		fContenuto.setText(rapporto.getContenuto());

		return area;
	}

	/**
	 * @return the selectedContenuto
	 */
	public String getSelectedContenuto() {
		return selectedContenuto;
	}

	/**
	 * @return the selectedNome
	 */
	public String getSelectedNome() {
		return selectedNome;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.jface.dialogs.Dialog#okPressed()
	 */
	@Override
	protected void okPressed() {
		if (!checkFields()) {
			return;
		}
		this.selectedContenuto = fContenuto.getText();
		this.selectedNome = fNome.getText();
		super.okPressed();
	}

	/**
	 * @return
	 */
	private boolean checkFields() {
		// reset message
		setMessage(null, IMessageProvider.ERROR);

		if (fNome.getText().trim().length() == 0) {
			setMessage("Il campo nome e cognome è obbligatorio", IMessageProvider.ERROR);
			return false;
		}

		if (fContenuto.getText().trim().length() == 0) {
			setMessage("Il campo contenuto è obbligatorio.", IMessageProvider.ERROR);
			return false;
		}
		return true;
	}
}
