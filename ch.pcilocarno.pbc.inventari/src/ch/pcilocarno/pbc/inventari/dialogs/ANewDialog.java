/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.dialogs;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

/**
 * @author elvis
 * 
 */
public class ANewDialog extends TitleAreaDialog {
	// UI elements
	Text fDenominazione;
	Text fNoPbc;
	Text fComune;
	Button fLocal;

	// selection
	String sDenominazione;
	String sNoPbc;
	String sComune;
	Boolean sLocal;

	final boolean connOpen;

	/**
	 * @param parentShell
	 */
	public ANewDialog(Shell parentShell, boolean remoteConnOpen) {
		super(parentShell);
		this.connOpen = remoteConnOpen;
	}

	@Override
	public void create() {
		super.create();

		setTitle("Nuova architettura");
		setMessage("Specificare i dati per la creazione di una nuova architettura.");
		setTitleImage(UIhelper.getImage(Icons.A_32));
		getButton(IDialogConstants.OK_ID).setText("Crea");

		if (!connOpen) {
			fLocal.setSelection(true);
			fLocal.setEnabled(false);
		}
	}

	@Override
	protected void okPressed() {
		this.sDenominazione = fDenominazione.getText();
		this.sComune = fComune.getText();
		this.sNoPbc = fNoPbc.getText();
		this.sLocal = fLocal.getSelection();

		super.okPressed();
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite c = new Composite(parent, SWT.NONE);
		c.setLayoutData(new GridData(GridData.FILL_BOTH));
		c.setLayout(new GridLayout(2, false));

		new Label(c, SWT.SINGLE).setText("Denominazione");
		fDenominazione = new Text(c, SWT.BORDER | SWT.SINGLE);
		fDenominazione.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(c, SWT.SINGLE).setText("Comune");
		fComune = new Text(c, SWT.BORDER | SWT.SINGLE);
		fComune.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(c, SWT.SINGLE).setText("No. PBC");
		fNoPbc = new Text(c, SWT.BORDER | SWT.SINGLE);
		fNoPbc.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(c, SWT.SINGLE).setText("Modalità");
		fLocal = new Button(c, SWT.CHECK);
		fLocal.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		fLocal.setText("Locale");
		return c;
	}

}
