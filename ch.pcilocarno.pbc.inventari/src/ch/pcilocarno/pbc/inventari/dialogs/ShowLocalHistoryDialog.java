/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.dialogs;

import java.text.SimpleDateFormat;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.data.Session;
import ch.pcilocarno.pbc.inventari.data.model.LocalHistory;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistenceFactory;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.util.UIhelper;
import ch.pcilocarno.pbc.inventari.views.content.ContentProviderAdapter;
import ch.pcilocarno.pbc.inventari.views.label.LabelProviderAdapter;

public class ShowLocalHistoryDialog extends TitleAreaDialog {
	private TableViewer viewer;

	/**
	 * @param parentShell
	 */
	public ShowLocalHistoryDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	public void create() {
		super.create();
		setTitle("Storico");
		setMessage("Storico dell'architettura selezionata.");
		setTitleImage(UIhelper.getImage(Icons.CLOCK_HISTORY_32));
		reload();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.TitleAreaDialog#createDialogArea(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = new Composite(parent, SWT.NONE);
		area.setLayout(new GridLayout(1, false));
		area.setLayoutData(new GridData(GridData.FILL_BOTH));

		viewer = new TableViewer(area, SWT.H_SCROLL | SWT.V_SCROLL | SWT.HIDE_SELECTION);
		createColumns(viewer);
		viewer.setContentProvider(getContentProvider());
		viewer.setLabelProvider(getLabelProvider());
		viewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));
		return area;
	}

	/**
	 * @return
	 */
	private IContentProvider getContentProvider() {
		return new ContentProviderAdapter<LocalHistory>(viewer) {
		};
	}

	/**
	 * @return
	 */
	private ITableLabelProvider getLabelProvider() {
		return new LabelProviderAdapter<LocalHistory>() {
			@Override
			public String getLabel(LocalHistory po, int columnIndex) {
				switch (columnIndex) {
				case 0:
					return new SimpleDateFormat("dd.MM.yyyy HH:mm:ss").format(po.getCreated());
				case 1:
					return po.getTipo().toString();
				case 2:
					return po.getAuthor();
				}
				return null;
			}

		};
	}

	/**
	 * @param viewer
	 */
	private void createColumns(TableViewer viewer) {
		String[] titles = { "Data", "Tipo", "Utente" };
		int[] bounds = { 140, 250, 100 };

		for (int i = 0; i < titles.length; i++) {
			TableViewerColumn column = new TableViewerColumn(viewer, SWT.NONE);
			column.getColumn().setText(titles[i]);
			column.getColumn().setWidth(bounds[i]);
			column.getColumn().setResizable(true);
			column.getColumn().setMoveable(false);
		}
		Table table = viewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
	}

	void reload() {
		setErrorMessage(null);
		try {
			viewer.setInput(PersistenceFactory.LHlocal().findByArch(Session.INSTANCE.getArchitetturaUUID()));
		} catch (Exception e) {
			Log.error(e, "Displaying Local History");
		}

	}
}
