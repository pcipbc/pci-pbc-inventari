/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.dialogs;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;

import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.data.persistence.PersistentObject;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.util.UIhelper;

/**
 * @author elvis
 * 
 */
public class RecordInfoDialog extends TitleAreaDialog {
	final PersistentObject record;

	static SimpleDateFormat df = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss.SSS");

	/**
	 * @param parentShell
	 */
	public RecordInfoDialog(Shell parentShell, PersistentObject record_) {
		super(parentShell);
		this.record = record_;
	}

	@Override
	public void create() {
		super.create();

		setTitle("Informazioni record");
		setMessage("Vengono elencate le informazioni relative al record.");
		setTitleImage(UIhelper.getImage(Icons.INFORMATION_32));
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = new Composite(parent, SWT.NONE);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 350;
		gd.widthHint = 600;
		area.setLayoutData(gd);
		area.setLayout(new FillLayout());

		Table table = new Table(area, SWT.SINGLE | SWT.BORDER | SWT.FULL_SELECTION);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		TableColumn col0 = new TableColumn(table, SWT.NONE);
		col0.setText("Campo");
		TableColumn col1 = new TableColumn(table, SWT.NONE);
		col1.setText("Valore");

		populateTable(table);
		col0.pack();
		col1.pack();
		return area;
	}

	private void populateTable(Table table) {
		System.out.println("ID = " + this.record.getId());
		new TableItem(table, SWT.NONE).setText(new String[] { "ID", this.record.getId() });
		new TableItem(table, SWT.NONE).setText(new String[] { "Stato", this.record.getStatus() + "" });
		new TableItem(table, SWT.NONE).setText(new String[] { "Creazione",
				df.format(this.record.getCreated()) + " (" + this.record.getAuthor() + ")" });
		new TableItem(table, SWT.NONE).setText(
				new String[] { "Modifica", df.format(this.record.getEdited()) + " (" + this.record.getEditor() + ")" });

		try {
			Field[] fields = record.getClass().getDeclaredFields();
			for (Field each : fields) {
				try {
					Method getter = record.getClass().getMethod(
							"get" + each.getName().substring(0, 1).toUpperCase() + each.getName().substring(1));
					if (getter == null) {
						new TableItem(table, SWT.NONE).setText(new String[] { each.getName(), "" });
						continue;
					}
					Object v = getter.invoke(record);
					if (v instanceof PersistentObject)
						new TableItem(table, SWT.NONE)
								.setText(new String[] { each.getName(), ((PersistentObject) v).getId() });
					else
						new TableItem(table, SWT.NONE).setText(new String[] { each.getName(), v + "" });
				} catch (NoSuchMethodException nsme) {
					new TableItem(table, SWT.NONE).setText(new String[] { each.getName(), "" });
				}
			}
		} catch (Exception e) {
			Log.error(e, "Populating record info table.");
		}
	}
}
