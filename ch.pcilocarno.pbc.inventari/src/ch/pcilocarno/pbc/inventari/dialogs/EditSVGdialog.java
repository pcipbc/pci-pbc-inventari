/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.dialogs;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.graphics.GC;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ch.pcilocarno.pbc.inventari.Activator;
import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.util.ByteArrayInOutStream;
import ch.pcilocarno.pbc.inventari.util.Log;
import ch.pcilocarno.pbc.inventari.util.UIhelper;
import ch.pcilocarno.pbc.inventari.util.svg.DisegnoTecnico;

/**
 * @author elvis
 * 
 */
public class EditSVGdialog extends TitleAreaDialog {

	ComboViewer fDisegnoTecnico;

	Composite fieldsC;

	Canvas canvas, c;

	Image currentImage;

	CLabel lAggiuntaMP;

	// created image
	byte[] createdFile = null;

	byte[] modelloProtezione = null;

	/**
	 * @param parentShell
	 */
	public EditSVGdialog(Shell parentShell) {
		super(parentShell);
	}

	public byte[] getCreatedFile() {
		return createdFile;
	}

	public byte[] getModelloProtezione() {
		return modelloProtezione;
	}

	@Override
	public void create() {
		super.create();
		resizeShell();
		setTitle("Aggiunta disegno tecnico");
		setMessage("Scegli il disegno tecnico, immetti le misure (in cm) e clicca 'Salva'.");
		getButton(Dialog.OK).setText("Salva");
		postCreate();
	}

	private final void postCreate() {
		Display.getCurrent().asyncExec(new Runnable() {
			@Override
			public void run() {
				fDisegnoTecnico.getCombo().select(0);
				updateFieldsComposite(getSelectedDisegnoTecnico());
				canvas.redraw();
			}
		});
	}

	@Override
	protected void okPressed() {
		try {
			// adapt svg
			Map<String, String> guiFields = collectGUIfields();
			Document doc = getXMLdocument(getDisegnoTecnicoFile(getSelectedDisegnoTecnico()));

			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			NodeList nodes = (NodeList) xpath.compile("//text").evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				Element e = (Element) node;
				String id = e.getAttribute("id");
				if (id != null && id.startsWith("pci_")) //
					if (guiFields.containsKey(id)) //
						e.setTextContent(guiFields.get(id));
			}

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			ByteArrayInOutStream baios = new ByteArrayInOutStream();
			StreamResult result = new StreamResult(baios);
			transformer.transform(source, result);

			// transformer.transform(source, new StreamResult(new
			// FileOutputStream(File.createTempFile("CASSANDRA_DT_", ".svg"))));

			// create JPG
			JPEGTranscoder transcoder = new JPEGTranscoder();
			transcoder.addTranscodingHint(JPEGTranscoder.KEY_QUALITY, new Float(1.0));
			transcoder.addTranscodingHint(JPEGTranscoder.KEY_WIDTH, new Float(1200));
			transcoder.addTranscodingHint(JPEGTranscoder.KEY_HEIGHT, new Float(1200));
			TranscoderInput input = new TranscoderInput(baios.getInputStream());
			ByteArrayOutputStream ostream = new ByteArrayOutputStream();
			TranscoderOutput output = new TranscoderOutput(ostream);

			transcoder.transcode(input, output);
			ostream.close();

			this.createdFile = ostream.toByteArray();

			if (getSelectedDisegnoTecnico().getImmagineMP() != null) {
				this.modelloProtezione = Files
						.readAllBytes(Paths.get(getModelloProtezioneFile(getSelectedDisegnoTecnico()).toURI()));
			}
			super.okPressed();
		} catch (Exception ex) {
			setErrorMessage(ex.toString());
			Log.error(ex, "Creating jpg byte[]");
		}
	}

	protected void resizeShell() {
		final Rectangle bounds = getShell().getBounds();
		final int extraW = 200;
		final int extraH = 150;
		int nuw = bounds.width + extraW;
		int nuh = bounds.height + extraH;
		int nux = bounds.x;
		int nuy = bounds.y;
		// getShell().setSize(nu_w, nu_h);
		try {
			nux = bounds.x - (extraW / 2);
			nuy = bounds.y - (extraH / 2);
		} catch (Exception ex) {
		}
		// recenter
		getShell().setBounds(nux, nuy, nuw, nuh);

	}

	private DisegnoTecnico getSelectedDisegnoTecnico() {
		if (fDisegnoTecnico == null)
			return null;

		if (fDisegnoTecnico.getSelection() == null)
			return null;

		return (DisegnoTecnico) ((IStructuredSelection) fDisegnoTecnico.getSelection()).getFirstElement();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.jface.dialogs.TitleAreaDialog#createDialogArea(org.eclipse.
	 * swt.widgets.Composite)
	 */
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite main = new Composite(parent, SWT.NONE);
		main.setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).create());
		main.setLayout(GridLayoutFactory.fillDefaults().equalWidth(false).numColumns(2).create());

		fDisegnoTecnico = new ComboViewer(main, SWT.BORDER | SWT.READ_ONLY);
		fDisegnoTecnico.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof DisegnoTecnico) //
					return ((DisegnoTecnico) element).getDescrizione();

				return super.getText(element);
			}
		});
		fDisegnoTecnico.setContentProvider(ArrayContentProvider.getInstance());
		fDisegnoTecnico.getCombo().setLayoutData(
				GridDataFactory.fillDefaults().span(2, 1).align(SWT.FILL, SWT.CENTER).grab(true, false).create());
		fDisegnoTecnico.setInput(DisegnoTecnico.values());

		fDisegnoTecnico.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				updateFieldsComposite(getSelectedDisegnoTecnico());
				lAggiuntaMP.setVisible(getSelectedDisegnoTecnico().getImmagineMP() != null);
				canvas.redraw();
			}
		});

		createImageComposite(main);
		createFieldsComposite(main);

		lAggiuntaMP = new CLabel(main, SWT.SHADOW_NONE);
		lAggiuntaMP.setLayoutData(
				GridDataFactory.fillDefaults().span(2, 1).align(SWT.FILL, SWT.CENTER).grab(true, false).create());
		lAggiuntaMP.setImage(UIhelper.getImage(Icons.INFORMATION_16));
		lAggiuntaMP.setText("Questo oggetto prevede l'inserimento automatico della misura di protezione standard.");
		updateFieldsComposite(getSelectedDisegnoTecnico());
		return main;
	}

	/**
	 * @param selected
	 */
	private void updateFieldsComposite(DisegnoTecnico selected) {
		if (selected == null)
			return;
		final Map<String, String> fields = collectSVGfields(selected);

		Control[] children = fieldsC.getChildren();
		for (Control child : children) {
			child.setVisible(false);
			child.dispose();
			child = null;
		}

		for (Entry<String, String> f : fields.entrySet()) {
			Label l = new Label(fieldsC, SWT.NONE);
			l.setText(f.getValue());

			Text t = new Text(fieldsC, SWT.BORDER | SWT.SINGLE);
			t.setData("svg.field.id", f.getKey());
			t.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

			Label cm = new Label(fieldsC, SWT.NONE);
			cm.setText("cm");
		}

		fieldsC.layout();
		fieldsC.update();
	}

	private File getDisegnoTecnicoFile(DisegnoTecnico selected) throws IOException {
		URL url = FileLocator.find(Activator.getDefault().getBundle(),
				new Path(DisegnoTecnico.PATH_DT + selected.getImmagineDT()), null);

		return new File(FileLocator.toFileURL(url).getPath());
	}

	private File getModelloProtezioneFile(DisegnoTecnico selected) throws IOException {
		URL url = FileLocator.find(Activator.getDefault().getBundle(),
				new Path(DisegnoTecnico.PATH_MP + selected.getImmagineMP()), null);

		return new File(FileLocator.toFileURL(url).getPath());
	}

	/**
	 * @return
	 */
	private Map<String, String> collectGUIfields() {
		Map<String, String> fields = new HashMap<>();
		Control[] controls = fieldsC.getChildren();
		for (Control c : controls) {
			if (!(c instanceof Text))
				continue; // not a field
			Text field = (Text) c;
			if (field.getText().length() == 0)
				continue; // field is empty
			Object id = field.getData("svg.field.id");
			if (id == null)
				continue;

			fields.put("" + id, field.getText());
		}

		return fields;
	}

	private Map<String, String> collectSVGfields(DisegnoTecnico selected) {
		Map<String, String> fields = new TreeMap<>();
		try {
			Document doc = getXMLdocument(getDisegnoTecnicoFile(selected));

			XPathFactory xPathfactory = XPathFactory.newInstance();
			XPath xpath = xPathfactory.newXPath();
			NodeList nodes = (NodeList) xpath.compile("//text").evaluate(doc, XPathConstants.NODESET);
			for (int i = 0; i < nodes.getLength(); i++) {
				Node node = nodes.item(i);
				Element e = (Element) node;
				String id = e.getAttribute("id");
				if (id != null && id.startsWith("pci_")) //
					fields.put(e.getAttribute("id"), e.getTextContent());
			}
		} catch (Exception ex) {
			Log.error(ex, "Parsing XML and collecting fields");
		}

		return fields;
	}

	private Document getXMLdocument(File file) throws IOException, SAXException, ParserConfigurationException {

		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(false);
		dbFactory.setValidating(false);

		// dbFactory.setFeature("http://xml.org/sax/features/namespaces",
		// false);
		// dbFactory.setFeature("http://xml.org/sax/features/validation",
		// false);
		// dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar",
		// false);
		// dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd",
		// false);

		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		dBuilder.setEntityResolver(new EntityResolver() {
			@Override
			public InputSource resolveEntity(String arg0, String arg1) throws SAXException, IOException {
				return new InputSource(new StringReader(""));
			}
		});

		Document doc = dBuilder.parse(file);
		doc.getDocumentElement().normalize();

		return doc;
	}

	private void createFieldsComposite(Composite main) {
		fieldsC = new Composite(main, SWT.NONE);
		fieldsC.setLayout(new GridLayout(3, false));
		fieldsC.setLayoutData(GridDataFactory.fillDefaults().align(SWT.BEGINNING, SWT.FILL).grab(false, true)
				.hint(125, SWT.DEFAULT).create());

	}

	private void createImageComposite(Composite main) {
		canvas = new Canvas(main, SWT.NONE);
		canvas.setLayoutData(
				GridDataFactory.fillDefaults().grab(true, true).align(SWT.FILL, SWT.FILL).hint(400, 400).create());
		canvas.addListener(SWT.Resize, new Listener() {
			@Override
			public void handleEvent(Event e) {
				if (getSelectedDisegnoTecnico() != null) //
					canvas.redraw();
			}
		});

		canvas.addListener(SWT.Paint, new Listener() {
			@Override
			public void handleEvent(final Event e) {
				// dispose current image, if any
				if (currentImage != null && !currentImage.isDisposed()) {
					currentImage.dispose();
					currentImage = null;
				}

				// nothing selected and nothing to display
				Image selected = getSelectedImage();
				if (selected == null) //
					return;

				currentImage = adaptImage(selected);
				if (currentImage == null)
					return;//

				c = (Canvas) e.widget;
				GC gc = e.gc;

				if (gc == null || gc.isDisposed())
					return;
				if (c == null || c.isDisposed())
					return;

				gc.setClipping(c.getClientArea());
				gc.fillRectangle(c.getClientArea());

				gc.drawImage(currentImage, 0, 0);

				Rectangle rect = currentImage.getBounds();
				Rectangle client = c.getClientArea();

				int marginWidth = client.width - rect.width;
				if (marginWidth > 0) {
					gc.fillRectangle(rect.width, 0, marginWidth, client.height);
				}

				int marginHeight = client.height - rect.height;
				if (marginHeight > 0) {
					gc.fillRectangle(0, rect.height, client.width, marginHeight);
				}

			}
		});
	}

	private Image getSelectedImage() {
		if (getSelectedDisegnoTecnico() == null) //
			return null;

		Rectangle r = new Rectangle(0, 0, 600, 600);
		if (canvas != null)
			r = canvas.getClientArea();
		try {
			final File file = getDisegnoTecnicoFile(getSelectedDisegnoTecnico());

			JPEGTranscoder transcoder = new JPEGTranscoder();
			transcoder.addTranscodingHint(JPEGTranscoder.KEY_QUALITY, new Float(1.0));
			transcoder.addTranscodingHint(JPEGTranscoder.KEY_WIDTH, new Float(r.width));
			transcoder.addTranscodingHint(JPEGTranscoder.KEY_HEIGHT, new Float(r.height));
			TranscoderInput input = new TranscoderInput(new FileInputStream(file));
			ByteArrayInOutStream ostream = new ByteArrayInOutStream();
			TranscoderOutput output = new TranscoderOutput(ostream);

			transcoder.transcode(input, output);
			ostream.close();

			return new Image(Display.getCurrent(), ostream.getInputStream());
		} catch (Exception ex) {
			Log.error(ex, "Transforming SVG to JPEG");
		}
		return null;
	}

	protected Rectangle calcImageDimensions(Rectangle image, Rectangle carea) {
		final int original_width = image.width;
		final int original_height = image.height;
		final int bound_width = carea.width;
		final int bound_height = carea.height;
		int new_width = original_width;
		int new_height = original_height;

		// do not enlarge small images (avoid pixelation)
		if (original_width <= bound_width && original_height <= bound_height)
			return null; // no resize needed

		// first check if we need to scale width
		if (original_width > bound_width) {
			// scale width to fit
			new_width = bound_width;
			// scale height to maintain aspect ratio
			new_height = (new_width * original_height) / original_width;
		}

		// then check if we need to scale even with the new height
		if (new_height > bound_height) {
			// scale height to fit instead
			new_height = bound_height;
			// scale width to maintain aspect ratio
			new_width = (new_height * original_width) / original_height;
		}
		// Tools.log("\nImg size = h: %s\tw: %s\nArea size = h: %s\tw: %s\nNew
		// size = h: %s\tw: %s\n", original_height, original_width,
		// bound_height, bound_width, new_height, new_width);

		return new Rectangle(0, 0, new_width, new_height);
	}

	private Image adaptImage(Image e) {
		if (e == null || e.isDisposed())
			return null;

		Rectangle nuImgSize = calcImageDimensions(e.getBounds(), canvas.getClientArea());
		if (nuImgSize == null)
			return e;
		Log.debug("Nu image size: %s x %s", nuImgSize.width, nuImgSize.height);

		// scale img
		ImageData imgData = e.getImageData().scaledTo(nuImgSize.width, nuImgSize.height);
		ImageDescriptor imgDescr = ImageDescriptor.createFromImageData(imgData);
		Image scaledE = imgDescr.createImage();
		e.dispose();

		return scaledE;
	}

}
