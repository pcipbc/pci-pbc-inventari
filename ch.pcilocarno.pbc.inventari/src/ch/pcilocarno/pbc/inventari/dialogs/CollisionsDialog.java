/* MIT License
 * 
 * Copyright (c) 2016 Protezione Civile Locarno e Vallemaggia
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE. 
 */
package ch.pcilocarno.pbc.inventari.dialogs;

import java.util.List;

import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;

import ch.pcilocarno.pbc.inventari.Icons;
import ch.pcilocarno.pbc.inventari.util.Collision;
import ch.pcilocarno.pbc.inventari.util.UIhelper;
import ch.pcilocarno.pbc.inventari.views.content.CollisionsContentProvider;
import ch.pcilocarno.pbc.inventari.views.label.CollisionsLabelProvider;

public class CollisionsDialog extends TitleAreaDialog {
	final List<Collision> collisions;

	TableViewer table;

	/**
	 * @param parentShell
	 * @param collisions_
	 */
	public CollisionsDialog(Shell parentShell, List<Collision> collisions_) {
		super(parentShell);
		this.collisions = collisions_;
	}

	@Override
	public void create() {
		super.create();
		setTitle("Collisioni foto");
		setMessage("Le seguenti OA hanno un riferimento alla stessa foto e verranno ignorate.");
		setTitleImage(UIhelper.getImage(Icons.PICTURES_QUESTION_32));
		getButton(IDialogConstants.OK_ID).setText("Continua");
		getButton(IDialogConstants.CANCEL_ID).setText("Interrompi");
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = new Composite(parent, SWT.NONE);
		area.setLayoutData(new GridData(GridData.FILL_BOTH));
		area.setLayout(new GridLayout(1, false));

		// add table
		table = new TableViewer(area);
		table.getTable().setLayoutData(GridDataFactory.fillDefaults().align(SWT.FILL, SWT.FILL).grab(true, true).hint(SWT.DEFAULT, 250).create());
		initColumns(table);
		initContentProvider(table);
		initLabelProvider(table);
		table.setInput(collisions);

		return area;
	}

	private void initLabelProvider(TableViewer table2) {
		table2.setLabelProvider(new CollisionsLabelProvider());
	}

	private void initContentProvider(TableViewer table2) {
		table2.setContentProvider(new CollisionsContentProvider(table2));
	}

	private void initColumns(TableViewer table2) {
		String[] titles = { "OA", "OA" };
		int[] bounds = { 300, 300 };

		for (int i = 0; i < titles.length; i++) {
			TableViewerColumn column = new TableViewerColumn(table2, SWT.NONE);
			column.getColumn().setText(titles[i]);
			column.getColumn().setWidth(bounds[i]);
			column.getColumn().setResizable(false);
			column.getColumn().setMoveable(false);
		}
		table2.getTable().setHeaderVisible(true);
		table2.getTable().setLinesVisible(true);
	}
}
