select * from (select 
	'' as NOPCI,
	r.[NUMERO OGGETTO] & " " & f.SOTTONUMEROFOTOGRAFIA as NOPREC,
	'MOBILE' as STATO,
	r.SVILUPPATODA as AUTHOR,
	r.DATASVILUPPO as CREATED,
	replace(replace(nz(r.NUMEROSPECIALEOGGETTO,""),chr(13)&chr(10),""),";",",") as NOUBC,	
	replace(nz(r.TITOLORULLINO,""), ";",",") as GENERE,

	replace(nz(r.DENOMINAZIONE,""), ";",",") as DENOMINAZIONE,
	r.STATOOGGETTO as CONSFORMALE,
	replace(replace(nz(r.NOTE,""),chr(13)&chr(10),""),";",",") as OSSER,

	replace(nz(r.ARTISTA,""), ";",",") as ARTISTA,
	r.DATAZIONE as DATAZIONE,
	replace(nz(f.NOMESOGGETTO,""), ";",",") as NOTATECNICA,
	'' as PA,

	replace(nz(f.UBICAZIONE,""), ";",",") as UBICAZIONE,
	f.[CODICE PRIORITARIO] as PRIORITARIO,

	f.PEZZI as PEZZI,

	f.PESO as PESO,
	f.ALT as MISALTCC,
	f.LAR as MISLARCC,

	f.PRO as MISPROCC,

	replace(nz(f.IDENTIFICAZIONERULLINO,""), ";",",") as FOTO,
	0 as ORE,
	0 as PERSONE,
	'' as ATTREZZI,
	'' as MATERIALE,
	'' as PROT
from RULLINI r, FOTOGRAFIE f

where r.IDRULLINO = f.IDRULLINO

UNION ALL

select 
	'' as NOPCI,
	r.[NUMERO OGGETTO] as NOPREC,
	'IMMOBILE' as STATO,
	r.AUTORE as AUTHOR,
	r.[DATA INPUT] as CREATED,
	'' as NOUBC,
	replace(replace(nz(r.[OGGETTO DA PROTEGGERE],""),chr(13)&chr(10),""),";",",") as GENERE,	
	replace(replace(nz(r.DENOMINAZIONE,""),chr(13)&chr(10),""),";",",") as DENOMINAZIONE,
	r.[STATO OGGETTO] as CONSFORMALE,
	'' as OSSER,

	replace(nz(r.ARTISTA,""), ";",",") as ARTISTA,
	r.DATAZIONE as DATAZIONE,
	replace(replace(nz(f.NOTE,""),chr(13)&chr(10),""),";",",") as NOTATECNICA,
	'' as PA,

	replace(nz(r.UBICAZIONE,""), ";",",") as UBICAZIONE,
	r.[CODICE PRIORITARIO] as PRIORITARIO,

	1 as PEZZI,

	0 as PESO,

	0 as MISALTCC,
	0 as MISLARCC,

	0 as MISPROCC,

	'' as FOTO,
	r.ORE as ORE,
	r.PERSONE as PERSONE,
	replace(replace(nz(r.[ELENCO ATTREZZI],""),chr(13)&chr(10),""),";",",") as ATTREZZI,
	replace(replace(nz(r.[ELENCO MATERIALE],""),chr(13)&chr(10),""),";",",") as ELENCOMATERIALE,
	replace(replace(nz(r.[MISURE DI PROTEZIONE],""),chr(13)&chr(10),""),";",",") as PROT
from FOGLIODETTAGLIOPROTEZIONE r, DISEGNO f

where f.[ID NUMERO OGGETTO] = r.[BENE CULTURALE ID] and f.NOTE is not null) 
order by nopci