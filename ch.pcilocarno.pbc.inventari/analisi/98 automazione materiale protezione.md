# 98 automazione materiale protezione

##In generale

- **Dobbiamo rivedere la lista del materiale**, poiché secondo Fabio alcuni materiali sono un po' troppo particolari. Idealmente facciamo una doppia tabella; una con il materiale standard ed una con il materiale particolare.
- I militi DEVONO analizzare il materiale proposta da Cassandra e verificare che abbia senso, alcune cose non possono essere gestite, come rilievi cornici o posizione dell'opera da proteggere.
- Non risolviamo tutti i casi, per ogni genere di opera defniamo un minimo e massimo.
  Esempio un affresco di 10x10 cm viene gestito a mano, un affresco di 20mx15m anche.
- Sarebbe più utile specificare la lunghezza delle viti piuttosto che la testa (torx, croce, ...) cosa che oggi non viene fatta!

## Affreschi

- per difetto usiamo listoni e pannelli, assi e travetti sono per casi particolari
- teniamo almeno 10 cm di distanza dall'affresco per forare il muro.
- un listone centrale viene applicato se è molto grande (congiungere più pannelli)
- La plastica viene fissata con del nastro.

